﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACMessageDefs.Lib {
    /// <summary>
    /// List which is packable for network
    /// </summary>
    public class PackableList<T> {
        /// <summary>
        /// Number of items in the list
        /// </summary>
        public int count; // int

        /// <summary>
        /// Subclass to hold list data
        /// </summary>
        public List<T> items { get; set; } = new List<T>();

        public PackableList() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            count = BinaryHelpers.ReadInt32(buffer); // int
            for (var i = 0; i < count; i++) {
                if (typeof(T).GetInterfaces().Contains(typeof(IACDataType))) {
                    var item = Activator.CreateInstance<T>();
                    (item as IACDataType).ReadFromBuffer(buffer);
                    items.Add(item);
                }
                else if (typeof(T).IsEnum) {
                    items.Add((T)BinaryHelpers.FromType(buffer, Enum.GetUnderlyingType(typeof(T))));
                }
                else {
                    items.Add((T)BinaryHelpers.FromType(buffer, typeof(T)));
                }
            }
        }
    }

    /// <summary>
    /// HashTable which is packable for network
    /// </summary>
    public class PackableHashTable<T, U> : IACDataType {
        /// <summary>
        /// number of items in the table
        /// </summary>
        public short count; // short

        /// <summary>
        /// max size of the table
        /// </summary>
        public short tableSize; // short

        /// <summary>
        /// Subclass to hold table data
        /// </summary>
        public Dictionary<T, U> items = new Dictionary<T, U>();

        public PackableHashTable() { }
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            count = BinaryHelpers.ReadInt16(buffer); // short
            Logger.Log($"count {count}");
            tableSize = BinaryHelpers.ReadInt16(buffer); // short
            for (var i = 0; i < count; i++) {
                T key;
                U value;

                if (typeof(T).GetInterfaces().Contains(typeof(IACDataType))) {
                    var item = Activator.CreateInstance<T>();
                    (item as IACDataType).ReadFromBuffer(buffer);
                    key = item;
                }
                else if (typeof(T).IsEnum) {
                    key = (T)BinaryHelpers.FromType(buffer, Enum.GetUnderlyingType(typeof(T)));
                }
                else {
                    key = (T)BinaryHelpers.FromType(buffer, typeof(T));
                }

                if (typeof(U).GetInterfaces().Contains(typeof(IACDataType))) {
                    var item = Activator.CreateInstance<U>();
                    (item as IACDataType).ReadFromBuffer(buffer);
                    value = item;
                }
                else if (typeof(U).IsEnum) {
                    value = (U)BinaryHelpers.FromType(buffer, Enum.GetUnderlyingType(typeof(U)));
                }
                else {
                    value = (U)BinaryHelpers.FromType(buffer, typeof(U));
                }

                Logger.Log($"T<{key.GetType()}> {key} = {value} ({value.GetType()})");

                items.Add(key, value);
            }
        }

    }

    /// <summary>
    /// List which is packable for network
    /// </summary>
    public class PList<T> : IACDataType {
        /// <summary>
        /// Number of items in the list
        /// </summary>
        public uint count; // uint

        /// <summary>
        /// Subclass to hold list data
        /// </summary>
        public List<T> items { get; set; } = new List<T>();

        public PList() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            count = BinaryHelpers.ReadUInt32(buffer); // uint
            for (var i = 0; i < count; i++) {
                if (typeof(T).GetInterfaces().Contains(typeof(IACDataType))) {
                    var item = Activator.CreateInstance<T>();
                    (item as IACDataType).ReadFromBuffer(buffer);
                    items.Add(item);
                }
                else if (typeof(T).IsEnum) {
                    items.Add((T)BinaryHelpers.FromType(buffer, Enum.GetUnderlyingType(typeof(T))));
                }
                else {
                    items.Add((T)BinaryHelpers.FromType(buffer, typeof(T)));
                }
            }
        }
    }

    /// <summary>
    /// HashTable which is packable for network
    /// </summary>
    public class PHashTable<T, U> : IACDataType {
        public uint packedSize; // uint

        /// <summary>
        /// Derived from packedSize. 
        /// </summary>
        public uint buckets { get => (uint)(uint)1 << ((int)packedSize >> 24); } // uint

        /// <summary>
        /// Derived from packedSize. 
        /// </summary>
        public uint count { get => (uint)(packedSize & 0xFFFFFF); } // uint

        /// <summary>
        /// Subclass to hold table data
        /// </summary>
        public Dictionary<T, U> table { get; set; } = new Dictionary<T, U>();

        public PHashTable() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            packedSize = BinaryHelpers.ReadUInt32(buffer); // uint
            for (var i = 0; i < count; i++) {
                T key;
                U value;

                if (typeof(T).GetInterfaces().Contains(typeof(IACDataType))) {
                    var item = Activator.CreateInstance<T>();
                    (item as IACDataType).ReadFromBuffer(buffer);
                    key = item;
                }
                else if (typeof(T).IsEnum) {
                    key = (T)BinaryHelpers.FromType(buffer, Enum.GetUnderlyingType(typeof(T)));
                }
                else {
                    key = (T)BinaryHelpers.FromType(buffer, typeof(T));
                }

                if (typeof(U).GetInterfaces().Contains(typeof(IACDataType))) {
                    var item = Activator.CreateInstance<U>();
                    (item as IACDataType).ReadFromBuffer(buffer);
                    value = item;
                }
                else if (typeof(U).IsEnum) {
                    value = (U)BinaryHelpers.FromType(buffer, Enum.GetUnderlyingType(typeof(U)));
                }
                else {
                    value = (U)BinaryHelpers.FromType(buffer, typeof(U));
                }

                Logger.Log($"T<{key.GetType()}> {key} = {value} ({value.GetType()})");

                table.Add(key, value);
            }
        }

    }

    /// <summary>
    /// List which is packable for network
    /// </summary>
    public class PSmartArray<T> : IACDataType {
        /// <summary>
        /// Number of items in the list
        /// </summary>
        public uint count; // uint

        /// <summary>
        /// Subclass to hold list data
        /// </summary>
        public List<T> items { get; set; } = new List<T>();

        public PSmartArray() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            count = BinaryHelpers.ReadUInt32(buffer); // uint
            for (var i = 0; i < count; i++) {
                if (typeof(T).GetInterfaces().Contains(typeof(IACDataType))) {
                    var item = Activator.CreateInstance<T>();
                    (item as IACDataType).ReadFromBuffer(buffer);
                    items.Add(item);
                }
                else if (typeof(T).IsEnum) {
                    items.Add((T)BinaryHelpers.FromType(buffer, Enum.GetUnderlyingType(typeof(T))));
                }
                else {
                    items.Add((T)BinaryHelpers.FromType(buffer, typeof(T)));
                }
            }
        }

    }
}
