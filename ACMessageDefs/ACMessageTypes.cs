﻿//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
//                                                            //
//                          WARNING                           //
//                                                            //
//           DO NOT MAKE LOCAL CHANGES TO THIS FILE           //
//               EDIT THE .tt TEMPLATE INSTEAD                //
//                                                            //
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//


using System.IO;
using System.Collections.Generic;
using ACMessageDefs.Lib;
using ACMessageDefs.DataTypes;
using ACMessageDefs.Enums;

namespace ACMessageDefs.Messages {
    /// <summary>
    /// Allegiance update cancelled
    /// </summary>
    public class Allegiance_AllegianceUpdateAborted_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Allegiance_AllegianceUpdateAborted;

        /// <summary>
        /// Failure type
        /// </summary>
        public StatusMessage failureType; // StatusMessage

        public Allegiance_AllegianceUpdateAborted_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            failureType = (StatusMessage)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"failureType = {failureType}");
        }

    }

    /// <summary>
    /// Display a message in a popup message window.
    /// </summary>
    public class Communication_PopUpString_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Communication_PopUpString;

        /// <summary>
        /// the message text
        /// </summary>
        public string message; // string

        public Communication_PopUpString_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            message = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"message = {message}");
        }

    }

    /// <summary>
    /// Set a single character option.
    /// </summary>
    public class Character_PlayerOptionChangedEvent_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Character_PlayerOptionChangedEvent;

        /// <summary>
        /// the option being set
        /// </summary>
        public OptionPropertyID po; // OptionPropertyID

        /// <summary>
        /// the value of the option
        /// </summary>
        public bool value; // bool

        public Character_PlayerOptionChangedEvent_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            po = (OptionPropertyID)BinaryHelpers.ReadInt32(buffer);
            Logger.Log($"po = {po}");
            value = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"value = {value}");
        }

    }

    /// <summary>
    /// Starts a melee attack against a target
    /// </summary>
    public class Combat_TargetedMeleeAttack_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Combat_TargetedMeleeAttack;

        /// <summary>
        /// ID of creature being attacked
        /// </summary>
        public uint target; // ObjectID

        /// <summary>
        /// Height of the attack
        /// </summary>
        public AttackHeight attackHeight; // AttackHeight

        /// <summary>
        /// Attack power level
        /// </summary>
        public float powerLevel; // float

        public Combat_TargetedMeleeAttack_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            target = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"target = {target}");
            attackHeight = (AttackHeight)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"attackHeight = {attackHeight}");
            powerLevel = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"powerLevel = {powerLevel}");
        }

    }

    /// <summary>
    /// Starts a missle attack against a target
    /// </summary>
    public class Combat_TargetedMissileAttack_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Combat_TargetedMissileAttack;

        /// <summary>
        /// ID of creature being attacked
        /// </summary>
        public uint target; // ObjectID

        /// <summary>
        /// Height of the attack
        /// </summary>
        public AttackHeight attackHeight; // AttackHeight

        /// <summary>
        /// Accuracy level
        /// </summary>
        public float accuracyLevel; // float

        public Combat_TargetedMissileAttack_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            target = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"target = {target}");
            attackHeight = (AttackHeight)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"attackHeight = {attackHeight}");
            accuracyLevel = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"accuracyLevel = {accuracyLevel}");
        }

    }

    /// <summary>
    /// Set AFK mode.
    /// </summary>
    public class Communication_SetAFKMode_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Communication_SetAFKMode;

        /// <summary>
        /// Whether the user is AFK
        /// </summary>
        public bool afk; // bool

        public Communication_SetAFKMode_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            afk = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"afk = {afk}");
        }

    }

    /// <summary>
    /// Set AFK message.
    /// </summary>
    public class Communication_SetAFKMessage_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Communication_SetAFKMessage;

        /// <summary>
        /// The message text
        /// </summary>
        public string message; // string

        public Communication_SetAFKMessage_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            message = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"message = {message}");
        }

    }

    /// <summary>
    /// Information describing your character.
    /// </summary>
    public class Login_PlayerDescription_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Login_PlayerDescription;

        public ACQualities qualities; // ACQualities

        public PlayerModule playerModule; // PlayerModule

        /// <summary>
        /// Number of items in your main pack
        /// </summary>
        public PackableList<ContentProfile> contentProfile; // PackableList

        /// <summary>
        /// Items being equipt.
        /// </summary>
        public PackableList<InventoryPlacement> inventoryPlacement; // PackableList

        public Login_PlayerDescription_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            qualities = new ACQualities();
            qualities.ReadFromBuffer(buffer);
            Logger.Log($"qualities = {qualities}");
            playerModule = new PlayerModule();
            playerModule.ReadFromBuffer(buffer);
            Logger.Log($"playerModule = {playerModule}");
            contentProfile = new PackableList<ContentProfile>();
            contentProfile.ReadFromBuffer(buffer);
            Logger.Log($"contentProfile = {contentProfile}");
            inventoryPlacement = new PackableList<InventoryPlacement>();
            inventoryPlacement.ReadFromBuffer(buffer);
            Logger.Log($"inventoryPlacement = {inventoryPlacement}");
        }

    }

    /// <summary>
    /// Talking
    /// </summary>
    public class Communication_Talk_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Communication_Talk;

        /// <summary>
        /// The message text
        /// </summary>
        public string msg; // string

        public Communication_Talk_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            msg = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"msg = {msg}");
        }

    }

    /// <summary>
    /// Removes a friend
    /// </summary>
    public class Social_RemoveFriend_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Social_RemoveFriend;

        /// <summary>
        /// The id of the friend to remove
        /// </summary>
        public uint friendID; // ObjectID

        public Social_RemoveFriend_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            friendID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"friendID = {friendID}");
        }

    }

    /// <summary>
    /// Adds a friend
    /// </summary>
    public class Social_AddFriend_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Social_AddFriend;

        /// <summary>
        /// The name of the friend to add
        /// </summary>
        public string friendName; // string

        public Social_AddFriend_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            friendName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"friendName = {friendName}");
        }

    }

    /// <summary>
    /// Store an item in a container.
    /// </summary>
    public class Inventory_PutItemInContainer_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Inventory_PutItemInContainer;

        /// <summary>
        /// The item being stored
        /// </summary>
        public uint item; // ObjectID

        /// <summary>
        /// The container the item is being stored in
        /// </summary>
        public uint container; // ObjectID

        /// <summary>
        /// The position in the container where the item is being placed
        /// </summary>
        public uint loc; // uint

        public Inventory_PutItemInContainer_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            item = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"item = {item}");
            container = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"container = {container}");
            loc = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"loc = {loc}");
        }

    }

    /// <summary>
    /// Gets and weilds an item.
    /// </summary>
    public class Inventory_GetAndWieldItem_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Inventory_GetAndWieldItem;

        /// <summary>
        /// The item being equipped
        /// </summary>
        public uint item; // ObjectID

        /// <summary>
        /// The position in the container where the item is being placed
        /// </summary>
        public EquipMask slot; // EquipMask

        public Inventory_GetAndWieldItem_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            item = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"item = {item}");
            slot = (EquipMask)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"slot = {slot}");
        }

    }

    /// <summary>
    /// Drop an item.
    /// </summary>
    public class Inventory_DropItem_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Inventory_DropItem;

        /// <summary>
        /// The item being dropped
        /// </summary>
        public uint item; // ObjectID

        public Inventory_DropItem_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            item = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"item = {item}");
        }

    }

    /// <summary>
    /// Swear allegiance
    /// </summary>
    public class Allegiance_SwearAllegiance_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Allegiance_SwearAllegiance;

        /// <summary>
        /// The person you are swearing allegiance to
        /// </summary>
        public uint target; // ObjectID

        public Allegiance_SwearAllegiance_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            target = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"target = {target}");
        }

    }

    /// <summary>
    /// Break allegiance
    /// </summary>
    public class Allegiance_BreakAllegiance_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Allegiance_BreakAllegiance;

        /// <summary>
        /// The person you are breaking allegiance from
        /// </summary>
        public uint target; // ObjectID

        public Allegiance_BreakAllegiance_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            target = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"target = {target}");
        }

    }

    /// <summary>
    /// Allegiance update request
    /// </summary>
    public class Allegiance_UpdateRequest_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Allegiance_UpdateRequest;

        /// <summary>
        /// Whether the UI panel is open
        /// </summary>
        public bool on; // bool

        public Allegiance_UpdateRequest_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            on = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"on = {on}");
        }

    }

    /// <summary>
    /// Returns info related to your monarch, patron and vassals.
    /// </summary>
    public class Allegiance_AllegianceUpdate_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Allegiance_AllegianceUpdate;

        public uint rank; // uint

        public AllegianceProfile prof; // AllegianceProfile

        public Allegiance_AllegianceUpdate_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            rank = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"rank = {rank}");
            prof = new AllegianceProfile();
            prof.ReadFromBuffer(buffer);
            Logger.Log($"prof = {prof}");
        }

    }

    /// <summary>
    /// Friends list update
    /// </summary>
    public class Social_FriendsUpdate_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Social_FriendsUpdate;

        /// <summary>
        /// The set of friend data for the player
        /// </summary>
        public FriendData friendData; // FriendData

        /// <summary>
        /// The type of the update
        /// </summary>
        public FriendsUpdateType updateType; // FriendsUpdateType

        public Social_FriendsUpdate_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            friendData = new FriendData();
            friendData.ReadFromBuffer(buffer);
            Logger.Log($"friendData = {friendData}");
            updateType = (FriendsUpdateType)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"updateType = {updateType}");
        }

    }

    /// <summary>
    /// Store an item in a container.
    /// </summary>
    public class Item_ServerSaysContainID_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Item_ServerSaysContainID;

        /// <summary>
        /// the object ID of the item being stored
        /// </summary>
        public uint itemID; // ObjectID

        /// <summary>
        /// the object ID of the container the item is being stored in
        /// </summary>
        public uint containerID; // ObjectID

        /// <summary>
        /// the item slot within the container where the item is being placed (0-based)
        /// </summary>
        public uint slot; // uint

        /// <summary>
        /// the type of item being stored (pack, foci or regular item)
        /// </summary>
        public ContainerProperties containerProperties; // ContainerProperties

        public Item_ServerSaysContainID_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            itemID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"itemID = {itemID}");
            containerID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"containerID = {containerID}");
            slot = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"slot = {slot}");
            containerProperties = (ContainerProperties)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"containerProperties = {containerProperties}");
        }

    }

    /// <summary>
    /// Equip an item.
    /// </summary>
    public class Item_WearItem_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Item_WearItem;

        /// <summary>
        /// the object ID of the item being equipped
        /// </summary>
        public uint itemID; // ObjectID

        /// <summary>
        /// the slot(s) the item uses
        /// </summary>
        public EquipMask slot; // EquipMask

        public Item_WearItem_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            itemID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"itemID = {itemID}");
            slot = (EquipMask)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"slot = {slot}");
        }

    }

    /// <summary>
    /// Sent every time an object you are aware of ceases to exist. Merely running out of range does not generate this message - in that case, the client just automatically stops tracking it after receiving no updates for a while (which I presume is a very short while).
    /// </summary>
    public class Item_ServerSaysRemove_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Item_ServerSaysRemove;

        /// <summary>
        /// The object that ceases to exist.
        /// </summary>
        public uint objectID; // ObjectID

        public Item_ServerSaysRemove_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            objectID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectID = {objectID}");
        }

    }

    /// <summary>
    /// Clears friend list
    /// </summary>
    public class Social_ClearFriends_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Social_ClearFriends;

        public Social_ClearFriends_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Teleport to the PKLite Arena
    /// </summary>
    public class Character_TeleToPKLArena_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Character_TeleToPKLArena;

        public Character_TeleToPKLArena_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Teleport to the PK Arena
    /// </summary>
    public class Character_TeleToPKArena_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Character_TeleToPKArena;

        public Character_TeleToPKArena_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Titles for the current character.
    /// </summary>
    public class Social_CharacterTitleTable_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Social_CharacterTitleTable;

        /// <summary>
        /// the title ID of the currently active title
        /// </summary>
        public uint displayTitle; // uint

        /// <summary>
        /// Titles character currently has.
        /// </summary>
        public PList<uint> titleList; // PList

        public Social_CharacterTitleTable_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            displayTitle = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"displayTitle = {displayTitle}");
            titleList = new PList<uint>();
            titleList.ReadFromBuffer(buffer);
            Logger.Log($"titleList = {titleList}");
        }

    }

    /// <summary>
    /// Set a title for the current character.
    /// </summary>
    public class Social_AddOrSetCharacterTitle_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Social_AddOrSetCharacterTitle;

        /// <summary>
        /// the title ID of the new title
        /// </summary>
        public uint newTitle; // uint

        /// <summary>
        /// true if the title should be made the current title, false if it should just be added to the title list
        /// </summary>
        public bool setAsDisplayTitle; // bool

        public Social_AddOrSetCharacterTitle_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            newTitle = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"newTitle = {newTitle}");
            setAsDisplayTitle = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"setAsDisplayTitle = {setAsDisplayTitle}");
        }

    }

    /// <summary>
    /// Sets a character&#39;s display title
    /// </summary>
    public class Social_SetDisplayCharacterTitle_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Social_SetDisplayCharacterTitle;

        /// <summary>
        /// Title id
        /// </summary>
        public uint titleID; // uint

        public Social_SetDisplayCharacterTitle_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            titleID = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"titleID = {titleID}");
        }

    }

    /// <summary>
    /// Query the allegiance name
    /// </summary>
    public class Allegiance_QueryAllegianceName_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Allegiance_QueryAllegianceName;

        public Allegiance_QueryAllegianceName_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Clears the allegiance name
    /// </summary>
    public class Allegiance_ClearAllegianceName_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Allegiance_ClearAllegianceName;

        public Allegiance_ClearAllegianceName_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Direct message by ID
    /// </summary>
    public class Communication_TalkDirect_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Communication_TalkDirect;

        /// <summary>
        /// The message text
        /// </summary>
        public string msg; // string

        public uint targetID; // ObjectID

        public Communication_TalkDirect_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            msg = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"msg = {msg}");
            targetID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"targetID = {targetID}");
        }

    }

    /// <summary>
    /// Sets the allegiance name
    /// </summary>
    public class Allegiance_SetAllegianceName_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Allegiance_SetAllegianceName;

        /// <summary>
        /// The new allegiance name
        /// </summary>
        public string msg; // string

        public Allegiance_SetAllegianceName_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            msg = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"msg = {msg}");
        }

    }

    /// <summary>
    /// Attempt to use an item with a target.
    /// </summary>
    public class Inventory_UseWithTargetEvent_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Inventory_UseWithTargetEvent;

        /// <summary>
        /// The item being used
        /// </summary>
        public uint objectId; // ObjectID

        /// <summary>
        /// The target
        /// </summary>
        public uint target; // ObjectID

        public Inventory_UseWithTargetEvent_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            objectId = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectId = {objectId}");
            target = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"target = {target}");
        }

    }

    /// <summary>
    /// Attempt to use an item.
    /// </summary>
    public class Inventory_UseEvent_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Inventory_UseEvent;

        /// <summary>
        /// The item being used
        /// </summary>
        public uint objectId; // ObjectID

        public Inventory_UseEvent_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            objectId = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectId = {objectId}");
        }

    }

    /// <summary>
    /// Sets an allegiance officer
    /// </summary>
    public class Allegiance_SetAllegianceOfficer_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Allegiance_SetAllegianceOfficer;

        /// <summary>
        /// The allegiance officer&#39;s name
        /// </summary>
        public string charName; // string

        /// <summary>
        /// Level of the officer
        /// </summary>
        public AllegianceOfficerLevel level; // AllegianceOfficerLevel

        public Allegiance_SetAllegianceOfficer_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            charName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"charName = {charName}");
            level = (AllegianceOfficerLevel)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"level = {level}");
        }

    }

    /// <summary>
    /// Sets an allegiance officer title for a given level
    /// </summary>
    public class Allegiance_SetAllegianceOfficerTitle_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Allegiance_SetAllegianceOfficerTitle;

        /// <summary>
        /// The allegiance officer level
        /// </summary>
        public AllegianceOfficerLevel level; // AllegianceOfficerLevel

        /// <summary>
        /// The new title for officers at that level
        /// </summary>
        public string title; // string

        public Allegiance_SetAllegianceOfficerTitle_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            level = (AllegianceOfficerLevel)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"level = {level}");
            title = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"title = {title}");
        }

    }

    /// <summary>
    /// List the allegiance officer titles
    /// </summary>
    public class Allegiance_ListAllegianceOfficerTitles_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Allegiance_ListAllegianceOfficerTitles;

        public Allegiance_ListAllegianceOfficerTitles_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Clears the allegiance officer titles
    /// </summary>
    public class Allegiance_ClearAllegianceOfficerTitles_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Allegiance_ClearAllegianceOfficerTitles;

        public Allegiance_ClearAllegianceOfficerTitles_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Perform the allegiance lock action
    /// </summary>
    public class Allegiance_DoAllegianceLockAction_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Allegiance_DoAllegianceLockAction;

        /// <summary>
        /// Allegiance housing action to take
        /// </summary>
        public AllegianceLockAction action; // AllegianceLockAction

        public Allegiance_DoAllegianceLockAction_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            action = (AllegianceLockAction)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"action = {action}");
        }

    }

    /// <summary>
    /// Sets a person as an approved vassal
    /// </summary>
    public class Allegiance_SetAllegianceApprovedVassal_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Allegiance_SetAllegianceApprovedVassal;

        /// <summary>
        /// Player who is being approved as a vassal
        /// </summary>
        public string charName; // string

        public Allegiance_SetAllegianceApprovedVassal_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            charName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"charName = {charName}");
        }

    }

    /// <summary>
    /// Gags a person in allegiance chat
    /// </summary>
    public class Allegiance_AllegianceChatGag_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Allegiance_AllegianceChatGag;

        /// <summary>
        /// Player who is being gagged or ungagged
        /// </summary>
        public string charName; // string

        /// <summary>
        /// Whether the gag is on
        /// </summary>
        public bool on; // bool

        public Allegiance_AllegianceChatGag_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            charName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"charName = {charName}");
            on = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"on = {on}");
        }

    }

    /// <summary>
    /// Perform the allegiance house action
    /// </summary>
    public class Allegiance_DoAllegianceHouseAction_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Allegiance_DoAllegianceHouseAction;

        /// <summary>
        /// Allegiance housing action to take
        /// </summary>
        public AllegianceHouseAction action; // AllegianceHouseAction

        public Allegiance_DoAllegianceHouseAction_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            action = (AllegianceHouseAction)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"action = {action}");
        }

    }

    /// <summary>
    /// Spend XP to raise a vital.
    /// </summary>
    public class Train_TrainAttribute2nd_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Train_TrainAttribute2nd;

        /// <summary>
        /// The ID of the vital
        /// </summary>
        public VitalID atype; // VitalID

        /// <summary>
        /// The amount of XP being spent
        /// </summary>
        public uint xpSpent; // uint

        public Train_TrainAttribute2nd_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            atype = (VitalID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"atype = {atype}");
            xpSpent = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"xpSpent = {xpSpent}");
        }

    }

    /// <summary>
    /// Spend XP to raise an attribute.
    /// </summary>
    public class Train_TrainAttribute_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Train_TrainAttribute;

        /// <summary>
        /// The ID of the attribute
        /// </summary>
        public AttributeID atype; // AttributeID

        /// <summary>
        /// The amount of XP being spent
        /// </summary>
        public uint xpSpent; // uint

        public Train_TrainAttribute_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            atype = (AttributeID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"atype = {atype}");
            xpSpent = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"xpSpent = {xpSpent}");
        }

    }

    /// <summary>
    /// Spend XP to raise a skill.
    /// </summary>
    public class Train_TrainSkill_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Train_TrainSkill;

        /// <summary>
        /// The ID of the skill
        /// </summary>
        public SkillID stype; // SkillID

        /// <summary>
        /// The amount of XP being spent
        /// </summary>
        public uint xpSpent; // uint

        public Train_TrainSkill_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            stype = (SkillID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"stype = {stype}");
            xpSpent = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"xpSpent = {xpSpent}");
        }

    }

    /// <summary>
    /// Spend skill credits to train a skill.
    /// </summary>
    public class Train_TrainSkillAdvancementClass_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Train_TrainSkillAdvancementClass;

        /// <summary>
        /// The ID of the skill
        /// </summary>
        public SkillID stype; // SkillID

        /// <summary>
        /// The number of skill credits being spent
        /// </summary>
        public uint creditsSpent; // uint

        public Train_TrainSkillAdvancementClass_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            stype = (SkillID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"stype = {stype}");
            creditsSpent = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"creditsSpent = {creditsSpent}");
        }

    }

    /// <summary>
    /// Cast a spell with no target.
    /// </summary>
    public class Magic_CastUntargetedSpell_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Magic_CastUntargetedSpell;

        /// <summary>
        /// The ID of the spell
        /// </summary>
        public LayeredSpellID spellID; // LayeredSpellID

        public Magic_CastUntargetedSpell_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            spellID = new LayeredSpellID();
            spellID.ReadFromBuffer(buffer);
            Logger.Log($"spellID = {spellID}");
        }

    }

    /// <summary>
    /// Cast a spell on a target
    /// </summary>
    public class Magic_CastTargetedSpell_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Magic_CastTargetedSpell;

        /// <summary>
        /// The target of the spell
        /// </summary>
        public uint target; // ObjectID

        /// <summary>
        /// The ID of the spell
        /// </summary>
        public LayeredSpellID spellID; // LayeredSpellID

        public Magic_CastTargetedSpell_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            target = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"target = {target}");
            spellID = new LayeredSpellID();
            spellID.ReadFromBuffer(buffer);
            Logger.Log($"spellID = {spellID}");
        }

    }

    /// <summary>
    /// Close Container - Only sent when explicitly closed
    /// </summary>
    public class Item_StopViewingObjectContents_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Item_StopViewingObjectContents;

        /// <summary>
        /// Chest or corpse being closed
        /// </summary>
        public uint objectID; // ObjectID

        public Item_StopViewingObjectContents_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            objectID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectID = {objectID}");
        }

    }

    /// <summary>
    /// Changes the combat mode
    /// </summary>
    public class Combat_ChangeCombatMode_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Combat_ChangeCombatMode;

        /// <summary>
        /// New combat mode for player
        /// </summary>
        public CombatMode mode; // CombatMode

        public Combat_ChangeCombatMode_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            mode = (CombatMode)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"mode = {mode}");
        }

    }

    /// <summary>
    /// Merges one stack with another
    /// </summary>
    public class Inventory_StackableMerge_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Inventory_StackableMerge;

        /// <summary>
        /// ID of object where items are being taken from
        /// </summary>
        public uint mergeFromID; // ObjectID

        /// <summary>
        /// ID of object where items are being merged into
        /// </summary>
        public uint mergeToID; // ObjectID

        /// <summary>
        /// Number of items from the source stack to be added to the destination stack
        /// </summary>
        public uint amount; // uint

        public Inventory_StackableMerge_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            mergeFromID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"mergeFromID = {mergeFromID}");
            mergeToID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"mergeToID = {mergeToID}");
            amount = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"amount = {amount}");
        }

    }

    /// <summary>
    /// Split a stack and place it into a container
    /// </summary>
    public class Inventory_StackableSplitToContainer_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Inventory_StackableSplitToContainer;

        /// <summary>
        /// ID of object where items are being taken from
        /// </summary>
        public uint stackID; // ObjectID

        /// <summary>
        /// ID of container where items are being moved to
        /// </summary>
        public uint contianerID; // ObjectID

        /// <summary>
        /// Slot in container where items are being moved to
        /// </summary>
        public uint place; // uint

        /// <summary>
        /// Number of items from the stack to placed into the container
        /// </summary>
        public uint amount; // uint

        public Inventory_StackableSplitToContainer_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            stackID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"stackID = {stackID}");
            contianerID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"contianerID = {contianerID}");
            place = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"place = {place}");
            amount = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"amount = {amount}");
        }

    }

    /// <summary>
    /// Split a stack and place it into the world
    /// </summary>
    public class Inventory_StackableSplitTo3D_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Inventory_StackableSplitTo3D;

        /// <summary>
        /// ID of object where items are being taken from
        /// </summary>
        public uint stackID; // ObjectID

        /// <summary>
        /// Number of items from the stack to placed into the world
        /// </summary>
        public uint amount; // uint

        public Inventory_StackableSplitTo3D_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            stackID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"stackID = {stackID}");
            amount = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"amount = {amount}");
        }

    }

    /// <summary>
    /// Changes an account squelch
    /// </summary>
    public class Communication_ModifyCharacterSquelch_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Communication_ModifyCharacterSquelch;

        /// <summary>
        /// 0 = unsquelch, 1 = squelch
        /// </summary>
        public bool add; // bool

        /// <summary>
        /// The character id who&#39;s acount should be squelched
        /// </summary>
        public uint characterID; // ObjectID

        /// <summary>
        /// The character&#39;s name who&#39;s acount should be squelched
        /// </summary>
        public string characterName; // string

        /// <summary>
        /// The message type to squelch or unsquelch
        /// </summary>
        public ChatMessageType msgType; // ChatMessageType

        public Communication_ModifyCharacterSquelch_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            add = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"add = {add}");
            characterID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"characterID = {characterID}");
            characterName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"characterName = {characterName}");
            msgType = (ChatMessageType)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"msgType = {msgType}");
        }

    }

    /// <summary>
    /// Changes an account squelch
    /// </summary>
    public class Communication_ModifyAccountSquelch_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Communication_ModifyAccountSquelch;

        /// <summary>
        /// 0 = unsquelch, 1 = squelch
        /// </summary>
        public bool add; // bool

        /// <summary>
        /// The character who&#39;s acount should be squelched
        /// </summary>
        public string characterName; // string

        public Communication_ModifyAccountSquelch_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            add = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"add = {add}");
            characterName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"characterName = {characterName}");
        }

    }

    /// <summary>
    /// Changes the global filters, /filter -type as well as /chat and /notell
    /// </summary>
    public class Communication_ModifyGlobalSquelch_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Communication_ModifyGlobalSquelch;

        /// <summary>
        /// 0 = unsquelch, 1 = squelch
        /// </summary>
        public bool add; // bool

        /// <summary>
        /// The message type to squelch or unsquelch
        /// </summary>
        public ChatMessageType msgType; // ChatMessageType

        public Communication_ModifyGlobalSquelch_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            add = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"add = {add}");
            msgType = (ChatMessageType)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"msgType = {msgType}");
        }

    }

    /// <summary>
    /// Direct message by name
    /// </summary>
    public class Communication_TalkDirectByName_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Communication_TalkDirectByName;

        /// <summary>
        /// The message text
        /// </summary>
        public string msg; // string

        /// <summary>
        /// Name of person you are sending a message to
        /// </summary>
        public string targetName; // string

        public Communication_TalkDirectByName_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            msg = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"msg = {msg}");
            targetName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"targetName = {targetName}");
        }

    }

    /// <summary>
    /// Buy from a vendor
    /// </summary>
    public class Vendor_Buy_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Vendor_Buy;

        /// <summary>
        /// ID of vendor being bought from
        /// </summary>
        public uint vendorID; // ObjectID

        /// <summary>
        /// Items being purchased from the vendor
        /// </summary>
        public PackableList<ItemProfile> items; // PackableList

        /// <summary>
        /// the type of alternate currency being used
        /// </summary>
        public uint alternateCurrencyID; // uint

        public Vendor_Buy_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            vendorID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"vendorID = {vendorID}");
            items = new PackableList<ItemProfile>();
            items.ReadFromBuffer(buffer);
            Logger.Log($"items = {items}");
            alternateCurrencyID = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"alternateCurrencyID = {alternateCurrencyID}");
        }

    }

    /// <summary>
    /// Sell to a vendor
    /// </summary>
    public class Vendor_Sell_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Vendor_Sell;

        /// <summary>
        /// ID of vendor being sold to
        /// </summary>
        public uint vendorID; // ObjectID

        /// <summary>
        /// Items being sold to the vendor
        /// </summary>
        public PackableList<ItemProfile> items; // PackableList

        public Vendor_Sell_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            vendorID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"vendorID = {vendorID}");
            items = new PackableList<ItemProfile>();
            items.ReadFromBuffer(buffer);
            Logger.Log($"items = {items}");
        }

    }

    /// <summary>
    /// Open the buy/sell panel for a merchant.
    /// </summary>
    public class Vendor_VendorInfo_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Vendor_VendorInfo;

        /// <summary>
        /// the object ID of the merchant
        /// </summary>
        public uint merchantID; // ObjectID

        /// <summary>
        /// the object ID of the merchant
        /// </summary>
        public VendorProfile vendorProfile; // VendorProfile

        /// <summary>
        /// Items available from the vendor
        /// </summary>
        public PackableList<ItemProfile> items; // PackableList

        public Vendor_VendorInfo_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            merchantID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"merchantID = {merchantID}");
            vendorProfile = new VendorProfile();
            vendorProfile.ReadFromBuffer(buffer);
            Logger.Log($"vendorProfile = {vendorProfile}");
            items = new PackableList<ItemProfile>();
            items.ReadFromBuffer(buffer);
            Logger.Log($"items = {items}");
        }

    }

    /// <summary>
    /// Teleport to your lifestone. (/lifestone)
    /// </summary>
    public class Character_TeleToLifestone_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Character_TeleToLifestone;

        public Character_TeleToLifestone_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Opens barber UI
    /// </summary>
    public class Character_StartBarber_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Character_StartBarber;

        public DataID basePalette; // DataID

        public DataID headObject; // DataID

        public DataID headTexture; // DataID

        public DataID defaultHeadTexture; // DataID

        public DataID eyesTexture; // DataID

        public DataID defaultEyesTexture; // DataID

        public DataID noseTexture; // DataID

        public DataID defaultNoseTexture; // DataID

        public DataID mouthTexture; // DataID

        public DataID defaultMouthTexture; // DataID

        public DataID skinPalette; // DataID

        public DataID hairPalette; // DataID

        public DataID eyesPalette; // DataID

        public DataID setupID; // DataID

        /// <summary>
        /// Specifies the toggle option for some races, such as floating empyrean or flaming head on undead
        /// </summary>
        public int option1; // int

        /// <summary>
        /// Seems to be unused
        /// </summary>
        public int option2; // int

        public Character_StartBarber_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            basePalette = new DataID();
            basePalette.ReadFromBuffer(buffer);
            Logger.Log($"basePalette = {basePalette}");
            headObject = new DataID();
            headObject.ReadFromBuffer(buffer);
            Logger.Log($"headObject = {headObject}");
            headTexture = new DataID();
            headTexture.ReadFromBuffer(buffer);
            Logger.Log($"headTexture = {headTexture}");
            defaultHeadTexture = new DataID();
            defaultHeadTexture.ReadFromBuffer(buffer);
            Logger.Log($"defaultHeadTexture = {defaultHeadTexture}");
            eyesTexture = new DataID();
            eyesTexture.ReadFromBuffer(buffer);
            Logger.Log($"eyesTexture = {eyesTexture}");
            defaultEyesTexture = new DataID();
            defaultEyesTexture.ReadFromBuffer(buffer);
            Logger.Log($"defaultEyesTexture = {defaultEyesTexture}");
            noseTexture = new DataID();
            noseTexture.ReadFromBuffer(buffer);
            Logger.Log($"noseTexture = {noseTexture}");
            defaultNoseTexture = new DataID();
            defaultNoseTexture.ReadFromBuffer(buffer);
            Logger.Log($"defaultNoseTexture = {defaultNoseTexture}");
            mouthTexture = new DataID();
            mouthTexture.ReadFromBuffer(buffer);
            Logger.Log($"mouthTexture = {mouthTexture}");
            defaultMouthTexture = new DataID();
            defaultMouthTexture.ReadFromBuffer(buffer);
            Logger.Log($"defaultMouthTexture = {defaultMouthTexture}");
            skinPalette = new DataID();
            skinPalette.ReadFromBuffer(buffer);
            Logger.Log($"skinPalette = {skinPalette}");
            hairPalette = new DataID();
            hairPalette.ReadFromBuffer(buffer);
            Logger.Log($"hairPalette = {hairPalette}");
            eyesPalette = new DataID();
            eyesPalette.ReadFromBuffer(buffer);
            Logger.Log($"eyesPalette = {eyesPalette}");
            setupID = new DataID();
            setupID.ReadFromBuffer(buffer);
            Logger.Log($"setupID = {setupID}");
            option1 = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"option1 = {option1}");
            option2 = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"option2 = {option2}");
        }

    }

    /// <summary>
    /// Failure to give an item
    /// </summary>
    public class Character_ServerSaysAttemptFailed_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Character_ServerSaysAttemptFailed;

        /// <summary>
        /// Item that could not be given
        /// </summary>
        public uint itemID; // ObjectID

        /// <summary>
        /// Failure reason code
        /// </summary>
        public StatusMessage reason; // StatusMessage

        public Character_ServerSaysAttemptFailed_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            itemID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"itemID = {itemID}");
            reason = (StatusMessage)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"reason = {reason}");
        }

    }

    /// <summary>
    /// The client is ready for the character to materialize after portalling or logging on.
    /// </summary>
    public class Character_LoginCompleteNotification_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Character_LoginCompleteNotification;

        public Character_LoginCompleteNotification_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Create a fellowship
    /// </summary>
    public class Fellowship_Create_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Fellowship_Create;

        /// <summary>
        /// Name of the fellowship
        /// </summary>
        public string name; // string

        /// <summary>
        /// Whether fellowship shares xp
        /// </summary>
        public bool shareXP; // bool

        public Fellowship_Create_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            name = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"name = {name}");
            shareXP = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"shareXP = {shareXP}");
        }

    }

    /// <summary>
    /// Member left fellowship
    /// </summary>
    public class Fellowship_Quit_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Fellowship_Quit;

        /// <summary>
        /// Whether to disband the fellowship while quiting
        /// </summary>
        public bool disband; // bool

        public Fellowship_Quit_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            disband = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"disband = {disband}");
        }

    }

    /// <summary>
    /// Quit the fellowship
    /// </summary>
    public class Fellowship_Quit_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Fellowship_Quit;

        /// <summary>
        /// Whether to disband the fellowship while quiting
        /// </summary>
        public bool disband; // bool

        public Fellowship_Quit_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            disband = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"disband = {disband}");
        }

    }

    /// <summary>
    /// Member dismissed from fellowship
    /// </summary>
    public class Fellowship_Dismiss_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Fellowship_Dismiss;

        /// <summary>
        /// ID of player being dismissed from the fellowship
        /// </summary>
        public uint target; // ObjectID

        public Fellowship_Dismiss_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            target = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"target = {target}");
        }

    }

    /// <summary>
    /// Dismiss a player from the fellowship
    /// </summary>
    public class Fellowship_Dismiss_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Fellowship_Dismiss;

        /// <summary>
        /// ID of player being dismissed from the fellowship
        /// </summary>
        public uint target; // ObjectID

        public Fellowship_Dismiss_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            target = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"target = {target}");
        }

    }

    /// <summary>
    /// Recruit a player to the fellowship
    /// </summary>
    public class Fellowship_Recruit_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Fellowship_Recruit;

        /// <summary>
        /// ID of player being recruited to the fellowship
        /// </summary>
        public uint target; // ObjectID

        public Fellowship_Recruit_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            target = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"target = {target}");
        }

    }

    /// <summary>
    /// Update request
    /// </summary>
    public class Fellowship_UpdateRequest_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Fellowship_UpdateRequest;

        /// <summary>
        /// Whether the fellowship UI is visible
        /// </summary>
        public bool on; // bool

        public Fellowship_UpdateRequest_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            on = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"on = {on}");
        }

    }

    /// <summary>
    /// Request update to book data (seems to be sent after failed add page)
    /// </summary>
    public class Writing_BookAddPage_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Writing_BookAddPage;

        /// <summary>
        /// Object id of book
        /// </summary>
        public uint objectID; // ObjectID

        public Writing_BookAddPage_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            objectID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectID = {objectID}");
        }

    }

    /// <summary>
    /// Updates a page in a book
    /// </summary>
    public class Writing_BookModifyPage_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Writing_BookModifyPage;

        /// <summary>
        /// ID of book
        /// </summary>
        public uint objectID; // ObjectID

        /// <summary>
        /// Number of page being updated
        /// </summary>
        public int pageNum; // int

        /// <summary>
        /// New text for the page
        /// </summary>
        public string pageText; // string

        public Writing_BookModifyPage_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            objectID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectID = {objectID}");
            pageNum = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"pageNum = {pageNum}");
            pageText = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"pageText = {pageText}");
        }

    }

    /// <summary>
    /// Add a page to a book
    /// </summary>
    public class Writing_BookData_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Writing_BookData;

        /// <summary>
        /// ID of book to add a page to
        /// </summary>
        public uint objectID; // ObjectID

        public Writing_BookData_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            objectID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectID = {objectID}");
        }

    }

    /// <summary>
    /// Removes a page from a book
    /// </summary>
    public class Writing_BookDeletePage_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Writing_BookDeletePage;

        /// <summary>
        /// ID of book to remove a page from
        /// </summary>
        public uint objectID; // ObjectID

        /// <summary>
        /// Number of page to remove
        /// </summary>
        public int pageNum; // int

        public Writing_BookDeletePage_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            objectID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectID = {objectID}");
            pageNum = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"pageNum = {pageNum}");
        }

    }

    /// <summary>
    /// Requests data for a page in a book
    /// </summary>
    public class Writing_BookPageData_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Writing_BookPageData;

        /// <summary>
        /// ID of book
        /// </summary>
        public uint objectID; // ObjectID

        /// <summary>
        /// Number of page to get data for
        /// </summary>
        public int pageNum; // int

        public Writing_BookPageData_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            objectID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectID = {objectID}");
            pageNum = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"pageNum = {pageNum}");
        }

    }

    /// <summary>
    /// Sent when you first open a book, contains the entire table of contents.
    /// </summary>
    public class Writing_BookOpen_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Writing_BookOpen;

        /// <summary>
        /// The readable object you have just opened.
        /// </summary>
        public uint bookID; // ObjectID

        /// <summary>
        /// The total number of pages in the book.
        /// </summary>
        public uint maxNumPages; // uint

        /// <summary>
        /// The set of page data
        /// </summary>
        public PageDataList pageData; // PageDataList

        /// <summary>
        /// The inscription comment and the book title.
        /// </summary>
        public string inscription; // string

        /// <summary>
        /// The author of the inscription (and not coincidentally, the book title).
        /// </summary>
        public uint scribeId; // ObjectID

        /// <summary>
        /// The name of the inscription author.
        /// </summary>
        public string scribeName; // string

        public Writing_BookOpen_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            bookID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"bookID = {bookID}");
            maxNumPages = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"maxNumPages = {maxNumPages}");
            pageData = new PageDataList();
            pageData.ReadFromBuffer(buffer);
            Logger.Log($"pageData = {pageData}");
            inscription = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"inscription = {inscription}");
            scribeId = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"scribeId = {scribeId}");
            scribeName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"scribeName = {scribeName}");
        }

    }

    /// <summary>
    /// Response to an attempt to add a page to a book.
    /// </summary>
    public class Writing_BookAddPageResponse_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Writing_BookAddPageResponse;

        /// <summary>
        /// The readable object being changed.
        /// </summary>
        public uint bookID; // ObjectID

        /// <summary>
        /// The number the of page being added in the book.
        /// </summary>
        public uint pageNumber; // uint

        /// <summary>
        /// Whether the request was successful
        /// </summary>
        public bool success; // bool

        public Writing_BookAddPageResponse_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            bookID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"bookID = {bookID}");
            pageNumber = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"pageNumber = {pageNumber}");
            success = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"success = {success}");
        }

    }

    /// <summary>
    /// Response to an attempt to delete a page from a book.
    /// </summary>
    public class Writing_BookDeletePageResponse_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Writing_BookDeletePageResponse;

        /// <summary>
        /// The readable object being changed.
        /// </summary>
        public uint bookID; // ObjectID

        /// <summary>
        /// The number the of page being deleted in the book.
        /// </summary>
        public uint pageNumber; // uint

        /// <summary>
        /// Whether the request was successful
        /// </summary>
        public bool success; // bool

        public Writing_BookDeletePageResponse_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            bookID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"bookID = {bookID}");
            pageNumber = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"pageNumber = {pageNumber}");
            success = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"success = {success}");
        }

    }

    /// <summary>
    /// Contains the text of a single page of a book, parchment or sign.
    /// </summary>
    public class Writing_BookPageDataResponse_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Writing_BookPageDataResponse;

        /// <summary>
        /// The object id for the readable object.
        /// </summary>
        public uint objectID; // ObjectID

        /// <summary>
        /// The 0-based index of the page you are currently viewing.
        /// </summary>
        public uint page; // uint

        public PageData pageData; // PageData

        public Writing_BookPageDataResponse_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            objectID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectID = {objectID}");
            page = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"page = {page}");
            pageData = new PageData();
            pageData.ReadFromBuffer(buffer);
            Logger.Log($"pageData = {pageData}");
        }

    }

    /// <summary>
    /// Sets the inscription on an object
    /// </summary>
    public class Writing_SetInscription_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Writing_SetInscription;

        /// <summary>
        /// ID of object being inscribed
        /// </summary>
        public uint objectID; // ObjectID

        /// <summary>
        /// Inscription text
        /// </summary>
        public string inscription; // string

        public Writing_SetInscription_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            objectID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectID = {objectID}");
            inscription = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"inscription = {inscription}");
        }

    }

    /// <summary>
    /// Get Inscription Response, doesn&#39;t seem to be really used by client
    /// </summary>
    public class Item_GetInscriptionResponse_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Item_GetInscriptionResponse;

        /// <summary>
        /// The inscription comment
        /// </summary>
        public string inscription; // string

        /// <summary>
        /// The name of the inscription author.
        /// </summary>
        public string scribeName; // string

        public string scribeAccount; // string

        public Item_GetInscriptionResponse_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            inscription = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"inscription = {inscription}");
            scribeName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"scribeName = {scribeName}");
            scribeAccount = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"scribeAccount = {scribeAccount}");
        }

    }

    /// <summary>
    /// Appraise something
    /// </summary>
    public class Item_Appraise_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Item_Appraise;

        /// <summary>
        /// The object being appraised
        /// </summary>
        public uint objectID; // ObjectID

        public Item_Appraise_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            objectID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectID = {objectID}");
        }

    }

    /// <summary>
    /// The result of an attempt to assess an item or creature.
    /// </summary>
    public class Item_SetAppraiseInfo_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Item_SetAppraiseInfo;

        /// <summary>
        /// the object ID of the item or creature being assessed
        /// </summary>
        public uint objectID; // ObjectID

        public uint flags; // uint

        /// <summary>
        /// assessment successful: 0=no, 1=yes
        /// </summary>
        public bool success; // bool

        public PackableHashTable<IntPropertyID, int> intStatsTable; // PackableHashTable

        public PackableHashTable<Int64PropertyID, long> int64StatsTable; // PackableHashTable

        public PackableHashTable<BooleanPropertyID, bool> boolStatsTable; // PackableHashTable

        public PackableHashTable<FloatPropertyID, double> floatStatsTable; // PackableHashTable

        public PackableHashTable<StringPropertyID, string> strStatsTable; // PackableHashTable

        public PackableHashTable<DataPropertyID, DataID> didStatsTable; // PackableHashTable

        public PSmartArray<LayeredSpellID> spellBook; // PSmartArray

        public ArmorProfile armorProfile; // ArmorProfile

        public CreatureAppraisalProfile creatureProfile; // CreatureAppraisalProfile

        public WeaponProfile weaponProfile; // WeaponProfile

        public HookAppraisalProfile hookAppraisalProfile; // HookAppraisalProfile

        /// <summary>
        /// highlight enable bitmask: 0=no, 1=yes
        /// </summary>
        public ArmorHighlightMask protHighlight; // ArmorHighlightMask

        /// <summary>
        /// highlight color bitmask: 0=red, 1=green
        /// </summary>
        public ArmorHighlightMask protColor; // ArmorHighlightMask

        /// <summary>
        /// highlight enable bitmask: 0=no, 1=yes
        /// </summary>
        public WeaponHighlightMask weapHighlight; // WeaponHighlightMask

        /// <summary>
        /// highlight color bitmask: 0=red, 1=green
        /// </summary>
        public WeaponHighlightMask weapColor; // WeaponHighlightMask

        /// <summary>
        /// highlight enable bitmask: 0=no, 1=yes
        /// </summary>
        public ResistHighlightMask resistHighlight; // ResistHighlightMask

        /// <summary>
        /// highlight color bitmask: 0=red, 1=green
        /// </summary>
        public ResistHighlightMask resistColor; // ResistHighlightMask

        /// <summary>
        /// Armor level
        /// </summary>
        public uint baseArmorHead; // uint

        /// <summary>
        /// Armor level
        /// </summary>
        public uint baseArmorChest; // uint

        /// <summary>
        /// Armor level
        /// </summary>
        public uint baseArmorGroin; // uint

        /// <summary>
        /// Armor level
        /// </summary>
        public uint baseArmorBicep; // uint

        /// <summary>
        /// Armor level
        /// </summary>
        public uint baseArmorWrist; // uint

        /// <summary>
        /// Armor level
        /// </summary>
        public uint baseArmorHand; // uint

        /// <summary>
        /// Armor level
        /// </summary>
        public uint baseArmorThigh; // uint

        /// <summary>
        /// Armor level
        /// </summary>
        public uint baseArmorShin; // uint

        /// <summary>
        /// Armor level
        /// </summary>
        public uint baseArmorFoot; // uint

        public Item_SetAppraiseInfo_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            objectID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectID = {objectID}");
            flags = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"flags = {flags}");
            success = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"success = {success}");
            if (((uint)flags & 0x00000001) != 0) {
                intStatsTable = new PackableHashTable<IntPropertyID, int>();
                intStatsTable.ReadFromBuffer(buffer);
                Logger.Log($"intStatsTable = {intStatsTable}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00002000) != 0) {
                int64StatsTable = new PackableHashTable<Int64PropertyID, long>();
                int64StatsTable.ReadFromBuffer(buffer);
                Logger.Log($"int64StatsTable = {int64StatsTable}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000002) != 0) {
                boolStatsTable = new PackableHashTable<BooleanPropertyID, bool>();
                boolStatsTable.ReadFromBuffer(buffer);
                Logger.Log($"boolStatsTable = {boolStatsTable}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000004) != 0) {
                floatStatsTable = new PackableHashTable<FloatPropertyID, double>();
                floatStatsTable.ReadFromBuffer(buffer);
                Logger.Log($"floatStatsTable = {floatStatsTable}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000008) != 0) {
                strStatsTable = new PackableHashTable<StringPropertyID, string>();
                strStatsTable.ReadFromBuffer(buffer);
                Logger.Log($"strStatsTable = {strStatsTable}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00001000) != 0) {
                didStatsTable = new PackableHashTable<DataPropertyID, DataID>();
                didStatsTable.ReadFromBuffer(buffer);
                Logger.Log($"didStatsTable = {didStatsTable}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000010) != 0) {
                spellBook = new PSmartArray<LayeredSpellID>();
                spellBook.ReadFromBuffer(buffer);
                Logger.Log($"spellBook = {spellBook}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000080) != 0) {
                armorProfile = new ArmorProfile();
                armorProfile.ReadFromBuffer(buffer);
                Logger.Log($"armorProfile = {armorProfile}");
            }
            if (((uint)flags & 0x00000100) != 0) {
                creatureProfile = new CreatureAppraisalProfile();
                creatureProfile.ReadFromBuffer(buffer);
                Logger.Log($"creatureProfile = {creatureProfile}");
            }
            if (((uint)flags & 0x00000020) != 0) {
                weaponProfile = new WeaponProfile();
                weaponProfile.ReadFromBuffer(buffer);
                Logger.Log($"weaponProfile = {weaponProfile}");
            }
            if (((uint)flags & 0x00000040) != 0) {
                hookAppraisalProfile = new HookAppraisalProfile();
                hookAppraisalProfile.ReadFromBuffer(buffer);
                Logger.Log($"hookAppraisalProfile = {hookAppraisalProfile}");
            }
            if (((uint)flags & 0x00000200) != 0) {
                protHighlight = (ArmorHighlightMask)BinaryHelpers.ReadUInt16(buffer);
                Logger.Log($"protHighlight = {protHighlight}");
                protColor = (ArmorHighlightMask)BinaryHelpers.ReadUInt16(buffer);
                Logger.Log($"protColor = {protColor}");
            }
            if (((uint)flags & 0x00000800) != 0) {
                weapHighlight = (WeaponHighlightMask)BinaryHelpers.ReadUInt16(buffer);
                Logger.Log($"weapHighlight = {weapHighlight}");
                weapColor = (WeaponHighlightMask)BinaryHelpers.ReadUInt16(buffer);
                Logger.Log($"weapColor = {weapColor}");
            }
            if (((uint)flags & 0x00000400) != 0) {
                resistHighlight = (ResistHighlightMask)BinaryHelpers.ReadUInt16(buffer);
                Logger.Log($"resistHighlight = {resistHighlight}");
                resistColor = (ResistHighlightMask)BinaryHelpers.ReadUInt16(buffer);
                Logger.Log($"resistColor = {resistColor}");
            }
            if (((uint)flags & 0x00004000) != 0) {
                baseArmorHead = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"baseArmorHead = {baseArmorHead}");
                baseArmorChest = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"baseArmorChest = {baseArmorChest}");
                baseArmorGroin = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"baseArmorGroin = {baseArmorGroin}");
                baseArmorBicep = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"baseArmorBicep = {baseArmorBicep}");
                baseArmorWrist = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"baseArmorWrist = {baseArmorWrist}");
                baseArmorHand = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"baseArmorHand = {baseArmorHand}");
                baseArmorThigh = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"baseArmorThigh = {baseArmorThigh}");
                baseArmorShin = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"baseArmorShin = {baseArmorShin}");
                baseArmorFoot = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"baseArmorFoot = {baseArmorFoot}");
            }
        }

    }

    /// <summary>
    /// Give an item to someone.
    /// </summary>
    public class Inventory_GiveObjectRequest_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Inventory_GiveObjectRequest;

        /// <summary>
        /// The recipient of the item
        /// </summary>
        public uint targetID; // ObjectID

        /// <summary>
        /// The item being given
        /// </summary>
        public uint objectID; // ObjectID

        /// <summary>
        /// The amount from a stack being given
        /// </summary>
        public uint amount; // uint

        public Inventory_GiveObjectRequest_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            targetID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"targetID = {targetID}");
            objectID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectID = {objectID}");
            amount = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"amount = {amount}");
        }

    }

    /// <summary>
    /// Advocate Teleport
    /// </summary>
    public class Advocate_Teleport_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Advocate_Teleport;

        /// <summary>
        /// Person being teleported
        /// </summary>
        public string target; // string

        /// <summary>
        /// Destination to teleport target to
        /// </summary>
        public Position dest; // Position

        public Advocate_Teleport_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            target = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"target = {target}");
            dest = new Position();
            dest.ReadFromBuffer(buffer);
            Logger.Log($"dest = {dest}");
        }

    }

    /// <summary>
    /// Sends an abuse report.
    /// </summary>
    public class Character_AbuseLogRequest_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Character_AbuseLogRequest;

        /// <summary>
        /// Name of character
        /// </summary>
        public string target; // string

        /// <summary>
        /// 1
        /// </summary>
        public uint status; // uint

        /// <summary>
        /// Description of complaint
        /// </summary>
        public string complaint; // string

        public Character_AbuseLogRequest_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            target = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"target = {target}");
            status = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"status = {status}");
            complaint = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"complaint = {complaint}");
        }

    }

    /// <summary>
    /// Joins a chat channel
    /// </summary>
    public class Communication_AddToChannel_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Communication_AddToChannel;

        /// <summary>
        /// Channel id, TODO get enum
        /// </summary>
        public uint chan; // uint

        public Communication_AddToChannel_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            chan = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"chan = {chan}");
        }

    }

    /// <summary>
    /// Leaves a chat channel
    /// </summary>
    public class Communication_RemoveFromChannel_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Communication_RemoveFromChannel;

        /// <summary>
        /// Channel id, TODO get enum
        /// </summary>
        public uint chan; // uint

        public Communication_RemoveFromChannel_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            chan = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"chan = {chan}");
        }

    }

    /// <summary>
    /// ChannelBroadcast: Group Chat
    /// </summary>
    public class Communication_ChannelBroadcast_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Communication_ChannelBroadcast;

        /// <summary>
        /// Channel id, TODO get enum
        /// </summary>
        public uint chan; // uint

        /// <summary>
        /// Message being sent
        /// </summary>
        public string msg; // string

        public Communication_ChannelBroadcast_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            chan = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"chan = {chan}");
            msg = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"msg = {msg}");
        }

    }

    /// <summary>
    /// Sends a message to a chat channel
    /// </summary>
    public class Communication_ChannelBroadcast_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Communication_ChannelBroadcast;

        /// <summary>
        /// Channel id, TODO get enum
        /// </summary>
        public uint chan; // uint

        /// <summary>
        /// the name of the player sending the message
        /// </summary>
        public string senderName; // string

        /// <summary>
        /// Message being sent
        /// </summary>
        public string msg; // string

        public Communication_ChannelBroadcast_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            chan = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"chan = {chan}");
            senderName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"senderName = {senderName}");
            msg = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"msg = {msg}");
        }

    }

    /// <summary>
    /// ChannelList: Provides list of characters listening to a channel, I assume in response to a command
    /// </summary>
    public class Communication_ChannelList_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Communication_ChannelList;

        public PackableList<string> characters; // PackableList

        public Communication_ChannelList_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            characters = new PackableList<string>();
            characters.ReadFromBuffer(buffer);
            Logger.Log($"characters = {characters}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    public class Communication_ChannelList_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Communication_ChannelList;

        /// <summary>
        /// Channel id, TODO get enum
        /// </summary>
        public uint chan; // uint

        public Communication_ChannelList_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            chan = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"chan = {chan}");
        }

    }

    /// <summary>
    /// ChannelIndex: Provides list of channels available to the player, I assume in response to a command
    /// </summary>
    public class Communication_ChannelIndex_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Communication_ChannelIndex;

        public PackableList<string> channels; // PackableList

        public Communication_ChannelIndex_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            channels = new PackableList<string>();
            channels.ReadFromBuffer(buffer);
            Logger.Log($"channels = {channels}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// Requests a channel index
    /// </summary>
    public class Communication_ChannelIndex_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Communication_ChannelIndex;

        public Communication_ChannelIndex_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Stop viewing the contents of a container
    /// </summary>
    public class Inventory_NoLongerViewingContents_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Inventory_NoLongerViewingContents;

        /// <summary>
        /// ID of the container
        /// </summary>
        public uint container; // ObjectID

        public Inventory_NoLongerViewingContents_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            container = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"container = {container}");
        }

    }

    /// <summary>
    /// Set Pack Contents
    /// </summary>
    public class Item_OnViewContents_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Item_OnViewContents;

        /// <summary>
        /// The pack we are setting the contents of. This pack objects and the contained objects may be created before or after the message.
        /// </summary>
        public uint containerID; // ObjectID

        public PackableList<ContentProfile> items; // PackableList

        public Item_OnViewContents_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            containerID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"containerID = {containerID}");
            items = new PackableList<ContentProfile>();
            items.ReadFromBuffer(buffer);
            Logger.Log($"items = {items}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// For stackable items, this changes the number of items in the stack.
    /// </summary>
    public class Item_UpdateStackSize_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Item_UpdateStackSize;

        /// <summary>
        /// Sequence number for this message
        /// </summary>
        public byte sequence; // byte

        /// <summary>
        /// Item getting it&#39;s stack adjusted.
        /// </summary>
        public uint itemID; // ObjectID

        /// <summary>
        /// New number of items in the stack.
        /// </summary>
        public uint amount; // uint

        /// <summary>
        /// New value for the item.
        /// </summary>
        public uint newValue; // uint

        public Item_UpdateStackSize_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            itemID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"itemID = {itemID}");
            amount = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"amount = {amount}");
            newValue = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"newValue = {newValue}");
        }

    }

    /// <summary>
    /// ServerSaysMoveItem: Removes an item from inventory (when you place it on the ground or give it away)
    /// </summary>
    public class Item_ServerSaysMoveItem_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Item_ServerSaysMoveItem;

        /// <summary>
        /// The item leaving your inventory.
        /// </summary>
        public uint item; // ObjectID

        public Item_ServerSaysMoveItem_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            item = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"item = {item}");
        }

    }

    /// <summary>
    /// Splits an item to a wield location.
    /// </summary>
    public class Inventory_StackableSplitToWield_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Inventory_StackableSplitToWield;

        /// <summary>
        /// ID of object being split
        /// </summary>
        public uint stackID; // ObjectID

        /// <summary>
        /// Equip location to place the stack
        /// </summary>
        public EquipMask loc; // EquipMask

        /// <summary>
        /// Amount of stack being split
        /// </summary>
        public int amount; // int

        public Inventory_StackableSplitToWield_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            stackID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"stackID = {stackID}");
            loc = (EquipMask)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"loc = {loc}");
            amount = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"amount = {amount}");
        }

    }

    /// <summary>
    /// Add an item to the shortcut bar.
    /// </summary>
    public class Character_AddShortCut_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Character_AddShortCut;

        /// <summary>
        /// Shortcut information
        /// </summary>
        public ShortCutData scData; // ShortCutData

        public Character_AddShortCut_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            scData = new ShortCutData();
            scData.ReadFromBuffer(buffer);
            Logger.Log($"scData = {scData}");
        }

    }

    /// <summary>
    /// Remove an item from the shortcut bar.
    /// </summary>
    public class Character_RemoveShortCut_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Character_RemoveShortCut;

        /// <summary>
        /// Position on the shortcut bar (0-8) of the item to be removed
        /// </summary>
        public uint index; // uint

        public Character_RemoveShortCut_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            index = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"index = {index}");
        }

    }

    /// <summary>
    /// A Player Kill occurred nearby (also sent for suicides).
    /// </summary>
    public class Combat_HandlePlayerDeathEvent_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Combat_HandlePlayerDeathEvent;

        /// <summary>
        /// The death message
        /// </summary>
        public string msg; // string

        /// <summary>
        /// The ID of the departed.
        /// </summary>
        public uint killed; // ObjectID

        /// <summary>
        /// The ID of the character doing the killing.
        /// </summary>
        public uint killer; // ObjectID

        public Combat_HandlePlayerDeathEvent_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            msg = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"msg = {msg}");
            killed = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"killed = {killed}");
            killer = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"killer = {killer}");
        }

    }

    /// <summary>
    /// Set multiple character options.
    /// </summary>
    public class Character_CharacterOptionsEvent_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Character_CharacterOptionsEvent;

        public PlayerModule options; // PlayerModule

        public Character_CharacterOptionsEvent_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            options = new PlayerModule();
            options.ReadFromBuffer(buffer);
            Logger.Log($"options = {options}");
        }

    }

    /// <summary>
    /// HandleAttackDoneEvent: Melee attack completed
    /// </summary>
    public class Combat_HandleAttackDoneEvent_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Combat_HandleAttackDoneEvent;

        /// <summary>
        /// Number of user attacks, doesn&#39;t appear to be used by client
        /// </summary>
        public uint number; // uint

        public Combat_HandleAttackDoneEvent_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            number = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"number = {number}");
        }

    }

    /// <summary>
    /// RemoveSpell: Delete a spell from your spellbook.
    /// </summary>
    public class Magic_RemoveSpell_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Magic_RemoveSpell;

        public LayeredSpellID spellID; // LayeredSpellID

        public Magic_RemoveSpell_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            spellID = new LayeredSpellID();
            spellID.ReadFromBuffer(buffer);
            Logger.Log($"spellID = {spellID}");
        }

    }

    /// <summary>
    /// Removes a spell from the spell book
    /// </summary>
    public class Magic_RemoveSpell_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Magic_RemoveSpell;

        public LayeredSpellID spellID; // LayeredSpellID

        public Magic_RemoveSpell_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            spellID = new LayeredSpellID();
            spellID.ReadFromBuffer(buffer);
            Logger.Log($"spellID = {spellID}");
        }

    }

    /// <summary>
    /// You just died.
    /// </summary>
    public class Combat_HandleVictimNotificationEventSelf_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Combat_HandleVictimNotificationEventSelf;

        /// <summary>
        /// Your (typically mocking) death message.
        /// </summary>
        public string msg; // string

        public Combat_HandleVictimNotificationEventSelf_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            msg = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"msg = {msg}");
        }

    }

    /// <summary>
    /// Message for a death, something you killed or your own death message.
    /// </summary>
    public class Combat_HandleVictimNotificationEventOther_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Combat_HandleVictimNotificationEventOther;

        /// <summary>
        /// The text of the nearby or present death message.
        /// </summary>
        public string msg; // string

        public Combat_HandleVictimNotificationEventOther_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            msg = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"msg = {msg}");
        }

    }

    /// <summary>
    /// HandleAttackerNotificationEvent: You have hit your target with a melee attack.
    /// </summary>
    public class Combat_HandleAttackerNotificationEvent_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Combat_HandleAttackerNotificationEvent;

        /// <summary>
        /// the name of your target
        /// </summary>
        public string defendersName; // string

        /// <summary>
        /// the type of damage done
        /// </summary>
        public DamageType dtype; // DamageType

        /// <summary>
        /// percentage of targets hp removed by damage. 0.0 (low) to 1.0 (high)
        /// </summary>
        public float php; // float

        /// <summary>
        /// the amount of damage done
        /// </summary>
        public uint hp; // uint

        /// <summary>
        /// critical hit: 0=no, 1=yes
        /// </summary>
        public bool critical; // bool

        /// <summary>
        /// additional information about the attack, such as whether it was a Sneak Attack, etc
        /// </summary>
        public AttackConditionsMask attackConditions; // AttackConditionsMask

        public Combat_HandleAttackerNotificationEvent_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            defendersName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"defendersName = {defendersName}");
            dtype = (DamageType)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"dtype = {dtype}");
            php = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"php = {php}");
            hp = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"hp = {hp}");
            critical = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"critical = {critical}");
            attackConditions = (AttackConditionsMask)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"attackConditions = {attackConditions}");
        }

    }

    /// <summary>
    /// HandleDefenderNotificationEvent: You have been hit by a creature&#39;s melee attack.
    /// </summary>
    public class Combat_HandleDefenderNotificationEvent_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Combat_HandleDefenderNotificationEvent;

        /// <summary>
        /// the name of the creature
        /// </summary>
        public string attackersName; // string

        /// <summary>
        /// the type of damage done
        /// </summary>
        public DamageType dtype; // DamageType

        /// <summary>
        /// percentage of targets hp removed by damage. 0.0 (low) to 1.0 (high)
        /// </summary>
        public float php; // float

        /// <summary>
        /// the amount of damage done
        /// </summary>
        public uint hp; // uint

        /// <summary>
        /// the location of the damage done
        /// </summary>
        public DamageLocation part; // DamageLocation

        /// <summary>
        /// critical hit: 0=no, 1=yes
        /// </summary>
        public bool critical; // bool

        /// <summary>
        /// additional information about the attack, such as whether it was a Sneak Attack, etc.
        /// </summary>
        public AttackConditionsMask attackConditions; // AttackConditionsMask

        public Combat_HandleDefenderNotificationEvent_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            attackersName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"attackersName = {attackersName}");
            dtype = (DamageType)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"dtype = {dtype}");
            php = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"php = {php}");
            hp = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"hp = {hp}");
            part = (DamageLocation)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"part = {part}");
            critical = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"critical = {critical}");
            attackConditions = (AttackConditionsMask)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"attackConditions = {attackConditions}");
        }

    }

    /// <summary>
    /// HandleEvasionAttackerNotificationEvent: Your target has evaded your melee attack.
    /// </summary>
    public class Combat_HandleEvasionAttackerNotificationEvent_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Combat_HandleEvasionAttackerNotificationEvent;

        /// <summary>
        /// the name of your target
        /// </summary>
        public string defendersName; // string

        public Combat_HandleEvasionAttackerNotificationEvent_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            defendersName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"defendersName = {defendersName}");
        }

    }

    /// <summary>
    /// HandleEvasionDefenderNotificationEvent: You have evaded a creature&#39;s melee attack.
    /// </summary>
    public class Combat_HandleEvasionDefenderNotificationEvent_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Combat_HandleEvasionDefenderNotificationEvent;

        /// <summary>
        /// the name of the creature
        /// </summary>
        public string attackersName; // string

        public Combat_HandleEvasionDefenderNotificationEvent_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            attackersName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"attackersName = {attackersName}");
        }

    }

    /// <summary>
    /// Cancels an attack
    /// </summary>
    public class Combat_CancelAttack_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Combat_CancelAttack;

        public Combat_CancelAttack_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// HandleCommenceAttackEvent: Start melee attack
    /// </summary>
    public class Combat_HandleCommenceAttackEvent_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Combat_HandleCommenceAttackEvent;

        public Combat_HandleCommenceAttackEvent_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Query&#39;s a creatures health
    /// </summary>
    public class Combat_QueryHealth_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Combat_QueryHealth;

        public uint targetID; // ObjectID

        public Combat_QueryHealth_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            targetID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"targetID = {targetID}");
        }

    }

    /// <summary>
    /// QueryHealthResponse: Update a creature&#39;s health bar.
    /// </summary>
    public class Combat_QueryHealthResponse_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Combat_QueryHealthResponse;

        /// <summary>
        /// the object ID of the creature
        /// </summary>
        public uint target; // ObjectID

        /// <summary>
        /// the amount of health remaining, scaled from 0.0 (none) to 1.0 (full)
        /// </summary>
        public float health; // float

        public Combat_QueryHealthResponse_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            target = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"target = {target}");
            health = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"health = {health}");
        }

    }

    /// <summary>
    /// Query a character&#39;s age
    /// </summary>
    public class Character_QueryAge_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Character_QueryAge;

        public uint targetID; // ObjectID

        public Character_QueryAge_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            targetID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"targetID = {targetID}");
        }

    }

    /// <summary>
    /// QueryAgeResponse: happens when you do /age in the game
    /// </summary>
    public class Character_QueryAgeResponse_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Character_QueryAgeResponse;

        /// <summary>
        /// Name of target, or empty if self
        /// </summary>
        public string targetName; // string

        /// <summary>
        /// Your age in the format 1mo 1d 1h 1m 1s
        /// </summary>
        public string age; // string

        public Character_QueryAgeResponse_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            targetName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"targetName = {targetName}");
            age = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"age = {age}");
        }

    }

    /// <summary>
    /// Query a character&#39;s birth day
    /// </summary>
    public class Character_QueryBirth_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Character_QueryBirth;

        public uint targetID; // ObjectID

        public Character_QueryBirth_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            targetID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"targetID = {targetID}");
        }

    }

    /// <summary>
    /// UseDone: Ready. Previous action complete
    /// </summary>
    public class Item_UseDone_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Item_UseDone;

        public StatusMessage failureType; // StatusMessage

        public Item_UseDone_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            failureType = (StatusMessage)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"failureType = {failureType}");
        }

    }

    /// <summary>
    /// Fellow update is done
    /// </summary>
    public class Fellowship_FellowUpdateDone_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Fellowship_FellowUpdateDone;

        public Fellowship_FellowUpdateDone_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Fellow stats are done
    /// </summary>
    public class Fellowship_FellowStatsDone_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Fellowship_FellowStatsDone;

        public Fellowship_FellowStatsDone_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Close Assess Panel
    /// </summary>
    public class Item_AppraiseDone_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Item_AppraiseDone;

        /// <summary>
        /// Seems to always be 0. Client does not use it.
        /// </summary>
        public uint Unknown; // uint

        public Item_AppraiseDone_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Unknown = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"Unknown = {Unknown}");
        }

    }

    /// <summary>
    /// Private Remove Int Event
    /// </summary>
    public class Qualities_PrivateRemoveIntEvent_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_PrivateRemoveIntEvent;

        public byte sequence; // byte

        public IntPropertyID type; // IntPropertyID

        public Qualities_PrivateRemoveIntEvent_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            type = (IntPropertyID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"type = {type}");
        }

    }

    /// <summary>
    /// Remove Int Event
    /// </summary>
    public class Qualities_RemoveIntEvent_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_RemoveIntEvent;

        public byte sequence; // byte

        public uint sender; // ObjectID

        public IntPropertyID type; // IntPropertyID

        public Qualities_RemoveIntEvent_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            sender = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"sender = {sender}");
            type = (IntPropertyID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"type = {type}");
        }

    }

    /// <summary>
    /// Private Remove Bool Event
    /// </summary>
    public class Qualities_PrivateRemoveBoolEvent_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_PrivateRemoveBoolEvent;

        public byte sequence; // byte

        public BooleanPropertyID type; // BooleanPropertyID

        public Qualities_PrivateRemoveBoolEvent_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            type = (BooleanPropertyID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"type = {type}");
        }

    }

    /// <summary>
    /// Remove Bool Event
    /// </summary>
    public class Qualities_RemoveBoolEvent_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_RemoveBoolEvent;

        public byte sequence; // byte

        public uint sender; // ObjectID

        public BooleanPropertyID type; // BooleanPropertyID

        public Qualities_RemoveBoolEvent_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            sender = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"sender = {sender}");
            type = (BooleanPropertyID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"type = {type}");
        }

    }

    /// <summary>
    /// Private Remove Float Event
    /// </summary>
    public class Qualities_PrivateRemoveFloatEvent_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_PrivateRemoveFloatEvent;

        public byte sequence; // byte

        public FloatPropertyID type; // FloatPropertyID

        public Qualities_PrivateRemoveFloatEvent_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            type = (FloatPropertyID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"type = {type}");
        }

    }

    /// <summary>
    /// Remove Float Event
    /// </summary>
    public class Qualities_RemoveFloatEvent_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_RemoveFloatEvent;

        public byte sequence; // byte

        public uint sender; // ObjectID

        public FloatPropertyID type; // FloatPropertyID

        public Qualities_RemoveFloatEvent_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            sender = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"sender = {sender}");
            type = (FloatPropertyID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"type = {type}");
        }

    }

    /// <summary>
    /// Private Remove String Event
    /// </summary>
    public class Qualities_PrivateRemoveStringEvent_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_PrivateRemoveStringEvent;

        public byte sequence; // byte

        public StringPropertyID type; // StringPropertyID

        public Qualities_PrivateRemoveStringEvent_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            type = (StringPropertyID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"type = {type}");
        }

    }

    /// <summary>
    /// Remove String Event
    /// </summary>
    public class Qualities_RemoveStringEvent_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_RemoveStringEvent;

        public byte sequence; // byte

        public uint sender; // ObjectID

        public StringPropertyID type; // StringPropertyID

        public Qualities_RemoveStringEvent_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            sender = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"sender = {sender}");
            type = (StringPropertyID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"type = {type}");
        }

    }

    /// <summary>
    /// Private Remove DID Event
    /// </summary>
    public class Qualities_PrivateRemoveDataIDEvent_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_PrivateRemoveDataIDEvent;

        public byte sequence; // byte

        public DataPropertyID type; // DataPropertyID

        public Qualities_PrivateRemoveDataIDEvent_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            type = (DataPropertyID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"type = {type}");
        }

    }

    /// <summary>
    /// Remove DID Event
    /// </summary>
    public class Qualities_RemoveDataIDEvent_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_RemoveDataIDEvent;

        public byte sequence; // byte

        public uint sender; // ObjectID

        public DataPropertyID type; // DataPropertyID

        public Qualities_RemoveDataIDEvent_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            sender = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"sender = {sender}");
            type = (DataPropertyID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"type = {type}");
        }

    }

    /// <summary>
    /// Private Remove IID Event
    /// </summary>
    public class Qualities_PrivateRemoveInstanceIDEvent_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_PrivateRemoveInstanceIDEvent;

        public byte sequence; // byte

        public InstancePropertyID type; // InstancePropertyID

        public Qualities_PrivateRemoveInstanceIDEvent_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            type = (InstancePropertyID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"type = {type}");
        }

    }

    /// <summary>
    /// Remove IID Event
    /// </summary>
    public class Qualities_RemoveInstanceIDEvent_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_RemoveInstanceIDEvent;

        public byte sequence; // byte

        public uint sender; // ObjectID

        public InstancePropertyID type; // InstancePropertyID

        public Qualities_RemoveInstanceIDEvent_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            sender = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"sender = {sender}");
            type = (InstancePropertyID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"type = {type}");
        }

    }

    /// <summary>
    /// Private Remove Position Event
    /// </summary>
    public class Qualities_PrivateRemovePositionEvent_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_PrivateRemovePositionEvent;

        public byte sequence; // byte

        public PositionPropertyID type; // PositionPropertyID

        public Qualities_PrivateRemovePositionEvent_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            type = (PositionPropertyID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"type = {type}");
        }

    }

    /// <summary>
    /// Remove Position Event
    /// </summary>
    public class Qualities_RemovePositionEvent_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_RemovePositionEvent;

        public byte sequence; // byte

        public uint sender; // ObjectID

        public PositionPropertyID type; // PositionPropertyID

        public Qualities_RemovePositionEvent_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            sender = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"sender = {sender}");
            type = (PositionPropertyID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"type = {type}");
        }

    }

    /// <summary>
    /// Emote message
    /// </summary>
    public class Communication_Emote_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Communication_Emote;

        public string msg; // string

        public Communication_Emote_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            msg = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"msg = {msg}");
        }

    }

    /// <summary>
    /// Indirect &#39;/e&#39; text.
    /// </summary>
    public class Communication_HearEmote_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Communication_HearEmote;

        /// <summary>
        /// The ID of the character performing the emote - used for squelch/radar filtering.
        /// </summary>
        public uint sender; // ObjectID

        /// <summary>
        /// Name of the character performing the emote.
        /// </summary>
        public string senderName; // string

        /// <summary>
        /// Text representation of the emote.
        /// </summary>
        public string text; // string

        public Communication_HearEmote_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sender = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"sender = {sender}");
            senderName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"senderName = {senderName}");
            text = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"text = {text}");
        }

    }

    /// <summary>
    /// Soul emote message
    /// </summary>
    public class Communication_SoulEmote_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Communication_SoulEmote;

        public string msg; // string

        public Communication_SoulEmote_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            msg = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"msg = {msg}");
        }

    }

    /// <summary>
    /// Contains the text associated with an emote action.
    /// </summary>
    public class Communication_HearSoulEmote_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Communication_HearSoulEmote;

        /// <summary>
        /// The ID of the character performing the emote - used for squelch/radar filtering.
        /// </summary>
        public uint sender; // ObjectID

        /// <summary>
        /// Name of the character performing the emote.
        /// </summary>
        public string senderName; // string

        /// <summary>
        /// Text representation of the emote.
        /// </summary>
        public string text; // string

        public Communication_HearSoulEmote_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sender = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"sender = {sender}");
            senderName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"senderName = {senderName}");
            text = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"text = {text}");
        }

    }

    /// <summary>
    /// Add a spell to a spell bar.
    /// </summary>
    public class Character_AddSpellFavorite_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Character_AddSpellFavorite;

        /// <summary>
        /// The spell&#39;s ID
        /// </summary>
        public LayeredSpellID spid; // LayeredSpellID

        /// <summary>
        /// Position on the spell bar where the spell is to be added
        /// </summary>
        public uint index; // uint

        /// <summary>
        /// The spell bar number
        /// </summary>
        public uint list; // uint

        public Character_AddSpellFavorite_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            spid = new LayeredSpellID();
            spid.ReadFromBuffer(buffer);
            Logger.Log($"spid = {spid}");
            index = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"index = {index}");
            list = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"list = {list}");
        }

    }

    /// <summary>
    /// Remove a spell from a spell bar.
    /// </summary>
    public class Character_RemoveSpellFavorite_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Character_RemoveSpellFavorite;

        /// <summary>
        /// The spell&#39;s ID
        /// </summary>
        public LayeredSpellID spid; // LayeredSpellID

        /// <summary>
        /// The spell bar number
        /// </summary>
        public uint list; // uint

        public Character_RemoveSpellFavorite_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            spid = new LayeredSpellID();
            spid.ReadFromBuffer(buffer);
            Logger.Log($"spid = {spid}");
            list = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"list = {list}");
        }

    }

    /// <summary>
    /// Request a ping
    /// </summary>
    public class Character_RequestPing_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Character_RequestPing;

        public Character_RequestPing_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Ping Reply
    /// </summary>
    public class Character_ReturnPing_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Character_ReturnPing;

        public Character_ReturnPing_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Squelch and Filter List
    /// </summary>
    public class Communication_SetSquelchDB_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Communication_SetSquelchDB;

        /// <summary>
        /// The set of squelch information for the user
        /// </summary>
        public SquelchDB squelchDB; // SquelchDB

        public Communication_SetSquelchDB_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            squelchDB = new SquelchDB();
            squelchDB.ReadFromBuffer(buffer);
            Logger.Log($"squelchDB = {squelchDB}");
        }

    }

    /// <summary>
    /// Starts trading with another player.
    /// </summary>
    public class Trade_OpenTradeNegotiations_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Trade_OpenTradeNegotiations;

        /// <summary>
        /// ID of player to trade with
        /// </summary>
        public uint other; // ObjectID

        public Trade_OpenTradeNegotiations_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            other = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"other = {other}");
        }

    }

    /// <summary>
    /// Ends trading, when trade window is closed?
    /// </summary>
    public class Trade_CloseTradeNegotiations_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Trade_CloseTradeNegotiations;

        public Trade_CloseTradeNegotiations_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Adds an object to the trade.
    /// </summary>
    public class Trade_AddToTrade_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Trade_AddToTrade_C2S;

        /// <summary>
        /// ID of object to add to the trade
        /// </summary>
        public uint item; // ObjectID

        /// <summary>
        /// Slot in trade window to add the object
        /// </summary>
        public uint loc; // uint

        public Trade_AddToTrade_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            item = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"item = {item}");
            loc = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"loc = {loc}");
        }

    }

    /// <summary>
    /// Accepts a trade.
    /// </summary>
    public class Trade_AcceptTrade_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Trade_AcceptTrade_C2S;

        /// <summary>
        /// The contents of the trade
        /// </summary>
        public Trade stuff; // Trade

        public Trade_AcceptTrade_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            stuff = new Trade();
            stuff.ReadFromBuffer(buffer);
            Logger.Log($"stuff = {stuff}");
        }

    }

    /// <summary>
    /// Declines a trade, when cancel is clicked?
    /// </summary>
    public class Trade_DeclineTrade_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Trade_DeclineTrade_C2S;

        public Trade_DeclineTrade_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// RegisterTrade: Send to begin a trade and display the trade window
    /// </summary>
    public class Trade_RegisterTrade_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Trade_RegisterTrade;

        /// <summary>
        /// Person initiating the trade
        /// </summary>
        public uint initiator; // ObjectID

        /// <summary>
        /// Person receiving the trade
        /// </summary>
        public uint partner; // ObjectID

        /// <summary>
        /// Some kind of stamp
        /// </summary>
        public long stamp; // long

        public Trade_RegisterTrade_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            initiator = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"initiator = {initiator}");
            partner = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"partner = {partner}");
            stamp = BinaryHelpers.ReadInt64(buffer); // long
            Logger.Log($"stamp = {stamp}");
        }

    }

    /// <summary>
    /// OpenTrade: Open trade window
    /// </summary>
    public class Trade_OpenTrade_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Trade_OpenTrade;

        /// <summary>
        /// Person opening the trade
        /// </summary>
        public uint sourceID; // ObjectID

        public Trade_OpenTrade_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sourceID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"sourceID = {sourceID}");
        }

    }

    /// <summary>
    /// CloseTrade: End trading
    /// </summary>
    public class Trade_CloseTrade_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Trade_CloseTrade;

        /// <summary>
        /// Reason trade was cancelled
        /// </summary>
        public EndTradeReason reason; // EndTradeReason

        public Trade_CloseTrade_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            reason = (EndTradeReason)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"reason = {reason}");
        }

    }

    /// <summary>
    /// RemoveFromTrade: Item was removed from trade window
    /// </summary>
    public class Trade_AddToTrade_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Trade_AddToTrade;

        /// <summary>
        /// The item being removed from trade window
        /// </summary>
        public uint itemID; // ObjectID

        /// <summary>
        /// Side of the trade window object was removed
        /// </summary>
        public TradeSide side; // TradeSide

        public Trade_AddToTrade_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            itemID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"itemID = {itemID}");
            side = (TradeSide)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"side = {side}");
        }

    }

    /// <summary>
    /// Removes an item from the trade window, not sure if this is used still?
    /// </summary>
    public class Trade_RemoveFromTrade_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Trade_RemoveFromTrade;

        /// <summary>
        /// The item being removed from the trade window
        /// </summary>
        public uint itemID; // ObjectID

        /// <summary>
        /// Side of the trade window object was removed
        /// </summary>
        public TradeSide side; // TradeSide

        public Trade_RemoveFromTrade_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            itemID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"itemID = {itemID}");
            side = (TradeSide)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"side = {side}");
        }

    }

    /// <summary>
    /// AcceptTrade: The trade was accepted
    /// </summary>
    public class Trade_AcceptTrade_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Trade_AcceptTrade;

        /// <summary>
        /// Person who accepted the trade
        /// </summary>
        public uint sourceID; // ObjectID

        public Trade_AcceptTrade_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sourceID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"sourceID = {sourceID}");
        }

    }

    /// <summary>
    /// DeclineTrade: The trade was declined
    /// </summary>
    public class Trade_DeclineTrade_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Trade_DeclineTrade;

        /// <summary>
        /// Person who declined the trade
        /// </summary>
        public uint sourceID; // ObjectID

        public Trade_DeclineTrade_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sourceID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"sourceID = {sourceID}");
        }

    }

    /// <summary>
    /// Resets the trade, when clear all is clicked?
    /// </summary>
    public class Trade_ResetTrade_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Trade_ResetTrade_C2S;

        public Trade_ResetTrade_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// ResetTrade: The trade window was reset
    /// </summary>
    public class Trade_ResetTrade_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Trade_ResetTrade;

        /// <summary>
        /// Person who cleared the window
        /// </summary>
        public uint sourceID; // ObjectID

        public Trade_ResetTrade_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sourceID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"sourceID = {sourceID}");
        }

    }

    /// <summary>
    /// TradeFailure: Failure to add a trade item
    /// </summary>
    public class Trade_TradeFailure_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Trade_TradeFailure;

        /// <summary>
        /// Item that could not be added to trade window
        /// </summary>
        public uint itemID; // ObjectID

        /// <summary>
        /// The numeric reason it couldn&#39;t be traded. Client does not appear to use this.
        /// </summary>
        public uint reason; // uint

        public Trade_TradeFailure_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            itemID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"itemID = {itemID}");
            reason = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"reason = {reason}");
        }

    }

    /// <summary>
    /// ClearTradeAcceptance: Failure to complete a trade
    /// </summary>
    public class Trade_ClearTradeAcceptance_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Trade_ClearTradeAcceptance;

        public Trade_ClearTradeAcceptance_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Clears the player&#39;s corpse looting consent list, /consent clear
    /// </summary>
    public class Character_ClearPlayerConsentList_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Character_ClearPlayerConsentList;

        public Character_ClearPlayerConsentList_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Display the player&#39;s corpse looting consent list, /consent who 
    /// </summary>
    public class Character_DisplayPlayerConsentList_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Character_DisplayPlayerConsentList;

        public Character_DisplayPlayerConsentList_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Remove your corpse looting permission for the given player, /consent remove 
    /// </summary>
    public class Character_RemoveFromPlayerConsentList_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Character_RemoveFromPlayerConsentList;

        /// <summary>
        /// Name of player to remove permission to loot the playes corpses
        /// </summary>
        public string targetName; // string

        public Character_RemoveFromPlayerConsentList_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            targetName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"targetName = {targetName}");
        }

    }

    /// <summary>
    /// Grants a player corpse looting permission, /permit add
    /// </summary>
    public class Character_AddPlayerPermission_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Character_AddPlayerPermission;

        /// <summary>
        /// Name of player to grant permission to loot the playes corpses
        /// </summary>
        public string targetName; // string

        public Character_AddPlayerPermission_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            targetName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"targetName = {targetName}");
        }

    }

    /// <summary>
    /// Buy a house
    /// </summary>
    public class House_BuyHouse_C2S : IACDataType {
        public const MessageType Opcode = MessageType.House_BuyHouse;

        public uint slumlord; // ObjectID

        /// <summary>
        /// items being used to buy the house
        /// </summary>
        public PackableList<uint> items; // PackableList

        public House_BuyHouse_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            slumlord = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"slumlord = {slumlord}");
            items = new PackableList<uint>();
            items.ReadFromBuffer(buffer);
            Logger.Log($"items = {items}");
        }

    }

    /// <summary>
    /// Buy a dwelling or pay maintenance
    /// </summary>
    public class House_HouseProfile_S2C : IACDataType {
        public const MessageType Opcode = MessageType.House_HouseProfile;

        /// <summary>
        /// the object ID of the dwelling&#39;s covenant crystal
        /// </summary>
        public uint objectID; // ObjectID

        public HouseProfile houseProfile; // HouseProfile

        public House_HouseProfile_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            objectID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectID = {objectID}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            houseProfile = new HouseProfile();
            houseProfile.ReadFromBuffer(buffer);
            Logger.Log($"houseProfile = {houseProfile}");
        }

    }

    /// <summary>
    /// Query your house info, during signin
    /// </summary>
    public class House_QueryHouse_C2S : IACDataType {
        public const MessageType Opcode = MessageType.House_QueryHouse;

        public House_QueryHouse_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Abandons your house
    /// </summary>
    public class House_AbandonHouse_C2S : IACDataType {
        public const MessageType Opcode = MessageType.House_AbandonHouse;

        public House_AbandonHouse_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Revokes a player&#39;s corpse looting permission, /permit remove
    /// </summary>
    public class Character_RemovePlayerPermission_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Character_RemovePlayerPermission;

        /// <summary>
        /// Name of player to remove permission to loot the playes corpses
        /// </summary>
        public string targetName; // string

        public Character_RemovePlayerPermission_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            targetName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"targetName = {targetName}");
        }

    }

    /// <summary>
    /// Pay rent for a house
    /// </summary>
    public class House_RentHouse_C2S : IACDataType {
        public const MessageType Opcode = MessageType.House_RentHouse;

        public uint slumlord; // ObjectID

        /// <summary>
        /// items being used to pay the house rent
        /// </summary>
        public PackableList<uint> items; // PackableList

        public House_RentHouse_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            slumlord = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"slumlord = {slumlord}");
            items = new PackableList<uint>();
            items.ReadFromBuffer(buffer);
            Logger.Log($"items = {items}");
        }

    }

    /// <summary>
    /// Sets a new fill complevel for a component
    /// </summary>
    public class Character_SetDesiredComponentLevel_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Character_SetDesiredComponentLevel;

        /// <summary>
        /// class id of the component
        /// </summary>
        public uint wcid; // uint

        public uint amount; // uint

        public Character_SetDesiredComponentLevel_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            wcid = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"wcid = {wcid}");
            amount = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"amount = {amount}");
        }

    }

    /// <summary>
    /// House panel information for owners.
    /// </summary>
    public class House_HouseData_S2C : IACDataType {
        public const MessageType Opcode = MessageType.House_HouseData;

        /// <summary>
        /// the house data
        /// </summary>
        public HouseData houseData; // HouseData

        public House_HouseData_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            houseData = new HouseData();
            houseData.ReadFromBuffer(buffer);
            Logger.Log($"houseData = {houseData}");
        }

    }

    /// <summary>
    /// House Data
    /// </summary>
    public class House_HouseStatus_S2C : IACDataType {
        public const MessageType Opcode = MessageType.House_HouseStatus;

        /// <summary>
        /// Type of message to display
        /// </summary>
        public uint noticeType; // uint

        public House_HouseStatus_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            noticeType = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"noticeType = {noticeType}");
        }

    }

    /// <summary>
    /// Update Rent Time
    /// </summary>
    public class House_UpdateRentTime_S2C : IACDataType {
        public const MessageType Opcode = MessageType.House_UpdateRentTime;

        /// <summary>
        /// when the current maintenance period began (Unix timestamp)
        /// </summary>
        public uint rentTime; // uint

        public House_UpdateRentTime_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            rentTime = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"rentTime = {rentTime}");
        }

    }

    /// <summary>
    /// Update Rent Payment
    /// </summary>
    public class House_UpdateRentPayment_S2C : IACDataType {
        public const MessageType Opcode = MessageType.House_UpdateRentPayment;

        public PackableList<HousePayment> rent; // PackableList

        public House_UpdateRentPayment_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            rent = new PackableList<HousePayment>();
            rent.ReadFromBuffer(buffer);
            Logger.Log($"rent = {rent}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// Adds a guest to your house guest list
    /// </summary>
    public class House_AddPermanentGuest_C2S : IACDataType {
        public const MessageType Opcode = MessageType.House_AddPermanentGuest;

        /// <summary>
        /// Player name to add to your house guest list
        /// </summary>
        public string guestName; // string

        public House_AddPermanentGuest_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            guestName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"guestName = {guestName}");
        }

    }

    /// <summary>
    /// Removes a specific player from your house guest list, /house guest remove
    /// </summary>
    public class House_RemovePermanentGuest_C2S : IACDataType {
        public const MessageType Opcode = MessageType.House_RemovePermanentGuest;

        /// <summary>
        /// Player name to remove from your house guest list
        /// </summary>
        public string guestName; // string

        public House_RemovePermanentGuest_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            guestName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"guestName = {guestName}");
        }

    }

    /// <summary>
    /// Sets your house open status
    /// </summary>
    public class House_SetOpenHouseStatus_C2S : IACDataType {
        public const MessageType Opcode = MessageType.House_SetOpenHouseStatus;

        /// <summary>
        /// Whether the house is open or not
        /// </summary>
        public bool openHouse; // bool

        public House_SetOpenHouseStatus_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            openHouse = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"openHouse = {openHouse}");
        }

    }

    /// <summary>
    /// Update Restrictions
    /// </summary>
    public class House_UpdateRestrictions_S2C : IACDataType {
        public const MessageType Opcode = MessageType.House_UpdateRestrictions;

        /// <summary>
        /// Sequence value for restrictions list for this house
        /// </summary>
        public byte sequence; // byte

        /// <summary>
        /// Object having restrictions updated
        /// </summary>
        public uint sender; // ObjectID

        /// <summary>
        /// Restrictions database
        /// </summary>
        public RestrictionDB db; // RestrictionDB

        public House_UpdateRestrictions_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            sender = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"sender = {sender}");
            db = new RestrictionDB();
            db.ReadFromBuffer(buffer);
            Logger.Log($"db = {db}");
        }

    }

    /// <summary>
    /// Changes a specific players storage permission, /house storage add/remove
    /// </summary>
    public class House_ChangeStoragePermission_C2S : IACDataType {
        public const MessageType Opcode = MessageType.House_ChangeStoragePermission;

        /// <summary>
        /// Player name to boot from your house
        /// </summary>
        public string guestName; // string

        /// <summary>
        /// Whether the player has permission on your storage
        /// </summary>
        public bool hasPermission; // bool

        public House_ChangeStoragePermission_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            guestName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"guestName = {guestName}");
            hasPermission = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"hasPermission = {hasPermission}");
        }

    }

    /// <summary>
    /// Boots a specific player from your house /house boot
    /// </summary>
    public class House_BootSpecificHouseGuest_C2S : IACDataType {
        public const MessageType Opcode = MessageType.House_BootSpecificHouseGuest;

        /// <summary>
        /// Player name to boot from your house
        /// </summary>
        public string guestName; // string

        public House_BootSpecificHouseGuest_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            guestName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"guestName = {guestName}");
        }

    }

    /// <summary>
    /// Removes all storage permissions, /house storage remove_all
    /// </summary>
    public class House_RemoveAllStoragePermission_C2S : IACDataType {
        public const MessageType Opcode = MessageType.House_RemoveAllStoragePermission;

        public House_RemoveAllStoragePermission_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Requests your full guest list, /house guest list
    /// </summary>
    public class House_RequestFullGuestList_C2S : IACDataType {
        public const MessageType Opcode = MessageType.House_RequestFullGuestList;

        public House_RequestFullGuestList_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Sets the allegiance message of the day, /allegiance motd set
    /// </summary>
    public class Allegiance_SetMotd_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Allegiance_SetMotd;

        /// <summary>
        /// Text motd to display
        /// </summary>
        public string msg; // string

        public Allegiance_SetMotd_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            msg = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"msg = {msg}");
        }

    }

    /// <summary>
    /// Query the motd, /allegiance motd
    /// </summary>
    public class Allegiance_QueryMotd_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Allegiance_QueryMotd;

        public Allegiance_QueryMotd_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Clear the motd, /allegiance motd clear
    /// </summary>
    public class Allegiance_ClearMotd_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Allegiance_ClearMotd;

        public Allegiance_ClearMotd_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// House Guest List
    /// </summary>
    public class House_UpdateHAR_S2C : IACDataType {
        public const MessageType Opcode = MessageType.House_UpdateHAR;

        /// <summary>
        /// Set of house access records
        /// </summary>
        public HAR har; // HAR

        public House_UpdateHAR_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            har = new HAR();
            har.ReadFromBuffer(buffer);
            Logger.Log($"har = {har}");
        }

    }

    /// <summary>
    /// Gets SlumLord info, sent after getting a failed house transaction
    /// </summary>
    public class House_QueryLord_C2S : IACDataType {
        public const MessageType Opcode = MessageType.House_QueryLord;

        /// <summary>
        /// SlumLord ID to request info for
        /// </summary>
        public uint lord; // ObjectID

        public House_QueryLord_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            lord = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"lord = {lord}");
        }

    }

    /// <summary>
    /// House Profile
    /// </summary>
    public class House_HouseTransaction_S2C : IACDataType {
        public const MessageType Opcode = MessageType.House_HouseTransaction;

        /// <summary>
        /// Type of message to display
        /// </summary>
        public uint noticeType; // uint

        public House_HouseTransaction_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            noticeType = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"noticeType = {noticeType}");
        }

    }

    /// <summary>
    /// Adds all to your storage permissions, /house storage add -all
    /// </summary>
    public class House_AddAllStoragePermission_C2S : IACDataType {
        public const MessageType Opcode = MessageType.House_AddAllStoragePermission;

        public House_AddAllStoragePermission_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Removes all guests, /house guest remove_all
    /// </summary>
    public class House_RemoveAllPermanentGuests_C2S : IACDataType {
        public const MessageType Opcode = MessageType.House_RemoveAllPermanentGuests;

        public House_RemoveAllPermanentGuests_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Boot everyone from your house, /house boot -all
    /// </summary>
    public class House_BootEveryone_C2S : IACDataType {
        public const MessageType Opcode = MessageType.House_BootEveryone;

        public House_BootEveryone_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Teleports you to your house, /house recall
    /// </summary>
    public class House_TeleToHouse_C2S : IACDataType {
        public const MessageType Opcode = MessageType.House_TeleToHouse;

        public House_TeleToHouse_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Queries an item&#39;s mana
    /// </summary>
    public class Item_QueryItemMana_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Item_QueryItemMana;

        /// <summary>
        /// ID of object whos mana is being queried
        /// </summary>
        public uint target; // ObjectID

        public Item_QueryItemMana_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            target = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"target = {target}");
        }

    }

    /// <summary>
    /// Update an item&#39;s mana bar.
    /// </summary>
    public class Item_QueryItemManaResponse_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Item_QueryItemManaResponse;

        /// <summary>
        /// the object ID of the item
        /// </summary>
        public uint targetID; // ObjectID

        /// <summary>
        /// the amount of mana remaining, scaled from 0.0 (none) to 1.0 (full)
        /// </summary>
        public float mana; // float

        /// <summary>
        /// show mana bar: 0=no, 1=yes
        /// </summary>
        public bool success; // bool

        public Item_QueryItemManaResponse_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            targetID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"targetID = {targetID}");
            mana = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"mana = {mana}");
            success = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"success = {success}");
        }

    }

    /// <summary>
    /// Modify whether house hooks are visibile or not, /house hooks on/off
    /// </summary>
    public class House_SetHooksVisibility_C2S : IACDataType {
        public const MessageType Opcode = MessageType.House_SetHooksVisibility;

        /// <summary>
        /// Whether hooks are visible or not
        /// </summary>
        public bool visible; // bool

        public House_SetHooksVisibility_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            visible = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"visible = {visible}");
        }

    }

    /// <summary>
    /// Modify whether allegiance members are guests, /house guest add_allegiance/remove_allegiance
    /// </summary>
    public class House_ModifyAllegianceGuestPermission_C2S : IACDataType {
        public const MessageType Opcode = MessageType.House_ModifyAllegianceGuestPermission;

        /// <summary>
        /// Whether we are adding or removing permissions
        /// </summary>
        public bool add; // bool

        public House_ModifyAllegianceGuestPermission_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            add = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"add = {add}");
        }

    }

    /// <summary>
    /// Modify whether allegiance members can access storage, /house storage add_allegiance/remove_allegiance
    /// </summary>
    public class House_ModifyAllegianceStoragePermission_C2S : IACDataType {
        public const MessageType Opcode = MessageType.House_ModifyAllegianceStoragePermission;

        /// <summary>
        /// Whether we are adding or removing permissions
        /// </summary>
        public bool add; // bool

        public House_ModifyAllegianceStoragePermission_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            add = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"add = {add}");
        }

    }

    /// <summary>
    /// Joins a chess game
    /// </summary>
    public class Game_Join_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Game_Join;

        /// <summary>
        /// ID of the game the player is joining
        /// </summary>
        public uint gameID; // uint

        /// <summary>
        /// Which team the player is joining as
        /// </summary>
        public uint whichTeam; // uint

        public Game_Join_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            gameID = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"gameID = {gameID}");
            whichTeam = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"whichTeam = {whichTeam}");
        }

    }

    /// <summary>
    /// Quits a chess game
    /// </summary>
    public class Game_Quit_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Game_Quit;

        public Game_Quit_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Makes a chess move
    /// </summary>
    public class Game_Move_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Game_Move;

        /// <summary>
        /// Current x location of piece being moved
        /// </summary>
        public int xFrom; // int

        /// <summary>
        /// Current y location of piece being moved
        /// </summary>
        public int yFrom; // int

        /// <summary>
        /// Destination x location of piece being moved
        /// </summary>
        public int xTo; // int

        /// <summary>
        /// Destination y location of piece being moved
        /// </summary>
        public int yTo; // int

        public Game_Move_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            xFrom = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"xFrom = {xFrom}");
            yFrom = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"yFrom = {yFrom}");
            xTo = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"xTo = {xTo}");
            yTo = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"yTo = {yTo}");
        }

    }

    /// <summary>
    /// Skip a move?
    /// </summary>
    public class Game_MovePass_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Game_MovePass;

        public Game_MovePass_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Offer or confirm stalemate
    /// </summary>
    public class Game_Stalemate_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Game_Stalemate;

        /// <summary>
        /// Whether stalemate offer is active or not
        /// </summary>
        public bool on; // bool

        public Game_Stalemate_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            on = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"on = {on}");
        }

    }

    /// <summary>
    /// Lists available house /house available
    /// </summary>
    public class House_ListAvailableHouses_C2S : IACDataType {
        public const MessageType Opcode = MessageType.House_ListAvailableHouses;

        /// <summary>
        /// Type of house being listed
        /// </summary>
        public HouseType houseType; // HouseType

        public House_ListAvailableHouses_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            houseType = (HouseType)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"houseType = {houseType}");
        }

    }

    /// <summary>
    /// Display a list of available dwellings in the chat window.
    /// </summary>
    public class House_AvailableHouses_S2C : IACDataType {
        public const MessageType Opcode = MessageType.House_AvailableHouses;

        /// <summary>
        /// The type of house (1=cottage, 2=villa, 3=mansion, 4=apartment)
        /// </summary>
        public HouseType houseType; // HouseType

        /// <summary>
        /// Landcell location of the houses
        /// </summary>
        public PackableList<uint> houses; // PackableList

        /// <summary>
        /// The total number of houses of this type available
        /// </summary>
        public int numHouses; // int

        public House_AvailableHouses_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            houseType = (HouseType)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"houseType = {houseType}");
            houses = new PackableList<uint>();
            houses.ReadFromBuffer(buffer);
            Logger.Log($"houses = {houses}");
            numHouses = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"numHouses = {numHouses}");
        }

    }

    /// <summary>
    /// Display a confirmation panel.
    /// </summary>
    public class Character_ConfirmationRequest_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Character_ConfirmationRequest;

        /// <summary>
        /// the type of confirmation panel to display
        /// </summary>
        public ConfirmationType confirmationType; // ConfirmationType

        /// <summary>
        /// sequence number
        /// </summary>
        public uint contextID; // uint

        /// <summary>
        /// text to be included in the confirmation panel message
        /// </summary>
        public string text; // string

        public Character_ConfirmationRequest_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            confirmationType = (ConfirmationType)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"confirmationType = {confirmationType}");
            contextID = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"contextID = {contextID}");
            text = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"text = {text}");
        }

    }

    /// <summary>
    /// Confirms a dialog
    /// </summary>
    public class Character_ConfirmationResponse_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Character_ConfirmationResponse;

        public ConfirmationType confirmType; // ConfirmationType

        public uint context; // uint

        public bool accepted; // bool

        public Character_ConfirmationResponse_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            confirmType = (ConfirmationType)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"confirmType = {confirmType}");
            context = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"context = {context}");
            accepted = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"accepted = {accepted}");
        }

    }

    /// <summary>
    /// Confirmation done
    /// </summary>
    public class Character_ConfirmationDone_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Character_ConfirmationDone;

        /// <summary>
        /// the type of confirmation panel being closed
        /// </summary>
        public ConfirmationType confirmationType; // ConfirmationType

        /// <summary>
        /// sequence number
        /// </summary>
        public uint contextID; // uint

        public Character_ConfirmationDone_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            confirmationType = (ConfirmationType)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"confirmationType = {confirmationType}");
            contextID = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"contextID = {contextID}");
        }

    }

    /// <summary>
    /// Boots a player from the allegiance, optionally all characters on their account
    /// </summary>
    public class Allegiance_BreakAllegianceBoot_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Allegiance_BreakAllegianceBoot;

        /// <summary>
        /// Name of character to boot
        /// </summary>
        public string booteeName; // string

        /// <summary>
        /// Whether to boot all characters on their account
        /// </summary>
        public bool accountBoot; // bool

        public Allegiance_BreakAllegianceBoot_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            booteeName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"booteeName = {booteeName}");
            accountBoot = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"accountBoot = {accountBoot}");
        }

    }

    /// <summary>
    /// Teleports player to their allegiance housing, /house mansion_recall
    /// </summary>
    public class House_TeleToMansion_C2S : IACDataType {
        public const MessageType Opcode = MessageType.House_TeleToMansion;

        public House_TeleToMansion_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Player is commiting suicide
    /// </summary>
    public class Character_Suicide_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Character_Suicide;

        public Character_Suicide_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Display an allegiance login/logout message in the chat window.
    /// </summary>
    public class Allegiance_AllegianceLoginNotificationEvent_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Allegiance_AllegianceLoginNotificationEvent;

        /// <summary>
        /// the object ID of the player logging in or out
        /// </summary>
        public uint member; // ObjectID

        /// <summary>
        /// 0=logout, 1=login
        /// </summary>
        public bool nowLoggedIn; // bool

        public Allegiance_AllegianceLoginNotificationEvent_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            member = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"member = {member}");
            nowLoggedIn = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"nowLoggedIn = {nowLoggedIn}");
        }

    }

    /// <summary>
    /// Request allegiance info for a player
    /// </summary>
    public class Allegiance_AllegianceInfoRequest_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Allegiance_AllegianceInfoRequest;

        /// <summary>
        /// Name of player whom the request is targeting
        /// </summary>
        public string targetName; // string

        public Allegiance_AllegianceInfoRequest_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            targetName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"targetName = {targetName}");
        }

    }

    /// <summary>
    /// Returns data for a player&#39;s allegiance information
    /// </summary>
    public class Allegiance_AllegianceInfoResponseEvent_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Allegiance_AllegianceInfoResponseEvent;

        /// <summary>
        /// Target of the request
        /// </summary>
        public uint target; // ObjectID

        /// <summary>
        /// Allegiance Profile Data
        /// </summary>
        public AllegianceProfile prof; // AllegianceProfile

        public Allegiance_AllegianceInfoResponseEvent_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            target = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"target = {target}");
            prof = new AllegianceProfile();
            prof.ReadFromBuffer(buffer);
            Logger.Log($"prof = {prof}");
        }

    }

    /// <summary>
    /// Salvages items
    /// </summary>
    public class Inventory_CreateTinkeringTool_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Inventory_CreateTinkeringTool;

        public uint toolID; // ObjectID

        /// <summary>
        /// Set of object ID&#39;s to be salvaged
        /// </summary>
        public PackableList<uint> items; // PackableList

        public Inventory_CreateTinkeringTool_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            toolID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"toolID = {toolID}");
            items = new PackableList<uint>();
            items.ReadFromBuffer(buffer);
            Logger.Log($"items = {items}");
        }

    }

    /// <summary>
    /// Joining game response
    /// </summary>
    public class Game_JoinGameResponse_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Game_JoinGameResponse;

        /// <summary>
        /// Some kind of identifier for this game
        /// </summary>
        public uint idGame; // uint

        /// <summary>
        /// -1 indicates failure, otherwise which team you are for this game
        /// </summary>
        public int team; // int

        public Game_JoinGameResponse_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            idGame = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"idGame = {idGame}");
            team = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"team = {team}");
        }

    }

    /// <summary>
    /// Start game
    /// </summary>
    public class Game_StartGame_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Game_StartGame;

        /// <summary>
        /// Some kind of identifier for this game
        /// </summary>
        public uint idGame; // uint

        /// <summary>
        /// Which team that should go first
        /// </summary>
        public int team; // int

        public Game_StartGame_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            idGame = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"idGame = {idGame}");
            team = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"team = {team}");
        }

    }

    /// <summary>
    /// Move response
    /// </summary>
    public class Game_MoveResponse_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Game_MoveResponse;

        /// <summary>
        /// Some kind of identifier for this game
        /// </summary>
        public uint idGame; // uint

        /// <summary>
        /// If less than or equal to 0, then failure
        /// </summary>
        public ChessMoveResult moveResult; // ChessMoveResult

        public Game_MoveResponse_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            idGame = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"idGame = {idGame}");
            moveResult = (ChessMoveResult)BinaryHelpers.ReadInt32(buffer);
            Logger.Log($"moveResult = {moveResult}");
        }

    }

    /// <summary>
    /// Opponent Turn
    /// </summary>
    public class Game_OpponentTurn_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Game_OpponentTurn;

        /// <summary>
        /// Some kind of identifier for this game
        /// </summary>
        public uint idGame; // uint

        /// <summary>
        /// Team making this move
        /// </summary>
        public int team; // int

        /// <summary>
        /// Data related to the piece move
        /// </summary>
        public GameMoveData gameMoveData; // GameMoveData

        public Game_OpponentTurn_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            idGame = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"idGame = {idGame}");
            team = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"team = {team}");
            gameMoveData = new GameMoveData();
            gameMoveData.ReadFromBuffer(buffer);
            Logger.Log($"gameMoveData = {gameMoveData}");
        }

    }

    /// <summary>
    /// Opponent Stalemate State
    /// </summary>
    public class Game_OpponentStalemateState_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Game_OpponentStalemateState;

        /// <summary>
        /// Some kind of identifier for this game
        /// </summary>
        public uint idGame; // uint

        /// <summary>
        /// Team
        /// </summary>
        public int team; // int

        /// <summary>
        /// 1 = offering stalemate, 0 = retracting stalemate
        /// </summary>
        public bool on; // bool

        public Game_OpponentStalemateState_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            idGame = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"idGame = {idGame}");
            team = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"team = {team}");
            on = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"on = {on}");
        }

    }

    /// <summary>
    /// Changes the spell book filter
    /// </summary>
    public class Character_SpellbookFilterEvent_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Character_SpellbookFilterEvent;

        /// <summary>
        /// Mask containing the different filters applied to the spellbook
        /// </summary>
        public SpellBookFilterOptions options; // SpellBookFilterOptions

        public Character_SpellbookFilterEvent_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            options = (SpellBookFilterOptions)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"options = {options}");
        }

    }

    /// <summary>
    /// Display a status message in the chat window.
    /// </summary>
    public class Communication_WeenieError_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Communication_WeenieError;

        /// <summary>
        /// the type of status message to display
        /// </summary>
        public StatusMessage type; // StatusMessage

        public Communication_WeenieError_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            type = (StatusMessage)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"type = {type}");
        }

    }

    /// <summary>
    /// Display a parameterized status message in the chat window.
    /// </summary>
    public class Communication_WeenieErrorWithString_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Communication_WeenieErrorWithString;

        /// <summary>
        /// the type of status message to display
        /// </summary>
        public StatusMessage type; // StatusMessage

        /// <summary>
        /// text to be included in the status message
        /// </summary>
        public string text; // string

        public Communication_WeenieErrorWithString_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            type = (StatusMessage)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"type = {type}");
            text = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"text = {text}");
        }

    }

    /// <summary>
    /// End of Chess game
    /// </summary>
    public class Game_GameOver_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Game_GameOver;

        public uint idGame; // uint

        /// <summary>
        /// Which team was the winner for this game
        /// </summary>
        public int teamWinner; // int

        public Game_GameOver_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            idGame = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"idGame = {idGame}");
            teamWinner = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"teamWinner = {teamWinner}");
        }

    }

    /// <summary>
    /// Teleport to the marketplace
    /// </summary>
    public class Character_TeleToMarketplace_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Character_TeleToMarketplace;

        public Character_TeleToMarketplace_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Enter PKLite mode
    /// </summary>
    public class Character_EnterPKLite_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Character_EnterPKLite;

        public Character_EnterPKLite_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Fellowship Assign a new leader
    /// </summary>
    public class Fellowship_AssignNewLeader_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Fellowship_AssignNewLeader;

        /// <summary>
        /// ID of player to make the new leader of the fellowship
        /// </summary>
        public uint target; // ObjectID

        public Fellowship_AssignNewLeader_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            target = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"target = {target}");
        }

    }

    /// <summary>
    /// Fellowship Change openness
    /// </summary>
    public class Fellowship_ChangeFellowOpeness_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Fellowship_ChangeFellowOpeness;

        /// <summary>
        /// Sets whether the fellowship is open or not
        /// </summary>
        public bool open; // bool

        public Fellowship_ChangeFellowOpeness_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            open = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"open = {open}");
        }

    }

    /// <summary>
    /// Set Turbine Chat channel numbers.
    /// </summary>
    public class Communication_ChatRoomTracker_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Communication_ChatRoomTracker;

        /// <summary>
        /// the channel number of the allegiance channel
        /// </summary>
        public uint allegianceRoomID; // uint

        /// <summary>
        /// the channel number of the general channel
        /// </summary>
        public uint generalChatRoomID; // uint

        /// <summary>
        /// the channel number of the trade channel
        /// </summary>
        public uint tradeChatRoomID; // uint

        /// <summary>
        /// the channel number of the looking-for-group channel
        /// </summary>
        public uint LFGChatRoomID; // uint

        /// <summary>
        /// the channel number of the roleplay channel
        /// </summary>
        public uint roleplayChatRoomID; // uint

        /// <summary>
        /// the channel number of the olthoi channel
        /// </summary>
        public uint olthoiChatRoomID; // uint

        /// <summary>
        /// the channel number of the society channel
        /// </summary>
        public uint societyChatRoomID; // uint

        /// <summary>
        /// the channel number of the Celestial Hand channel
        /// </summary>
        public uint societyCelHanChatRoomID; // uint

        /// <summary>
        /// the channel number of the Eldrich Web channel
        /// </summary>
        public uint societyEldWebChatRoomID; // uint

        /// <summary>
        /// the channel number of the Radiant Blood channel
        /// </summary>
        public uint societyRadBloChatRoomID; // uint

        public Communication_ChatRoomTracker_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            allegianceRoomID = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"allegianceRoomID = {allegianceRoomID}");
            generalChatRoomID = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"generalChatRoomID = {generalChatRoomID}");
            tradeChatRoomID = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"tradeChatRoomID = {tradeChatRoomID}");
            LFGChatRoomID = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"LFGChatRoomID = {LFGChatRoomID}");
            roleplayChatRoomID = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"roleplayChatRoomID = {roleplayChatRoomID}");
            olthoiChatRoomID = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"olthoiChatRoomID = {olthoiChatRoomID}");
            societyChatRoomID = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"societyChatRoomID = {societyChatRoomID}");
            societyCelHanChatRoomID = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"societyCelHanChatRoomID = {societyCelHanChatRoomID}");
            societyEldWebChatRoomID = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"societyEldWebChatRoomID = {societyEldWebChatRoomID}");
            societyRadBloChatRoomID = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"societyRadBloChatRoomID = {societyRadBloChatRoomID}");
        }

    }

    /// <summary>
    /// Boots a player from the allegiance chat
    /// </summary>
    public class Allegiance_AllegianceChatBoot_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Allegiance_AllegianceChatBoot;

        /// <summary>
        /// Character name being booted
        /// </summary>
        public string charName; // string

        /// <summary>
        /// Reason for getting booted
        /// </summary>
        public string reason; // string

        public Allegiance_AllegianceChatBoot_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            charName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"charName = {charName}");
            reason = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"reason = {reason}");
        }

    }

    /// <summary>
    /// Bans a player from the allegiance
    /// </summary>
    public class Allegiance_AddAllegianceBan_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Allegiance_AddAllegianceBan;

        /// <summary>
        /// Character name being banned
        /// </summary>
        public string charName; // string

        public Allegiance_AddAllegianceBan_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            charName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"charName = {charName}");
        }

    }

    /// <summary>
    /// Removes a player ban from the allegiance
    /// </summary>
    public class Allegiance_RemoveAllegianceBan_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Allegiance_RemoveAllegianceBan;

        /// <summary>
        /// Character name being unbanned
        /// </summary>
        public string charName; // string

        public Allegiance_RemoveAllegianceBan_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            charName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"charName = {charName}");
        }

    }

    /// <summary>
    /// Display allegiance bans
    /// </summary>
    public class Allegiance_ListAllegianceBans_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Allegiance_ListAllegianceBans;

        public Allegiance_ListAllegianceBans_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Removes an allegiance officer
    /// </summary>
    public class Allegiance_RemoveAllegianceOfficer_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Allegiance_RemoveAllegianceOfficer;

        /// <summary>
        /// Character name being removed
        /// </summary>
        public string charName; // string

        public Allegiance_RemoveAllegianceOfficer_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            charName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"charName = {charName}");
        }

    }

    /// <summary>
    /// List allegiance officers
    /// </summary>
    public class Allegiance_ListAllegianceOfficers_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Allegiance_ListAllegianceOfficers;

        public Allegiance_ListAllegianceOfficers_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Clear allegiance officers
    /// </summary>
    public class Allegiance_ClearAllegianceOfficers_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Allegiance_ClearAllegianceOfficers;

        public Allegiance_ClearAllegianceOfficers_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Recalls to players allegiance hometown
    /// </summary>
    public class Allegiance_RecallAllegianceHometown_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Allegiance_RecallAllegianceHometown;

        public Allegiance_RecallAllegianceHometown_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// TODO: QueryPluginList
    /// </summary>
    public class Admin_QueryPluginList_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Admin_QueryPluginList;

        public Admin_QueryPluginList_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Admin Returns a plugin list response
    /// </summary>
    public class Admin_QueryPluginListResponse_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Admin_QueryPluginListResponse;

        public uint context; // uint

        public string pluginList; // string

        public Admin_QueryPluginListResponse_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            context = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"context = {context}");
            pluginList = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"pluginList = {pluginList}");
        }

    }

    /// <summary>
    /// TODO: QueryPlugin
    /// </summary>
    public class Admin_QueryPlugin_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Admin_QueryPlugin;

        public Admin_QueryPlugin_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Admin Returns plugin info
    /// </summary>
    public class Admin_QueryPluginResponse_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Admin_QueryPluginResponse;

        public uint context; // uint

        public bool success; // bool

        public string pluginName; // string

        public string pluginAuthor; // string

        public string pluginEmail; // string

        public string pluginWebpage; // string

        public Admin_QueryPluginResponse_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            context = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"context = {context}");
            success = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"success = {success}");
            pluginName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"pluginName = {pluginName}");
            pluginAuthor = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"pluginAuthor = {pluginAuthor}");
            pluginEmail = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"pluginEmail = {pluginEmail}");
            pluginWebpage = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"pluginWebpage = {pluginWebpage}");
        }

    }

    /// <summary>
    /// TODO: QueryPluginResponse
    /// </summary>
    public class Admin_QueryPluginResponse2_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Admin_QueryPluginResponse2;

        public Admin_QueryPluginResponse2_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Salvage operation results
    /// </summary>
    public class Inventory_SalvageOperationsResultData_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Inventory_SalvageOperationsResultData;

        public SalvageOperationsResultData operationsResultData; // SalvageOperationsResultData

        public Inventory_SalvageOperationsResultData_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            operationsResultData = new SalvageOperationsResultData();
            operationsResultData.ReadFromBuffer(buffer);
            Logger.Log($"operationsResultData = {operationsResultData}");
        }

    }

    /// <summary>
    /// Private Remove Int Event
    /// </summary>
    public class Qualities_PrivateRemoveInt64Event_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_PrivateRemoveInt64Event;

        public byte sequence; // byte

        public Int64PropertyID type; // Int64PropertyID

        public Qualities_PrivateRemoveInt64Event_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            type = (Int64PropertyID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"type = {type}");
        }

    }

    /// <summary>
    /// Private Remove Int Event
    /// </summary>
    public class Qualities_RemoveInt64Event_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_RemoveInt64Event;

        public byte sequence; // byte

        /// <summary>
        /// sender ID
        /// </summary>
        public uint sender; // ObjectID

        public Int64PropertyID type; // Int64PropertyID

        public Qualities_RemoveInt64Event_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            sender = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"sender = {sender}");
            type = (Int64PropertyID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"type = {type}");
        }

    }

    /// <summary>
    /// A message to be displayed in the chat window, spoken by a nearby player, NPC or creature
    /// </summary>
    public class Communication_HearSpeech_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Communication_HearSpeech;

        /// <summary>
        /// message text
        /// </summary>
        public string msg; // string

        /// <summary>
        /// sender name
        /// </summary>
        public string senderName; // string

        /// <summary>
        /// sender ID
        /// </summary>
        public uint senderID; // ObjectID

        /// <summary>
        /// message type
        /// </summary>
        public ChatMessageType type; // ChatMessageType

        public Communication_HearSpeech_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            msg = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"msg = {msg}");
            senderName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"senderName = {senderName}");
            senderID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"senderID = {senderID}");
            type = (ChatMessageType)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"type = {type}");
        }

    }

    /// <summary>
    /// A message to be displayed in the chat window, spoken by a nearby player, NPC or creature
    /// </summary>
    public class Communication_HearRangedSpeech_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Communication_HearRangedSpeech;

        /// <summary>
        /// message text
        /// </summary>
        public string msg; // string

        /// <summary>
        /// sender name
        /// </summary>
        public string senderName; // string

        /// <summary>
        /// sender ID, must be between 0x50000001 and 0x6FFFFFFF to appear as clickable
        /// </summary>
        public uint senderID; // ObjectID

        /// <summary>
        /// broadcast range
        /// </summary>
        public float range; // float

        /// <summary>
        /// message type
        /// </summary>
        public ChatMessageType type; // ChatMessageType

        public Communication_HearRangedSpeech_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            msg = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"msg = {msg}");
            senderName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"senderName = {senderName}");
            senderID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"senderID = {senderID}");
            range = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"range = {range}");
            type = (ChatMessageType)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"type = {type}");
        }

    }

    /// <summary>
    /// Someone has sent you a @tell.
    /// </summary>
    public class Communication_HearDirectSpeech_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Communication_HearDirectSpeech;

        /// <summary>
        /// the message text
        /// </summary>
        public string msg; // string

        /// <summary>
        /// the name of the creature sending you the message
        /// </summary>
        public string sender; // string

        /// <summary>
        /// the object ID of the creature sending you the message
        /// </summary>
        public uint senderID; // ObjectID

        /// <summary>
        /// the object ID of the recipient of the message (should be you)
        /// </summary>
        public uint targetID; // ObjectID

        /// <summary>
        /// the message type, controls color and @filter processing
        /// </summary>
        public ChatMessageType type; // ChatMessageType

        /// <summary>
        /// doesn&#39;t seem to be used by the client
        /// </summary>
        public uint secretFlags; // uint

        public Communication_HearDirectSpeech_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            msg = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"msg = {msg}");
            sender = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"sender = {sender}");
            senderID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"senderID = {senderID}");
            targetID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"targetID = {targetID}");
            type = (ChatMessageType)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"type = {type}");
            secretFlags = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"secretFlags = {secretFlags}");
        }

    }

    /// <summary>
    /// Create or join a fellowship
    /// </summary>
    public class Fellowship_FullUpdate_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Fellowship_FullUpdate;

        /// <summary>
        /// Full set of fellowship information
        /// </summary>
        public Fellowship fellowship; // Fellowship

        public Fellowship_FullUpdate_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            fellowship = new Fellowship();
            fellowship.ReadFromBuffer(buffer);
            Logger.Log($"fellowship = {fellowship}");
        }

    }

    /// <summary>
    /// Disband your fellowship.
    /// </summary>
    public class Fellowship_Disband_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Fellowship_Disband;

        public Fellowship_Disband_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Add/Update a member to your fellowship.
    /// </summary>
    public class Fellowship_UpdateFellow_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Fellowship_UpdateFellow;

        public Fellow fellow; // Fellow

        public FellowUpdateType updateType; // FellowUpdateType

        public Fellowship_UpdateFellow_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            fellow = new Fellow();
            fellow.ReadFromBuffer(buffer);
            Logger.Log($"fellow = {fellow}");
            updateType = (FellowUpdateType)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"updateType = {updateType}");
        }

    }

    /// <summary>
    /// Add a spell to your spellbook.
    /// </summary>
    public class Magic_UpdateSpell_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Magic_UpdateSpell;

        /// <summary>
        /// the spell ID of the new spell
        /// </summary>
        public LayeredSpellID spell; // LayeredSpellID

        public Magic_UpdateSpell_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            spell = new LayeredSpellID();
            spell.ReadFromBuffer(buffer);
            Logger.Log($"spell = {spell}");
        }

    }

    /// <summary>
    /// Apply an enchantment to your character.
    /// </summary>
    public class Magic_UpdateEnchantment_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Magic_UpdateEnchantment;

        public Enchantment enchantment; // Enchantment

        public Magic_UpdateEnchantment_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            enchantment = new Enchantment();
            enchantment.ReadFromBuffer(buffer);
            Logger.Log($"enchantment = {enchantment}");
        }

    }

    /// <summary>
    /// Remove an enchantment from your character.
    /// </summary>
    public class Magic_RemoveEnchantment_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Magic_RemoveEnchantment;

        /// <summary>
        /// the spell ID of the enchantment to be removed
        /// </summary>
        public LayeredSpellID spell; // LayeredSpellID

        public Magic_RemoveEnchantment_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            spell = new LayeredSpellID();
            spell.ReadFromBuffer(buffer);
            Logger.Log($"spell = {spell}");
        }

    }

    /// <summary>
    /// Update multiple enchantments from your character.
    /// </summary>
    public class Magic_UpdateMultipleEnchantments_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Magic_UpdateMultipleEnchantments;

        /// <summary>
        /// List of enchantments getting updated
        /// </summary>
        public PackableList<Enchantment> enchantments; // PackableList

        public Magic_UpdateMultipleEnchantments_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            enchantments = new PackableList<Enchantment>();
            enchantments.ReadFromBuffer(buffer);
            Logger.Log($"enchantments = {enchantments}");
        }

    }

    /// <summary>
    /// Remove multiple enchantments from your character.
    /// </summary>
    public class Magic_RemoveMultipleEnchantments_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Magic_RemoveMultipleEnchantments;

        /// <summary>
        /// List of enchantments getting removed
        /// </summary>
        public PackableList<LayeredSpellID> enchantments; // PackableList

        public Magic_RemoveMultipleEnchantments_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            enchantments = new PackableList<LayeredSpellID>();
            enchantments.ReadFromBuffer(buffer);
            Logger.Log($"enchantments = {enchantments}");
        }

    }

    /// <summary>
    /// Silently remove all enchantments from your character, e.g. when you die (no message in the chat window).
    /// </summary>
    public class Magic_PurgeEnchantments_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Magic_PurgeEnchantments;

        public Magic_PurgeEnchantments_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Silently remove An enchantment from your character.
    /// </summary>
    public class Magic_DispelEnchantment_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Magic_DispelEnchantment;

        /// <summary>
        /// the spell ID of the enchantment to be removed
        /// </summary>
        public LayeredSpellID spell; // LayeredSpellID

        public Magic_DispelEnchantment_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            spell = new LayeredSpellID();
            spell.ReadFromBuffer(buffer);
            Logger.Log($"spell = {spell}");
        }

    }

    /// <summary>
    /// Silently remove multiple enchantments from your character (no message in the chat window).
    /// </summary>
    public class Magic_DispelMultipleEnchantments_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Magic_DispelMultipleEnchantments;

        /// <summary>
        /// List of enchantments getting removed
        /// </summary>
        public PackableList<LayeredSpellID> enchantments; // PackableList

        public Magic_DispelMultipleEnchantments_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            enchantments = new PackableList<LayeredSpellID>();
            enchantments.ReadFromBuffer(buffer);
            Logger.Log($"enchantments = {enchantments}");
        }

    }

    /// <summary>
    /// A portal storm is brewing.
    /// </summary>
    public class Misc_PortalStormBrewing_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Misc_PortalStormBrewing;

        /// <summary>
        /// Less than or equal to 0.0 resets timer, otherwise sets timer
        /// </summary>
        public float extent; // float

        public Misc_PortalStormBrewing_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            extent = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"extent = {extent}");
        }

    }

    /// <summary>
    /// A portal storm is imminent.
    /// </summary>
    public class Misc_PortalStormImminent_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Misc_PortalStormImminent;

        /// <summary>
        /// Less than or equal to 0.0 resets timer, otherwise sets timer
        /// </summary>
        public float extent; // float

        public Misc_PortalStormImminent_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            extent = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"extent = {extent}");
        }

    }

    /// <summary>
    /// You have been portal stormed.
    /// </summary>
    public class Misc_PortalStorm_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Misc_PortalStorm;

        public Misc_PortalStorm_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// The portal storm has subsided.
    /// </summary>
    public class Misc_PortalStormSubsided_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Misc_PortalStormSubsided;

        public Misc_PortalStormSubsided_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Set or update a Character Int property value
    /// </summary>
    public class Qualities_PrivateUpdateInt_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_PrivateUpdateInt;

        /// <summary>
        /// sequence number
        /// </summary>
        public byte sequence; // byte

        /// <summary>
        /// Int property ID
        /// </summary>
        public IntPropertyID key; // IntPropertyID

        /// <summary>
        /// Int property value
        /// </summary>
        public int value; // int

        public Qualities_PrivateUpdateInt_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            key = (IntPropertyID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"key = {key}");
            value = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"value = {value}");
        }

    }

    /// <summary>
    /// Set or update an Object Int property value
    /// </summary>
    public class Qualities_UpdateInt_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_UpdateInt;

        /// <summary>
        /// sequence number
        /// </summary>
        public byte sequence; // byte

        /// <summary>
        /// object ID
        /// </summary>
        public uint objectId; // ObjectID

        /// <summary>
        /// Int property ID
        /// </summary>
        public IntPropertyID key; // IntPropertyID

        /// <summary>
        /// Int property value
        /// </summary>
        public int value; // int

        public Qualities_UpdateInt_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            objectId = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectId = {objectId}");
            key = (IntPropertyID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"key = {key}");
            value = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"value = {value}");
        }

    }

    /// <summary>
    /// Set or update a Character Int64 property value
    /// </summary>
    public class Qualities_PrivateUpdateInt64_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_PrivateUpdateInt64;

        /// <summary>
        /// sequence number
        /// </summary>
        public byte sequence; // byte

        /// <summary>
        /// Int64 property ID
        /// </summary>
        public Int64PropertyID key; // Int64PropertyID

        /// <summary>
        /// Int64 property value
        /// </summary>
        public long value; // long

        public Qualities_PrivateUpdateInt64_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            key = (Int64PropertyID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"key = {key}");
            value = BinaryHelpers.ReadInt64(buffer); // long
            Logger.Log($"value = {value}");
        }

    }

    /// <summary>
    /// Set or update a Character Int64 property value
    /// </summary>
    public class Qualities_UpdateInt64_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_UpdateInt64;

        /// <summary>
        /// sequence number
        /// </summary>
        public byte sequence; // byte

        /// <summary>
        /// object ID
        /// </summary>
        public uint objectId; // ObjectID

        /// <summary>
        /// Int64 property ID
        /// </summary>
        public Int64PropertyID key; // Int64PropertyID

        /// <summary>
        /// Int64 property value
        /// </summary>
        public long value; // long

        public Qualities_UpdateInt64_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            objectId = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectId = {objectId}");
            key = (Int64PropertyID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"key = {key}");
            value = BinaryHelpers.ReadInt64(buffer); // long
            Logger.Log($"value = {value}");
        }

    }

    /// <summary>
    /// Set or update a Character Boolean property value
    /// </summary>
    public class Qualities_PrivateUpdateBool_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_PrivateUpdateBool;

        /// <summary>
        /// sequence number
        /// </summary>
        public byte sequence; // byte

        /// <summary>
        /// Boolean property ID
        /// </summary>
        public BooleanPropertyID key; // BooleanPropertyID

        /// <summary>
        /// Boolean property value (0=False, 1=True)
        /// </summary>
        public bool value; // bool

        public Qualities_PrivateUpdateBool_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            key = (BooleanPropertyID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"key = {key}");
            value = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"value = {value}");
        }

    }

    /// <summary>
    /// Set or update an Object Boolean property value
    /// </summary>
    public class Qualities_UpdateBool_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_UpdateBool;

        /// <summary>
        /// sequence number
        /// </summary>
        public byte sequence; // byte

        /// <summary>
        /// object ID
        /// </summary>
        public uint objectId; // ObjectID

        /// <summary>
        /// Boolean property ID
        /// </summary>
        public BooleanPropertyID key; // BooleanPropertyID

        /// <summary>
        /// Boolean property value (0=False, 1=True)
        /// </summary>
        public bool value; // bool

        public Qualities_UpdateBool_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            objectId = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectId = {objectId}");
            key = (BooleanPropertyID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"key = {key}");
            value = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"value = {value}");
        }

    }

    /// <summary>
    /// Set or update an Object float property value
    /// </summary>
    public class Qualities_PrivateUpdateFloat_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_PrivateUpdateFloat;

        /// <summary>
        /// sequence number
        /// </summary>
        public byte sequence; // byte

        /// <summary>
        /// Float property ID
        /// </summary>
        public FloatPropertyID key; // FloatPropertyID

        /// <summary>
        /// Float property value
        /// </summary>
        public float value; // float

        public Qualities_PrivateUpdateFloat_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            key = (FloatPropertyID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"key = {key}");
            value = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"value = {value}");
        }

    }

    /// <summary>
    /// Set or update an Object float property value
    /// </summary>
    public class Qualities_UpdateFloat_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_UpdateFloat;

        /// <summary>
        /// sequence number
        /// </summary>
        public byte sequence; // byte

        /// <summary>
        /// object ID
        /// </summary>
        public uint objectId; // ObjectID

        /// <summary>
        /// Float property ID
        /// </summary>
        public FloatPropertyID key; // FloatPropertyID

        /// <summary>
        /// Float property value
        /// </summary>
        public float value; // float

        public Qualities_UpdateFloat_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            objectId = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectId = {objectId}");
            key = (FloatPropertyID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"key = {key}");
            value = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"value = {value}");
        }

    }

    /// <summary>
    /// Set or update an Object String property value
    /// </summary>
    public class Qualities_PrivateUpdateString_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_PrivateUpdateString;

        /// <summary>
        /// sequence number
        /// </summary>
        public byte sequence; // byte

        /// <summary>
        /// String property ID
        /// </summary>
        public StringPropertyID key; // StringPropertyID

        /// <summary>
        /// String property value
        /// </summary>
        public string value; // string

        public Qualities_PrivateUpdateString_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            key = (StringPropertyID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"key = {key}");
            if ((buffer.BaseStream.Position % 4) != 0) { // align DWORD
                buffer.BaseStream.Position += 4 - (buffer.BaseStream.Position % 4);
            }
            value = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"value = {value}");
        }

    }

    /// <summary>
    /// Set or update an Object String property value
    /// </summary>
    public class Qualities_UpdateString_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_UpdateString;

        /// <summary>
        /// sequence number
        /// </summary>
        public byte sequence; // byte

        /// <summary>
        /// object ID
        /// </summary>
        public uint objectId; // ObjectID

        /// <summary>
        /// String property ID
        /// </summary>
        public StringPropertyID key; // StringPropertyID

        /// <summary>
        /// String property value
        /// </summary>
        public string value; // string

        public Qualities_UpdateString_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            objectId = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectId = {objectId}");
            key = (StringPropertyID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"key = {key}");
            if ((buffer.BaseStream.Position % 4) != 0) { // align DWORD
                buffer.BaseStream.Position += 4 - (buffer.BaseStream.Position % 4);
            }
            value = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"value = {value}");
        }

    }

    /// <summary>
    /// Set or update an Object DID property value
    /// </summary>
    public class Qualities_PrivateUpdateDataID_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_PrivateUpdateDataID;

        /// <summary>
        /// sequence number
        /// </summary>
        public byte sequence; // byte

        /// <summary>
        /// Data property ID
        /// </summary>
        public DataPropertyID key; // DataPropertyID

        /// <summary>
        /// Resource property value
        /// </summary>
        public uint value; // uint

        public Qualities_PrivateUpdateDataID_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            key = (DataPropertyID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"key = {key}");
            value = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"value = {value}");
        }

    }

    /// <summary>
    /// Set or update an Object DID property value
    /// </summary>
    public class Qualities_UpdateDataID_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_UpdateDataID;

        /// <summary>
        /// sequence number
        /// </summary>
        public byte sequence; // byte

        /// <summary>
        /// object ID
        /// </summary>
        public uint objectId; // ObjectID

        /// <summary>
        /// Data property ID
        /// </summary>
        public DataPropertyID key; // DataPropertyID

        /// <summary>
        /// Resource property value
        /// </summary>
        public uint value; // uint

        public Qualities_UpdateDataID_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            objectId = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectId = {objectId}");
            key = (DataPropertyID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"key = {key}");
            value = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"value = {value}");
        }

    }

    /// <summary>
    /// Set or update a IID property value
    /// </summary>
    public class Qualities_PrivateUpdateInstanceID_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_PrivateUpdateInstanceID;

        /// <summary>
        /// sequence number
        /// </summary>
        public byte sequence; // byte

        /// <summary>
        /// Instance property ID
        /// </summary>
        public InstancePropertyID key; // InstancePropertyID

        /// <summary>
        /// Link property value
        /// </summary>
        public uint value; // ObjectID

        public Qualities_PrivateUpdateInstanceID_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            key = (InstancePropertyID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"key = {key}");
            value = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"value = {value}");
        }

    }

    /// <summary>
    /// Set or update an Object IID property value
    /// </summary>
    public class Qualities_UpdateInstanceID_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_UpdateInstanceID;

        /// <summary>
        /// sequence number
        /// </summary>
        public byte sequence; // byte

        /// <summary>
        /// object ID
        /// </summary>
        public uint objectId; // ObjectID

        /// <summary>
        /// Instance property ID
        /// </summary>
        public InstancePropertyID key; // InstancePropertyID

        /// <summary>
        /// Link property value
        /// </summary>
        public uint value; // ObjectID

        public Qualities_UpdateInstanceID_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            objectId = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectId = {objectId}");
            key = (InstancePropertyID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"key = {key}");
            value = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"value = {value}");
        }

    }

    /// <summary>
    /// Set or update a Character Position property value
    /// </summary>
    public class Qualities_PrivateUpdatePosition_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_PrivateUpdatePosition;

        /// <summary>
        /// sequence number
        /// </summary>
        public byte sequence; // byte

        /// <summary>
        /// Position property ID
        /// </summary>
        public PositionPropertyID key; // PositionPropertyID

        /// <summary>
        /// Position property value
        /// </summary>
        public Position value; // Position

        public Qualities_PrivateUpdatePosition_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            key = (PositionPropertyID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"key = {key}");
            value = new Position();
            value.ReadFromBuffer(buffer);
            Logger.Log($"value = {value}");
        }

    }

    /// <summary>
    /// Set or update a Character Position property value
    /// </summary>
    public class Qualities_UpdatePosition_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_UpdatePosition;

        /// <summary>
        /// sequence number
        /// </summary>
        public byte sequence; // byte

        /// <summary>
        /// object ID
        /// </summary>
        public uint objectId; // ObjectID

        /// <summary>
        /// Position property ID
        /// </summary>
        public PositionPropertyID key; // PositionPropertyID

        /// <summary>
        /// Position property value
        /// </summary>
        public Position value; // Position

        public Qualities_UpdatePosition_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            objectId = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectId = {objectId}");
            key = (PositionPropertyID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"key = {key}");
            value = new Position();
            value.ReadFromBuffer(buffer);
            Logger.Log($"value = {value}");
        }

    }

    /// <summary>
    /// Set or update a Character Skill value
    /// </summary>
    public class Qualities_PrivateUpdateSkill_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_PrivateUpdateSkill;

        /// <summary>
        /// sequence number
        /// </summary>
        public byte sequence; // byte

        /// <summary>
        /// skill ID
        /// </summary>
        public SkillID key; // SkillID

        /// <summary>
        /// skill information
        /// </summary>
        public Skill value; // Skill

        public Qualities_PrivateUpdateSkill_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            key = (SkillID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"key = {key}");
            value = new Skill();
            value.ReadFromBuffer(buffer);
            Logger.Log($"value = {value}");
        }

    }

    /// <summary>
    /// Set or update a Character Skill value
    /// </summary>
    public class Qualities_UpdateSkill_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_UpdateSkill;

        /// <summary>
        /// sequence number
        /// </summary>
        public byte sequence; // byte

        /// <summary>
        /// object ID
        /// </summary>
        public uint objectId; // ObjectID

        /// <summary>
        /// skill ID
        /// </summary>
        public SkillID key; // SkillID

        /// <summary>
        /// skill information
        /// </summary>
        public Skill value; // Skill

        public Qualities_UpdateSkill_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            objectId = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectId = {objectId}");
            key = (SkillID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"key = {key}");
            value = new Skill();
            value.ReadFromBuffer(buffer);
            Logger.Log($"value = {value}");
        }

    }

    /// <summary>
    /// Set or update a Character Skill Level
    /// </summary>
    public class Qualities_PrivateUpdateSkillLevel_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_PrivateUpdateSkillLevel;

        /// <summary>
        /// sequence number
        /// </summary>
        public byte sequence; // byte

        /// <summary>
        /// skill ID
        /// </summary>
        public SkillID key; // SkillID

        /// <summary>
        /// skill level value
        /// </summary>
        public uint value; // uint

        public Qualities_PrivateUpdateSkillLevel_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            key = (SkillID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"key = {key}");
            value = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"value = {value}");
        }

    }

    /// <summary>
    /// Set or update a Character Skill Level
    /// </summary>
    public class Qualities_UpdateSkillLevel_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_UpdateSkillLevel;

        /// <summary>
        /// sequence number
        /// </summary>
        public byte sequence; // byte

        /// <summary>
        /// object ID
        /// </summary>
        public uint objectId; // ObjectID

        /// <summary>
        /// skill ID
        /// </summary>
        public SkillID key; // SkillID

        /// <summary>
        /// skill level value
        /// </summary>
        public uint value; // uint

        public Qualities_UpdateSkillLevel_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            objectId = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectId = {objectId}");
            key = (SkillID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"key = {key}");
            value = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"value = {value}");
        }

    }

    /// <summary>
    /// Set or update a Character Skill state
    /// </summary>
    public class Qualities_PrivateUpdateSkillAC_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_PrivateUpdateSkillAC;

        /// <summary>
        /// sequence number
        /// </summary>
        public byte sequence; // byte

        /// <summary>
        /// skill ID
        /// </summary>
        public SkillID key; // SkillID

        /// <summary>
        /// skill state
        /// </summary>
        public SkillState value; // SkillState

        public Qualities_PrivateUpdateSkillAC_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            key = (SkillID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"key = {key}");
            value = (SkillState)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"value = {value}");
        }

    }

    /// <summary>
    /// Set or update a Character Skill state
    /// </summary>
    public class Qualities_UpdateSkillAC_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_UpdateSkillAC;

        /// <summary>
        /// sequence number
        /// </summary>
        public byte sequence; // byte

        /// <summary>
        /// object ID
        /// </summary>
        public uint objectId; // ObjectID

        /// <summary>
        /// skill ID
        /// </summary>
        public SkillID key; // SkillID

        /// <summary>
        /// skill state
        /// </summary>
        public SkillState value; // SkillState

        public Qualities_UpdateSkillAC_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            objectId = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectId = {objectId}");
            key = (SkillID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"key = {key}");
            value = (SkillState)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"value = {value}");
        }

    }

    /// <summary>
    /// Set or update a Character Attribute value
    /// </summary>
    public class Qualities_PrivateUpdateAttribute_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_PrivateUpdateAttribute;

        /// <summary>
        /// sequence number
        /// </summary>
        public byte sequence; // byte

        /// <summary>
        /// attribute ID
        /// </summary>
        public AttributeID key; // AttributeID

        /// <summary>
        /// attribute information
        /// </summary>
        public Attribute value; // Attribute

        public Qualities_PrivateUpdateAttribute_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            key = (AttributeID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"key = {key}");
            value = new Attribute();
            value.ReadFromBuffer(buffer);
            Logger.Log($"value = {value}");
        }

    }

    /// <summary>
    /// Set or update a Character Attribute value
    /// </summary>
    public class Qualities_UpdateAttribute_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_UpdateAttribute;

        /// <summary>
        /// sequence number
        /// </summary>
        public byte sequence; // byte

        /// <summary>
        /// object ID
        /// </summary>
        public uint objectId; // ObjectID

        /// <summary>
        /// attribute ID
        /// </summary>
        public AttributeID key; // AttributeID

        /// <summary>
        /// attribute information
        /// </summary>
        public Attribute value; // Attribute

        public Qualities_UpdateAttribute_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            objectId = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectId = {objectId}");
            key = (AttributeID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"key = {key}");
            value = new Attribute();
            value.ReadFromBuffer(buffer);
            Logger.Log($"value = {value}");
        }

    }

    /// <summary>
    /// Set or update a Character Attribute Level
    /// </summary>
    public class Qualities_PrivateUpdateAttributeLevel_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_PrivateUpdateAttributeLevel;

        /// <summary>
        /// sequence number
        /// </summary>
        public byte sequence; // byte

        /// <summary>
        /// attribute ID
        /// </summary>
        public AttributeID key; // AttributeID

        /// <summary>
        /// current value
        /// </summary>
        public uint value; // uint

        public Qualities_PrivateUpdateAttributeLevel_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            key = (AttributeID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"key = {key}");
            value = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"value = {value}");
        }

    }

    /// <summary>
    /// Set or update a Character Attribute Level
    /// </summary>
    public class Qualities_UpdateAttributeLevel_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_UpdateAttributeLevel;

        /// <summary>
        /// sequence number
        /// </summary>
        public byte sequence; // byte

        /// <summary>
        /// object ID
        /// </summary>
        public uint objectId; // ObjectID

        /// <summary>
        /// attribute ID
        /// </summary>
        public AttributeID key; // AttributeID

        /// <summary>
        /// current value
        /// </summary>
        public uint value; // uint

        public Qualities_UpdateAttributeLevel_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            objectId = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectId = {objectId}");
            key = (AttributeID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"key = {key}");
            value = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"value = {value}");
        }

    }

    /// <summary>
    /// Set or update a Character Vital value
    /// </summary>
    public class Qualities_PrivateUpdateAttribute2nd_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_PrivateUpdateAttribute2nd;

        /// <summary>
        /// sequence number
        /// </summary>
        public byte sequence; // byte

        /// <summary>
        /// vital ID
        /// </summary>
        public VitalID key; // VitalID

        /// <summary>
        /// vital information
        /// </summary>
        public SecondaryAttribute value; // SecondaryAttribute

        public Qualities_PrivateUpdateAttribute2nd_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            key = (VitalID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"key = {key}");
            value = new SecondaryAttribute();
            value.ReadFromBuffer(buffer);
            Logger.Log($"value = {value}");
        }

    }

    /// <summary>
    /// Set or update a Character Vital value
    /// </summary>
    public class Qualities_UpdateAttribute2nd_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_UpdateAttribute2nd;

        /// <summary>
        /// sequence number
        /// </summary>
        public byte sequence; // byte

        /// <summary>
        /// object ID
        /// </summary>
        public uint objectId; // ObjectID

        /// <summary>
        /// vital ID
        /// </summary>
        public VitalID key; // VitalID

        /// <summary>
        /// vital information
        /// </summary>
        public SecondaryAttribute value; // SecondaryAttribute

        public Qualities_UpdateAttribute2nd_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            objectId = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectId = {objectId}");
            key = (VitalID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"key = {key}");
            value = new SecondaryAttribute();
            value.ReadFromBuffer(buffer);
            Logger.Log($"value = {value}");
        }

    }

    /// <summary>
    /// Set or update a Character Vital value
    /// </summary>
    public class Qualities_PrivateUpdateAttribute2ndLevel_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_PrivateUpdateAttribute2ndLevel;

        /// <summary>
        /// sequence number
        /// </summary>
        public byte sequence; // byte

        /// <summary>
        /// vital ID
        /// </summary>
        public CurVitalID key; // CurVitalID

        /// <summary>
        /// current value
        /// </summary>
        public uint value; // uint

        public Qualities_PrivateUpdateAttribute2ndLevel_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            key = (CurVitalID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"key = {key}");
            value = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"value = {value}");
        }

    }

    /// <summary>
    /// Set or update a Character Vital value
    /// </summary>
    public class Qualities_UpdateAttribute2ndLevel_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Qualities_UpdateAttribute2ndLevel;

        /// <summary>
        /// sequence number
        /// </summary>
        public byte sequence; // byte

        /// <summary>
        /// object ID
        /// </summary>
        public uint objectId; // ObjectID

        /// <summary>
        /// vital ID
        /// </summary>
        public CurVitalID key; // CurVitalID

        /// <summary>
        /// current value
        /// </summary>
        public uint value; // uint

        public Qualities_UpdateAttribute2ndLevel_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"sequence = {sequence}");
            objectId = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectId = {objectId}");
            key = (CurVitalID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"key = {key}");
            value = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"value = {value}");
        }

    }

    /// <summary>
    /// Display a status message on the Action Viewscreen (the red text overlaid on the 3D area).
    /// </summary>
    public class Communication_TransientString_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Communication_TransientString;

        /// <summary>
        /// the message text
        /// </summary>
        public string text; // string

        public Communication_TransientString_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            text = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"text = {text}");
        }

    }

    /// <summary>
    /// Completes the barber interaction
    /// </summary>
    public class Character_FinishBarber_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Character_FinishBarber;

        public DataID basePalette; // DataID

        public DataID headObject; // DataID

        public DataID headTexture; // DataID

        public DataID defaultHeadTexture; // DataID

        public DataID eyesTexture; // DataID

        public DataID defaultEyesTexture; // DataID

        public DataID noseTexture; // DataID

        public DataID defaultNoseTexture; // DataID

        public DataID mouthTexture; // DataID

        public DataID defaultMouthTexture; // DataID

        public DataID skinPalette; // DataID

        public DataID hairPalette; // DataID

        public DataID eyesPalette; // DataID

        public DataID setupID; // DataID

        /// <summary>
        /// Specifies the toggle option for some races, such as floating empyrean or flaming head on undead
        /// </summary>
        public int option1; // int

        /// <summary>
        /// Seems to be unused
        /// </summary>
        public int option2; // int

        public Character_FinishBarber_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            basePalette = new DataID();
            basePalette.ReadFromBuffer(buffer);
            Logger.Log($"basePalette = {basePalette}");
            headObject = new DataID();
            headObject.ReadFromBuffer(buffer);
            Logger.Log($"headObject = {headObject}");
            headTexture = new DataID();
            headTexture.ReadFromBuffer(buffer);
            Logger.Log($"headTexture = {headTexture}");
            defaultHeadTexture = new DataID();
            defaultHeadTexture.ReadFromBuffer(buffer);
            Logger.Log($"defaultHeadTexture = {defaultHeadTexture}");
            eyesTexture = new DataID();
            eyesTexture.ReadFromBuffer(buffer);
            Logger.Log($"eyesTexture = {eyesTexture}");
            defaultEyesTexture = new DataID();
            defaultEyesTexture.ReadFromBuffer(buffer);
            Logger.Log($"defaultEyesTexture = {defaultEyesTexture}");
            noseTexture = new DataID();
            noseTexture.ReadFromBuffer(buffer);
            Logger.Log($"noseTexture = {noseTexture}");
            defaultNoseTexture = new DataID();
            defaultNoseTexture.ReadFromBuffer(buffer);
            Logger.Log($"defaultNoseTexture = {defaultNoseTexture}");
            mouthTexture = new DataID();
            mouthTexture.ReadFromBuffer(buffer);
            Logger.Log($"mouthTexture = {mouthTexture}");
            defaultMouthTexture = new DataID();
            defaultMouthTexture.ReadFromBuffer(buffer);
            Logger.Log($"defaultMouthTexture = {defaultMouthTexture}");
            skinPalette = new DataID();
            skinPalette.ReadFromBuffer(buffer);
            Logger.Log($"skinPalette = {skinPalette}");
            hairPalette = new DataID();
            hairPalette.ReadFromBuffer(buffer);
            Logger.Log($"hairPalette = {hairPalette}");
            eyesPalette = new DataID();
            eyesPalette.ReadFromBuffer(buffer);
            Logger.Log($"eyesPalette = {eyesPalette}");
            setupID = new DataID();
            setupID.ReadFromBuffer(buffer);
            Logger.Log($"setupID = {setupID}");
            option1 = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"option1 = {option1}");
            option2 = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"option2 = {option2}");
        }

    }

    /// <summary>
    /// Remove all bad enchantments from your character.
    /// </summary>
    public class Magic_PurgeBadEnchantments_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Magic_PurgeBadEnchantments;

        public Magic_PurgeBadEnchantments_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Sends all contract data
    /// </summary>
    public class Social_SendClientContractTrackerTable_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Social_SendClientContractTrackerTable;

        public ContractTrackerTable contractTrackerTable; // ContractTrackerTable

        public Social_SendClientContractTrackerTable_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            contractTrackerTable = new ContractTrackerTable();
            contractTrackerTable.ReadFromBuffer(buffer);
            Logger.Log($"contractTrackerTable = {contractTrackerTable}");
        }

    }

    /// <summary>
    /// Updates a contract data
    /// </summary>
    public class Social_SendClientContractTracker_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Social_SendClientContractTracker;

        public ContractTracker contractTracker; // ContractTracker

        public bool deleteContract; // bool

        public bool setAsDisplayContract; // bool

        public Social_SendClientContractTracker_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            contractTracker = new ContractTracker();
            contractTracker.ReadFromBuffer(buffer);
            Logger.Log($"contractTracker = {contractTracker}");
            deleteContract = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"deleteContract = {deleteContract}");
            setAsDisplayContract = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"setAsDisplayContract = {setAsDisplayContract}");
        }

    }

    /// <summary>
    /// Abandons a contract
    /// </summary>
    public class Social_AbandonContract_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Social_AbandonContract;

        /// <summary>
        /// ID of contact being abandoned
        /// </summary>
        public uint contractID; // uint

        public Social_AbandonContract_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            contractID = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"contractID = {contractID}");
        }

    }

    /// <summary>
    /// This appears to be an admin command to change the environment (light, fog, sounds, colors)
    /// </summary>
    public class Admin_Environs_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Admin_Environs;

        /// <summary>
        /// ID of option set to change the environs
        /// </summary>
        public EnvrionChangeType envrionsOptions; // EnvrionChangeType

        public Admin_Environs_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            envrionsOptions = (EnvrionChangeType)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"envrionsOptions = {envrionsOptions}");
        }

    }

    /// <summary>
    /// Sets both the position and movement, such as when materializing at a lifestone
    /// </summary>
    public class Movement_PositionAndMovementEvent_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Movement_PositionAndMovementEvent;

        /// <summary>
        /// ObjectID of the character doing the animation
        /// </summary>
        public uint objectID; // ObjectID

        public PositionPack position; // PositionPack

        /// <summary>
        /// Set of movement data
        /// </summary>
        public MovementData movementData; // MovementData

        public Movement_PositionAndMovementEvent_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            objectID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectID = {objectID}");
            position = new PositionPack();
            position.ReadFromBuffer(buffer);
            Logger.Log($"position = {position}");
            movementData = new MovementData();
            movementData.ReadFromBuffer(buffer);
            Logger.Log($"movementData = {movementData}");
        }

    }

    /// <summary>
    /// Performs a jump
    /// </summary>
    public class Movement_Jump_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Movement_Jump;

        /// <summary>
        /// set of jumping data
        /// </summary>
        public JumpPack jp; // JumpPack

        public Movement_Jump_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            jp = new JumpPack();
            jp.ReadFromBuffer(buffer);
            Logger.Log($"jp = {jp}");
        }

    }

    /// <summary>
    /// Move to state data
    /// </summary>
    public class Movement_MoveToState_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Movement_MoveToState;

        /// <summary>
        /// set of move to data, currently not in client, may not be used?
        /// </summary>
        public MoveToStatePack mtsp; // MoveToStatePack

        public Movement_MoveToState_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            mtsp = new MoveToStatePack();
            mtsp.ReadFromBuffer(buffer);
            Logger.Log($"mtsp = {mtsp}");
        }

    }

    /// <summary>
    /// Performs a movement based on input
    /// </summary>
    public class Movement_DoMovementCommand_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Movement_DoMovementCommand;

        /// <summary>
        /// motion command
        /// </summary>
        public uint motion; // uint

        /// <summary>
        /// speed of movement
        /// </summary>
        public float speed; // float

        /// <summary>
        /// run key being held
        /// </summary>
        public uint holdKey; // uint

        public Movement_DoMovementCommand_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            motion = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"motion = {motion}");
            speed = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"speed = {speed}");
            holdKey = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"holdKey = {holdKey}");
        }

    }

    /// <summary>
    /// Sent whenever a character changes their clothes. It contains the entire description of what their wearing (and possibly their facial features as well). This message is only sent for changes, when the character is first created, the body of this message is included inside the creation message.
    /// </summary>
    public class Item_ObjDescEvent_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Item_ObjDescEvent;

        /// <summary>
        /// The ID of character whose visual description is changing.
        /// </summary>
        public uint objectID; // ObjectID

        /// <summary>
        /// Set of visual description information for the object
        /// </summary>
        public ObjDesc objDesc; // ObjDesc

        /// <summary>
        /// The instance sequence value for the object (number of logins for players)
        /// </summary>
        public ushort objectInstanceSequence; // ushort

        /// <summary>
        /// The position sequence value for the object
        /// </summary>
        public ushort objectVisualDescSequence; // ushort

        public Item_ObjDescEvent_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            objectID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectID = {objectID}");
            objDesc = new ObjDesc();
            objDesc.ReadFromBuffer(buffer);
            Logger.Log($"objDesc = {objDesc}");
            objectInstanceSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectInstanceSequence = {objectInstanceSequence}");
            objectVisualDescSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectVisualDescSequence = {objectVisualDescSequence}");
        }

    }

    /// <summary>
    /// Sets the player visual desc, TODO confirm this
    /// </summary>
    public class Character_SetPlayerVisualDesc_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Character_SetPlayerVisualDesc;

        /// <summary>
        /// Set of visual description information for the player
        /// </summary>
        public ObjDesc objDesc; // ObjDesc

        public Character_SetPlayerVisualDesc_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            objDesc = new ObjDesc();
            objDesc.ReadFromBuffer(buffer);
            Logger.Log($"objDesc = {objDesc}");
        }

    }

    /// <summary>
    /// Character creation screen initilised.
    /// </summary>
    public class Character_CharGenVerificationResponse_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Character_CharGenVerificationResponse;

        /// <summary>
        /// Type of response
        /// </summary>
        public CharGenResponseType responseType; // CharGenResponseType

        /// <summary>
        /// The character ID for this entry.
        /// </summary>
        public uint gid; // ObjectID

        /// <summary>
        /// The name of this character.
        /// </summary>
        public string name; // string

        /// <summary>
        /// When 0, this character is not being deleted (not shown crossed out). Otherwise, it&#39;s a countdown timer in the number of seconds until the character is submitted for deletion.
        /// </summary>
        public uint secondsGreyedOut; // uint

        public Character_CharGenVerificationResponse_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            responseType = (CharGenResponseType)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"responseType = {responseType}");
            switch((int)responseType) {
                case 0x1:
                    gid = BinaryHelpers.ReadUInt32(buffer); // ObjectID
                    Logger.Log($"gid = {gid}");
                    name = BinaryHelpers.ReadString(buffer); // string
                    Logger.Log($"name = {name}");
                    secondsGreyedOut = BinaryHelpers.ReadUInt32(buffer); // uint
                    Logger.Log($"secondsGreyedOut = {secondsGreyedOut}");
                    if ((buffer.BaseStream.Position % 4) != 0) { // align DWORD
                        buffer.BaseStream.Position += 4 - (buffer.BaseStream.Position % 4);
                    }
                    break;
            }
        }

    }

    /// <summary>
    /// Sent when your subsciption is about to expire
    /// </summary>
    public class Login_AwaitingSubscriptionExpiration_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Login_AwaitingSubscriptionExpiration;

        /// <summary>
        /// Time remaining before your subscription expires.
        /// </summary>
        public uint secondsRemaining; // uint

        public Login_AwaitingSubscriptionExpiration_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            secondsRemaining = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"secondsRemaining = {secondsRemaining}");
        }

    }

    /// <summary>
    /// Instructs the client to return to 2D mode - the character list.
    /// </summary>
    public class Login_LogOffCharacter_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Login_LogOffCharacter;

        public Login_LogOffCharacter_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Instructs the client to return to 2D mode - the character list.
    /// </summary>
    public class Login_LogOffCharacter_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Login_LogOffCharacter;

        public Login_LogOffCharacter_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Mark a character for deletetion.
    /// </summary>
    public class Character_CharacterDelete_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Character_CharacterDelete;

        /// <summary>
        /// The account for the character
        /// </summary>
        public string account; // string

        /// <summary>
        /// Slot to delete
        /// </summary>
        public int slot; // int

        public Character_CharacterDelete_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            account = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"account = {account}");
            slot = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"slot = {slot}");
        }

    }

    /// <summary>
    /// A character was marked for deletetion.
    /// </summary>
    public class Character_CharacterDelete_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Character_CharacterDelete;

        public Character_CharacterDelete_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Character creation result
    /// </summary>
    public class Character_SendCharGenResult_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Character_SendCharGenResult;

        /// <summary>
        /// The account for the character
        /// </summary>
        public string account; // string

        /// <summary>
        /// The data for the character generation
        /// </summary>
        public CharGenResult charGenResult; // CharGenResult

        public Character_SendCharGenResult_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            account = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"account = {account}");
            charGenResult = new CharGenResult();
            charGenResult.ReadFromBuffer(buffer);
            Logger.Log($"charGenResult = {charGenResult}");
        }

    }

    /// <summary>
    /// The character to log in.
    /// </summary>
    public class Login_SendEnterWorld_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Login_SendEnterWorld;

        /// <summary>
        /// The character ID of the character to log in
        /// </summary>
        public uint gid; // ObjectID

        /// <summary>
        /// The account name associated with the character
        /// </summary>
        public string account; // string

        public Login_SendEnterWorld_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            gid = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"gid = {gid}");
            account = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"account = {account}");
        }

    }

    /// <summary>
    /// The list of characters on the current account.
    /// </summary>
    public class Login_LoginCharacterSet_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Login_LoginCharacterSet;

        public uint status; // uint

        /// <summary>
        /// The number of characters in the list.  Characters appear in the list ordered most-recently-used first, but are displayed alphabetically.
        /// </summary>
        public uint characterCount; // uint

        public List<CharacterIdentity> set { get; set; } = new List<CharacterIdentity>(); // test

        /// <summary>
        /// This is a second list which goes into delSet, not sure what it is, deleted characters?
        /// </summary>
        public uint characterCount2; // uint

        public List<CharacterIdentity> delset { get; set; } = new List<CharacterIdentity>(); // test

        /// <summary>
        /// The total count of character slots.
        /// </summary>
        public uint numAllowedCharacters; // uint

        /// <summary>
        /// The name for this account.
        /// </summary>
        public string account; // string

        /// <summary>
        /// Whether or not Turbine Chat (Allegiance chat) enabled.
        /// </summary>
        public bool useTurbineChat; // bool

        /// <summary>
        /// Whether or not this account has purchansed ToD
        /// </summary>
        public bool hasThroneofDestiny; // bool

        public Login_LoginCharacterSet_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            status = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"status = {status}");
            characterCount = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"characterCount = {characterCount}");
            for (var i=0; i < characterCount; i++) {
                var id = new CharacterIdentity();
                id.ReadFromBuffer(buffer);
                Logger.Log($"id = {id}");
                set.Add(id);
            }
            characterCount2 = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"characterCount2 = {characterCount2}");
            for (var i=0; i < characterCount2; i++) {
                var id = new CharacterIdentity();
                id.ReadFromBuffer(buffer);
                Logger.Log($"id = {id}");
                delset.Add(id);
            }
            numAllowedCharacters = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"numAllowedCharacters = {numAllowedCharacters}");
            account = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"account = {account}");
            useTurbineChat = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"useTurbineChat = {useTurbineChat}");
            hasThroneofDestiny = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"hasThroneofDestiny = {hasThroneofDestiny}");
        }

    }

    /// <summary>
    /// Failure to log in
    /// </summary>
    public class Character_CharacterError_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Character_CharacterError;

        /// <summary>
        /// Identifies type of error
        /// </summary>
        public CharacterErrorType reason; // CharacterErrorType

        public Character_CharacterError_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            reason = (CharacterErrorType)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"reason = {reason}");
        }

    }

    /// <summary>
    /// Stops a movement
    /// </summary>
    public class Movement_StopMovementCommand_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Movement_StopMovementCommand;

        /// <summary>
        /// Motion being stopped
        /// </summary>
        public uint motion; // uint

        /// <summary>
        /// Key being held
        /// </summary>
        public uint holdKey; // uint

        public Movement_StopMovementCommand_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            motion = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"motion = {motion}");
            holdKey = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"holdKey = {holdKey}");
        }

    }

    /// <summary>
    /// Asks server for a new object description
    /// </summary>
    public class Object_SendForceObjdesc_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Object_SendForceObjdesc;

        /// <summary>
        /// Object to get new Obj Desc for
        /// </summary>
        public uint objectID; // ObjectID

        public Object_SendForceObjdesc_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            objectID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectID = {objectID}");
        }

    }

    /// <summary>
    /// Create an object somewhere in the world
    /// </summary>
    public class Item_CreateObject_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Item_CreateObject;

        /// <summary>
        /// object ID
        /// </summary>
        public uint objectID; // ObjectID

        public ObjDesc objDesc; // ObjDesc

        public PhysicsDesc physicsdesc; // PhysicsDesc

        public PublicWeenieDesc wdesc; // PublicWeenieDesc

        public Item_CreateObject_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            objectID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectID = {objectID}");
            objDesc = new ObjDesc();
            objDesc.ReadFromBuffer(buffer);
            Logger.Log($"objDesc = {objDesc}");
            physicsdesc = new PhysicsDesc();
            physicsdesc.ReadFromBuffer(buffer);
            Logger.Log($"physicsdesc = {physicsdesc}");
            wdesc = new PublicWeenieDesc();
            wdesc.ReadFromBuffer(buffer);
            Logger.Log($"wdesc = {wdesc}");
        }

    }

    /// <summary>
    /// Login of player
    /// </summary>
    public class Login_CreatePlayer_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Login_CreatePlayer;

        /// <summary>
        /// ID of the character logging on - should be you.
        /// </summary>
        public uint character; // ObjectID

        public Login_CreatePlayer_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            character = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"character = {character}");
        }

    }

    /// <summary>
    /// Sent whenever an object is being deleted from the scene.
    /// </summary>
    public class Item_DeleteObject_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Item_DeleteObject;

        /// <summary>
        /// The object that was recently erased.
        /// </summary>
        public uint objectID; // ObjectID

        /// <summary>
        /// The instance sequence value for the object (number of logins for players)
        /// </summary>
        public ushort objectInstanceSequence; // ushort

        public Item_DeleteObject_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            objectID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectID = {objectID}");
            objectInstanceSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectInstanceSequence = {objectInstanceSequence}");
            if ((buffer.BaseStream.Position % 4) != 0) { // align DWORD
                buffer.BaseStream.Position += 4 - (buffer.BaseStream.Position % 4);
            }
        }

    }

    /// <summary>
    /// Sets the position/motion of an object
    /// </summary>
    public class Movement_PositionEvent_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Movement_PositionEvent;

        /// <summary>
        /// The object with the position changing.
        /// </summary>
        public uint objectID; // ObjectID

        /// <summary>
        /// The current or starting location.
        /// </summary>
        public PositionPack position; // PositionPack

        public Movement_PositionEvent_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            objectID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectID = {objectID}");
            position = new PositionPack();
            position.ReadFromBuffer(buffer);
            Logger.Log($"position = {position}");
        }

    }

    /// <summary>
    /// Sets the parent for an object, eg. equipting an object.
    /// </summary>
    public class Item_ParentEvent_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Item_ParentEvent;

        /// <summary>
        /// id of the parent object
        /// </summary>
        public uint parentID; // ObjectID

        /// <summary>
        /// id of the child object
        /// </summary>
        public uint childID; // ObjectID

        /// <summary>
        /// Location object is being equipt to (Read from CSetup table in dat)
        /// </summary>
        public uint childLocation; // uint

        /// <summary>
        /// Placement frame id
        /// </summary>
        public uint placementID; // uint

        /// <summary>
        /// The instance sequence value for the parent object (number of logins for players)
        /// </summary>
        public ushort objectInstanceSequence; // ushort

        /// <summary>
        /// The position sequence value for the child object
        /// </summary>
        public ushort childPositionSequence; // ushort

        public Item_ParentEvent_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            parentID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"parentID = {parentID}");
            childID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"childID = {childID}");
            childLocation = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"childLocation = {childLocation}");
            placementID = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"placementID = {placementID}");
            objectInstanceSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectInstanceSequence = {objectInstanceSequence}");
            childPositionSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"childPositionSequence = {childPositionSequence}");
        }

    }

    /// <summary>
    /// Sent when picking up an object
    /// </summary>
    public class Inventory_PickupEvent_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Inventory_PickupEvent;

        /// <summary>
        /// The object being picked up
        /// </summary>
        public uint objectID; // ObjectID

        /// <summary>
        /// The instance sequence value for the object (number of logins for players)
        /// </summary>
        public ushort objectInstanceSequence; // ushort

        /// <summary>
        /// The position sequence value for the object
        /// </summary>
        public ushort objectPositionSequence; // ushort

        public Inventory_PickupEvent_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            objectID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectID = {objectID}");
            objectInstanceSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectInstanceSequence = {objectInstanceSequence}");
            objectPositionSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectPositionSequence = {objectPositionSequence}");
        }

    }

    /// <summary>
    /// Set&#39;s the current state of the object. Client appears to only process the following state changes post creation: NoDraw, LightingOn, Hidden
    /// </summary>
    public class Item_SetState_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Item_SetState;

        /// <summary>
        /// The object being changed
        /// </summary>
        public uint objectId; // ObjectID

        /// <summary>
        /// The new state for the object
        /// </summary>
        public PhysicsState newState; // PhysicsState

        /// <summary>
        /// The instance sequence value for this object (number of logins for players)
        /// </summary>
        public ushort objectInstanceSequence; // ushort

        /// <summary>
        /// The state sequence value for this object
        /// </summary>
        public ushort objectStateSequence; // ushort

        public Item_SetState_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            objectId = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectId = {objectId}");
            newState = (PhysicsState)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"newState = {newState}");
            objectInstanceSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectInstanceSequence = {objectInstanceSequence}");
            objectStateSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectStateSequence = {objectStateSequence}");
        }

    }

    /// <summary>
    /// These are animations. Whenever a human, monster or object moves - one of these little messages is sent. Even idle emotes (like head scratching and nodding) are sent in this manner.
    /// </summary>
    public class Movement_SetObjectMovement_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Movement_SetObjectMovement;

        /// <summary>
        /// ID of the character moving
        /// </summary>
        public uint objectID; // ObjectID

        /// <summary>
        /// The instance sequence value for this object (number of logins for players)
        /// </summary>
        public ushort objectInstanceSequence; // ushort

        /// <summary>
        /// Set of movement data
        /// </summary>
        public MovementData movementData; // MovementData

        public Movement_SetObjectMovement_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            objectID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectID = {objectID}");
            objectInstanceSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectInstanceSequence = {objectInstanceSequence}");
            movementData = new MovementData();
            movementData.ReadFromBuffer(buffer);
            Logger.Log($"movementData = {movementData}");
        }

    }

    /// <summary>
    /// Changes an objects vector, for things like jumping
    /// </summary>
    public class Movement_VectorUpdate_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Movement_VectorUpdate;

        /// <summary>
        /// ID of the object being updated
        /// </summary>
        public uint objectID; // ObjectID

        /// <summary>
        /// new velocity component
        /// </summary>
        public Vector3 Velocity; // Vector3

        /// <summary>
        /// new omega component
        /// </summary>
        public Vector3 Omega; // Vector3

        /// <summary>
        /// The instance sequence value for this object (number of logins for players)
        /// </summary>
        public ushort objectInstanceSequence; // ushort

        /// <summary>
        /// The vector sequence value for this object
        /// </summary>
        public ushort objectVectorSequence; // ushort

        public Movement_VectorUpdate_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            objectID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectID = {objectID}");
            Velocity = new Vector3();
            Velocity.ReadFromBuffer(buffer);
            Logger.Log($"Velocity = {Velocity}");
            Omega = new Vector3();
            Omega.ReadFromBuffer(buffer);
            Logger.Log($"Omega = {Omega}");
            objectInstanceSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectInstanceSequence = {objectInstanceSequence}");
            objectVectorSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectVectorSequence = {objectVectorSequence}");
        }

    }

    /// <summary>
    /// Applies a sound effect.
    /// </summary>
    public class Effects_SoundEvent_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Effects_SoundEvent;

        /// <summary>
        /// ID of the object from which the effect originates. Can be you, another char/npc or an item.
        /// </summary>
        public uint objectID; // ObjectID

        /// <summary>
        /// The sound type ID, which is referenced in the Sound Table.
        /// </summary>
        public int soundType; // int

        /// <summary>
        /// Volume to play the sound
        /// </summary>
        public float volume; // float

        public Effects_SoundEvent_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            objectID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectID = {objectID}");
            soundType = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"soundType = {soundType}");
            volume = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"volume = {volume}");
        }

    }

    /// <summary>
    /// Instructs the client to show the portal graphic.
    /// </summary>
    public class Effects_PlayerTeleport_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Effects_PlayerTeleport;

        /// <summary>
        /// The teleport sequence value for the object
        /// </summary>
        public ushort objectTeleportSequence; // ushort

        public Effects_PlayerTeleport_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            objectTeleportSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectTeleportSequence = {objectTeleportSequence}");
            if ((buffer.BaseStream.Position % 4) != 0) { // align DWORD
                buffer.BaseStream.Position += 4 - (buffer.BaseStream.Position % 4);
            }
        }

    }

    /// <summary>
    /// Sets an autonomy level
    /// </summary>
    public class Movement_AutonomyLevel_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Movement_AutonomyLevel;

        /// <summary>
        /// Seems to be 0, 1 or 2. I think 0/1 is server controlled, 2 is client controlled
        /// </summary>
        public uint autonomyLevel; // uint

        public Movement_AutonomyLevel_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            autonomyLevel = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"autonomyLevel = {autonomyLevel}");
            if ((buffer.BaseStream.Position % 4) != 0) { // align DWORD
                buffer.BaseStream.Position += 4 - (buffer.BaseStream.Position % 4);
            }
        }

    }

    /// <summary>
    /// Sends an autonomous position
    /// </summary>
    public class Movement_AutonomousPosition_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Movement_AutonomousPosition;

        /// <summary>
        /// Set of autonomous position data
        /// </summary>
        public AutonomousPositionPack app; // AutonomousPositionPack

        public Movement_AutonomousPosition_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            app = new AutonomousPositionPack();
            app.ReadFromBuffer(buffer);
            Logger.Log($"app = {app}");
        }

    }

    /// <summary>
    /// Instructs the client to play a script. (Not seen so far, maybe prefered PlayScriptType)
    /// </summary>
    public class Effects_PlayScriptID_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Effects_PlayScriptID;

        /// <summary>
        /// ID of the object to play the script
        /// </summary>
        public uint objectID; // ObjectID

        /// <summary>
        /// ID of script to be played
        /// </summary>
        public DataID scriptID; // DataID

        public Effects_PlayScriptID_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            objectID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectID = {objectID}");
            scriptID = new DataID();
            scriptID.ReadFromBuffer(buffer);
            Logger.Log($"scriptID = {scriptID}");
        }

    }

    /// <summary>
    /// Applies an effect with visual and sound by providing the type to be looked up in the Physics Script Table
    /// </summary>
    public class Effects_PlayScriptType_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Effects_PlayScriptType;

        /// <summary>
        /// ID of the object from which the effect originates. Can be you, another char/npc or an item.
        /// </summary>
        public uint objectID; // ObjectID

        /// <summary>
        /// The script type id
        /// </summary>
        public int scriptType; // int

        /// <summary>
        /// Speed to play the particle effect at.  1.0 is default, lower for slower, higher for faster.
        /// </summary>
        public float speed; // float

        public Effects_PlayScriptType_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            objectID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectID = {objectID}");
            scriptType = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"scriptType = {scriptType}");
            speed = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"speed = {speed}");
        }

    }

    /// <summary>
    /// Account has been banned
    /// </summary>
    public class Login_AccountBanned_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Login_AccountBanned;

        /// <summary>
        /// Timestamp when ban expires, or 0 for permaban
        /// </summary>
        public uint bannedUntil; // uint

        /// <summary>
        /// I believe this will be empty (len=1) in current version
        /// </summary>
        public string oldText; // string

        public Login_AccountBanned_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            bannedUntil = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"bannedUntil = {bannedUntil}");
            oldText = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"oldText = {oldText}");
        }

    }

    /// <summary>
    /// The user has clicked &#39;Enter&#39;. This message does not contain the ID of the character logging on; that comes later.
    /// </summary>
    public class Login_SendEnterWorldRequest_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Login_SendEnterWorldRequest;

        public Login_SendEnterWorldRequest_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Performs a non autonomous jump
    /// </summary>
    public class Movement_Jump_NonAutonomous_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Movement_Jump_NonAutonomous;

        /// <summary>
        /// Power of jump
        /// </summary>
        public float extent; // float

        public Movement_Jump_NonAutonomous_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            extent = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"extent = {extent}");
        }

    }

    /// <summary>
    /// Admin Receive Account Data
    /// </summary>
    public class Admin_ReceiveAccountData_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Admin_ReceiveAccountData;

        public uint unknown; // uint

        /// <summary>
        /// Set of account data
        /// </summary>
        public PackableList<AdminAccountData> adminAccountData; // PackableList

        public Admin_ReceiveAccountData_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            unknown = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"unknown = {unknown}");
            adminAccountData = new PackableList<AdminAccountData>();
            adminAccountData.ReadFromBuffer(buffer);
            Logger.Log($"adminAccountData = {adminAccountData}");
        }

    }

    /// <summary>
    /// Admin Receive Player Data
    /// </summary>
    public class Admin_ReceivePlayerData_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Admin_ReceivePlayerData;

        public int unknown; // int

        /// <summary>
        /// Set of player data
        /// </summary>
        public PackableList<AdminPlayerData> adminPlayerData; // PackableList

        public Admin_ReceivePlayerData_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            unknown = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"unknown = {unknown}");
            adminPlayerData = new PackableList<AdminPlayerData>();
            adminPlayerData.ReadFromBuffer(buffer);
            Logger.Log($"adminPlayerData = {adminPlayerData}");
        }

    }

    /// <summary>
    /// Sent if player is an PSR, I assume it displays the server version number
    /// </summary>
    public class Admin_SendAdminGetServerVersion_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Admin_SendAdminGetServerVersion;

        public Admin_SendAdminGetServerVersion_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Seems to be a legacy friends command, /friends old, for when Jan 2006 event changed the friends list
    /// </summary>
    public class Social_SendFriendsCommand_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Social_SendFriendsCommand;

        /// <summary>
        /// Only 0 is used in client, I suspect it is list/display
        /// </summary>
        public uint cmd; // uint

        /// <summary>
        /// I assume it would be used to pass a friend to add/remove.  Display passes null string.
        /// </summary>
        public string player; // string

        public Social_SendFriendsCommand_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            cmd = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"cmd = {cmd}");
            player = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"player = {player}");
        }

    }

    /// <summary>
    /// Admin command to restore a character
    /// </summary>
    public class Admin_SendAdminRestoreCharacter_C2S : IACDataType {
        public const MessageType Opcode = MessageType.Admin_SendAdminRestoreCharacter;

        /// <summary>
        /// ID of character to restore
        /// </summary>
        public uint iid; // ObjectID

        /// <summary>
        /// Name of character to restore
        /// </summary>
        public string restoredCharName; // string

        /// <summary>
        /// Account name to restore the character on
        /// </summary>
        public string acctToRestoreTo; // string

        public Admin_SendAdminRestoreCharacter_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            iid = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"iid = {iid}");
            restoredCharName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"restoredCharName = {restoredCharName}");
            acctToRestoreTo = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"acctToRestoreTo = {acctToRestoreTo}");
        }

    }

    /// <summary>
    /// Update an existing object&#39;s data.
    /// </summary>
    public class Item_UpdateObject_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Item_UpdateObject;

        /// <summary>
        /// the object being updated
        /// </summary>
        public uint objectId; // ObjectID

        /// <summary>
        /// updated model data
        /// </summary>
        public ObjDesc objDesc; // ObjDesc

        /// <summary>
        /// updated physics data
        /// </summary>
        public PhysicsDesc physicsDesc; // PhysicsDesc

        /// <summary>
        /// updated game data
        /// </summary>
        public PublicWeenieDesc wdesc; // PublicWeenieDesc

        public Item_UpdateObject_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            objectId = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectId = {objectId}");
            objDesc = new ObjDesc();
            objDesc.ReadFromBuffer(buffer);
            Logger.Log($"objDesc = {objDesc}");
            physicsDesc = new PhysicsDesc();
            physicsDesc.ReadFromBuffer(buffer);
            Logger.Log($"physicsDesc = {physicsDesc}");
            wdesc = new PublicWeenieDesc();
            wdesc.ReadFromBuffer(buffer);
            Logger.Log($"wdesc = {wdesc}");
        }

    }

    /// <summary>
    /// Account has been booted
    /// </summary>
    public class Login_AccountBooted_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Login_AccountBooted;

        public string additionalReasonText; // string

        public string reasonText; // string

        public Login_AccountBooted_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            additionalReasonText = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"additionalReasonText = {additionalReasonText}");
            reasonText = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"reasonText = {reasonText}");
        }

    }

    /// <summary>
    /// Send or receive a message using Turbine Chat.
    /// </summary>
    public class Communication_TurbineChat_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Communication_TurbineChat;

        /// <summary>
        /// the number of bytes that follow after this DWORD
        /// </summary>
        public uint messageSize; // uint

        /// <summary>
        /// the type of data contained in this message
        /// </summary>
        public TurbineChatType type; // TurbineChatType

        public uint blobDispatchType; // uint

        public int targetType; // int

        public int targetID; // int

        public int transportType; // int

        public int transportID; // int

        public int cookie; // int

        /// <summary>
        /// the number of bytes that follow after this DWORD
        /// </summary>
        public uint payloadSize; // uint

        /// <summary>
        /// the channel number of the message
        /// </summary>
        public uint roomID; // uint

        /// <summary>
        /// the name of the player sending the message
        /// </summary>
        public string displayName; // WString

        /// <summary>
        /// the message text
        /// </summary>
        public string text; // WString

        /// <summary>
        /// the number of bytes that follow after this DWORD
        /// </summary>
        public uint extraDataSize; // uint

        /// <summary>
        /// the object ID of the player sending the message
        /// </summary>
        public uint speakerID; // ObjectID

        public int hResult; // int

        public uint chatType; // uint

        public uint contextID; // uint

        public uint responseID; // uint

        public uint methodID; // uint

        public Communication_TurbineChat_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            messageSize = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"messageSize = {messageSize}");
            type = (TurbineChatType)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"type = {type}");
            blobDispatchType = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"blobDispatchType = {blobDispatchType}");
            targetType = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"targetType = {targetType}");
            targetID = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"targetID = {targetID}");
            transportType = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"transportType = {transportType}");
            transportID = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"transportID = {transportID}");
            cookie = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"cookie = {cookie}");
            payloadSize = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"payloadSize = {payloadSize}");
            switch((int)type) {
                case 0x01:
                    switch((int)blobDispatchType) {
                        case 0x01:
                            roomID = BinaryHelpers.ReadUInt32(buffer); // uint
                            Logger.Log($"roomID = {roomID}");
                            displayName = BinaryHelpers.ReadWString(buffer); // WString
                            Logger.Log($"displayName = {displayName}");
                            text = BinaryHelpers.ReadWString(buffer); // WString
                            Logger.Log($"text = {text}");
                            extraDataSize = BinaryHelpers.ReadUInt32(buffer); // uint
                            Logger.Log($"extraDataSize = {extraDataSize}");
                            speakerID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
                            Logger.Log($"speakerID = {speakerID}");
                            hResult = BinaryHelpers.ReadInt32(buffer); // int
                            Logger.Log($"hResult = {hResult}");
                            chatType = BinaryHelpers.ReadUInt32(buffer); // uint
                            Logger.Log($"chatType = {chatType}");
                            break;
                    }
                    break;
                case 0x03:
                    switch((int)blobDispatchType) {
                        case 0x02:
                            contextID = BinaryHelpers.ReadUInt32(buffer); // uint
                            Logger.Log($"contextID = {contextID}");
                            responseID = BinaryHelpers.ReadUInt32(buffer); // uint
                            Logger.Log($"responseID = {responseID}");
                            methodID = BinaryHelpers.ReadUInt32(buffer); // uint
                            Logger.Log($"methodID = {methodID}");
                            roomID = BinaryHelpers.ReadUInt32(buffer); // uint
                            Logger.Log($"roomID = {roomID}");
                            text = BinaryHelpers.ReadWString(buffer); // WString
                            Logger.Log($"text = {text}");
                            extraDataSize = BinaryHelpers.ReadUInt32(buffer); // uint
                            Logger.Log($"extraDataSize = {extraDataSize}");
                            speakerID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
                            Logger.Log($"speakerID = {speakerID}");
                            hResult = BinaryHelpers.ReadInt32(buffer); // int
                            Logger.Log($"hResult = {hResult}");
                            chatType = BinaryHelpers.ReadUInt32(buffer); // uint
                            Logger.Log($"chatType = {chatType}");
                            break;
                    }
                    break;
                case 0x05:
                    switch((int)blobDispatchType) {
                        case 0x01:
                            contextID = BinaryHelpers.ReadUInt32(buffer); // uint
                            Logger.Log($"contextID = {contextID}");
                            responseID = BinaryHelpers.ReadUInt32(buffer); // uint
                            Logger.Log($"responseID = {responseID}");
                            methodID = BinaryHelpers.ReadUInt32(buffer); // uint
                            Logger.Log($"methodID = {methodID}");
                            hResult = BinaryHelpers.ReadInt32(buffer); // int
                            Logger.Log($"hResult = {hResult}");
                            break;
                        case 0x02:
                            contextID = BinaryHelpers.ReadUInt32(buffer); // uint
                            Logger.Log($"contextID = {contextID}");
                            responseID = BinaryHelpers.ReadUInt32(buffer); // uint
                            Logger.Log($"responseID = {responseID}");
                            methodID = BinaryHelpers.ReadUInt32(buffer); // uint
                            Logger.Log($"methodID = {methodID}");
                            hResult = BinaryHelpers.ReadInt32(buffer); // int
                            Logger.Log($"hResult = {hResult}");
                            break;
                    }
                    break;
            }
        }

    }

    /// <summary>
    /// Switch from the character display to the game display.
    /// </summary>
    public class Login_EnterGame_ServerReady_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Login_EnterGame_ServerReady;

        public Login_EnterGame_ServerReady_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Display a message in the chat window.
    /// </summary>
    public class Communication_TextboxString_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Communication_TextboxString;

        /// <summary>
        /// the message text
        /// </summary>
        public string text; // string

        /// <summary>
        /// the message type, controls color and @filter processing
        /// </summary>
        public ChatMessageType type; // ChatMessageType

        public Communication_TextboxString_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            text = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"text = {text}");
            type = (ChatMessageType)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"type = {type}");
        }

    }

    /// <summary>
    /// The name of the current world.
    /// </summary>
    public class Login_WorldInfo_S2C : IACDataType {
        public const MessageType Opcode = MessageType.Login_WorldInfo;

        /// <summary>
        /// the number of players connected
        /// </summary>
        public uint connections; // uint

        /// <summary>
        /// the max number of players allowed to connect
        /// </summary>
        public uint maxConnections; // uint

        /// <summary>
        /// the name of the current world
        /// </summary>
        public string worldName; // string

        public Login_WorldInfo_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            connections = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"connections = {connections}");
            maxConnections = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"maxConnections = {maxConnections}");
            worldName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"worldName = {worldName}");
        }

    }

    /// <summary>
    /// Add or update a dat file Resource.
    /// </summary>
    public class DDD_DataMessage_S2C : IACDataType {
        public const MessageType Opcode = MessageType.DDD_DataMessage;

        /// <summary>
        /// which dat file should store this resource
        /// </summary>
        public ResourceType idDatFile; // ResourceType

        /// <summary>
        /// the resource type
        /// </summary>
        public uint resourceType; // uint

        /// <summary>
        /// the resource ID number
        /// </summary>
        public DataID resourceId; // DataID

        /// <summary>
        /// the file version number
        /// </summary>
        public uint idIteration; // uint

        /// <summary>
        /// the type of compression used
        /// </summary>
        public CompressionType compression; // CompressionType

        /// <summary>
        /// version of some sort
        /// </summary>
        public uint version; // uint

        /// <summary>
        /// the number of bytes required for the remainder of this message, including this DWORD
        /// </summary>
        public uint dataSize; // uint

        public List<byte> data { get; set; } = new List<byte>(); // test

        /// <summary>
        /// the size of the uncompressed file
        /// </summary>
        public uint fileSize; // uint

        public DDD_DataMessage_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            idDatFile = (ResourceType)BinaryHelpers.ReadInt64(buffer);
            Logger.Log($"idDatFile = {idDatFile}");
            resourceType = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"resourceType = {resourceType}");
            resourceId = new DataID();
            resourceId.ReadFromBuffer(buffer);
            Logger.Log($"resourceId = {resourceId}");
            idIteration = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"idIteration = {idIteration}");
            compression = (CompressionType)BinaryHelpers.ReadByte(buffer);
            Logger.Log($"compression = {compression}");
            version = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"version = {version}");
            dataSize = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"dataSize = {dataSize}");
            switch((int)compression) {
                case 0x00:
                    for (var i=0; i < dataSize; i++) {
                        var dbyte = BinaryHelpers.ReadByte(buffer); // byte
                        Logger.Log($"dbyte = {dbyte}");
                        data.Add(dbyte);
                    }
                    break;
                case 0x01:
                    fileSize = BinaryHelpers.ReadUInt32(buffer); // uint
                    Logger.Log($"fileSize = {fileSize}");
                    for (var i=0; i < dataSize; i++) {
                        var dbyte = BinaryHelpers.ReadByte(buffer); // byte
                        Logger.Log($"dbyte = {dbyte}");
                        data.Add(dbyte);
                    }
                    break;
            }
        }

    }

    /// <summary>
    /// DDD request for data
    /// </summary>
    public class DDD_RequestDataMessage_C2S : IACDataType {
        public const MessageType Opcode = MessageType.DDD_RequestDataMessage;

        /// <summary>
        /// the resource type
        /// </summary>
        public uint resourceType; // uint

        /// <summary>
        /// the resource ID number
        /// </summary>
        public DataID resourceId; // DataID

        public DDD_RequestDataMessage_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            resourceType = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"resourceType = {resourceType}");
            resourceId = new DataID();
            resourceId.ReadFromBuffer(buffer);
            Logger.Log($"resourceId = {resourceId}");
        }

    }

    /// <summary>
    /// DDD error
    /// </summary>
    public class DDD_ErrorMessage_S2C : IACDataType {
        public const MessageType Opcode = MessageType.DDD_ErrorMessage;

        /// <summary>
        /// the resource type
        /// </summary>
        public uint resourceType; // uint

        /// <summary>
        /// the resource ID number
        /// </summary>
        public DataID resourceId; // DataID

        public uint error; // uint

        public DDD_ErrorMessage_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            resourceType = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"resourceType = {resourceType}");
            resourceId = new DataID();
            resourceId.ReadFromBuffer(buffer);
            Logger.Log($"resourceId = {resourceId}");
            error = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"error = {error}");
        }

    }

    /// <summary>
    /// Add or update a dat file Resource.
    /// </summary>
    public class DDD_InterrogationMessage_S2C : IACDataType {
        public const MessageType Opcode = MessageType.DDD_InterrogationMessage;

        public uint serversRegion; // uint

        public uint nameRuleLanguage; // uint

        public uint productID; // uint

        /// <summary>
        /// Number of supported languanges
        /// </summary>
        public uint supportedLanguagesCount; // uint

        public List<uint> supportedLanguages { get; set; } = new List<uint>(); // test

        public DDD_InterrogationMessage_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            serversRegion = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"serversRegion = {serversRegion}");
            nameRuleLanguage = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"nameRuleLanguage = {nameRuleLanguage}");
            productID = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"productID = {productID}");
            supportedLanguagesCount = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"supportedLanguagesCount = {supportedLanguagesCount}");
            for (var i=0; i < supportedLanguagesCount; i++) {
                var language = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"language = {language}");
                supportedLanguages.Add(language);
            }
        }

    }

    /// <summary>
    /// TODO
    /// </summary>
    public class DDD_InterrogationResponseMessage_C2S : IACDataType {
        public const MessageType Opcode = MessageType.DDD_InterrogationResponseMessage;

        public uint clientLanguage; // uint

        public uint count; // uint

        public List<long> files { get; set; } = new List<long>(); // test

        public DDD_InterrogationResponseMessage_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            clientLanguage = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"clientLanguage = {clientLanguage}");
            count = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"count = {count}");
            for (var i=0; i < count; i++) {
                var idDataFile = BinaryHelpers.ReadInt64(buffer); // long
                Logger.Log($"idDataFile = {idDataFile}");
                files.Add(idDataFile);
            }
        }

    }

    /// <summary>
    /// A list of dat files that need to be patched
    /// </summary>
    public class DDD_BeginDDDMessage_S2C : IACDataType {
        public const MessageType Opcode = MessageType.DDD_BeginDDDMessage;

        /// <summary>
        /// Amount of data expected
        /// </summary>
        public uint dataExpected; // uint

        /// <summary>
        /// Total number of revisions to follow
        /// </summary>
        public uint revisionCount; // uint

        public class RevisionVectorItem {
            /// <summary>
            /// Dat File header offset 0x0150, Dat File header offset 0x014C
            /// </summary>
            public ulong idDatFile;

            /// <summary>
            /// The corresponding Dat file revision for this patch set
            /// </summary>
            public uint idIteration;

            /// <summary>
            /// Number of resources to add in this revision
            /// </summary>
            public uint IDsToDownloadCount;

            public List<DataID> IDsToDownload { get; set; } = new List<DataID>(); // test

            /// <summary>
            /// Number of resources to purge in this revision
            /// </summary>
            public uint IDsToPurgeCount;

            public List<DataID> IDsToPurge { get; set; } = new List<DataID>(); // test

        }

        /// <summary>
        /// Subclass to hold revisions data
        /// </summary>
        public List<RevisionVectorItem> revisions { get; set; } = new List<RevisionVectorItem>(); // test

        public DDD_BeginDDDMessage_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            dataExpected = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"dataExpected = {dataExpected}");
            revisionCount = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"revisionCount = {revisionCount}");
            for (var i=0; i < revisionCount; i++) {
                var idDatFile = BinaryHelpers.ReadUInt64(buffer); // ulong
                Logger.Log($"idDatFile = {idDatFile}");
                var idIteration = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"idIteration = {idIteration}");
                var IDsToDownloadCount = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"IDsToDownloadCount = {IDsToDownloadCount}");
                var IDsToDownload = new List<DataID>();
                for (var x=0; x < IDsToDownloadCount; x++) {
                    var resource = new DataID();
                    resource.ReadFromBuffer(buffer);
                    Logger.Log($"resource = {resource}");
                    IDsToDownload.Add(resource);
                }
                var IDsToPurgeCount = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"IDsToPurgeCount = {IDsToPurgeCount}");
                var IDsToPurge = new List<DataID>();
                for (var x=0; x < IDsToPurgeCount; x++) {
                    var resource = new DataID();
                    resource.ReadFromBuffer(buffer);
                    Logger.Log($"resource = {resource}");
                    IDsToPurge.Add(resource);
                }
                revisions.Add(new RevisionVectorItem() {
                    idDatFile = idDatFile,
                    idIteration = idIteration,
                    IDsToDownloadCount = IDsToDownloadCount,
                    IDsToDownload = IDsToDownload,
                    IDsToPurgeCount = IDsToPurgeCount,
                    IDsToPurge = IDsToPurge,
                });
            }
        }

    }

    /// <summary>
    /// Ends DDD update
    /// </summary>
    public class DDD_OnEndDDD_S2C : IACDataType {
        public const MessageType Opcode = MessageType.DDD_OnEndDDD;

        public DDD_OnEndDDD_S2C() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Ends DDD update
    /// </summary>
    public class DDD_EndDDDMessage_C2S : IACDataType {
        public const MessageType Opcode = MessageType.DDD_EndDDDMessage;

        public DDD_EndDDDMessage_C2S() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

}