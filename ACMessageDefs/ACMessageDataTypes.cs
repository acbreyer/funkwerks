﻿//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
//                                                            //
//                          WARNING                           //
//                                                            //
//           DO NOT MAKE LOCAL CHANGES TO THIS FILE           //
//               EDIT THE .tt TEMPLATE INSTEAD                //
//                                                            //
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//


using System.IO;
using System.Collections.Generic;
using ACMessageDefs.Lib;
using ACMessageDefs.Enums;

namespace ACMessageDefs.DataTypes {
    /// <summary>
    /// Full spell ID combining the spell id with the spell layer.
    /// </summary>
    public class DataID : IACDataType {
        public ushort key; // ushort

        public ushort value; // ushort

        public DataID() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            key = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"key = {key}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            value = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"value = {value}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// Full spell ID combining the spell id with the spell layer.
    /// </summary>
    public class LayeredSpellID : IACDataType {
        /// <summary>
        /// ID of the spell
        /// </summary>
        public ushort id; // SpellID

        /// <summary>
        /// Layer of the spell, seperating multiple instances of the same spell
        /// </summary>
        public ushort layer; // ushort

        public LayeredSpellID() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            id = BinaryHelpers.ReadUInt16(buffer); // SpellID
            Logger.Log($"id = {id}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            layer = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"layer = {layer}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    public class Vector3 : IACDataType {
        public float x; // float

        public float y; // float

        public float z; // float

        public Vector3() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            x = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"x = {x}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            y = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"y = {y}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            z = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"z = {z}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    public class Quaternion : IACDataType {
        public float w; // float

        public float x; // float

        public float y; // float

        public float z; // float

        public Quaternion() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            w = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"w = {w}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            x = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"x = {x}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            y = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"y = {y}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            z = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"z = {z}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// Landcell location, without orientation
    /// </summary>
    public class Origin : IACDataType {
        /// <summary>
        /// the landcell in which the object is located
        /// </summary>
        public uint landcell; // uint

        /// <summary>
        /// the location in the landcell for the object
        /// </summary>
        public Vector3 origin; // Vector3

        public Origin() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            landcell = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"landcell = {landcell}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            origin = new Vector3();
            origin.ReadFromBuffer(buffer);
            Logger.Log($"origin = {origin}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// Landcell location, including orientation
    /// </summary>
    public class Position : IACDataType {
        /// <summary>
        /// the landcell in which the object is located
        /// </summary>
        public uint landcell; // uint

        /// <summary>
        /// the location and orientation in the landcell
        /// </summary>
        public Frame frame; // Frame

        public Position() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            landcell = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"landcell = {landcell}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            frame = new Frame();
            frame.ReadFromBuffer(buffer);
            Logger.Log($"frame = {frame}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// A the location and orientation of an object within a landcell
    /// </summary>
    public class Frame : IACDataType {
        /// <summary>
        /// the location in a landcell in which the object is located
        /// </summary>
        public Vector3 origin; // Vector3

        /// <summary>
        /// a quaternion describing the object&#39;s orientation
        /// </summary>
        public Quaternion orientation; // Quaternion

        public Frame() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            origin = new Vector3();
            origin.ReadFromBuffer(buffer);
            Logger.Log($"origin = {origin}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            orientation = new Quaternion();
            orientation.ReadFromBuffer(buffer);
            Logger.Log($"orientation = {orientation}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// Character properties.
    /// </summary>
    public class ACBaseQualities : IACDataType {
        /// <summary>
        /// determines which property types appear in the message
        /// </summary>
        public uint flags; // uint

        /// <summary>
        /// Expect it always should be 0xA
        /// </summary>
        public ObjectType weenieType; // ObjectType

        public PackableHashTable<IntPropertyID, int> intStatsTable; // PackableHashTable

        public PackableHashTable<Int64PropertyID, long> int64StatsTable; // PackableHashTable

        public PackableHashTable<BooleanPropertyID, bool> boolStatsTable; // PackableHashTable

        public PackableHashTable<FloatPropertyID, double> floatStatsTable; // PackableHashTable

        public PackableHashTable<StringPropertyID, string> strStatsTable; // PackableHashTable

        public PackableHashTable<DataPropertyID, DataID> didStatsTable; // PackableHashTable

        public PackableHashTable<InstancePropertyID, uint> iidStatsTable; // PackableHashTable

        public PackableHashTable<PositionPropertyID, Position> posStatsTable; // PackableHashTable

        public ACBaseQualities() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            flags = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"flags = {flags}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            weenieType = (ObjectType)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"weenieType = {weenieType}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            if (((uint)flags & 0x00000001) != 0) {
                intStatsTable = new PackableHashTable<IntPropertyID, int>();
                intStatsTable.ReadFromBuffer(buffer);
                Logger.Log($"intStatsTable = {intStatsTable}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000080) != 0) {
                int64StatsTable = new PackableHashTable<Int64PropertyID, long>();
                int64StatsTable.ReadFromBuffer(buffer);
                Logger.Log($"int64StatsTable = {int64StatsTable}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000002) != 0) {
                boolStatsTable = new PackableHashTable<BooleanPropertyID, bool>();
                boolStatsTable.ReadFromBuffer(buffer);
                Logger.Log($"boolStatsTable = {boolStatsTable}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000004) != 0) {
                floatStatsTable = new PackableHashTable<FloatPropertyID, double>();
                floatStatsTable.ReadFromBuffer(buffer);
                Logger.Log($"floatStatsTable = {floatStatsTable}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000010) != 0) {
                strStatsTable = new PackableHashTable<StringPropertyID, string>();
                strStatsTable.ReadFromBuffer(buffer);
                Logger.Log($"strStatsTable = {strStatsTable}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000040) != 0) {
                didStatsTable = new PackableHashTable<DataPropertyID, DataID>();
                didStatsTable.ReadFromBuffer(buffer);
                Logger.Log($"didStatsTable = {didStatsTable}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000008) != 0) {
                iidStatsTable = new PackableHashTable<InstancePropertyID, uint>();
                iidStatsTable.ReadFromBuffer(buffer);
                Logger.Log($"iidStatsTable = {iidStatsTable}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000020) != 0) {
                posStatsTable = new PackableHashTable<PositionPropertyID, Position>();
                posStatsTable.ReadFromBuffer(buffer);
                Logger.Log($"posStatsTable = {posStatsTable}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
        }

    }

    /// <summary>
    /// The ACQualities structure contains character property lists.
    /// </summary>
    public class ACQualities : IACDataType {
        /// <summary>
        /// Contains basic data types (int, float bool, etc.)
        /// </summary>
        public ACBaseQualities baseQualities; // ACBaseQualities

        /// <summary>
        /// determines which property vector types appear in the message
        /// </summary>
        public uint flags; // uint

        /// <summary>
        /// seems to indicate this object has health attribute
        /// </summary>
        public bool hasHealth; // bool

        /// <summary>
        /// The character attributes
        /// </summary>
        public AttributeCache attribCache; // AttributeCache

        public PackableHashTable<SkillID, Skill> skillTable; // PackableHashTable

        public Body body; // Body

        /// <summary>
        /// Spells in the characters spellbook
        /// </summary>
        public SpellBook spellBook; // SpellBook

        /// <summary>
        /// The enchantments active on the character
        /// </summary>
        public EnchantmentRegistry enchantmentRegistry; // EnchantmentRegistry

        /// <summary>
        /// Some kind of event filter
        /// </summary>
        public EventFilter eventFilter; // EventFilter

        public EmoteTable emoteTable; // EmoteTable

        public PackableList<CreationProfile> creationProfile; // PackableList

        public PageDataList pageDataList; // PageDataList

        public GeneratorTable generatorTable; // GeneratorTable

        public GeneratorRegistry generatorRegistry; // GeneratorRegistry

        public GeneratorQueue generatorQueue; // GeneratorQueue

        public ACQualities() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            baseQualities = new ACBaseQualities();
            baseQualities.ReadFromBuffer(buffer);
            Logger.Log($"baseQualities = {baseQualities}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            flags = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"flags = {flags}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            hasHealth = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"hasHealth = {hasHealth}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            if (((uint)flags & 0x00000001) != 0) {
                attribCache = new AttributeCache();
                attribCache.ReadFromBuffer(buffer);
                Logger.Log($"attribCache = {attribCache}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000002) != 0) {
                skillTable = new PackableHashTable<SkillID, Skill>();
                skillTable.ReadFromBuffer(buffer);
                Logger.Log($"skillTable = {skillTable}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000004) != 0) {
                body = new Body();
                body.ReadFromBuffer(buffer);
                Logger.Log($"body = {body}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000100) != 0) {
                spellBook = new SpellBook();
                spellBook.ReadFromBuffer(buffer);
                Logger.Log($"spellBook = {spellBook}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000200) != 0) {
                enchantmentRegistry = new EnchantmentRegistry();
                enchantmentRegistry.ReadFromBuffer(buffer);
                Logger.Log($"enchantmentRegistry = {enchantmentRegistry}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000008) != 0) {
                eventFilter = new EventFilter();
                eventFilter.ReadFromBuffer(buffer);
                Logger.Log($"eventFilter = {eventFilter}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000010) != 0) {
                emoteTable = new EmoteTable();
                emoteTable.ReadFromBuffer(buffer);
                Logger.Log($"emoteTable = {emoteTable}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000020) != 0) {
                creationProfile = new PackableList<CreationProfile>();
                creationProfile.ReadFromBuffer(buffer);
                Logger.Log($"creationProfile = {creationProfile}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000040) != 0) {
                pageDataList = new PageDataList();
                pageDataList.ReadFromBuffer(buffer);
                Logger.Log($"pageDataList = {pageDataList}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000080) != 0) {
                generatorTable = new GeneratorTable();
                generatorTable.ReadFromBuffer(buffer);
                Logger.Log($"generatorTable = {generatorTable}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000400) != 0) {
                generatorRegistry = new GeneratorRegistry();
                generatorRegistry.ReadFromBuffer(buffer);
                Logger.Log($"generatorRegistry = {generatorRegistry}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000800) != 0) {
                generatorQueue = new GeneratorQueue();
                generatorQueue.ReadFromBuffer(buffer);
                Logger.Log($"generatorQueue = {generatorQueue}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
        }

    }

    /// <summary>
    /// The AttributeCache structure contains information about a character attributes.
    /// </summary>
    public class AttributeCache : IACDataType {
        /// <summary>
        /// The attributes included in the character description - this is always 0x1FF
        /// </summary>
        public uint attributeFlags; // uint

        /// <summary>
        /// strength attribute information
        /// </summary>
        public Attribute strength; // Attribute

        /// <summary>
        /// endurance attribute information
        /// </summary>
        public Attribute endurance; // Attribute

        /// <summary>
        /// quickness attribute information
        /// </summary>
        public Attribute quickness; // Attribute

        /// <summary>
        /// coordination attribute information
        /// </summary>
        public Attribute coordination; // Attribute

        /// <summary>
        /// focus attribute information
        /// </summary>
        public Attribute focus; // Attribute

        /// <summary>
        /// self attribute information
        /// </summary>
        public Attribute self; // Attribute

        /// <summary>
        /// health vital information
        /// </summary>
        public SecondaryAttribute health; // SecondaryAttribute

        /// <summary>
        /// stamina vital information
        /// </summary>
        public SecondaryAttribute stamina; // SecondaryAttribute

        /// <summary>
        /// mana vital information
        /// </summary>
        public SecondaryAttribute mana; // SecondaryAttribute

        public AttributeCache() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            attributeFlags = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"attributeFlags = {attributeFlags}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            if (((uint)attributeFlags & 0x00000001) != 0) {
                strength = new Attribute();
                strength.ReadFromBuffer(buffer);
                Logger.Log($"strength = {strength}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)attributeFlags & 0x00000002) != 0) {
                endurance = new Attribute();
                endurance.ReadFromBuffer(buffer);
                Logger.Log($"endurance = {endurance}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)attributeFlags & 0x00000004) != 0) {
                quickness = new Attribute();
                quickness.ReadFromBuffer(buffer);
                Logger.Log($"quickness = {quickness}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)attributeFlags & 0x00000008) != 0) {
                coordination = new Attribute();
                coordination.ReadFromBuffer(buffer);
                Logger.Log($"coordination = {coordination}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)attributeFlags & 0x00000010) != 0) {
                focus = new Attribute();
                focus.ReadFromBuffer(buffer);
                Logger.Log($"focus = {focus}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)attributeFlags & 0x00000020) != 0) {
                self = new Attribute();
                self.ReadFromBuffer(buffer);
                Logger.Log($"self = {self}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)attributeFlags & 0x00000040) != 0) {
                health = new SecondaryAttribute();
                health.ReadFromBuffer(buffer);
                Logger.Log($"health = {health}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)attributeFlags & 0x00000080) != 0) {
                stamina = new SecondaryAttribute();
                stamina.ReadFromBuffer(buffer);
                Logger.Log($"stamina = {stamina}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)attributeFlags & 0x00000100) != 0) {
                mana = new SecondaryAttribute();
                mana.ReadFromBuffer(buffer);
                Logger.Log($"mana = {mana}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
        }

    }

    /// <summary>
    /// The Attribute structure contains information about a character attribute.
    /// </summary>
    public class Attribute : IACDataType {
        /// <summary>
        /// points raised
        /// </summary>
        public uint levelFromCp; // uint

        /// <summary>
        /// innate points
        /// </summary>
        public uint initLevel; // uint

        /// <summary>
        /// XP spent on this attribute
        /// </summary>
        public uint cpSpent; // uint

        public Attribute() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            levelFromCp = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"levelFromCp = {levelFromCp}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            initLevel = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"initLevel = {initLevel}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            cpSpent = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"cpSpent = {cpSpent}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// The SecondaryAttribute structure contains information about a character vital.
    /// </summary>
    public class SecondaryAttribute : IACDataType {
        /// <summary>
        /// secondary attribute&#39;s data
        /// </summary>
        public Attribute attribute; // Attribute

        /// <summary>
        /// current value of the vital
        /// </summary>
        public uint currentLevel; // uint

        public SecondaryAttribute() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            attribute = new Attribute();
            attribute.ReadFromBuffer(buffer);
            Logger.Log($"attribute = {attribute}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            currentLevel = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"currentLevel = {currentLevel}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// The Skill structure contains information about a character skill.
    /// </summary>
    public class Skill : IACDataType {
        /// <summary>
        /// points raised
        /// </summary>
        public ushort levelFromPP; // ushort

        /// <summary>
        /// If this is not 0, it appears to trigger the initLevel to be treated as extra XP applied to the skill
        /// </summary>
        public ushort adjustPP; // ushort

        /// <summary>
        /// skill state
        /// </summary>
        public SkillState sac; // SkillState

        /// <summary>
        /// XP spent on this skill
        /// </summary>
        public uint pp; // uint

        /// <summary>
        /// starting point for advancement of the skill (eg bonus points)
        /// </summary>
        public uint initLevel; // uint

        /// <summary>
        /// last use difficulty
        /// </summary>
        public uint resistanceOfLastCheck; // uint

        /// <summary>
        /// time skill was last used
        /// </summary>
        public double lastUsedTime; // double

        public Skill() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            levelFromPP = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"levelFromPP = {levelFromPP}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            adjustPP = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"adjustPP = {adjustPP}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            sac = (SkillState)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"sac = {sac}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            pp = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"pp = {pp}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            initLevel = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"initLevel = {initLevel}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            resistanceOfLastCheck = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"resistanceOfLastCheck = {resistanceOfLastCheck}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            lastUsedTime = BinaryHelpers.ReadDouble(buffer); // double
            Logger.Log($"lastUsedTime = {lastUsedTime}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// Contains body part table
    /// </summary>
    public class Body : IACDataType {
        public PackableHashTable<uint, BodyPart> bodyPartTable; // PackableHashTable

        public Body() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            bodyPartTable = new PackableHashTable<uint, BodyPart>();
            bodyPartTable.ReadFromBuffer(buffer);
            Logger.Log($"bodyPartTable = {bodyPartTable}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// Information on individual body parts. (Needs to be confirmed if this was used in prod)
    /// </summary>
    public class BodyPart : IACDataType {
        public int hasBpsd; // int

        public int dType; // int

        public int dVal; // int

        public int dVar; // int

        /// <summary>
        /// Armor info
        /// </summary>
        public ArmorCache aCache; // ArmorCache

        public int bh; // int

        public BodyPartSelectionData bpsd; // BodyPartSelectionData

        public BodyPart() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            hasBpsd = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"hasBpsd = {hasBpsd}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            dType = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"dType = {dType}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            dVal = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"dVal = {dVal}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            dVar = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"dVar = {dVar}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            aCache = new ArmorCache();
            aCache.ReadFromBuffer(buffer);
            Logger.Log($"aCache = {aCache}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            bh = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"bh = {bh}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            if (((uint)hasBpsd & 0x00000001) != 0) {
                bpsd = new BodyPartSelectionData();
                bpsd.ReadFromBuffer(buffer);
                Logger.Log($"bpsd = {bpsd}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
        }

    }

    /// <summary>
    /// Information on armor levels
    /// </summary>
    public class ArmorCache : IACDataType {
        public int baseArmor; // int

        public int armorVsSlash; // int

        public int armorVsPierce; // int

        public int armorVsBludgeon; // int

        public int armorVsCold; // int

        public int armorVsFire; // int

        public int armorVsAcid; // int

        public int armorVsElectric; // int

        public int armorVsNether; // int

        public ArmorCache() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            baseArmor = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"baseArmor = {baseArmor}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            armorVsSlash = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"armorVsSlash = {armorVsSlash}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            armorVsPierce = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"armorVsPierce = {armorVsPierce}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            armorVsBludgeon = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"armorVsBludgeon = {armorVsBludgeon}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            armorVsCold = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"armorVsCold = {armorVsCold}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            armorVsFire = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"armorVsFire = {armorVsFire}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            armorVsAcid = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"armorVsAcid = {armorVsAcid}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            armorVsElectric = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"armorVsElectric = {armorVsElectric}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            armorVsNether = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"armorVsNether = {armorVsNether}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    public class BodyPartSelectionData : IACDataType {
        public int HLF; // int

        public int MLF; // int

        public int LLF; // int

        public int HRF; // int

        public int MRF; // int

        public int LRF; // int

        public int HLB; // int

        public int MLB; // int

        public int LLB; // int

        public int HRB; // int

        public int MRB; // int

        public int LRB; // int

        public BodyPartSelectionData() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            HLF = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"HLF = {HLF}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            MLF = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"MLF = {MLF}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            LLF = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"LLF = {LLF}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            HRF = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"HRF = {HRF}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            MRF = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"MRF = {MRF}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            LRF = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"LRF = {LRF}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            HLB = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"HLB = {HLB}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            MLB = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"MLB = {MLB}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            LLB = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"LLB = {LLB}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            HRB = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"HRB = {HRB}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            MRB = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"MRB = {MRB}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            LRB = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"LRB = {LRB}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// Contains information related to your spellbook
    /// </summary>
    public class SpellBook : IACDataType {
        /// <summary>
        /// Spells in the characters spellbook
        /// </summary>
        public PackableHashTable<LayeredSpellID, SpellBookPage> spellBook; // PackableHashTable

        public SpellBook() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            spellBook = new PackableHashTable<LayeredSpellID, SpellBookPage>();
            spellBook.ReadFromBuffer(buffer);
            Logger.Log($"spellBook = {spellBook}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// Contains information related to the spell in your spellbook
    /// </summary>
    public class SpellBookPage : IACDataType {
        /// <summary>
        /// Final value has 2.0 subtracted if network value &gt; 2.0.  Believe this is the charge of the spell which was unused later
        /// </summary>
        public float castingLikelihood; // float

        /// <summary>
        /// Client skips this value
        /// </summary>
        public int unknown; // int

        /// <summary>
        /// Replaces castingLikelihood
        /// </summary>
        public float castingLikelihood2; // float

        public SpellBookPage() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            castingLikelihood = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"castingLikelihood = {castingLikelihood}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            if (castingLikelihood < 2.0) {
                unknown = BinaryHelpers.ReadInt32(buffer); // int
                Logger.Log($"unknown = {unknown}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                castingLikelihood2 = BinaryHelpers.ReadSingle(buffer); // float
                Logger.Log($"castingLikelihood2 = {castingLikelihood2}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
        }

    }

    /// <summary>
    /// Contains information related to the spells in effect on the character
    /// </summary>
    public class EnchantmentRegistry : IACDataType {
        /// <summary>
        /// Enchantment mask.
        /// </summary>
        public uint enchantmentMask; // uint

        /// <summary>
        /// Life spells active on the player
        /// </summary>
        public PackableList<Enchantment> lifeSpells; // PackableList

        /// <summary>
        /// Creature spells active on the player
        /// </summary>
        public PackableList<Enchantment> creatureSpells; // PackableList

        /// <summary>
        /// Cooldown spells active on the player
        /// </summary>
        public PackableList<Enchantment> cooldowns; // PackableList

        /// <summary>
        /// Vitae Penalty.
        /// </summary>
        public Enchantment vitae; // Enchantment

        public EnchantmentRegistry() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            enchantmentMask = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"enchantmentMask = {enchantmentMask}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            if (((uint)enchantmentMask & 0x0001) != 0) {
                lifeSpells = new PackableList<Enchantment>();
                lifeSpells.ReadFromBuffer(buffer);
                Logger.Log($"lifeSpells = {lifeSpells}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)enchantmentMask & 0x0002) != 0) {
                creatureSpells = new PackableList<Enchantment>();
                creatureSpells.ReadFromBuffer(buffer);
                Logger.Log($"creatureSpells = {creatureSpells}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)enchantmentMask & 0x0008) != 0) {
                cooldowns = new PackableList<Enchantment>();
                cooldowns.ReadFromBuffer(buffer);
                Logger.Log($"cooldowns = {cooldowns}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)enchantmentMask & 0x0004) != 0) {
                vitae = new Enchantment();
                vitae.ReadFromBuffer(buffer);
                Logger.Log($"vitae = {vitae}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
        }

    }

    /// <summary>
    /// The Enchantment structure describes an active enchantment.
    /// </summary>
    public class Enchantment : IACDataType {
        /// <summary>
        /// the spell ID of the enchantment
        /// </summary>
        public LayeredSpellID id; // LayeredSpellID

        /// <summary>
        /// Value greater or equal to 1 means we read spellSetID
        /// </summary>
        public ushort hasSpellSetID; // ushort

        /// <summary>
        /// the family of related spells this enchantment belongs to
        /// </summary>
        public ushort spellCategory; // ushort

        /// <summary>
        /// the difficulty of the spell
        /// </summary>
        public uint powerLevel; // uint

        /// <summary>
        /// the amount of time this enchantment has been active
        /// </summary>
        public double startTime; // double

        /// <summary>
        /// the duration of the spell
        /// </summary>
        public double duration; // double

        /// <summary>
        /// the object ID of the creature or item that cast this enchantment
        /// </summary>
        public uint caster; // ObjectID

        /// <summary>
        /// unknown
        /// </summary>
        public float degradeModifier; // float

        /// <summary>
        /// unknown
        /// </summary>
        public float degradeLimit; // float

        /// <summary>
        /// the time when this enchantment was cast
        /// </summary>
        public double lastTimeDegraded; // double

        /// <summary>
        /// Stat modification information
        /// </summary>
        public StatMod smod; // StatMod

        /// <summary>
        /// Related to armor sets somehow?
        /// </summary>
        public uint spellSetID; // uint

        public Enchantment() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            id = new LayeredSpellID();
            id.ReadFromBuffer(buffer);
            Logger.Log($"id = {id}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            hasSpellSetID = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"hasSpellSetID = {hasSpellSetID}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            spellCategory = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"spellCategory = {spellCategory}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            powerLevel = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"powerLevel = {powerLevel}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            startTime = BinaryHelpers.ReadDouble(buffer); // double
            Logger.Log($"startTime = {startTime}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            duration = BinaryHelpers.ReadDouble(buffer); // double
            Logger.Log($"duration = {duration}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            caster = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"caster = {caster}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            degradeModifier = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"degradeModifier = {degradeModifier}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            degradeLimit = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"degradeLimit = {degradeLimit}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            lastTimeDegraded = BinaryHelpers.ReadDouble(buffer); // double
            Logger.Log($"lastTimeDegraded = {lastTimeDegraded}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            smod = new StatMod();
            smod.ReadFromBuffer(buffer);
            Logger.Log($"smod = {smod}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            if (hasSpellSetID > 0) {
                spellSetID = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"spellSetID = {spellSetID}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
        }

    }

    /// <summary>
    /// Information on stat modification
    /// </summary>
    public class StatMod : IACDataType {
        /// <summary>
        /// flags that indicate the type of effect the spell has
        /// </summary>
        public uint type; // uint

        /// <summary>
        /// along with flags, indicates which attribute is affected by the spell
        /// </summary>
        public uint key; // uint

        /// <summary>
        /// the effect value/amount
        /// </summary>
        public float value; // float

        public StatMod() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            type = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"type = {type}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            key = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"key = {key}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            value = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"value = {value}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// Contains a list of events to filter? Unknown what this does currently.
    /// </summary>
    public class EventFilter : IACDataType {
        /// <summary>
        /// List of events
        /// </summary>
        public PackableList<uint> eventFilter; // PackableList

        public EventFilter() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            eventFilter = new PackableList<uint>();
            eventFilter.ReadFromBuffer(buffer);
            Logger.Log($"eventFilter = {eventFilter}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// Contains a list of emotes for NPCs? Unknown what this does currently.
    /// </summary>
    public class EmoteTable : IACDataType {
        /// <summary>
        /// Key may be an EmoteCategory?
        /// </summary>
        public PackableHashTable<uint, EmoteSetList> emoteTable; // PackableHashTable

        public EmoteTable() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            emoteTable = new PackableHashTable<uint, EmoteSetList>();
            emoteTable.ReadFromBuffer(buffer);
            Logger.Log($"emoteTable = {emoteTable}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    public class EmoteSetList : IACDataType {
        /// <summary>
        /// List of emote sets
        /// </summary>
        public PackableList<EmoteSet> emoteSetList; // PackableList

        public EmoteSetList() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            emoteSetList = new PackableList<EmoteSet>();
            emoteSetList.ReadFromBuffer(buffer);
            Logger.Log($"emoteSetList = {emoteSetList}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    public class EmoteSet : IACDataType {
        public EmoteCategory category; // EmoteCategory

        public float probability; // float

        public uint classID; // uint

        public uint style; // uint

        public uint substyle; // uint

        public string quest; // string

        public uint vendorType; // uint

        public float minhealth; // float

        public float maxhealth; // float

        /// <summary>
        /// List of emotes
        /// </summary>
        public PackableList<Emote> emotes; // PackableList

        public EmoteSet() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            category = (EmoteCategory)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"category = {category}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            probability = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"probability = {probability}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            switch((int)category) {
                case 0x01:
                case 0x06:
                    classID = BinaryHelpers.ReadUInt32(buffer); // uint
                    Logger.Log($"classID = {classID}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0x05:
                    style = BinaryHelpers.ReadUInt32(buffer); // uint
                    Logger.Log($"style = {style}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    substyle = BinaryHelpers.ReadUInt32(buffer); // uint
                    Logger.Log($"substyle = {substyle}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0x0C:
                case 0x0D:
                case 0x16:
                case 0x17:
                case 0x1B:
                case 0x1C:
                case 0x1D:
                case 0x1E:
                case 0x1F:
                case 0x20:
                case 0x21:
                case 0x22:
                case 0x23:
                case 0x24:
                case 0x25:
                case 0x26:
                    quest = BinaryHelpers.ReadString(buffer); // string
                    Logger.Log($"quest = {quest}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0x02:
                    vendorType = BinaryHelpers.ReadUInt32(buffer); // uint
                    Logger.Log($"vendorType = {vendorType}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0x0F:
                    minhealth = BinaryHelpers.ReadSingle(buffer); // float
                    Logger.Log($"minhealth = {minhealth}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    maxhealth = BinaryHelpers.ReadSingle(buffer); // float
                    Logger.Log($"maxhealth = {maxhealth}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
            }
            emotes = new PackableList<Emote>();
            emotes.ReadFromBuffer(buffer);
            Logger.Log($"emotes = {emotes}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    public class Emote : IACDataType {
        public EmoteType type; // EmoteType

        public float delay; // float

        public float extent; // float

        public string msg; // string

        public uint amount; // uint

        public uint stat; // uint

        public double percent; // double

        public uint min; // uint

        public uint max; // uint

        public ulong amount64; // ulong

        public ulong heroxp64; // ulong

        public uint spellID; // uint

        public CreationProfile cprof; // CreationProfile

        /// <summary>
        /// Over 8 is invalid
        /// </summary>
        public int wealthRating; // int

        public int treasureClass; // int

        /// <summary>
        /// Valid values are 0 to 3 
        /// </summary>
        public int treasureType; // int

        public uint motion; // uint

        public Frame frame; // Frame

        public uint pscript; // uint

        public uint sound; // uint

        public string teststring; // string

        public ulong min64; // ulong

        public ulong max64; // ulong

        public double fmin; // double

        public double fmax; // double

        public bool display; // bool

        public Position position; // Position

        public Emote() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            type = (EmoteType)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"type = {type}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            delay = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"delay = {delay}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            extent = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"extent = {extent}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            switch((int)type) {
                case 0x01:
                case 0x08:
                case 0x0A:
                case 0x0D:
                case 0x10:
                case 0x11:
                case 0x12:
                case 0x14:
                case 0x15:
                case 0x16:
                case 0x17:
                case 0x18:
                case 0x19:
                case 0x1A:
                case 0x1F:
                case 0x33:
                case 0x3A:
                case 0x3C:
                case 0x3D:
                case 0x40:
                case 0x41:
                case 0x43:
                case 0x44:
                case 0x4F:
                case 0x50:
                case 0x51:
                case 0x53:
                case 0x58:
                case 0x79:
                    msg = BinaryHelpers.ReadString(buffer); // string
                    Logger.Log($"msg = {msg}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0x20:
                case 0x21:
                case 0x46:
                case 0x54:
                case 0x55:
                case 0x56:
                case 0x59:
                case 0x66:
                case 0x67:
                case 0x68:
                case 0x69:
                case 0x6A:
                case 0x6B:
                case 0x6C:
                case 0x6D:
                    msg = BinaryHelpers.ReadString(buffer); // string
                    Logger.Log($"msg = {msg}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    amount = BinaryHelpers.ReadUInt32(buffer); // uint
                    Logger.Log($"amount = {amount}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0x35:
                case 0x36:
                case 0x37:
                case 0x45:
                    stat = BinaryHelpers.ReadUInt32(buffer); // uint
                    Logger.Log($"stat = {stat}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    amount = BinaryHelpers.ReadUInt32(buffer); // uint
                    Logger.Log($"amount = {amount}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0x73:
                    stat = BinaryHelpers.ReadUInt32(buffer); // uint
                    Logger.Log($"stat = {stat}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0x76:
                    stat = BinaryHelpers.ReadUInt32(buffer); // uint
                    Logger.Log($"stat = {stat}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    percent = BinaryHelpers.ReadDouble(buffer); // double
                    Logger.Log($"percent = {percent}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0x1E:
                case 0x3B:
                case 0x47:
                case 0x52:
                    msg = BinaryHelpers.ReadString(buffer); // string
                    Logger.Log($"msg = {msg}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    min = BinaryHelpers.ReadUInt32(buffer); // uint
                    Logger.Log($"min = {min}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    max = BinaryHelpers.ReadUInt32(buffer); // uint
                    Logger.Log($"max = {max}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0x2:
                case 0x3E:
                    amount64 = BinaryHelpers.ReadUInt64(buffer); // ulong
                    Logger.Log($"amount64 = {amount64}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    heroxp64 = BinaryHelpers.ReadUInt64(buffer); // ulong
                    Logger.Log($"heroxp64 = {heroxp64}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0x70:
                case 0x71:
                    amount64 = BinaryHelpers.ReadUInt64(buffer); // ulong
                    Logger.Log($"amount64 = {amount64}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0x22:
                case 0x2F:
                case 0x30:
                case 0x5A:
                case 0x77:
                case 0x78:
                    amount = BinaryHelpers.ReadUInt32(buffer); // uint
                    Logger.Log($"amount = {amount}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0xE:
                case 0x13:
                case 0x1B:
                case 0x49:
                    spellID = BinaryHelpers.ReadUInt32(buffer); // uint
                    Logger.Log($"spellID = {spellID}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0x3:
                case 0x4A:
                    cprof = new CreationProfile();
                    cprof.ReadFromBuffer(buffer);
                    Logger.Log($"cprof = {cprof}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0x4C:
                    msg = BinaryHelpers.ReadString(buffer); // string
                    Logger.Log($"msg = {msg}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    cprof = new CreationProfile();
                    cprof.ReadFromBuffer(buffer);
                    Logger.Log($"cprof = {cprof}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0x38:
                    wealthRating = BinaryHelpers.ReadInt32(buffer); // int
                    Logger.Log($"wealthRating = {wealthRating}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    treasureClass = BinaryHelpers.ReadInt32(buffer); // int
                    Logger.Log($"treasureClass = {treasureClass}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    treasureType = BinaryHelpers.ReadInt32(buffer); // int
                    Logger.Log($"treasureType = {treasureType}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0x5:
                case 0x34:
                    motion = BinaryHelpers.ReadUInt32(buffer); // uint
                    Logger.Log($"motion = {motion}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0x4:
                case 0x6:
                case 0xB:
                case 0x57:
                    frame = new Frame();
                    frame.ReadFromBuffer(buffer);
                    Logger.Log($"frame = {frame}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0x7:
                    pscript = BinaryHelpers.ReadUInt32(buffer); // uint
                    Logger.Log($"pscript = {pscript}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0x9:
                    sound = BinaryHelpers.ReadUInt32(buffer); // uint
                    Logger.Log($"sound = {sound}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0x1C:
                case 0x1D:
                    amount = BinaryHelpers.ReadUInt32(buffer); // uint
                    Logger.Log($"amount = {amount}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    stat = BinaryHelpers.ReadUInt32(buffer); // uint
                    Logger.Log($"stat = {stat}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0x6E:
                    stat = BinaryHelpers.ReadUInt32(buffer); // uint
                    Logger.Log($"stat = {stat}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0x6F:
                    amount = BinaryHelpers.ReadUInt32(buffer); // uint
                    Logger.Log($"amount = {amount}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0x23:
                case 0x2D:
                case 0x2E:
                    msg = BinaryHelpers.ReadString(buffer); // string
                    Logger.Log($"msg = {msg}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    stat = BinaryHelpers.ReadUInt32(buffer); // uint
                    Logger.Log($"stat = {stat}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0x26:
                case 0x4B:
                    msg = BinaryHelpers.ReadString(buffer); // string
                    Logger.Log($"msg = {msg}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    teststring = BinaryHelpers.ReadString(buffer); // string
                    Logger.Log($"teststring = {teststring}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    stat = BinaryHelpers.ReadUInt32(buffer); // uint
                    Logger.Log($"stat = {stat}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0x24:
                case 0x27:
                case 0x28:
                case 0x29:
                case 0x2A:
                case 0x2B:
                case 0x2C:
                    msg = BinaryHelpers.ReadString(buffer); // string
                    Logger.Log($"msg = {msg}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    min = BinaryHelpers.ReadUInt32(buffer); // uint
                    Logger.Log($"min = {min}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    max = BinaryHelpers.ReadUInt32(buffer); // uint
                    Logger.Log($"max = {max}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    stat = BinaryHelpers.ReadUInt32(buffer); // uint
                    Logger.Log($"stat = {stat}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0x72:
                    msg = BinaryHelpers.ReadString(buffer); // string
                    Logger.Log($"msg = {msg}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    min64 = BinaryHelpers.ReadUInt64(buffer); // ulong
                    Logger.Log($"min64 = {min64}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    max64 = BinaryHelpers.ReadUInt64(buffer); // ulong
                    Logger.Log($"max64 = {max64}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    stat = BinaryHelpers.ReadUInt32(buffer); // uint
                    Logger.Log($"stat = {stat}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0x25:
                    msg = BinaryHelpers.ReadString(buffer); // string
                    Logger.Log($"msg = {msg}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    fmin = BinaryHelpers.ReadDouble(buffer); // double
                    Logger.Log($"fmin = {fmin}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    fmax = BinaryHelpers.ReadDouble(buffer); // double
                    Logger.Log($"fmax = {fmax}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    stat = BinaryHelpers.ReadUInt32(buffer); // uint
                    Logger.Log($"stat = {stat}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0x31:
                    percent = BinaryHelpers.ReadDouble(buffer); // double
                    Logger.Log($"percent = {percent}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    min64 = BinaryHelpers.ReadUInt64(buffer); // ulong
                    Logger.Log($"min64 = {min64}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    max64 = BinaryHelpers.ReadUInt64(buffer); // ulong
                    Logger.Log($"max64 = {max64}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0x32:
                    stat = BinaryHelpers.ReadUInt32(buffer); // uint
                    Logger.Log($"stat = {stat}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    percent = BinaryHelpers.ReadDouble(buffer); // double
                    Logger.Log($"percent = {percent}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    min = BinaryHelpers.ReadUInt32(buffer); // uint
                    Logger.Log($"min = {min}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    max = BinaryHelpers.ReadUInt32(buffer); // uint
                    Logger.Log($"max = {max}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    display = BinaryHelpers.ReadBool(buffer); // bool
                    Logger.Log($"display = {display}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0x3F:
                case 0x63:
                case 0x64:
                    position = new Position();
                    position.ReadFromBuffer(buffer);
                    Logger.Log($"position = {position}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
            }
        }

    }

    /// <summary>
    /// Set information about an item for creation
    /// </summary>
    public class CreationProfile : IACDataType {
        public uint wcid; // uint

        public uint palette; // uint

        public float shade; // float

        public uint destination; // uint

        public int stackSize; // int

        public bool tryToBond; // bool

        public CreationProfile() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            wcid = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"wcid = {wcid}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            palette = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"palette = {palette}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            shade = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"shade = {shade}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            destination = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"destination = {destination}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            stackSize = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"stackSize = {stackSize}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            tryToBond = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"tryToBond = {tryToBond}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// List of pages in a book
    /// </summary>
    public class PageDataList : IACDataType {
        public uint maxNumPages; // uint

        public uint maxNumCharsPerPage; // uint

        /// <summary>
        /// List of pages
        /// </summary>
        public PackableList<PageData> pages; // PackableList

        public PageDataList() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            maxNumPages = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"maxNumPages = {maxNumPages}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            maxNumCharsPerPage = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"maxNumCharsPerPage = {maxNumCharsPerPage}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            pages = new PackableList<PageData>();
            pages.ReadFromBuffer(buffer);
            Logger.Log($"pages = {pages}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// Data for an individual page
    /// </summary>
    public class PageData : IACDataType {
        public uint authorID; // ObjectID

        public string authorName; // string

        public string authorAccount; // string

        /// <summary>
        /// if HIWORD is not 0xFFFF, this is textIncluded. For our purpose this should always be 0xFFFF0002
        /// </summary>
        public uint version; // uint

        public bool textIncluded; // bool

        public bool ignoreAuthor; // bool

        public string pageText; // string

        public PageData() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            authorID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"authorID = {authorID}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            authorName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"authorName = {authorName}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            authorAccount = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"authorAccount = {authorAccount}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            version = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"version = {version}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            textIncluded = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"textIncluded = {textIncluded}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            ignoreAuthor = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"ignoreAuthor = {ignoreAuthor}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            if (textIncluded) {
                pageText = BinaryHelpers.ReadString(buffer); // string
                Logger.Log($"pageText = {pageText}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
        }

    }

    public class GeneratorTable : IACDataType {
        /// <summary>
        /// List of generator profiles
        /// </summary>
        public PackableList<GeneratorProfile> generators; // PackableList

        public GeneratorTable() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            generators = new PackableList<GeneratorProfile>();
            generators.ReadFromBuffer(buffer);
            Logger.Log($"generators = {generators}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    public class GeneratorProfile : IACDataType {
        public float probability; // float

        public uint typeID; // uint

        public double delay; // double

        public uint initCreate; // uint

        public uint maxNum; // uint

        public uint whenCreate; // uint

        public uint whereCreate; // uint

        public uint stackSize; // uint

        public uint ptid; // uint

        public float shade; // float

        public Position posVal; // Position

        public uint slot; // uint

        public GeneratorProfile() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            probability = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"probability = {probability}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            typeID = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"typeID = {typeID}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            delay = BinaryHelpers.ReadDouble(buffer); // double
            Logger.Log($"delay = {delay}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            initCreate = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"initCreate = {initCreate}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            maxNum = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"maxNum = {maxNum}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            whenCreate = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"whenCreate = {whenCreate}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            whereCreate = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"whereCreate = {whereCreate}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            stackSize = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"stackSize = {stackSize}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            ptid = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"ptid = {ptid}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            shade = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"shade = {shade}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            posVal = new Position();
            posVal.ReadFromBuffer(buffer);
            Logger.Log($"posVal = {posVal}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            slot = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"slot = {slot}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    public class GeneratorRegistry : IACDataType {
        public PackableHashTable<uint, GeneratorRegistryNode> registry; // PackableHashTable

        public GeneratorRegistry() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            registry = new PackableHashTable<uint, GeneratorRegistryNode>();
            registry.ReadFromBuffer(buffer);
            Logger.Log($"registry = {registry}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    public class GeneratorRegistryNode : IACDataType {
        public uint wcidOrTtype; // uint

        public double ts; // double

        public uint treasureType; // uint

        public uint slot; // uint

        public uint checkpointed; // uint

        public uint shop; // uint

        public uint amount; // uint

        public GeneratorRegistryNode() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            wcidOrTtype = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"wcidOrTtype = {wcidOrTtype}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            ts = BinaryHelpers.ReadDouble(buffer); // double
            Logger.Log($"ts = {ts}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            treasureType = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"treasureType = {treasureType}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            slot = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"slot = {slot}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            checkpointed = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"checkpointed = {checkpointed}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            shop = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"shop = {shop}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            amount = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"amount = {amount}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// Set of inventory items
    /// </summary>
    public class GeneratorQueue : IACDataType {
        public PackableList<GeneratorQueueNode> queue; // PackableList

        public GeneratorQueue() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            queue = new PackableList<GeneratorQueueNode>();
            queue.ReadFromBuffer(buffer);
            Logger.Log($"queue = {queue}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    public class GeneratorQueueNode : IACDataType {
        public uint slot; // uint

        public double when; // double

        public GeneratorQueueNode() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            slot = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"slot = {slot}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            when = BinaryHelpers.ReadDouble(buffer); // double
            Logger.Log($"when = {when}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// The PlayerModule structure contains character options.
    /// </summary>
    public class PlayerModule : IACDataType {
        public uint flags; // uint

        /// <summary>
        /// The options in the Character tab (F11 by default)
        /// </summary>
        public CharacterOptions1 options; // CharacterOptions1

        /// <summary>
        /// Shortcut items.
        /// </summary>
        public ShortCutManager shortCutManager; // ShortCutManager

        /// <summary>
        /// Spells in the spell tab
        /// </summary>
        public SpellTab tab1; // SpellTab

        /// <summary>
        /// Spells in the spell tab
        /// </summary>
        public SpellTab tab2; // SpellTab

        /// <summary>
        /// Spells in the spell tab
        /// </summary>
        public SpellTab tab3; // SpellTab

        /// <summary>
        /// Spells in the spell tab
        /// </summary>
        public SpellTab tab4; // SpellTab

        /// <summary>
        /// Spells in the spell tab
        /// </summary>
        public SpellTab tab5; // SpellTab

        /// <summary>
        /// Spells in the spell tab
        /// </summary>
        public SpellTab tab6; // SpellTab

        /// <summary>
        /// Spells in the spell tab
        /// </summary>
        public SpellTab tab7; // SpellTab

        /// <summary>
        /// Spells in the spell tab
        /// </summary>
        public SpellTab tab8; // SpellTab

        /// <summary>
        /// Lists component type and the desired level
        /// </summary>
        public PackableHashTable<DataID, uint> desiredComps; // PackableHashTable

        /// <summary>
        /// Defaults to 0x3FFF if not present
        /// </summary>
        public SpellBookFilterOptions spellFilters; // SpellBookFilterOptions

        /// <summary>
        /// Character options second batch, defaults to 9733888 if not present
        /// </summary>
        public CharacterOptions2 options2; // CharacterOptions2

        public string timestampFormat; // string

        public GenericQualitiesData genericQualities; // GenericQualitiesData

        public PlayerModule() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            flags = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"flags = {flags}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            options = (CharacterOptions1)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"options = {options}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            if (((uint)flags & 0x00000001) != 0) {
                shortCutManager = new ShortCutManager();
                shortCutManager.ReadFromBuffer(buffer);
                Logger.Log($"shortCutManager = {shortCutManager}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            tab1 = new SpellTab();
            tab1.ReadFromBuffer(buffer);
            Logger.Log($"tab1 = {tab1}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            if (((uint)flags & 0x00000004) != 0) {
                tab2 = new SpellTab();
                tab2.ReadFromBuffer(buffer);
                Logger.Log($"tab2 = {tab2}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                tab3 = new SpellTab();
                tab3.ReadFromBuffer(buffer);
                Logger.Log($"tab3 = {tab3}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                tab4 = new SpellTab();
                tab4.ReadFromBuffer(buffer);
                Logger.Log($"tab4 = {tab4}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                tab5 = new SpellTab();
                tab5.ReadFromBuffer(buffer);
                Logger.Log($"tab5 = {tab5}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000010) != 0) {
                tab2 = new SpellTab();
                tab2.ReadFromBuffer(buffer);
                Logger.Log($"tab2 = {tab2}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                tab3 = new SpellTab();
                tab3.ReadFromBuffer(buffer);
                Logger.Log($"tab3 = {tab3}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                tab4 = new SpellTab();
                tab4.ReadFromBuffer(buffer);
                Logger.Log($"tab4 = {tab4}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                tab5 = new SpellTab();
                tab5.ReadFromBuffer(buffer);
                Logger.Log($"tab5 = {tab5}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                tab6 = new SpellTab();
                tab6.ReadFromBuffer(buffer);
                Logger.Log($"tab6 = {tab6}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                tab7 = new SpellTab();
                tab7.ReadFromBuffer(buffer);
                Logger.Log($"tab7 = {tab7}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000400) != 0) {
                tab2 = new SpellTab();
                tab2.ReadFromBuffer(buffer);
                Logger.Log($"tab2 = {tab2}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                tab3 = new SpellTab();
                tab3.ReadFromBuffer(buffer);
                Logger.Log($"tab3 = {tab3}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                tab4 = new SpellTab();
                tab4.ReadFromBuffer(buffer);
                Logger.Log($"tab4 = {tab4}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                tab5 = new SpellTab();
                tab5.ReadFromBuffer(buffer);
                Logger.Log($"tab5 = {tab5}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                tab6 = new SpellTab();
                tab6.ReadFromBuffer(buffer);
                Logger.Log($"tab6 = {tab6}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                tab7 = new SpellTab();
                tab7.ReadFromBuffer(buffer);
                Logger.Log($"tab7 = {tab7}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                tab8 = new SpellTab();
                tab8.ReadFromBuffer(buffer);
                Logger.Log($"tab8 = {tab8}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000008) != 0) {
                desiredComps = new PackableHashTable<DataID, uint>();
                desiredComps.ReadFromBuffer(buffer);
                Logger.Log($"desiredComps = {desiredComps}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000020) != 0) {
                spellFilters = (SpellBookFilterOptions)BinaryHelpers.ReadUInt32(buffer);
                Logger.Log($"spellFilters = {spellFilters}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000040) != 0) {
                options2 = (CharacterOptions2)BinaryHelpers.ReadUInt32(buffer);
                Logger.Log($"options2 = {options2}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000080) != 0) {
                timestampFormat = BinaryHelpers.ReadString(buffer); // string
                Logger.Log($"timestampFormat = {timestampFormat}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000100) != 0) {
                genericQualities = new GenericQualitiesData();
                genericQualities.ReadFromBuffer(buffer);
                Logger.Log($"genericQualities = {genericQualities}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
        }

    }

    /// <summary>
    /// Set of shortcuts
    /// </summary>
    public class ShortCutManager : IACDataType {
        /// <summary>
        /// List of short cuts.
        /// </summary>
        public PackableList<ShortCutData> shortcuts; // PackableList

        public ShortCutManager() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            shortcuts = new PackableList<ShortCutData>();
            shortcuts.ReadFromBuffer(buffer);
            Logger.Log($"shortcuts = {shortcuts}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// Shortcut
    /// </summary>
    public class ShortCutData : IACDataType {
        /// <summary>
        /// Position
        /// </summary>
        public uint index; // uint

        /// <summary>
        /// Object ID
        /// </summary>
        public uint objectID; // ObjectID

        /// <summary>
        /// May not have been used in prod?  Maybe a remnet of before spell tabs?  I don&#39;t think you could put spells in shortcut spot...
        /// </summary>
        public LayeredSpellID spellID; // LayeredSpellID

        public ShortCutData() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            index = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"index = {index}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            objectID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectID = {objectID}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            spellID = new LayeredSpellID();
            spellID.ReadFromBuffer(buffer);
            Logger.Log($"spellID = {spellID}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// List of spells in spell tab
    /// </summary>
    public class SpellTab : IACDataType {
        /// <summary>
        /// List of spells on tab.
        /// </summary>
        public PackableList<LayeredSpellID> spells; // PackableList

        public SpellTab() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            spells = new PackableList<LayeredSpellID>();
            spells.ReadFromBuffer(buffer);
            Logger.Log($"spells = {spells}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    public class GenericQualitiesData : IACDataType {
        public uint flags; // uint

        public PackableHashTable<uint, uint> optionInts; // PackableHashTable

        public PackableHashTable<uint, bool> optionBools; // PackableHashTable

        public PackableHashTable<uint, float> optionFloats; // PackableHashTable

        public PackableHashTable<uint, string> optionStrings; // PackableHashTable

        public GenericQualitiesData() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            flags = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"flags = {flags}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            if (((uint)flags & 0x00000001) != 0) {
                optionInts = new PackableHashTable<uint, uint>();
                optionInts.ReadFromBuffer(buffer);
                Logger.Log($"optionInts = {optionInts}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000002) != 0) {
                optionBools = new PackableHashTable<uint, bool>();
                optionBools.ReadFromBuffer(buffer);
                Logger.Log($"optionBools = {optionBools}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000004) != 0) {
                optionFloats = new PackableHashTable<uint, float>();
                optionFloats.ReadFromBuffer(buffer);
                Logger.Log($"optionFloats = {optionFloats}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000008) != 0) {
                optionStrings = new PackableHashTable<uint, string>();
                optionStrings.ReadFromBuffer(buffer);
                Logger.Log($"optionStrings = {optionStrings}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
        }

    }

    /// <summary>
    /// Set of inventory items
    /// </summary>
    public class ContentProfile : IACDataType {
        public uint iid; // ObjectID

        /// <summary>
        /// Whether or not this object is a container.
        /// </summary>
        public ContainerProperties containerProperties; // ContainerProperties

        public ContentProfile() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            iid = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"iid = {iid}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            containerProperties = (ContainerProperties)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"containerProperties = {containerProperties}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// Set of inventory items
    /// </summary>
    public class InventoryPlacement : IACDataType {
        public uint iid; // ObjectID

        public EquipMask loc; // EquipMask

        public uint priority; // uint

        public InventoryPlacement() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            iid = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"iid = {iid}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            loc = (EquipMask)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"loc = {loc}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            priority = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"priority = {priority}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// Allegience information
    /// </summary>
    public class AllegianceProfile : IACDataType {
        /// <summary>
        /// The number of allegiance members.
        /// </summary>
        public uint totalMembers; // uint

        /// <summary>
        /// Your personal number of followers.
        /// </summary>
        public uint totalVassals; // uint

        public AllegianceHierarchy allegianceHierarchy; // AllegianceHierarchy

        public AllegianceProfile() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            totalMembers = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"totalMembers = {totalMembers}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            totalVassals = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"totalVassals = {totalVassals}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            allegianceHierarchy = new AllegianceHierarchy();
            allegianceHierarchy.ReadFromBuffer(buffer);
            Logger.Log($"allegianceHierarchy = {allegianceHierarchy}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// Allegience hierarchy information
    /// </summary>
    public class AllegianceHierarchy : IACDataType {
        /// <summary>
        /// Number of character allegiance records.
        /// </summary>
        public ushort recordCount; // ushort

        /// <summary>
        /// Defines which properties are available. 0x0B seems to be the latest version which includes all properties.
        /// </summary>
        public ushort oldVersion; // ushort

        /// <summary>
        /// Taking a guess on these values.  Guessing they may only be valid on Monarchs.
        /// </summary>
        public PHashTable<uint, AllegianceOfficerLevel> officers; // PHashTable

        /// <summary>
        /// Believe these may pass in the current officer title list.  Guessing they may only be valid on Monarchs.
        /// </summary>
        public PSmartArray<string> officerTitles; // PSmartArray

        /// <summary>
        /// May only be valid for Monarchs/Speakers?
        /// </summary>
        public uint monarchBroadcastTime; // uint

        /// <summary>
        /// May only be valid for Monarchs/Speakers?
        /// </summary>
        public uint monarchBroadcastsToday; // uint

        /// <summary>
        /// May only be valid for Monarchs/Speakers?
        /// </summary>
        public uint spokesBroadcastTime; // uint

        /// <summary>
        /// May only be valid for Monarchs/Speakers?
        /// </summary>
        public uint spokesBroadcastsToday; // uint

        /// <summary>
        /// Text for current motd. May only be valid for Monarchs/Speakers?
        /// </summary>
        public string motd; // string

        /// <summary>
        /// Who set the current motd. May only be valid for Monarchs/Speakers?
        /// </summary>
        public string motdSetBy; // string

        /// <summary>
        /// allegiance chat channel number
        /// </summary>
        public uint chatRoomID; // uint

        /// <summary>
        /// Location of monarchy bindpoint
        /// </summary>
        public Position bindpoint; // Position

        /// <summary>
        /// The name of the allegiance.
        /// </summary>
        public string allegianceName; // string

        /// <summary>
        /// Time name was last set.  Seems to count upward for some reason.
        /// </summary>
        public uint nameLastSetTime; // uint

        /// <summary>
        /// Whether allegiance is locked.
        /// </summary>
        public bool isLocked; // bool

        public int approvedVassal; // int

        /// <summary>
        /// Monarch&#39;s data
        /// </summary>
        public AllegianceData monarchData; // AllegianceData

        public class RecordVectorItem {
            /// <summary>
            /// The Object ID for the parent character to this character.  Used by the client to decide how to build the display in the Allegiance tab. 1 is the monarch.
            /// </summary>
            public uint treeParent;

            public AllegianceData allegianceData;

        }

        /// <summary>
        /// Subclass to hold records data
        /// </summary>
        public List<RecordVectorItem> records { get; set; } = new List<RecordVectorItem>(); // test

        public AllegianceHierarchy() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            recordCount = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"recordCount = {recordCount}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            oldVersion = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"oldVersion = {oldVersion}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            officers = new PHashTable<uint, AllegianceOfficerLevel>();
            officers.ReadFromBuffer(buffer);
            Logger.Log($"officers = {officers}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            officerTitles = new PSmartArray<string>();
            officerTitles.ReadFromBuffer(buffer);
            Logger.Log($"officerTitles = {officerTitles}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            monarchBroadcastTime = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"monarchBroadcastTime = {monarchBroadcastTime}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            monarchBroadcastsToday = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"monarchBroadcastsToday = {monarchBroadcastsToday}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            spokesBroadcastTime = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"spokesBroadcastTime = {spokesBroadcastTime}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            spokesBroadcastsToday = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"spokesBroadcastsToday = {spokesBroadcastsToday}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            motd = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"motd = {motd}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            motdSetBy = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"motdSetBy = {motdSetBy}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            chatRoomID = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"chatRoomID = {chatRoomID}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            bindpoint = new Position();
            bindpoint.ReadFromBuffer(buffer);
            Logger.Log($"bindpoint = {bindpoint}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            allegianceName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"allegianceName = {allegianceName}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            nameLastSetTime = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"nameLastSetTime = {nameLastSetTime}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            isLocked = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"isLocked = {isLocked}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            approvedVassal = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"approvedVassal = {approvedVassal}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            monarchData = new AllegianceData();
            monarchData.ReadFromBuffer(buffer);
            Logger.Log($"monarchData = {monarchData}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            for (var i=0; i < recordCount - 1; i++) {
                var treeParent = BinaryHelpers.ReadUInt32(buffer); // ObjectID
                Logger.Log($"treeParent = {treeParent}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                var allegianceData = new AllegianceData();
                allegianceData.ReadFromBuffer(buffer);
                Logger.Log($"allegianceData = {allegianceData}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                records.Add(new RecordVectorItem() {
                    treeParent = treeParent,
                    allegianceData = allegianceData,
                });
            }
        }

    }

    /// <summary>
    /// Set of allegiance data for a specific player
    /// </summary>
    public class AllegianceData : IACDataType {
        /// <summary>
        /// Character ID
        /// </summary>
        public uint characterID; // ObjectID

        /// <summary>
        /// XP gained while logged off
        /// </summary>
        public uint cpCached; // uint

        /// <summary>
        /// Total allegiance XP contribution.
        /// </summary>
        public uint cpTithed; // uint

        public uint bitfield; // uint

        /// <summary>
        /// The gender of the character (for determining title).
        /// </summary>
        public Gender gender; // Gender

        /// <summary>
        /// The heritage of the character (for determining title).
        /// </summary>
        public HeritageGroup hg; // HeritageGroup

        /// <summary>
        /// The numerical rank (1 is lowest).
        /// </summary>
        public ushort rank; // ushort

        public uint level; // uint

        /// <summary>
        /// Character loyalty.
        /// </summary>
        public ushort loyalty; // ushort

        /// <summary>
        /// Character leadership.
        /// </summary>
        public ushort leadership; // ushort

        public ulong timeOnline; // ulong

        public uint allegianceAge; // uint

        public string name; // string

        public AllegianceData() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            characterID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"characterID = {characterID}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            cpCached = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"cpCached = {cpCached}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            cpTithed = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"cpTithed = {cpTithed}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            bitfield = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"bitfield = {bitfield}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            gender = (Gender)BinaryHelpers.ReadByte(buffer);
            Logger.Log($"gender = {gender}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            hg = (HeritageGroup)BinaryHelpers.ReadByte(buffer);
            Logger.Log($"hg = {hg}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            rank = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"rank = {rank}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            if (((uint)bitfield & 0x8) != 0) {
                level = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"level = {level}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            loyalty = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"loyalty = {loyalty}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            leadership = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"leadership = {leadership}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            if (bitfield == 0x4) {
                timeOnline = BinaryHelpers.ReadUInt64(buffer); // ulong
                Logger.Log($"timeOnline = {timeOnline}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            else {
                timeOnline = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"timeOnline = {timeOnline}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                allegianceAge = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"allegianceAge = {allegianceAge}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            name = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"name = {name}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    public class FriendData : IACDataType {
        /// <summary>
        /// Friend&#39;s ID
        /// </summary>
        public uint id; // ObjectID

        /// <summary>
        /// Whether this friend is online
        /// </summary>
        public bool online; // bool

        /// <summary>
        /// Whether the friend should appear to be offline
        /// </summary>
        public bool appearOffline; // bool

        /// <summary>
        /// Name of the friend
        /// </summary>
        public string name; // string

        /// <summary>
        /// The people on this player&#39;s friends list
        /// </summary>
        public PList<uint> friends; // PList

        /// <summary>
        /// The people who have this player on their friends list
        /// </summary>
        public PList<uint> friendsOf; // PList

        public FriendData() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            id = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"id = {id}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            online = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"online = {online}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            appearOffline = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"appearOffline = {appearOffline}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            name = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"name = {name}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            friends = new PList<uint>();
            friends.ReadFromBuffer(buffer);
            Logger.Log($"friends = {friends}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            friendsOf = new PList<uint>();
            friendsOf.ReadFromBuffer(buffer);
            Logger.Log($"friendsOf = {friendsOf}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// Data related to an item, namely the amount and description
    /// </summary>
    public class ItemProfile : IACDataType {
        public uint packedAmount; // uint

        /// <summary>
        /// Derived from packedAmount. the number of items for sale (-1 for an unlimited supply)
        /// </summary>
        public int amount { get => (int)(packedAmount & 0xFFFFFF); } // int

        /// <summary>
        /// Derived from packedAmount. flag indicating whether the new or old PublicWeenieDesc is used
        /// </summary>
        public int pwdType { get => (int)(packedAmount >> 24); } // int

        /// <summary>
        /// the object ID of the item
        /// </summary>
        public uint objectID; // ObjectID

        /// <summary>
        /// details about the item
        /// </summary>
        public PublicWeenieDesc pwd; // PublicWeenieDesc

        /// <summary>
        /// details about the item
        /// </summary>
        public OldPublicWeenieDesc opwd; // OldPublicWeenieDesc

        public ItemProfile() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            packedAmount = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"packedAmount = {packedAmount}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            objectID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"objectID = {objectID}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            switch((int)pwdType) {
                case -1:
                    pwd = new PublicWeenieDesc();
                    pwd.ReadFromBuffer(buffer);
                    Logger.Log($"pwd = {pwd}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 1:
                    opwd = new OldPublicWeenieDesc();
                    opwd.ReadFromBuffer(buffer);
                    Logger.Log($"opwd = {opwd}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
            }
        }

    }

    /// <summary>
    /// The PublicWeenieDesc structure defines an object&#39;s game behavior.
    /// </summary>
    public class PublicWeenieDesc : IACDataType {
        /// <summary>
        /// game data flags
        /// </summary>
        public uint header; // uint

        /// <summary>
        /// object name
        /// </summary>
        public string name; // string

        /// <summary>
        /// object weenie class id
        /// </summary>
        public uint weenieClassID; // PackedDWORD

        /// <summary>
        /// icon ResourceID (minus 0x06000000)
        /// </summary>
        public uint icon; // PackedDWORD

        /// <summary>
        /// object type
        /// </summary>
        public ObjectType type; // ObjectType

        /// <summary>
        /// object behaviors
        /// </summary>
        public ObjectDescriptionFlag bitfield; // ObjectDescriptionFlag

        /// <summary>
        /// additional game data flags
        /// </summary>
        public uint header2; // uint

        /// <summary>
        /// plural object name (if not specified, use &lt;name&gt; followed by &#39;s&#39; or &#39;es&#39;)
        /// </summary>
        public string pluralName; // string

        /// <summary>
        /// number of item slots
        /// </summary>
        public byte itemsCapacity; // byte

        /// <summary>
        /// number of pack slots
        /// </summary>
        public byte containerCapacity; // byte

        /// <summary>
        /// missile ammunition type
        /// </summary>
        public AmmoType ammunitionType; // AmmoType

        /// <summary>
        /// object value
        /// </summary>
        public uint value; // uint

        public UsableType useability; // UsableType

        /// <summary>
        /// distance a player will walk to use an object
        /// </summary>
        public float useRadius; // float

        /// <summary>
        /// the object categories this object may be used on
        /// </summary>
        public ObjectType targetType; // ObjectType

        /// <summary>
        /// the type of highlight (outline) applied to the object&#39;s icon
        /// </summary>
        public IconHighlight effects; // IconHighlight

        /// <summary>
        /// the type of wieldable item this is
        /// </summary>
        public WieldType combatUse; // WieldType

        /// <summary>
        /// the number of uses remaining for this item (also salvage quantity)
        /// </summary>
        public ushort structure; // ushort

        /// <summary>
        /// the maximum number of uses possible for this item (also maximum salvage quantity)
        /// </summary>
        public ushort maxStructure; // ushort

        /// <summary>
        /// the number of items in this stack of objects
        /// </summary>
        public ushort stackSize; // ushort

        /// <summary>
        /// the maximum number of items possible in this stack of objects
        /// </summary>
        public ushort maxStackSize; // ushort

        /// <summary>
        /// the ID of the container holding this object
        /// </summary>
        public uint containerID; // ObjectID

        /// <summary>
        /// the ID of the creature equipping this object
        /// </summary>
        public uint wielderID; // ObjectID

        /// <summary>
        /// the potential equipment slots this object may be placed in
        /// </summary>
        public EquipMask validLocations; // EquipMask

        /// <summary>
        /// the actual equipment slots this object is currently placed in
        /// </summary>
        public EquipMask location; // EquipMask

        /// <summary>
        /// the parts of the body this object protects
        /// </summary>
        public CoverageMask priority; // CoverageMask

        /// <summary>
        /// radar dot color
        /// </summary>
        public byte blipColor; // byte

        /// <summary>
        /// radar type
        /// </summary>
        public byte radarEnum; // byte

        public ushort physicsScript; // ushort

        /// <summary>
        /// object workmanship
        /// </summary>
        public float workmanship; // float

        /// <summary>
        /// total burden of this object
        /// </summary>
        public ushort burden; // ushort

        /// <summary>
        /// the spell cast by this object
        /// </summary>
        public ushort spell; // SpellID

        /// <summary>
        /// the owner of this object
        /// </summary>
        public uint owner; // ObjectID

        /// <summary>
        /// the access control list for this dwelling object
        /// </summary>
        public RestrictionDB restrictions; // RestrictionDB

        /// <summary>
        /// what type of dwelling hook is this
        /// </summary>
        public HookType hookItemTypes; // HookType

        /// <summary>
        /// this player&#39;s monarch
        /// </summary>
        public uint monarch; // ObjectID

        /// <summary>
        /// the types of hooks this object may be placed on (-1 for hooks)
        /// </summary>
        public HookType hookType; // HookType

        /// <summary>
        /// icon overlay ResourceID (minus 0x06000000)
        /// </summary>
        public uint iconOverlay; // PackedDWORD

        /// <summary>
        /// icon underlay ResourceID (minus 0x06000000)
        /// </summary>
        public uint iconUnderlay; // PackedDWORD

        /// <summary>
        /// the type of material this object is made of
        /// </summary>
        public MaterialType material; // MaterialType

        public uint cooldownID; // uint

        public ulong cooldownDuration; // ulong

        public uint petOwner; // ObjectID

        public PublicWeenieDesc() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            header = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"header = {header}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            name = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"name = {name}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            weenieClassID = BinaryHelpers.ReadPackedDWORD(buffer); // PackedDWORD
            Logger.Log($"weenieClassID = {weenieClassID}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            icon = BinaryHelpers.ReadPackedDWORD(buffer); // PackedDWORD
            Logger.Log($"icon = {icon}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            type = (ObjectType)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"type = {type}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            bitfield = (ObjectDescriptionFlag)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"bitfield = {bitfield}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            if ((buffer.BaseStream.Position % 4) != 0) { // align DWORD
                buffer.BaseStream.Position += 4 - (buffer.BaseStream.Position % 4);
            }
            if (((uint)bitfield & 0x04000000) != 0) {
                header2 = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"header2 = {header2}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00000001) != 0) {
                pluralName = BinaryHelpers.ReadString(buffer); // string
                Logger.Log($"pluralName = {pluralName}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00000002) != 0) {
                itemsCapacity = BinaryHelpers.ReadByte(buffer); // byte
                Logger.Log($"itemsCapacity = {itemsCapacity}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00000004) != 0) {
                containerCapacity = BinaryHelpers.ReadByte(buffer); // byte
                Logger.Log($"containerCapacity = {containerCapacity}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00000100) != 0) {
                ammunitionType = (AmmoType)BinaryHelpers.ReadUInt16(buffer);
                Logger.Log($"ammunitionType = {ammunitionType}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00000008) != 0) {
                value = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"value = {value}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00000010) != 0) {
                useability = (UsableType)BinaryHelpers.ReadUInt32(buffer);
                Logger.Log($"useability = {useability}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00000020) != 0) {
                useRadius = BinaryHelpers.ReadSingle(buffer); // float
                Logger.Log($"useRadius = {useRadius}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00080000) != 0) {
                targetType = (ObjectType)BinaryHelpers.ReadUInt32(buffer);
                Logger.Log($"targetType = {targetType}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00000080) != 0) {
                effects = (IconHighlight)BinaryHelpers.ReadUInt32(buffer);
                Logger.Log($"effects = {effects}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00000200) != 0) {
                combatUse = (WieldType)BinaryHelpers.ReadByte(buffer);
                Logger.Log($"combatUse = {combatUse}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00000400) != 0) {
                structure = BinaryHelpers.ReadUInt16(buffer); // ushort
                Logger.Log($"structure = {structure}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00000800) != 0) {
                maxStructure = BinaryHelpers.ReadUInt16(buffer); // ushort
                Logger.Log($"maxStructure = {maxStructure}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00001000) != 0) {
                stackSize = BinaryHelpers.ReadUInt16(buffer); // ushort
                Logger.Log($"stackSize = {stackSize}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00002000) != 0) {
                maxStackSize = BinaryHelpers.ReadUInt16(buffer); // ushort
                Logger.Log($"maxStackSize = {maxStackSize}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00004000) != 0) {
                containerID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
                Logger.Log($"containerID = {containerID}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00008000) != 0) {
                wielderID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
                Logger.Log($"wielderID = {wielderID}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00010000) != 0) {
                validLocations = (EquipMask)BinaryHelpers.ReadUInt32(buffer);
                Logger.Log($"validLocations = {validLocations}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00020000) != 0) {
                location = (EquipMask)BinaryHelpers.ReadUInt32(buffer);
                Logger.Log($"location = {location}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00040000) != 0) {
                priority = (CoverageMask)BinaryHelpers.ReadUInt32(buffer);
                Logger.Log($"priority = {priority}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00100000) != 0) {
                blipColor = BinaryHelpers.ReadByte(buffer); // byte
                Logger.Log($"blipColor = {blipColor}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00800000) != 0) {
                radarEnum = BinaryHelpers.ReadByte(buffer); // byte
                Logger.Log($"radarEnum = {radarEnum}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x08000000) != 0) {
                physicsScript = BinaryHelpers.ReadUInt16(buffer); // ushort
                Logger.Log($"physicsScript = {physicsScript}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x01000000) != 0) {
                workmanship = BinaryHelpers.ReadSingle(buffer); // float
                Logger.Log($"workmanship = {workmanship}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00200000) != 0) {
                burden = BinaryHelpers.ReadUInt16(buffer); // ushort
                Logger.Log($"burden = {burden}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00400000) != 0) {
                spell = BinaryHelpers.ReadUInt16(buffer); // SpellID
                Logger.Log($"spell = {spell}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x02000000) != 0) {
                owner = BinaryHelpers.ReadUInt32(buffer); // ObjectID
                Logger.Log($"owner = {owner}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x04000000) != 0) {
                restrictions = new RestrictionDB();
                restrictions.ReadFromBuffer(buffer);
                Logger.Log($"restrictions = {restrictions}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x20000000) != 0) {
                hookItemTypes = (HookType)BinaryHelpers.ReadUInt16(buffer);
                Logger.Log($"hookItemTypes = {hookItemTypes}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00000040) != 0) {
                monarch = BinaryHelpers.ReadUInt32(buffer); // ObjectID
                Logger.Log($"monarch = {monarch}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x10000000) != 0) {
                hookType = (HookType)BinaryHelpers.ReadUInt16(buffer);
                Logger.Log($"hookType = {hookType}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x40000000) != 0) {
                iconOverlay = BinaryHelpers.ReadPackedDWORD(buffer); // PackedDWORD
                Logger.Log($"iconOverlay = {iconOverlay}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header2 & 0x00000001) != 0) {
                iconUnderlay = BinaryHelpers.ReadPackedDWORD(buffer); // PackedDWORD
                Logger.Log($"iconUnderlay = {iconUnderlay}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x80000000) != 0) {
                material = (MaterialType)BinaryHelpers.ReadUInt32(buffer);
                Logger.Log($"material = {material}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header2 & 0x00000002) != 0) {
                cooldownID = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"cooldownID = {cooldownID}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header2 & 0x00000004) != 0) {
                cooldownDuration = BinaryHelpers.ReadUInt64(buffer); // ulong
                Logger.Log($"cooldownDuration = {cooldownDuration}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header2 & 0x00000008) != 0) {
                petOwner = BinaryHelpers.ReadUInt32(buffer); // ObjectID
                Logger.Log($"petOwner = {petOwner}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if ((buffer.BaseStream.Position % 4) != 0) { // align DWORD
                buffer.BaseStream.Position += 4 - (buffer.BaseStream.Position % 4);
            }
        }

    }

    /// <summary>
    /// The RestrictionDB contains the access control list for a dwelling object.
    /// </summary>
    public class RestrictionDB : IACDataType {
        /// <summary>
        /// If high word is not 0, this value indicates the version of the message.
        /// </summary>
        public uint version; // uint

        /// <summary>
        /// 0 = private dwelling, 1 = open to public
        /// </summary>
        public uint bitmask; // uint

        /// <summary>
        /// allegiance monarch (if allegiance access granted)
        /// </summary>
        public uint monarchID; // ObjectID

        /// <summary>
        /// Set of permissions on a per user basis. Key is the character id, value is 0 = dwelling access only, 1 = storage access also
        /// </summary>
        public PHashTable<uint, uint> table; // PHashTable

        public RestrictionDB() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            version = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"version = {version}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            bitmask = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"bitmask = {bitmask}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            monarchID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"monarchID = {monarchID}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            table = new PHashTable<uint, uint>();
            table.ReadFromBuffer(buffer);
            Logger.Log($"table = {table}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// The OldPublicWeenieDesc structure defines an object&#39;s game behavior.
    /// </summary>
    public class OldPublicWeenieDesc : IACDataType {
        /// <summary>
        /// game data flags
        /// </summary>
        public uint header; // uint

        /// <summary>
        /// object name
        /// </summary>
        public string name; // string

        /// <summary>
        /// object weenie class id
        /// </summary>
        public uint weenieClassID; // PackedDWORD

        /// <summary>
        /// icon ResourceID (minus 0x06000000)
        /// </summary>
        public uint icon; // PackedDWORD

        /// <summary>
        /// object type
        /// </summary>
        public ObjectType type; // ObjectType

        /// <summary>
        /// object behaviors
        /// </summary>
        public ObjectDescriptionFlag bitfield; // ObjectDescriptionFlag

        /// <summary>
        /// plural object name (if not specified, use &lt;name&gt; followed by &#39;s&#39; or &#39;es&#39;)
        /// </summary>
        public string pluralName; // string

        /// <summary>
        /// number of item slots
        /// </summary>
        public byte itemsCapacity; // byte

        /// <summary>
        /// number of pack slots
        /// </summary>
        public byte containerCapacity; // byte

        /// <summary>
        /// object value
        /// </summary>
        public uint value; // uint

        public UsableType useability; // UsableType

        /// <summary>
        /// distance a player will walk to use an object
        /// </summary>
        public float useRadius; // float

        /// <summary>
        /// the object categories this object may be used on
        /// </summary>
        public ObjectType targetType; // ObjectType

        /// <summary>
        /// the type of highlight (outline) applied to the object&#39;s icon
        /// </summary>
        public IconHighlight effects; // IconHighlight

        /// <summary>
        /// missile ammunition type
        /// </summary>
        public AmmoType ammunitionType; // AmmoType

        /// <summary>
        /// the type of wieldable item this is
        /// </summary>
        public WieldType combatUse; // WieldType

        /// <summary>
        /// the number of uses remaining for this item (also salvage quantity)
        /// </summary>
        public ushort structure; // ushort

        /// <summary>
        /// the maximum number of uses possible for this item (also maximum salvage quantity)
        /// </summary>
        public ushort maxStructure; // ushort

        /// <summary>
        /// the number of items in this stack of objects
        /// </summary>
        public ushort stackSize; // ushort

        /// <summary>
        /// the maximum number of items possible in this stack of objects
        /// </summary>
        public ushort maxStackSize; // ushort

        /// <summary>
        /// the ID of the container holding this object
        /// </summary>
        public uint containerID; // ObjectID

        /// <summary>
        /// the ID of the creature equipping this object
        /// </summary>
        public uint wielderID; // ObjectID

        /// <summary>
        /// the potential equipment slots this object may be placed in
        /// </summary>
        public EquipMask validLocations; // EquipMask

        /// <summary>
        /// the actual equipment slots this object is currently placed in
        /// </summary>
        public EquipMask location; // EquipMask

        /// <summary>
        /// the parts of the body this object protects
        /// </summary>
        public CoverageMask priority; // CoverageMask

        /// <summary>
        /// radar dot color
        /// </summary>
        public byte blipColor; // byte

        /// <summary>
        /// radar type
        /// </summary>
        public byte radarEnum; // byte

        public float obviousDistance; // float

        public ushort vndwcid; // ushort

        /// <summary>
        /// the spell cast by this object
        /// </summary>
        public ushort spell; // SpellID

        public uint houseOwnerIID; // ObjectID

        public ushort physicsScript; // ushort

        /// <summary>
        /// the access control list for this dwelling object
        /// </summary>
        public RestrictionDB restrictions; // RestrictionDB

        /// <summary>
        /// the types of hooks this object may be placed on (-1 for hooks)
        /// </summary>
        public HookType hookType; // HookType

        /// <summary>
        /// what type of dwelling hook is this
        /// </summary>
        public HookType hookItemTypes; // HookType

        /// <summary>
        /// this player&#39;s monarch
        /// </summary>
        public uint monarch; // ObjectID

        /// <summary>
        /// icon overlay ResourceID (minus 0x06000000)
        /// </summary>
        public uint iconOverlay; // PackedDWORD

        /// <summary>
        /// the type of material this object is made of
        /// </summary>
        public MaterialType material; // MaterialType

        public OldPublicWeenieDesc() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            header = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"header = {header}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            name = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"name = {name}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            weenieClassID = BinaryHelpers.ReadPackedDWORD(buffer); // PackedDWORD
            Logger.Log($"weenieClassID = {weenieClassID}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            icon = BinaryHelpers.ReadPackedDWORD(buffer); // PackedDWORD
            Logger.Log($"icon = {icon}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            type = (ObjectType)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"type = {type}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            bitfield = (ObjectDescriptionFlag)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"bitfield = {bitfield}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            if (((uint)header & 0x00000001) != 0) {
                pluralName = BinaryHelpers.ReadString(buffer); // string
                Logger.Log($"pluralName = {pluralName}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00000002) != 0) {
                itemsCapacity = BinaryHelpers.ReadByte(buffer); // byte
                Logger.Log($"itemsCapacity = {itemsCapacity}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00000004) != 0) {
                containerCapacity = BinaryHelpers.ReadByte(buffer); // byte
                Logger.Log($"containerCapacity = {containerCapacity}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00000008) != 0) {
                value = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"value = {value}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00000010) != 0) {
                useability = (UsableType)BinaryHelpers.ReadUInt32(buffer);
                Logger.Log($"useability = {useability}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00000020) != 0) {
                useRadius = BinaryHelpers.ReadSingle(buffer); // float
                Logger.Log($"useRadius = {useRadius}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00080000) != 0) {
                targetType = (ObjectType)BinaryHelpers.ReadUInt32(buffer);
                Logger.Log($"targetType = {targetType}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00000080) != 0) {
                effects = (IconHighlight)BinaryHelpers.ReadUInt32(buffer);
                Logger.Log($"effects = {effects}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00000100) != 0) {
                ammunitionType = (AmmoType)BinaryHelpers.ReadUInt16(buffer);
                Logger.Log($"ammunitionType = {ammunitionType}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00000200) != 0) {
                combatUse = (WieldType)BinaryHelpers.ReadByte(buffer);
                Logger.Log($"combatUse = {combatUse}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00000400) != 0) {
                structure = BinaryHelpers.ReadUInt16(buffer); // ushort
                Logger.Log($"structure = {structure}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00000800) != 0) {
                maxStructure = BinaryHelpers.ReadUInt16(buffer); // ushort
                Logger.Log($"maxStructure = {maxStructure}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00001000) != 0) {
                stackSize = BinaryHelpers.ReadUInt16(buffer); // ushort
                Logger.Log($"stackSize = {stackSize}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00002000) != 0) {
                maxStackSize = BinaryHelpers.ReadUInt16(buffer); // ushort
                Logger.Log($"maxStackSize = {maxStackSize}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00004000) != 0) {
                containerID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
                Logger.Log($"containerID = {containerID}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00008000) != 0) {
                wielderID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
                Logger.Log($"wielderID = {wielderID}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00010000) != 0) {
                validLocations = (EquipMask)BinaryHelpers.ReadUInt32(buffer);
                Logger.Log($"validLocations = {validLocations}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00020000) != 0) {
                location = (EquipMask)BinaryHelpers.ReadUInt32(buffer);
                Logger.Log($"location = {location}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00040000) != 0) {
                priority = (CoverageMask)BinaryHelpers.ReadUInt32(buffer);
                Logger.Log($"priority = {priority}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00100000) != 0) {
                blipColor = BinaryHelpers.ReadByte(buffer); // byte
                Logger.Log($"blipColor = {blipColor}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00800000) != 0) {
                radarEnum = BinaryHelpers.ReadByte(buffer); // byte
                Logger.Log($"radarEnum = {radarEnum}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x01000000) != 0) {
                obviousDistance = BinaryHelpers.ReadSingle(buffer); // float
                Logger.Log($"obviousDistance = {obviousDistance}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00200000) != 0) {
                vndwcid = BinaryHelpers.ReadUInt16(buffer); // ushort
                Logger.Log($"vndwcid = {vndwcid}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00400000) != 0) {
                spell = BinaryHelpers.ReadUInt16(buffer); // SpellID
                Logger.Log($"spell = {spell}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x02000000) != 0) {
                houseOwnerIID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
                Logger.Log($"houseOwnerIID = {houseOwnerIID}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x08000000) != 0) {
                physicsScript = BinaryHelpers.ReadUInt16(buffer); // ushort
                Logger.Log($"physicsScript = {physicsScript}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x04000000) != 0) {
                restrictions = new RestrictionDB();
                restrictions.ReadFromBuffer(buffer);
                Logger.Log($"restrictions = {restrictions}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x10000000) != 0) {
                hookType = (HookType)BinaryHelpers.ReadUInt16(buffer);
                Logger.Log($"hookType = {hookType}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x20000000) != 0) {
                hookItemTypes = (HookType)BinaryHelpers.ReadUInt16(buffer);
                Logger.Log($"hookItemTypes = {hookItemTypes}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x00000040) != 0) {
                monarch = BinaryHelpers.ReadUInt32(buffer); // ObjectID
                Logger.Log($"monarch = {monarch}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x40000000) != 0) {
                iconOverlay = BinaryHelpers.ReadPackedDWORD(buffer); // PackedDWORD
                Logger.Log($"iconOverlay = {iconOverlay}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)header & 0x80000000) != 0) {
                material = (MaterialType)BinaryHelpers.ReadUInt32(buffer);
                Logger.Log($"material = {material}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if ((buffer.BaseStream.Position % 4) != 0) { // align DWORD
                buffer.BaseStream.Position += 4 - (buffer.BaseStream.Position % 4);
            }
        }

    }

    /// <summary>
    /// Information related to a secure trade.
    /// </summary>
    public class Trade : IACDataType {
        /// <summary>
        /// ID of other participant in the trade
        /// </summary>
        public uint partner; // ObjectID

        /// <summary>
        /// Some kind of sequence
        /// </summary>
        public ulong stamp; // ulong

        /// <summary>
        /// Some kind of status for the trade TODO
        /// </summary>
        public uint status; // uint

        /// <summary>
        /// ID of person who initiated the trade
        /// </summary>
        public uint initiator; // ObjectID

        /// <summary>
        /// Whether you accepted this trade
        /// </summary>
        public bool accepted; // bool

        /// <summary>
        /// Whether the partner accepted this trade
        /// </summary>
        public bool pAccepted; // bool

        public Trade() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            partner = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"partner = {partner}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            stamp = BinaryHelpers.ReadUInt64(buffer); // ulong
            Logger.Log($"stamp = {stamp}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            status = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"status = {status}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            initiator = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"initiator = {initiator}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            accepted = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"accepted = {accepted}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            pAccepted = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"pAccepted = {pAccepted}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// A jump with sequences
    /// </summary>
    public class JumpPack : IACDataType {
        /// <summary>
        /// Power of jump?
        /// </summary>
        public float extent; // float

        /// <summary>
        /// Velocity data
        /// </summary>
        public Vector3 velocity; // Vector3

        /// <summary>
        /// The instance sequence value for the object (number of logins for players)
        /// </summary>
        public ushort objectInstanceSequence; // ushort

        /// <summary>
        /// The server control sequence value for the object
        /// </summary>
        public ushort objectServerControlSequence; // ushort

        /// <summary>
        /// The teleport sequence value for the object
        /// </summary>
        public ushort objectTeleportSequence; // ushort

        /// <summary>
        /// The forced position sequence value for the object
        /// </summary>
        public ushort objectForcePositionSequence; // ushort

        public JumpPack() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            extent = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"extent = {extent}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            velocity = new Vector3();
            velocity.ReadFromBuffer(buffer);
            Logger.Log($"velocity = {velocity}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            objectInstanceSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectInstanceSequence = {objectInstanceSequence}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            objectServerControlSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectServerControlSequence = {objectServerControlSequence}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            objectTeleportSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectTeleportSequence = {objectTeleportSequence}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            objectForcePositionSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectForcePositionSequence = {objectForcePositionSequence}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            if ((buffer.BaseStream.Position % 4) != 0) { // align DWORD
                buffer.BaseStream.Position += 4 - (buffer.BaseStream.Position % 4);
            }
        }

    }

    /// <summary>
    /// A set of data related to changing states with sequences
    /// </summary>
    public class MoveToStatePack : IACDataType {
        /// <summary>
        /// Raw motion data
        /// </summary>
        public RawMotionState rawMotionState; // RawMotionState

        /// <summary>
        /// Position data
        /// </summary>
        public Position position; // Position

        /// <summary>
        /// The instance sequence value for the object (number of logins for players)
        /// </summary>
        public ushort objectInstanceSequence; // ushort

        /// <summary>
        /// The server control sequence value for the object
        /// </summary>
        public ushort objectServerControlSequence; // ushort

        /// <summary>
        /// The teleport sequence value for the object
        /// </summary>
        public ushort objectTeleportSequence; // ushort

        /// <summary>
        /// The forced position sequence value for the object
        /// </summary>
        public ushort objectForcePositionSequence; // ushort

        /// <summary>
        /// Whether the player has contact with the ground, or if we are in longjump_mode(1 = contact, 2 = longjump_mode)
        /// </summary>
        public byte contact; // byte

        public MoveToStatePack() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            rawMotionState = new RawMotionState();
            rawMotionState.ReadFromBuffer(buffer);
            Logger.Log($"rawMotionState = {rawMotionState}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            position = new Position();
            position.ReadFromBuffer(buffer);
            Logger.Log($"position = {position}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            objectInstanceSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectInstanceSequence = {objectInstanceSequence}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            objectServerControlSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectServerControlSequence = {objectServerControlSequence}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            objectTeleportSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectTeleportSequence = {objectTeleportSequence}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            objectForcePositionSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectForcePositionSequence = {objectForcePositionSequence}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            contact = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"contact = {contact}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            if ((buffer.BaseStream.Position % 4) != 0) { // align DWORD
                buffer.BaseStream.Position += 4 - (buffer.BaseStream.Position % 4);
            }
        }

    }

    /// <summary>
    /// Data related to the movement of the object sent from a client
    /// </summary>
    public class RawMotionState : IACDataType {
        /// <summary>
        /// Command ID
        /// </summary>
        public uint flags; // uint

        /// <summary>
        /// Derived from flags. Sequence of the animation.
        /// </summary>
        public ushort commandListLength { get => (ushort)((flags >> 11) & 0xF8); } // ushort

        public uint currentHoldkey; // uint

        /// <summary>
        /// Current stance.  If not present, defaults to 0x3D (NonCombat)
        /// </summary>
        public StanceMode currentStyle; // StanceMode

        /// <summary>
        /// Command for our forward movement. If not present, defaults to 0x03 (Ready)
        /// </summary>
        public MovementCommand forwardCommand; // MovementCommand

        /// <summary>
        /// Whether forward key is being held
        /// </summary>
        public uint forwardHoldkey; // uint

        /// <summary>
        /// Forward movement speed. If not present, defaults to 1.0
        /// </summary>
        public float forwardSpeed; // float

        /// <summary>
        /// Command for our sidestep movememnt. If not present, defaults to 0x00
        /// </summary>
        public MovementCommand sidestepCommand; // MovementCommand

        /// <summary>
        /// Whether sidestep key is being held
        /// </summary>
        public uint sidestepHoldkey; // uint

        /// <summary>
        /// Sidestep movement speed. If not present, defaults to 1.0
        /// </summary>
        public float sidestepSpeed; // float

        /// <summary>
        /// Command for our turn movememnt. If not present, defaults to 0x00
        /// </summary>
        public MovementCommand turnCommand; // MovementCommand

        /// <summary>
        /// Whether turn key is being held
        /// </summary>
        public uint turnHoldkey; // uint

        /// <summary>
        /// Turn movement speed. If not present, defaults to 1.0
        /// </summary>
        public float turnSpeed; // float

        public class CommandVectorItem {
            /// <summary>
            /// Command ID
            /// </summary>
            public Command commandID;

            /// <summary>
            /// Sequence of the animation.
            /// </summary>
            public ushort packedSequence;

            /// <summary>
            /// Command speed
            /// </summary>
            public float speed;

        }

        /// <summary>
        /// Subclass to hold commands data
        /// </summary>
        public List<CommandVectorItem> commands { get; set; } = new List<CommandVectorItem>(); // test

        public RawMotionState() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            flags = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"flags = {flags}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            if (((uint)flags & 0x00000001) != 0) {
                currentHoldkey = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"currentHoldkey = {currentHoldkey}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000002) != 0) {
                currentStyle = (StanceMode)BinaryHelpers.ReadUInt16(buffer);
                Logger.Log($"currentStyle = {currentStyle}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000004) != 0) {
                forwardCommand = (MovementCommand)BinaryHelpers.ReadUInt16(buffer);
                Logger.Log($"forwardCommand = {forwardCommand}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x0000008) != 0) {
                forwardHoldkey = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"forwardHoldkey = {forwardHoldkey}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000010) != 0) {
                forwardSpeed = BinaryHelpers.ReadSingle(buffer); // float
                Logger.Log($"forwardSpeed = {forwardSpeed}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000020) != 0) {
                sidestepCommand = (MovementCommand)BinaryHelpers.ReadUInt16(buffer);
                Logger.Log($"sidestepCommand = {sidestepCommand}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000040) != 0) {
                sidestepHoldkey = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"sidestepHoldkey = {sidestepHoldkey}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000080) != 0) {
                sidestepSpeed = BinaryHelpers.ReadSingle(buffer); // float
                Logger.Log($"sidestepSpeed = {sidestepSpeed}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000100) != 0) {
                turnCommand = (MovementCommand)BinaryHelpers.ReadUInt16(buffer);
                Logger.Log($"turnCommand = {turnCommand}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000200) != 0) {
                turnHoldkey = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"turnHoldkey = {turnHoldkey}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000400) != 0) {
                turnSpeed = BinaryHelpers.ReadSingle(buffer); // float
                Logger.Log($"turnSpeed = {turnSpeed}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            for (var i=0; i < commandListLength; i++) {
                var commandID = (Command)BinaryHelpers.ReadUInt16(buffer);
                Logger.Log($"commandID = {commandID}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                var packedSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
                Logger.Log($"packedSequence = {packedSequence}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                var speed = BinaryHelpers.ReadSingle(buffer); // float
                Logger.Log($"speed = {speed}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                commands.Add(new CommandVectorItem() {
                    commandID = commandID,
                    packedSequence = packedSequence,
                    speed = speed,
                });
            }
        }

    }

    /// <summary>
    /// An autonomous position with sequences
    /// </summary>
    public class AutonomousPositionPack : IACDataType {
        /// <summary>
        /// position
        /// </summary>
        public Position position; // Position

        /// <summary>
        /// The instance sequence value for the object (number of logins for players)
        /// </summary>
        public ushort objectInstanceSequence; // ushort

        /// <summary>
        /// The server control sequence value for the object
        /// </summary>
        public ushort objectServerControlSequence; // ushort

        /// <summary>
        /// The teleport sequence value for the object
        /// </summary>
        public ushort objectTeleportSequence; // ushort

        /// <summary>
        /// The forced position sequence value for the object
        /// </summary>
        public ushort objectForcePositionSequence; // ushort

        /// <summary>
        /// Whether the player has contact with the ground
        /// </summary>
        public byte contact; // byte

        public AutonomousPositionPack() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            position = new Position();
            position.ReadFromBuffer(buffer);
            Logger.Log($"position = {position}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            objectInstanceSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectInstanceSequence = {objectInstanceSequence}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            objectServerControlSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectServerControlSequence = {objectServerControlSequence}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            objectTeleportSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectTeleportSequence = {objectTeleportSequence}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            objectForcePositionSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectForcePositionSequence = {objectForcePositionSequence}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            contact = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"contact = {contact}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            if ((buffer.BaseStream.Position % 4) != 0) { // align DWORD
                buffer.BaseStream.Position += 4 - (buffer.BaseStream.Position % 4);
            }
        }

    }

    /// <summary>
    /// A position with sequences
    /// </summary>
    public class PositionPack : IACDataType {
        public PositionFlags flags; // PositionFlags

        /// <summary>
        /// the location of the object in the world
        /// </summary>
        public Origin origin; // Origin

        public float wQuat; // float

        public float xQuat; // float

        public float yQuat; // float

        public float zQuat; // float

        public Vector3 velocity; // Vector3

        public uint placementID; // uint

        /// <summary>
        /// The instance sequence value for the object (number of logins for players)
        /// </summary>
        public ushort objectInstanceSequence; // ushort

        /// <summary>
        /// The position sequence value for the object
        /// </summary>
        public ushort objectPositionSequence; // ushort

        /// <summary>
        /// The teleport sequence value for the object
        /// </summary>
        public ushort objectTeleportSequence; // ushort

        /// <summary>
        /// The forced position sequence value for the object
        /// </summary>
        public ushort objectForcePositionSequence; // ushort

        public PositionPack() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            flags = (PositionFlags)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"flags = {flags}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            origin = new Origin();
            origin.ReadFromBuffer(buffer);
            Logger.Log($"origin = {origin}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            if (((uint)flags & 0x00000008) != 0) {
                wQuat = BinaryHelpers.ReadSingle(buffer); // float
                Logger.Log($"wQuat = {wQuat}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000010) != 0) {
                xQuat = BinaryHelpers.ReadSingle(buffer); // float
                Logger.Log($"xQuat = {xQuat}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000020) != 0) {
                yQuat = BinaryHelpers.ReadSingle(buffer); // float
                Logger.Log($"yQuat = {yQuat}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000040) != 0) {
                zQuat = BinaryHelpers.ReadSingle(buffer); // float
                Logger.Log($"zQuat = {zQuat}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000001) != 0) {
                velocity = new Vector3();
                velocity.ReadFromBuffer(buffer);
                Logger.Log($"velocity = {velocity}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000002) != 0) {
                placementID = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"placementID = {placementID}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            objectInstanceSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectInstanceSequence = {objectInstanceSequence}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            objectPositionSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectPositionSequence = {objectPositionSequence}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            objectTeleportSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectTeleportSequence = {objectTeleportSequence}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            objectForcePositionSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectForcePositionSequence = {objectForcePositionSequence}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// Data related to the movement and animation of the object
    /// </summary>
    public class MovementData : IACDataType {
        /// <summary>
        /// The movement sequence value for this object
        /// </summary>
        public ushort objectMovementSequence; // ushort

        /// <summary>
        /// The server control sequence value for the object
        /// </summary>
        public ushort objectServerControlSequence; // ushort

        /// <summary>
        /// 0x0 - server controlled, 0x1 - autonomous
        /// </summary>
        public ushort autonomous; // ushort

        /// <summary>
        /// Determines the type of movement that follows
        /// </summary>
        public MovementType movementType; // MovementType

        /// <summary>
        /// Options for this movement (sticky, standing long jump)
        /// </summary>
        public MovementOption optionFlags; // MovementOption

        /// <summary>
        /// Current stance
        /// </summary>
        public StanceMode stance; // StanceMode

        /// <summary>
        /// Set of motion data
        /// </summary>
        public InterpertedMotionState state; // InterpertedMotionState

        /// <summary>
        /// object to stick to
        /// </summary>
        public uint stickyObject; // ObjectID

        /// <summary>
        /// The id of target that&#39;s being moved to
        /// </summary>
        public uint target; // ObjectID

        /// <summary>
        /// the location of the target in the world
        /// </summary>
        public Origin origin; // Origin

        /// <summary>
        /// Set of movement parameters
        /// </summary>
        public MoveToMovementParameters moveToParams; // MoveToMovementParameters

        /// <summary>
        /// Run speed of the moving object
        /// </summary>
        public float myRunRate; // float

        /// <summary>
        /// Heading of the target to turn to, this is used instead of the desiredHeading in the parameters
        /// </summary>
        public float desiredHeading; // float

        /// <summary>
        /// Set of movement parameters
        /// </summary>
        public TurnToMovementParameters turnToParams; // TurnToMovementParameters

        public MovementData() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            objectMovementSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectMovementSequence = {objectMovementSequence}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            objectServerControlSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectServerControlSequence = {objectServerControlSequence}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            autonomous = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"autonomous = {autonomous}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            movementType = (MovementType)BinaryHelpers.ReadByte(buffer);
            Logger.Log($"movementType = {movementType}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            optionFlags = (MovementOption)BinaryHelpers.ReadByte(buffer);
            Logger.Log($"optionFlags = {optionFlags}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            stance = (StanceMode)BinaryHelpers.ReadUInt16(buffer);
            Logger.Log($"stance = {stance}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            switch((int)movementType) {
                case 0x0000:
                    state = new InterpertedMotionState();
                    state.ReadFromBuffer(buffer);
                    Logger.Log($"state = {state}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    if (((uint)optionFlags & 0x01) != 0) {
                        stickyObject = BinaryHelpers.ReadUInt32(buffer); // ObjectID
                        Logger.Log($"stickyObject = {stickyObject}");
                        // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    }
                    break;
                case 0x0006:
                    target = BinaryHelpers.ReadUInt32(buffer); // ObjectID
                    Logger.Log($"target = {target}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    origin = new Origin();
                    origin.ReadFromBuffer(buffer);
                    Logger.Log($"origin = {origin}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    moveToParams = new MoveToMovementParameters();
                    moveToParams.ReadFromBuffer(buffer);
                    Logger.Log($"moveToParams = {moveToParams}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    myRunRate = BinaryHelpers.ReadSingle(buffer); // float
                    Logger.Log($"myRunRate = {myRunRate}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0x0007:
                    origin = new Origin();
                    origin.ReadFromBuffer(buffer);
                    Logger.Log($"origin = {origin}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    moveToParams = new MoveToMovementParameters();
                    moveToParams.ReadFromBuffer(buffer);
                    Logger.Log($"moveToParams = {moveToParams}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    myRunRate = BinaryHelpers.ReadSingle(buffer); // float
                    Logger.Log($"myRunRate = {myRunRate}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0x0008:
                    target = BinaryHelpers.ReadUInt32(buffer); // ObjectID
                    Logger.Log($"target = {target}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    desiredHeading = BinaryHelpers.ReadSingle(buffer); // float
                    Logger.Log($"desiredHeading = {desiredHeading}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    turnToParams = new TurnToMovementParameters();
                    turnToParams.ReadFromBuffer(buffer);
                    Logger.Log($"turnToParams = {turnToParams}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0x0009:
                    turnToParams = new TurnToMovementParameters();
                    turnToParams.ReadFromBuffer(buffer);
                    Logger.Log($"turnToParams = {turnToParams}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
            }
        }

    }

    /// <summary>
    /// Contains information for animations and general free motion
    /// </summary>
    public class InterpertedMotionState : IACDataType {
        public uint flags; // uint

        /// <summary>
        /// Derived from flags. 
        /// </summary>
        public uint commandListLength { get => (uint)((flags >> 7) & 0x7F); } // uint

        /// <summary>
        /// Stance.  If not present, defaults to 0x3D (NonCombat)
        /// </summary>
        public StanceMode currentStyle; // StanceMode

        /// <summary>
        /// Command for our forward movement. If not present, defaults to 0x03 (Ready)
        /// </summary>
        public MovementCommand forwardCommand; // MovementCommand

        /// <summary>
        /// Command for our sidestep movememnt. If not present, defaults to 0x00
        /// </summary>
        public MovementCommand sidestepCommand; // MovementCommand

        /// <summary>
        /// Command for our turn movememnt. If not present, defaults to 0x00
        /// </summary>
        public MovementCommand turnCommand; // MovementCommand

        /// <summary>
        /// Forward movement speed. If not present, defaults to 1.0
        /// </summary>
        public float forwardSpeed; // float

        /// <summary>
        /// Sidestep movement speed. If not present, defaults to 1.0
        /// </summary>
        public float sidestepSpeed; // float

        /// <summary>
        /// Turn movement speed. If not present, defaults to 1.0
        /// </summary>
        public float turnSpeed; // float

        public class CommandVectorItem {
            /// <summary>
            /// Command ID
            /// </summary>
            public Command commandID;

            /// <summary>
            /// Sequence of the animation.  Note the MSB appears to be used for autonomous flag.  Therefore the max size of this member would be 1/2 a WORD.
            /// </summary>
            public ushort packedSequence;

            /// <summary>
            /// Command speed
            /// </summary>
            public float speed;

        }

        /// <summary>
        /// Subclass to hold commands data
        /// </summary>
        public List<CommandVectorItem> commands { get; set; } = new List<CommandVectorItem>(); // test

        public InterpertedMotionState() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            flags = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"flags = {flags}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            if (((uint)flags & 0x00000001) != 0) {
                currentStyle = (StanceMode)BinaryHelpers.ReadUInt16(buffer);
                Logger.Log($"currentStyle = {currentStyle}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000002) != 0) {
                forwardCommand = (MovementCommand)BinaryHelpers.ReadUInt16(buffer);
                Logger.Log($"forwardCommand = {forwardCommand}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000008) != 0) {
                sidestepCommand = (MovementCommand)BinaryHelpers.ReadUInt16(buffer);
                Logger.Log($"sidestepCommand = {sidestepCommand}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000020) != 0) {
                turnCommand = (MovementCommand)BinaryHelpers.ReadUInt16(buffer);
                Logger.Log($"turnCommand = {turnCommand}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000004) != 0) {
                forwardSpeed = BinaryHelpers.ReadSingle(buffer); // float
                Logger.Log($"forwardSpeed = {forwardSpeed}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000010) != 0) {
                sidestepSpeed = BinaryHelpers.ReadSingle(buffer); // float
                Logger.Log($"sidestepSpeed = {sidestepSpeed}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000040) != 0) {
                turnSpeed = BinaryHelpers.ReadSingle(buffer); // float
                Logger.Log($"turnSpeed = {turnSpeed}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            for (var i=0; i < commandListLength; i++) {
                var commandID = (Command)BinaryHelpers.ReadUInt16(buffer);
                Logger.Log($"commandID = {commandID}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                var packedSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
                Logger.Log($"packedSequence = {packedSequence}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                var speed = BinaryHelpers.ReadSingle(buffer); // float
                Logger.Log($"speed = {speed}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                commands.Add(new CommandVectorItem() {
                    commandID = commandID,
                    packedSequence = packedSequence,
                    speed = speed,
                });
            }
            if ((buffer.BaseStream.Position % 4) != 0) { // align DWORD
                buffer.BaseStream.Position += 4 - (buffer.BaseStream.Position % 4);
            }
        }

    }

    /// <summary>
    /// Set of movement parameters required for a MoveTo movement
    /// </summary>
    public class MoveToMovementParameters : IACDataType {
        /// <summary>
        /// bitmember of some options related to the motion (TODO needs further research)
        /// </summary>
        public uint bitmember; // uint

        /// <summary>
        /// The distance to the given location
        /// </summary>
        public float distanceToObject; // float

        /// <summary>
        /// The minimum distance required for the movement
        /// </summary>
        public float minDistance; // float

        /// <summary>
        /// The distance at which the movement will fail
        /// </summary>
        public float failDistance; // float

        /// <summary>
        /// speed of animation
        /// </summary>
        public float animationSpeed; // float

        /// <summary>
        /// The distance from the location which determines whether you walk or run towards it.
        /// </summary>
        public float walkRunThreshold; // float

        /// <summary>
        /// Heading the object is turning to
        /// </summary>
        public float desiredHeading; // float

        public MoveToMovementParameters() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            bitmember = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"bitmember = {bitmember}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            distanceToObject = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"distanceToObject = {distanceToObject}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            minDistance = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"minDistance = {minDistance}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            failDistance = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"failDistance = {failDistance}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            animationSpeed = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"animationSpeed = {animationSpeed}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            walkRunThreshold = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"walkRunThreshold = {walkRunThreshold}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            desiredHeading = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"desiredHeading = {desiredHeading}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// Set of movement parameters required for a TurnTo motion
    /// </summary>
    public class TurnToMovementParameters : IACDataType {
        /// <summary>
        /// bitmember of some options related to the motion (TODO needs further research)
        /// </summary>
        public uint bitmember; // uint

        /// <summary>
        /// speed of animation
        /// </summary>
        public float animationSpeed; // float

        /// <summary>
        /// Heading the object is turning to
        /// </summary>
        public float desiredHeading; // float

        public TurnToMovementParameters() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            bitmember = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"bitmember = {bitmember}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            animationSpeed = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"animationSpeed = {animationSpeed}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            desiredHeading = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"desiredHeading = {desiredHeading}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// The ObjDesc structure defines an object&#39;s visual appearance.
    /// </summary>
    public class ObjDesc : IACDataType {
        /// <summary>
        /// always 0x11
        /// </summary>
        public byte version; // byte

        /// <summary>
        /// the number of palettes associated with this object
        /// </summary>
        public byte paletteCount; // byte

        /// <summary>
        /// the number of textures associated with this object
        /// </summary>
        public byte textureCount; // byte

        /// <summary>
        /// the number of models associated with this object
        /// </summary>
        public byte modelCount; // byte

        /// <summary>
        /// palette DataID (minus 0x04000000)
        /// </summary>
        public DataID palette; // DataID

        public List<Subpalette> subpalettes { get; set; } = new List<Subpalette>(); // test

        public List<TextureMapChange> tmChanges { get; set; } = new List<TextureMapChange>(); // test

        public List<AnimPartChange> apChanges { get; set; } = new List<AnimPartChange>(); // test

        public ObjDesc() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            version = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"version = {version}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            paletteCount = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"paletteCount = {paletteCount}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            textureCount = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"textureCount = {textureCount}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            modelCount = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"modelCount = {modelCount}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            if (paletteCount > 0) {
                palette = new DataID();
                palette.ReadFromBuffer(buffer);
                Logger.Log($"palette = {palette}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            for (var i=0; i < paletteCount; i++) {
                var subpalette = new Subpalette();
                subpalette.ReadFromBuffer(buffer);
                Logger.Log($"subpalette = {subpalette}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                subpalettes.Add(subpalette);
            }
            for (var i=0; i < textureCount; i++) {
                var tmChange = new TextureMapChange();
                tmChange.ReadFromBuffer(buffer);
                Logger.Log($"tmChange = {tmChange}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                tmChanges.Add(tmChange);
            }
            for (var i=0; i < modelCount; i++) {
                var apChange = new AnimPartChange();
                apChange.ReadFromBuffer(buffer);
                Logger.Log($"apChange = {apChange}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                apChanges.Add(apChange);
            }
            if ((buffer.BaseStream.Position % 4) != 0) { // align DWORD
                buffer.BaseStream.Position += 4 - (buffer.BaseStream.Position % 4);
            }
        }

    }

    /// <summary>
    /// Contains data for a subpalette
    /// </summary>
    public class Subpalette : IACDataType {
        /// <summary>
        /// palette DataID (minus 0x04000000)
        /// </summary>
        public DataID palette; // DataID

        /// <summary>
        /// The number of palette entries to skip
        /// </summary>
        public byte offset; // byte

        /// <summary>
        /// The number of palette entries to copy. This is multiplied by 8.  If it is 0, it defaults to 256*8.
        /// </summary>
        public byte numColors; // byte

        public Subpalette() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            palette = new DataID();
            palette.ReadFromBuffer(buffer);
            Logger.Log($"palette = {palette}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            offset = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"offset = {offset}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            numColors = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"numColors = {numColors}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// Contains data for texture map changes
    /// </summary>
    public class TextureMapChange : IACDataType {
        /// <summary>
        /// the index of the model we are replacing the texture in
        /// </summary>
        public byte partIndex; // byte

        /// <summary>
        /// texture DataID (minus 0x05000000)
        /// </summary>
        public DataID oldTexID; // DataID

        /// <summary>
        /// texture DataID (minus 0x05000000)
        /// </summary>
        public DataID newTexID; // DataID

        public TextureMapChange() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            partIndex = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"partIndex = {partIndex}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            oldTexID = new DataID();
            oldTexID.ReadFromBuffer(buffer);
            Logger.Log($"oldTexID = {oldTexID}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            newTexID = new DataID();
            newTexID.ReadFromBuffer(buffer);
            Logger.Log($"newTexID = {newTexID}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// Contains data for animation part changes
    /// </summary>
    public class AnimPartChange : IACDataType {
        /// <summary>
        /// The index of the model
        /// </summary>
        public byte partIndex; // byte

        /// <summary>
        /// model DataID (minus 0x01000000)
        /// </summary>
        public DataID partID; // DataID

        public AnimPartChange() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            partIndex = BinaryHelpers.ReadByte(buffer); // byte
            Logger.Log($"partIndex = {partIndex}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            partID = new DataID();
            partID.ReadFromBuffer(buffer);
            Logger.Log($"partID = {partID}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// Data for a character creation
    /// </summary>
    public class CharGenResult : IACDataType {
        /// <summary>
        /// Account name
        /// </summary>
        public string account; // string

        /// <summary>
        /// Always 1
        /// </summary>
        public uint one; // uint

        public HeritageGroup heritageGroup; // HeritageGroup

        public Gender gender; // Gender

        public uint eyesStrip; // uint

        public uint noseStrip; // uint

        public uint mouthStrip; // uint

        public uint hairColor; // uint

        public uint eyeColor; // uint

        public uint hairStyle; // uint

        public uint headgearStyle; // uint

        public uint headgearColor; // uint

        public uint shirtStyle; // uint

        public uint shirtColor; // uint

        public uint trousersStyle; // uint

        public uint trousersColor; // uint

        public uint footwearStyle; // uint

        public uint footwearColor; // uint

        public ulong skinShade; // ulong

        public ulong hairShade; // ulong

        public ulong headgearShade; // ulong

        public ulong shirtShade; // ulong

        public ulong trousersShade; // ulong

        public ulong footwearShade; // ulong

        public uint templateNum; // uint

        public uint strength; // uint

        public uint endurance; // uint

        public uint coordination; // uint

        public uint quickness; // uint

        public uint focus; // uint

        public uint self; // uint

        public uint slot; // uint

        public uint classID; // uint

        public uint numSkills; // uint

        public List<SkillState> skills { get; set; } = new List<SkillState>(); // test

        public string name; // string

        public uint startArea; // uint

        public uint isAdmin; // uint

        public uint isEnvoy; // uint

        /// <summary>
        /// Seems to be the total of heritageGroup, gender, eyesStrip, noseStrip, mouthStrip, hairColor, eyeColor, hairStyle, headgearStyle, shirtStyle, trousersStyle, footwearStyle, templateNum, strength, endurance, coordination, quickness, focus, self. Perhaps used for some kind of validation?
        /// </summary>
        public uint validation; // uint

        public CharGenResult() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            account = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"account = {account}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            one = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"one = {one}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            heritageGroup = (HeritageGroup)BinaryHelpers.ReadByte(buffer);
            Logger.Log($"heritageGroup = {heritageGroup}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            gender = (Gender)BinaryHelpers.ReadByte(buffer);
            Logger.Log($"gender = {gender}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            eyesStrip = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"eyesStrip = {eyesStrip}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            noseStrip = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"noseStrip = {noseStrip}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            mouthStrip = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"mouthStrip = {mouthStrip}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            hairColor = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"hairColor = {hairColor}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            eyeColor = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"eyeColor = {eyeColor}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            hairStyle = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"hairStyle = {hairStyle}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            headgearStyle = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"headgearStyle = {headgearStyle}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            headgearColor = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"headgearColor = {headgearColor}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            shirtStyle = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"shirtStyle = {shirtStyle}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            shirtColor = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"shirtColor = {shirtColor}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            trousersStyle = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"trousersStyle = {trousersStyle}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            trousersColor = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"trousersColor = {trousersColor}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            footwearStyle = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"footwearStyle = {footwearStyle}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            footwearColor = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"footwearColor = {footwearColor}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            skinShade = BinaryHelpers.ReadUInt64(buffer); // ulong
            Logger.Log($"skinShade = {skinShade}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            hairShade = BinaryHelpers.ReadUInt64(buffer); // ulong
            Logger.Log($"hairShade = {hairShade}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            headgearShade = BinaryHelpers.ReadUInt64(buffer); // ulong
            Logger.Log($"headgearShade = {headgearShade}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            shirtShade = BinaryHelpers.ReadUInt64(buffer); // ulong
            Logger.Log($"shirtShade = {shirtShade}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            trousersShade = BinaryHelpers.ReadUInt64(buffer); // ulong
            Logger.Log($"trousersShade = {trousersShade}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            footwearShade = BinaryHelpers.ReadUInt64(buffer); // ulong
            Logger.Log($"footwearShade = {footwearShade}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            templateNum = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"templateNum = {templateNum}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            strength = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"strength = {strength}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            endurance = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"endurance = {endurance}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            coordination = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"coordination = {coordination}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            quickness = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"quickness = {quickness}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            focus = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"focus = {focus}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            self = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"self = {self}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            slot = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"slot = {slot}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            classID = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"classID = {classID}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            numSkills = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"numSkills = {numSkills}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            for (var i=0; i < numSkills; i++) {
                var skillAdvancementClass = (SkillState)BinaryHelpers.ReadUInt32(buffer);
                Logger.Log($"skillAdvancementClass = {skillAdvancementClass}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                skills.Add(skillAdvancementClass);
            }
            name = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"name = {name}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            startArea = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"startArea = {startArea}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            isAdmin = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"isAdmin = {isAdmin}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            isEnvoy = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"isEnvoy = {isEnvoy}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            validation = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"validation = {validation}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// Basic information for a character used at the Login screen
    /// </summary>
    public class CharacterIdentity : IACDataType {
        /// <summary>
        /// The character ID for this entry.
        /// </summary>
        public uint gid; // ObjectID

        /// <summary>
        /// The name of this character.
        /// </summary>
        public string name; // string

        /// <summary>
        /// When 0, this character is not being deleted (not shown crossed out). Otherwise, it&#39;s a countdown timer in the number of seconds until the character is submitted for deletion.
        /// </summary>
        public uint secondsGreyedOut; // uint

        public CharacterIdentity() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            gid = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"gid = {gid}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            name = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"name = {name}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            secondsGreyedOut = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"secondsGreyedOut = {secondsGreyedOut}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            if ((buffer.BaseStream.Position % 4) != 0) { // align DWORD
                buffer.BaseStream.Position += 4 - (buffer.BaseStream.Position % 4);
            }
        }

    }

    /// <summary>
    /// The PhysicsDesc structure defines an object&#39;s physical behavior.
    /// </summary>
    public class PhysicsDesc : IACDataType {
        /// <summary>
        /// physics data flags
        /// </summary>
        public uint flags; // uint

        /// <summary>
        /// The current physics state for the object
        /// </summary>
        public PhysicsState state; // PhysicsState

        /// <summary>
        /// the number of BYTEs in the following movement buffer
        /// </summary>
        public uint movementByteCount; // uint

        /// <summary>
        /// The movement buffer for this object defining its initial movement state
        /// </summary>
        public List<byte> bytes { get; set; } = new List<byte>(); // test

        /// <summary>
        /// Whether the movement is autonomous (0 for no, 1 for yes).  NOTE that this is only present if movementByteCount &gt; 0
        /// </summary>
        public bool autonomous; // bool

        /// <summary>
        /// The current animation frame.  NOTE this can only be present if 0x00010000 is not set
        /// </summary>
        public uint animationFrame; // uint

        /// <summary>
        /// object position
        /// </summary>
        public Position position; // Position

        /// <summary>
        /// motion table id for this object
        /// </summary>
        public DataID motionTableID; // DataID

        /// <summary>
        /// sound table id for this object
        /// </summary>
        public DataID soundTableID; // DataID

        /// <summary>
        /// physics script table id for this object
        /// </summary>
        public DataID physicsScriptTableID; // DataID

        /// <summary>
        /// setup table id for this object
        /// </summary>
        public DataID setupTableID; // DataID

        /// <summary>
        /// the creature equipping this object
        /// </summary>
        public uint parent; // ObjectID

        /// <summary>
        /// the slot in which this object is equipped, referenced in the Setup table of the dats
        /// </summary>
        public uint locationID; // uint

        /// <summary>
        /// the number of items equipped by this creature
        /// </summary>
        public uint numChildren; // uint

        public class ChildrenVectorItem {
            public uint itemID;

            /// <summary>
            /// the slot in which this object is equipped, referenced in the Setup table of the dats
            /// </summary>
            public uint locationID;

        }

        /// <summary>
        /// Subclass to hold children data
        /// </summary>
        public List<ChildrenVectorItem> children { get; set; } = new List<ChildrenVectorItem>(); // test

        /// <summary>
        /// the size of this object
        /// </summary>
        public float scale; // float

        public float friction; // float

        public float elasticity; // float

        public float translucency; // float

        /// <summary>
        /// velocity vector x component
        /// </summary>
        public float velocityX; // float

        /// <summary>
        /// velocity vector y component
        /// </summary>
        public float velocityY; // float

        /// <summary>
        /// velocity vector z component
        /// </summary>
        public float velocityZ; // float

        /// <summary>
        /// acceleration vector x component
        /// </summary>
        public float accelerationX; // float

        /// <summary>
        /// acceleration vector y component
        /// </summary>
        public float accelerationY; // float

        /// <summary>
        /// acceleration vector z component
        /// </summary>
        public float accelerationZ; // float

        /// <summary>
        /// omega vector x component (rotation)
        /// </summary>
        public float omegaX; // float

        /// <summary>
        /// omega vector y component (rotation)
        /// </summary>
        public float omegaY; // float

        /// <summary>
        /// omega vector z component (rotation)
        /// </summary>
        public float omegaZ; // float

        public uint defaultScript; // uint

        public float defaultScriptIntensity; // float

        public ushort objectPositionSequence; // ushort

        public ushort objectMovementSequence; // ushort

        public ushort objectStateSequence; // ushort

        public ushort objectVectorSequence; // ushort

        public ushort objectTeleportSequence; // ushort

        public ushort objectServerControlSequence; // ushort

        public ushort objectForcePositionSequence; // ushort

        public ushort objectVisualDescSequence; // ushort

        public ushort objectInstanceSequence; // ushort

        public PhysicsDesc() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            flags = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"flags = {flags}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            state = (PhysicsState)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"state = {state}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            if (((uint)flags & 0x00010000) != 0) {
                movementByteCount = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"movementByteCount = {movementByteCount}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                if (movementByteCount > 0) {
                    for (var i=0; i < movementByteCount; i++) {
                        var data = BinaryHelpers.ReadByte(buffer); // byte
                        Logger.Log($"data = {data}");
                        // Unhandled child type: ACMessageDefs.Models.ACDataMember
                        bytes.Add(data);
                    }
                    autonomous = BinaryHelpers.ReadBool(buffer); // bool
                    Logger.Log($"autonomous = {autonomous}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                }
            }
            if (((uint)flags & 0x00020000) != 0) {
                animationFrame = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"animationFrame = {animationFrame}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00008000) != 0) {
                position = new Position();
                position.ReadFromBuffer(buffer);
                Logger.Log($"position = {position}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000002) != 0) {
                motionTableID = new DataID();
                motionTableID.ReadFromBuffer(buffer);
                Logger.Log($"motionTableID = {motionTableID}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000800) != 0) {
                soundTableID = new DataID();
                soundTableID.ReadFromBuffer(buffer);
                Logger.Log($"soundTableID = {soundTableID}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00001000) != 0) {
                physicsScriptTableID = new DataID();
                physicsScriptTableID.ReadFromBuffer(buffer);
                Logger.Log($"physicsScriptTableID = {physicsScriptTableID}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000001) != 0) {
                setupTableID = new DataID();
                setupTableID.ReadFromBuffer(buffer);
                Logger.Log($"setupTableID = {setupTableID}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000020) != 0) {
                parent = BinaryHelpers.ReadUInt32(buffer); // ObjectID
                Logger.Log($"parent = {parent}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                locationID = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"locationID = {locationID}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000040) != 0) {
                numChildren = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"numChildren = {numChildren}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                for (var i=0; i < numChildren; i++) {
                    var itemID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
                    Logger.Log($"itemID = {itemID}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    var locationID = BinaryHelpers.ReadUInt32(buffer); // uint
                    Logger.Log($"locationID = {locationID}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    children.Add(new ChildrenVectorItem() {
                        itemID = itemID,
                        locationID = locationID,
                    });
                }
            }
            if (((uint)flags & 0x00000080) != 0) {
                scale = BinaryHelpers.ReadSingle(buffer); // float
                Logger.Log($"scale = {scale}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000100) != 0) {
                friction = BinaryHelpers.ReadSingle(buffer); // float
                Logger.Log($"friction = {friction}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000200) != 0) {
                elasticity = BinaryHelpers.ReadSingle(buffer); // float
                Logger.Log($"elasticity = {elasticity}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00040000) != 0) {
                translucency = BinaryHelpers.ReadSingle(buffer); // float
                Logger.Log($"translucency = {translucency}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000004) != 0) {
                velocityX = BinaryHelpers.ReadSingle(buffer); // float
                Logger.Log($"velocityX = {velocityX}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                velocityY = BinaryHelpers.ReadSingle(buffer); // float
                Logger.Log($"velocityY = {velocityY}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                velocityZ = BinaryHelpers.ReadSingle(buffer); // float
                Logger.Log($"velocityZ = {velocityZ}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000008) != 0) {
                accelerationX = BinaryHelpers.ReadSingle(buffer); // float
                Logger.Log($"accelerationX = {accelerationX}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                accelerationY = BinaryHelpers.ReadSingle(buffer); // float
                Logger.Log($"accelerationY = {accelerationY}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                accelerationZ = BinaryHelpers.ReadSingle(buffer); // float
                Logger.Log($"accelerationZ = {accelerationZ}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000010) != 0) {
                omegaX = BinaryHelpers.ReadSingle(buffer); // float
                Logger.Log($"omegaX = {omegaX}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                omegaY = BinaryHelpers.ReadSingle(buffer); // float
                Logger.Log($"omegaY = {omegaY}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                omegaZ = BinaryHelpers.ReadSingle(buffer); // float
                Logger.Log($"omegaZ = {omegaZ}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00002000) != 0) {
                defaultScript = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"defaultScript = {defaultScript}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00004000) != 0) {
                defaultScriptIntensity = BinaryHelpers.ReadSingle(buffer); // float
                Logger.Log($"defaultScriptIntensity = {defaultScriptIntensity}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            objectPositionSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectPositionSequence = {objectPositionSequence}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            objectMovementSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectMovementSequence = {objectMovementSequence}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            objectStateSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectStateSequence = {objectStateSequence}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            objectVectorSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectVectorSequence = {objectVectorSequence}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            objectTeleportSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectTeleportSequence = {objectTeleportSequence}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            objectServerControlSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectServerControlSequence = {objectServerControlSequence}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            objectForcePositionSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectForcePositionSequence = {objectForcePositionSequence}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            objectVisualDescSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectVisualDescSequence = {objectVisualDescSequence}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            objectInstanceSequence = BinaryHelpers.ReadUInt16(buffer); // ushort
            Logger.Log($"objectInstanceSequence = {objectInstanceSequence}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            if ((buffer.BaseStream.Position % 4) != 0) { // align DWORD
                buffer.BaseStream.Position += 4 - (buffer.BaseStream.Position % 4);
            }
        }

    }

    public class AdminAccountData : IACDataType {
        public string accountName; // string

        public uint bookieID; // uint

        public AdminAccountData() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            accountName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"accountName = {accountName}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            bookieID = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"bookieID = {bookieID}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    public class AdminPlayerData : IACDataType {
        public string name; // string

        public uint bookieID; // uint

        public AdminPlayerData() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            name = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"name = {name}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            bookieID = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"bookieID = {bookieID}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    public class VendorProfile : IACDataType {
        /// <summary>
        /// the categories of items the merchant will buy
        /// </summary>
        public ObjectType itemTypes; // ObjectType

        /// <summary>
        /// the lowest value of an item the merchant will buy
        /// </summary>
        public uint minValue; // uint

        /// <summary>
        /// the highest value of an item the merchant will buy
        /// </summary>
        public uint maxValue; // uint

        public uint magic; // uint

        /// <summary>
        /// the merchant&#39;s buy rate
        /// </summary>
        public float buyPrice; // float

        /// <summary>
        /// the merchant&#39;s sell rate
        /// </summary>
        public float sellPrice; // float

        /// <summary>
        /// wcid of currency
        /// </summary>
        public uint tradeID; // uint

        /// <summary>
        /// amount of currency player has
        /// </summary>
        public uint tradeNum; // uint

        /// <summary>
        /// name of currency
        /// </summary>
        public string tradeName; // string

        public VendorProfile() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            itemTypes = (ObjectType)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"itemTypes = {itemTypes}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            minValue = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"minValue = {minValue}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            maxValue = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"maxValue = {maxValue}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            magic = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"magic = {magic}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            buyPrice = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"buyPrice = {buyPrice}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            sellPrice = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"sellPrice = {sellPrice}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            tradeID = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"tradeID = {tradeID}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            tradeNum = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"tradeNum = {tradeNum}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            tradeName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"tradeName = {tradeName}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    public class ArmorProfile : IACDataType {
        /// <summary>
        /// relative protection against slashing damage (multiply by AL for actual protection)
        /// </summary>
        public float protSlashing; // float

        /// <summary>
        /// relative protection against piercing damage (multiply by AL for actual protection)
        /// </summary>
        public float protPiercing; // float

        /// <summary>
        /// relative protection against bludgeoning damage (multiply by AL for actual protection)
        /// </summary>
        public float protBludgeoning; // float

        /// <summary>
        /// relative protection against cold damage (multiply by AL for actual protection)
        /// </summary>
        public float protCold; // float

        /// <summary>
        /// relative protection against fire damage (multiply by AL for actual protection)
        /// </summary>
        public float protFire; // float

        /// <summary>
        /// relative protection against acid damage (multiply by AL for actual protection)
        /// </summary>
        public float protAcid; // float

        /// <summary>
        /// relative protection against nether damage (multiply by AL for actual protection)
        /// </summary>
        public float protNether; // float

        /// <summary>
        /// relative protection against lightning damage (multiply by AL for actual protection)
        /// </summary>
        public float protLightning; // float

        public ArmorProfile() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            protSlashing = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"protSlashing = {protSlashing}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            protPiercing = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"protPiercing = {protPiercing}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            protBludgeoning = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"protBludgeoning = {protBludgeoning}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            protCold = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"protCold = {protCold}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            protFire = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"protFire = {protFire}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            protAcid = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"protAcid = {protAcid}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            protNether = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"protNether = {protNether}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            protLightning = BinaryHelpers.ReadSingle(buffer); // float
            Logger.Log($"protLightning = {protLightning}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    public class CreatureAppraisalProfile : IACDataType {
        /// <summary>
        /// These Flags indication which members will be available for assess.
        /// </summary>
        public uint flags; // uint

        /// <summary>
        /// current health
        /// </summary>
        public uint health; // uint

        /// <summary>
        /// maximum health
        /// </summary>
        public uint healthMax; // uint

        /// <summary>
        /// strength
        /// </summary>
        public uint strength; // uint

        /// <summary>
        /// endurance
        /// </summary>
        public uint endurance; // uint

        /// <summary>
        /// quickness
        /// </summary>
        public uint quickness; // uint

        /// <summary>
        /// coordination
        /// </summary>
        public uint coordination; // uint

        /// <summary>
        /// focus
        /// </summary>
        public uint focus; // uint

        /// <summary>
        /// self
        /// </summary>
        public uint self; // uint

        /// <summary>
        /// current stamina
        /// </summary>
        public uint stamina; // uint

        /// <summary>
        /// current mana
        /// </summary>
        public uint mana; // uint

        /// <summary>
        /// maximum stamina
        /// </summary>
        public uint staminaMax; // uint

        /// <summary>
        /// maximum mana
        /// </summary>
        public uint manaMax; // uint

        /// <summary>
        /// highlight enable bitmask: 0=no, 1=yes
        /// </summary>
        public AttributeMask attrHighlight; // AttributeMask

        /// <summary>
        /// highlight color bitmask: 0=red, 1=green
        /// </summary>
        public AttributeMask attrColor; // AttributeMask

        public CreatureAppraisalProfile() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            flags = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"flags = {flags}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            health = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"health = {health}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            healthMax = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"healthMax = {healthMax}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            if (((uint)flags & 0x00000008) != 0) {
                strength = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"strength = {strength}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                endurance = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"endurance = {endurance}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                quickness = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"quickness = {quickness}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                coordination = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"coordination = {coordination}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                focus = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"focus = {focus}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                self = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"self = {self}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                stamina = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"stamina = {stamina}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                mana = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"mana = {mana}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                staminaMax = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"staminaMax = {staminaMax}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                manaMax = BinaryHelpers.ReadUInt32(buffer); // uint
                Logger.Log($"manaMax = {manaMax}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
            if (((uint)flags & 0x00000001) != 0) {
                attrHighlight = (AttributeMask)BinaryHelpers.ReadUInt16(buffer);
                Logger.Log($"attrHighlight = {attrHighlight}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                attrColor = (AttributeMask)BinaryHelpers.ReadUInt16(buffer);
                Logger.Log($"attrColor = {attrColor}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
            }
        }

    }

    public class WeaponProfile : IACDataType {
        /// <summary>
        /// the type of damage done by the weapon
        /// </summary>
        public DamageType damageType; // DamageType

        /// <summary>
        /// the speed of the weapon
        /// </summary>
        public uint weaponTime; // uint

        /// <summary>
        /// the skill used by the weapon (-1 if none)
        /// </summary>
        public SkillID weaponSkill; // SkillID

        /// <summary>
        /// the maximum damage done by the weapon
        /// </summary>
        public uint weaponDamage; // uint

        /// <summary>
        /// the maximum damage variance of the weapon
        /// </summary>
        public double damageVariance; // double

        /// <summary>
        /// the damage modifier of the weapon
        /// </summary>
        public double damageMod; // double

        public double weaponLength; // double

        /// <summary>
        /// the power of the weapon (this affects range)
        /// </summary>
        public double maxVelocity; // double

        /// <summary>
        /// the attack skill bonus of the weapon
        /// </summary>
        public double weaponOffense; // double

        public uint weaponMaxVelocityEstimated; // uint

        public WeaponProfile() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            damageType = (DamageType)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"damageType = {damageType}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            weaponTime = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"weaponTime = {weaponTime}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            weaponSkill = (SkillID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"weaponSkill = {weaponSkill}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            weaponDamage = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"weaponDamage = {weaponDamage}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            damageVariance = BinaryHelpers.ReadDouble(buffer); // double
            Logger.Log($"damageVariance = {damageVariance}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            damageMod = BinaryHelpers.ReadDouble(buffer); // double
            Logger.Log($"damageMod = {damageMod}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            weaponLength = BinaryHelpers.ReadDouble(buffer); // double
            Logger.Log($"weaponLength = {weaponLength}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            maxVelocity = BinaryHelpers.ReadDouble(buffer); // double
            Logger.Log($"maxVelocity = {maxVelocity}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            weaponOffense = BinaryHelpers.ReadDouble(buffer); // double
            Logger.Log($"weaponOffense = {weaponOffense}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            weaponMaxVelocityEstimated = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"weaponMaxVelocityEstimated = {weaponMaxVelocityEstimated}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    public class HookAppraisalProfile : IACDataType {
        public HookAppraisalFlags hookBitfield; // HookAppraisalFlags

        public uint hookValidLocations; // uint

        public AmmoType hookAmmoType; // AmmoType

        public HookAppraisalProfile() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            hookBitfield = (HookAppraisalFlags)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"hookBitfield = {hookBitfield}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            hookValidLocations = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"hookValidLocations = {hookValidLocations}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            hookAmmoType = (AmmoType)BinaryHelpers.ReadUInt16(buffer);
            Logger.Log($"hookAmmoType = {hookAmmoType}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    public class SquelchDB : IACDataType {
        /// <summary>
        /// Account name and 
        /// </summary>
        public PackableHashTable<string, uint> accountHash; // PackableHashTable

        public PackableHashTable<uint, SquelchInfo> characterHash; // PackableHashTable

        /// <summary>
        /// Global squelch information
        /// </summary>
        public SquelchInfo globalInfo; // SquelchInfo

        public SquelchDB() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            accountHash = new PackableHashTable<string, uint>();
            accountHash.ReadFromBuffer(buffer);
            Logger.Log($"accountHash = {accountHash}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            characterHash = new PackableHashTable<uint, SquelchInfo>();
            characterHash.ReadFromBuffer(buffer);
            Logger.Log($"characterHash = {characterHash}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            globalInfo = new SquelchInfo();
            globalInfo.ReadFromBuffer(buffer);
            Logger.Log($"globalInfo = {globalInfo}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// Set of information related to a squelch entry
    /// </summary>
    public class SquelchInfo : IACDataType {
        public PackableList<LogTextType> filters; // PackableList

        /// <summary>
        /// the name of the squelched player
        /// </summary>
        public string name; // string

        /// <summary>
        /// Whether this squelch applies to the entire account
        /// </summary>
        public bool account; // bool

        public SquelchInfo() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            filters = new PackableList<LogTextType>();
            filters.ReadFromBuffer(buffer);
            Logger.Log($"filters = {filters}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            name = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"name = {name}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            account = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"account = {account}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// Set of information related to purchasing a housing
    /// </summary>
    public class HouseProfile : IACDataType {
        /// <summary>
        /// the number associated with this dwelling
        /// </summary>
        public uint dwellingID; // uint

        /// <summary>
        /// the object ID of the the current owner
        /// </summary>
        public uint ownerID; // ObjectID

        public uint bitmask; // uint

        /// <summary>
        /// the level requirement to purchase this dwelling (-1 if no requirement)
        /// </summary>
        public int minLevel; // int

        public int maxLevel; // int

        /// <summary>
        /// the rank requirement to purchase this dwelling (-1 if no requirement)
        /// </summary>
        public int minAllegRank; // int

        public int maxAllegRank; // int

        public bool maintenanceFree; // bool

        /// <summary>
        /// the type of dwelling (1=cottage, 2=villa, 3=mansion, 4=apartment)
        /// </summary>
        public HouseType type; // HouseType

        /// <summary>
        /// the name of the current owner
        /// </summary>
        public string ownerName; // string

        public PackableList<HousePayment> buy; // PackableList

        public PackableList<HousePayment> rent; // PackableList

        public HouseProfile() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            dwellingID = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"dwellingID = {dwellingID}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            ownerID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"ownerID = {ownerID}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            bitmask = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"bitmask = {bitmask}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            minLevel = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"minLevel = {minLevel}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            maxLevel = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"maxLevel = {maxLevel}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            minAllegRank = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"minAllegRank = {minAllegRank}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            maxAllegRank = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"maxAllegRank = {maxAllegRank}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            maintenanceFree = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"maintenanceFree = {maintenanceFree}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            type = (HouseType)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"type = {type}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            ownerName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"ownerName = {ownerName}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            buy = new PackableList<HousePayment>();
            buy.ReadFromBuffer(buffer);
            Logger.Log($"buy = {buy}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            rent = new PackableList<HousePayment>();
            rent.ReadFromBuffer(buffer);
            Logger.Log($"rent = {rent}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// The HousePayment structure contains information about a house purchase or maintenance item.
    /// </summary>
    public class HousePayment : IACDataType {
        /// <summary>
        /// the quantity required
        /// </summary>
        public uint num; // uint

        /// <summary>
        /// the quantity paid
        /// </summary>
        public uint paid; // uint

        /// <summary>
        /// the item&#39;s object type
        /// </summary>
        public uint wcid; // uint

        /// <summary>
        /// the name of this item
        /// </summary>
        public string name; // string

        /// <summary>
        /// the plural name of this item (if not specified, use &lt;name&gt; followed by &#39;s&#39; or &#39;es&#39;)
        /// </summary>
        public string pname; // string

        public HousePayment() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            num = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"num = {num}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            paid = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"paid = {paid}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            wcid = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"wcid = {wcid}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            name = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"name = {name}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            pname = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"pname = {pname}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// Set of information related to owning a housing
    /// </summary>
    public class HouseData : IACDataType {
        /// <summary>
        /// when the house was purchased (Unix timestamp)
        /// </summary>
        public uint buyTime; // uint

        /// <summary>
        /// when the current maintenance period began (Unix timestamp)
        /// </summary>
        public uint rentTime; // uint

        /// <summary>
        /// the type of house (1=cottage, 2=villa, 3=mansion, 4=apartment)
        /// </summary>
        public HouseType type; // HouseType

        /// <summary>
        /// indicates maintenance is free this period, I assume admin controlled
        /// </summary>
        public bool maintenanceFree; // bool

        public PackableList<HousePayment> buy; // PackableList

        public PackableList<HousePayment> rent; // PackableList

        /// <summary>
        /// house position
        /// </summary>
        public Position position; // Position

        public HouseData() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            buyTime = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"buyTime = {buyTime}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            rentTime = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"rentTime = {rentTime}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            type = (HouseType)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"type = {type}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            maintenanceFree = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"maintenanceFree = {maintenanceFree}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            buy = new PackableList<HousePayment>();
            buy.ReadFromBuffer(buffer);
            Logger.Log($"buy = {buy}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            rent = new PackableList<HousePayment>();
            rent.ReadFromBuffer(buffer);
            Logger.Log($"rent = {rent}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            position = new Position();
            position.ReadFromBuffer(buffer);
            Logger.Log($"position = {position}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// Set of information related to house access
    /// </summary>
    public class HAR : IACDataType {
        /// <summary>
        /// 0x10000002, seems to be some kind of version. Older version started with bitmask, so starting with 0x10000000 allows them to determine if this is V1 or V2.  The latter half appears to indicate wheither there is a roommate list or not.
        /// </summary>
        public uint version; // uint

        /// <summary>
        /// 0 is private house, 1 = open to public
        /// </summary>
        public uint bitmask; // uint

        /// <summary>
        /// populated when any allegiance access is specified
        /// </summary>
        public uint monarchID; // ObjectID

        /// <summary>
        /// Set of guests with their ID as the key and some additional info for them
        /// </summary>
        public PackableHashTable<uint, GuestInfo> guestList; // PackableHashTable

        /// <summary>
        /// List that has the ID&#39;s of your roommates
        /// </summary>
        public PList<uint> roomMateList; // PList

        public HAR() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            version = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"version = {version}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            bitmask = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"bitmask = {bitmask}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            monarchID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"monarchID = {monarchID}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            guestList = new PackableHashTable<uint, GuestInfo>();
            guestList.ReadFromBuffer(buffer);
            Logger.Log($"guestList = {guestList}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            roomMateList = new PList<uint>();
            roomMateList.ReadFromBuffer(buffer);
            Logger.Log($"roomMateList = {roomMateList}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// Set of information related to a house guest
    /// </summary>
    public class GuestInfo : IACDataType {
        /// <summary>
        /// 0 is just access to house, 1 = access to storage
        /// </summary>
        public bool itemStoragePermission; // bool

        /// <summary>
        /// Name of the guest
        /// </summary>
        public string guestName; // string

        public GuestInfo() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            itemStoragePermission = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"itemStoragePermission = {itemStoragePermission}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            guestName = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"guestName = {guestName}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// Set of information related to a chess game move
    /// </summary>
    public class GameMoveData : IACDataType {
        /// <summary>
        /// Type of move
        /// </summary>
        public int type; // int

        /// <summary>
        /// Player making the move
        /// </summary>
        public uint playerID; // ObjectID

        /// <summary>
        /// Team making this move
        /// </summary>
        public int team; // int

        /// <summary>
        /// ID of piece being moved
        /// </summary>
        public int idPieceToMove; // int

        public int yGrid; // int

        /// <summary>
        /// x position to move the piece
        /// </summary>
        public int xTo; // int

        /// <summary>
        /// y position to move the piece
        /// </summary>
        public int yTo; // int

        public GameMoveData() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            type = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"type = {type}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            playerID = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"playerID = {playerID}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            team = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"team = {team}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            switch((int)type) {
                case 0x4:
                    idPieceToMove = BinaryHelpers.ReadInt32(buffer); // int
                    Logger.Log($"idPieceToMove = {idPieceToMove}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    yGrid = BinaryHelpers.ReadInt32(buffer); // int
                    Logger.Log($"yGrid = {yGrid}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0x5:
                    idPieceToMove = BinaryHelpers.ReadInt32(buffer); // int
                    Logger.Log($"idPieceToMove = {idPieceToMove}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    yGrid = BinaryHelpers.ReadInt32(buffer); // int
                    Logger.Log($"yGrid = {yGrid}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    xTo = BinaryHelpers.ReadInt32(buffer); // int
                    Logger.Log($"xTo = {xTo}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    yTo = BinaryHelpers.ReadInt32(buffer); // int
                    Logger.Log($"yTo = {yTo}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
                case 0x6:
                    idPieceToMove = BinaryHelpers.ReadInt32(buffer); // int
                    Logger.Log($"idPieceToMove = {idPieceToMove}");
                    // Unhandled child type: ACMessageDefs.Models.ACDataMember
                    break;
            }
        }

    }

    /// <summary>
    /// Set of information related to a salvage operation
    /// </summary>
    public class SalvageOperationsResultData : IACDataType {
        /// <summary>
        /// Which skill was used for the salvaging action
        /// </summary>
        public SkillID skillUsed; // SkillID

        /// <summary>
        /// Set of items that could not be salvaged
        /// </summary>
        public PackableList<uint> notSalvagable; // PackableList

        /// <summary>
        /// Set of salvage returned
        /// </summary>
        public PackableList<SalvageResult> salvageResults; // PackableList

        /// <summary>
        /// Amount of units due to your Ciandra&#39;s Fortune augmentation
        /// </summary>
        public int augBonus; // int

        public SalvageOperationsResultData() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            skillUsed = (SkillID)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"skillUsed = {skillUsed}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            notSalvagable = new PackableList<uint>();
            notSalvagable.ReadFromBuffer(buffer);
            Logger.Log($"notSalvagable = {notSalvagable}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            salvageResults = new PackableList<SalvageResult>();
            salvageResults.ReadFromBuffer(buffer);
            Logger.Log($"salvageResults = {salvageResults}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            augBonus = BinaryHelpers.ReadInt32(buffer); // int
            Logger.Log($"augBonus = {augBonus}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// Set of information related to a salvage of an item
    /// </summary>
    public class SalvageResult : IACDataType {
        public MaterialType material; // MaterialType

        public double workmanship; // double

        public uint units; // uint

        public SalvageResult() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            material = (MaterialType)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"material = {material}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            workmanship = BinaryHelpers.ReadDouble(buffer); // double
            Logger.Log($"workmanship = {workmanship}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            units = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"units = {units}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// Set of information for a fellowship
    /// </summary>
    public class Fellowship : IACDataType {
        /// <summary>
        /// Set of fellowship members with their ID as the key and some additional info for them
        /// </summary>
        public PackableHashTable<uint, Fellow> fellowshipTable; // PackableHashTable

        /// <summary>
        /// the fellowship name
        /// </summary>
        public string name; // string

        /// <summary>
        /// the object ID of the fellowship leader
        /// </summary>
        public uint leader; // ObjectID

        /// <summary>
        /// XP sharing: 0=no, 1=yes
        /// </summary>
        public bool shareXp; // bool

        /// <summary>
        /// Even XP sharing: 0=no, 1=yes
        /// </summary>
        public bool evenXpSplit; // bool

        /// <summary>
        /// Open fellowship: 0=no, 1=yes
        /// </summary>
        public bool openFellow; // bool

        /// <summary>
        /// Locked fellowship: 0=no, 1=yes
        /// </summary>
        public bool locked; // bool

        /// <summary>
        /// I suspect this is a list of recently disconnected fellows which can be reinvited, even in locked fellowship
        /// </summary>
        public PackableHashTable<uint, int> fellowsDeparted; // PackableHashTable

        /// <summary>
        /// Some kind of additional table structure, it is not read by the client.  Appears to be related to some fellowship wide flag data, such as for Colloseum
        /// </summary>
        public ushort unknownCount; // WORD

        public ushort unknownTableSize; // WORD

        public class UnknownTableVectorItem {
            /// <summary>
            /// Identifies the quest/lock, such as AccessColosseum
            /// </summary>
            public string key;

            /// <summary>
            /// Always 0 in captures so far
            /// </summary>
            public uint unknownValue1;

            /// <summary>
            /// Value varies. May be 2 WORDs
            /// </summary>
            public uint unknownValue2;

            /// <summary>
            /// Appears to be some kind of timestamp
            /// </summary>
            public uint unknownValue3;

            /// <summary>
            /// Value stays constant at least for one occurence
            /// </summary>
            public uint unknownValue4;

            /// <summary>
            /// Always 1 in captures so far
            /// </summary>
            public uint unknownValue5;

        }

        /// <summary>
        /// Subclass to hold unknownTable data
        /// </summary>
        public List<UnknownTableVectorItem> unknownTable { get; set; } = new List<UnknownTableVectorItem>(); // test

        public Fellowship() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            fellowshipTable = new PackableHashTable<uint, Fellow>();
            fellowshipTable.ReadFromBuffer(buffer);
            Logger.Log($"fellowshipTable = {fellowshipTable}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            name = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"name = {name}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            leader = BinaryHelpers.ReadUInt32(buffer); // ObjectID
            Logger.Log($"leader = {leader}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            shareXp = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"shareXp = {shareXp}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            evenXpSplit = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"evenXpSplit = {evenXpSplit}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            openFellow = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"openFellow = {openFellow}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            locked = BinaryHelpers.ReadBool(buffer); // bool
            Logger.Log($"locked = {locked}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            fellowsDeparted = new PackableHashTable<uint, int>();
            fellowsDeparted.ReadFromBuffer(buffer);
            Logger.Log($"fellowsDeparted = {fellowsDeparted}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            unknownCount = BinaryHelpers.ReadUInt16(buffer); // WORD
            Logger.Log($"unknownCount = {unknownCount}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            unknownTableSize = BinaryHelpers.ReadUInt16(buffer); // WORD
            Logger.Log($"unknownTableSize = {unknownTableSize}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            for (var i=0; i < unknownCount; i++) {
                var key = BinaryHelpers.ReadString(buffer); // string
                Logger.Log($"key = {key}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                var unknownValue1 = BinaryHelpers.ReadUInt32(buffer); // DWORD
                Logger.Log($"unknownValue1 = {unknownValue1}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                var unknownValue2 = BinaryHelpers.ReadUInt32(buffer); // DWORD
                Logger.Log($"unknownValue2 = {unknownValue2}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                var unknownValue3 = BinaryHelpers.ReadUInt32(buffer); // DWORD
                Logger.Log($"unknownValue3 = {unknownValue3}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                var unknownValue4 = BinaryHelpers.ReadUInt32(buffer); // DWORD
                Logger.Log($"unknownValue4 = {unknownValue4}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                var unknownValue5 = BinaryHelpers.ReadUInt32(buffer); // DWORD
                Logger.Log($"unknownValue5 = {unknownValue5}");
                // Unhandled child type: ACMessageDefs.Models.ACDataMember
                unknownTable.Add(new UnknownTableVectorItem() {
                    key = key,
                    unknownValue1 = unknownValue1,
                    unknownValue2 = unknownValue2,
                    unknownValue3 = unknownValue3,
                    unknownValue4 = unknownValue4,
                    unknownValue5 = unknownValue5,
                });
            }
        }

    }

    /// <summary>
    /// The FellowInfo structure contains information about a fellowship member.
    /// </summary>
    public class Fellow : IACDataType {
        /// <summary>
        /// Perhaps cp stored up before distribution?
        /// </summary>
        public uint cpCached; // uint

        /// <summary>
        /// Perhaps lum stored up before distribution?
        /// </summary>
        public uint lumCached; // uint

        /// <summary>
        /// level of member
        /// </summary>
        public uint level; // uint

        /// <summary>
        /// Maximum Health
        /// </summary>
        public uint maxHealth; // uint

        /// <summary>
        /// Maximum Stamina
        /// </summary>
        public uint maxStamina; // uint

        /// <summary>
        /// Maximum Mana
        /// </summary>
        public uint maxMana; // uint

        /// <summary>
        /// Current Health
        /// </summary>
        public uint currentHealth; // uint

        /// <summary>
        /// Current Stamina
        /// </summary>
        public uint currentStamina; // uint

        /// <summary>
        /// Current Mana
        /// </summary>
        public uint currentMana; // uint

        /// <summary>
        /// if 0 then noSharePhatLoot, if 16 (0x0010) then sharePhatLoot
        /// </summary>
        public uint shareLoot; // uint

        /// <summary>
        /// Name of Member
        /// </summary>
        public string name; // string

        public Fellow() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            cpCached = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"cpCached = {cpCached}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            lumCached = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"lumCached = {lumCached}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            level = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"level = {level}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            maxHealth = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"maxHealth = {maxHealth}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            maxStamina = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"maxStamina = {maxStamina}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            maxMana = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"maxMana = {maxMana}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            currentHealth = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"currentHealth = {currentHealth}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            currentStamina = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"currentStamina = {currentStamina}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            currentMana = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"currentMana = {currentMana}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            shareLoot = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"shareLoot = {shareLoot}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            name = BinaryHelpers.ReadString(buffer); // string
            Logger.Log($"name = {name}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// Contains information about a contract.
    /// </summary>
    public class ContractTracker : IACDataType {
        public uint version; // uint

        public uint contractID; // uint

        public ContractStage contractStage; // ContractStage

        public long timeWhenDone; // long

        public long timeWhenRepeats; // long

        public ContractTracker() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            version = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"version = {version}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            contractID = BinaryHelpers.ReadUInt32(buffer); // uint
            Logger.Log($"contractID = {contractID}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            contractStage = (ContractStage)BinaryHelpers.ReadUInt32(buffer);
            Logger.Log($"contractStage = {contractStage}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            timeWhenDone = BinaryHelpers.ReadInt64(buffer); // long
            Logger.Log($"timeWhenDone = {timeWhenDone}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
            timeWhenRepeats = BinaryHelpers.ReadInt64(buffer); // long
            Logger.Log($"timeWhenRepeats = {timeWhenRepeats}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

    /// <summary>
    /// Contains table of ContractTrackers
    /// </summary>
    public class ContractTrackerTable : IACDataType {
        /// <summary>
        /// Set of contract trackers  with the contractID as the key and some additional info for them
        /// </summary>
        public PackableHashTable<uint, ContractTracker> contactTrackers; // PackableHashTable

        public ContractTrackerTable() { }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            contactTrackers = new PackableHashTable<uint, ContractTracker>();
            contactTrackers.ReadFromBuffer(buffer);
            Logger.Log($"contactTrackers = {contactTrackers}");
            // Unhandled child type: ACMessageDefs.Models.ACDataMember
        }

    }

}