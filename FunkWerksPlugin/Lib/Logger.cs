﻿using Decal.Adapter;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FunkWerksPlugin.Lib {
    public class Logger : IDisposable {
        public static Logger Instance { get; private set; }
        public string PluginAssemblyDirectory { get; internal set; } = @".\";

        public bool ShowDebugLogWindow { get; set; } = false;

        private Form form;

        public Logger() {
            Instance = this;

            if (ShowDebugLogWindow) {
                form = new Form();
                form.Text = "Test Form";
                form.Show();
            }
        }

        public static void WriteToChat(string message) {
            try {
                CoreManager.Current.Actions.AddChatText(message, 5);
            }
            catch { }
        }

        public static void WriteLine(string message) {
            try {
                using (StreamWriter writer = new StreamWriter(Path.Combine(Instance.PluginAssemblyDirectory, "exceptions.txt"), true)) {
                    writer.WriteLine(message);
                    writer.Close();
                }
            }
            catch { }
        }

        internal static void LogException(Exception ex) {
            try {
                using (StreamWriter writer = new StreamWriter(Path.Combine(Instance.PluginAssemblyDirectory, "exceptions.txt"), true)) {
                    writer.WriteLine(ex.ToString());
                    writer.Close();
                }
                WriteToChat(ex.ToString());
            }
            catch { }
        }

        public void Dispose() {
            if (form != null) {
                form.Hide();
                form.Dispose();
            }
        }
    }
}
