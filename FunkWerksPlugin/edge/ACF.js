// dont cache this module
delete require.cache[__filename];

const ACF = class ACF {
    constructor() {
        Reset();
    }

    Reset() {
        this.szName = '';
        this.eqm = 0;
        this.mcm = 0;
        this.ocm = 0;
        this.olc = 0;
        this.oty = 0;
        this.material = 0;
        this.workmanshipMin = 0;
        this.workmanshipMax = 0;
        this.burdenMin = 0;
        this.burdenMax = 0;
        this.acoWearer = null; // ACO
        this.cpyMin = 0;
        this.cpyMax = 0;
        this.citemStackMin = 0;
        this.citemStackMax = 0;
        this.cuseLeftMin = 0;
        this.cuseLeftMax = 0;
        this.distMin = 0;
        this.distMax = 0;
        this.maploc = null; // Maploc
        this.oidMonarch = 0;
    }

    CoacoGet(coaco) {

    }

    CoacoGetSorted(sortby, coaco) {

    }

    AcoGetSorted(sortby, coaco) {

    }

    FMatchAco(aco) {

    }
};

module.exports = ACF;