﻿// from skunkworks: http://skunkworks.sourceforge.net/

var skapiDefs = module.exports = {};

// arc: Action result codes
skapiDefs.arcSuccess                  =                           0;	// Your spellcasting attempt (or other action) succeeded.
skapiDefs.arcCombatMode               =                           2;	// One of the parties entered combat mode.
skapiDefs.arcChargedTooFar            =                          28;	// You charged too far!
skapiDefs.arcTooBusy                  =                          29;	// You're too busy!
skapiDefs.arcCantMoveToObject         =                          57;	// Unable to move to object!
skapiDefs.arcCanceled                 =                          81;	// One of the parties canceled or moved out of range.
skapiDefs.arcFatigued                 =                        1015;	// You are too fatigued to attack!
skapiDefs.arcOutOfAmmo                =                        1016;	// You are out of ammunition!
skapiDefs.arcMissleMisfire            =                        1017;	// Your missile attack misfired!
skapiDefs.arcImpossibleSpell          =                        1018;	// You've attempted an impossible spell path!
skapiDefs.arcDontKnowSpell            =                        1022;	// You don't know that spell!
skapiDefs.arcIncorrectTarget          =                        1023;	// Incorrect target type.
skapiDefs.arcOutOfComps               =                        1024;	// You don't have all the components for this spell.
skapiDefs.arcNotEnoughMana            =                        1025;	// You don't have enough Mana to cast this spell.
skapiDefs.arcFizzle                   =                        1026;	// Your spell fizzled.
skapiDefs.arcNoTarget                 =                        1027;	// Your spell's target is missing!
skapiDefs.arcSpellMisfire             =                        1028;	// Your projectile spell mislaunched!
skapiDefs.arcSummonFailure            =                        1188;	// You fail to summon the portal!
skapiDefs.arcNotAvailable             =                        1323;	// That person is not available.

// attr: Attribute IDs
skapiDefs.attrNil                     =                           0;	// No attribute.
skapiDefs.attrStrength                =                           1;
skapiDefs.attrEndurance               =                           2;
skapiDefs.attrQuickness               =                           3;
skapiDefs.attrCoordination            =                           4;
skapiDefs.attrFocus                   =                           5;
skapiDefs.attrSelf                    =                           6;
skapiDefs.attrMax                     =                           7;

// bpart: Body part IDs
skapiDefs.bpartHead                   =                           0;
skapiDefs.bpartChest                  =                           1;
skapiDefs.bpartAbdomen                =                           2;
skapiDefs.bpartUpperArm               =                           3;
skapiDefs.bpartLowerArm               =                           4;
skapiDefs.bpartHand                   =                           5;
skapiDefs.bpartUpperLeg               =                           6;
skapiDefs.bpartLowerLeg               =                           7;
skapiDefs.bpartFoot                   =                           8;

// chop: Character option masks
skapiDefs.chopRepeatAttacks           =                  0x00000002;	// Automatically Repeat Attacks
skapiDefs.chopIgnoreAllegiance        =                  0x00000004;	// Ignore Allegiance Requests
skapiDefs.chopIgnoreFellowship        =                  0x00000008;	// Ignore Fellowship Requests
skapiDefs.chopAcceptGifts             =                  0x00000040;	// Let Other Players Give You Items
skapiDefs.chopKeepTargetInView        =                  0x00000080;	// Keep Combat Targets in View
skapiDefs.chopTooltips                =                  0x00000100;	// Display 3D Tooltips
skapiDefs.chopDeceive                 =                  0x00000200;	// Attempt to Deceive Other Players
skapiDefs.chopRunAsDefault            =                  0x00000400;	// Run as Default Movement
skapiDefs.chopStayInChat              =                  0x00000800;	// Stay in Chat Mode After Sending a Message
skapiDefs.chopAdvancedCombat          =                  0x00001000;	// Advanced Combat Interface
skapiDefs.chopAutoTarget              =                  0x00002000;	// Auto Target
skapiDefs.chopVividTargeting          =                  0x00008000;	// Vivid Targeting Indicator
skapiDefs.chopIgnoreTrade             =                  0x00020000;	// Ignore All Trade Requests
skapiDefs.chopShareXP                 =                  0x00040000;	// Share Fellowship Experience
skapiDefs.chopAcceptCorpsePerm        =                  0x00080000;	// Accept Corpse-Looting Permissions
skapiDefs.chopShareLoot               =                  0x00100000;	// Share Fellowship Loot
skapiDefs.chopStretchUI               =                  0x00200000;	// Stretch UI
skapiDefs.chopShowCoords              =                  0x00400000;	// Show Coordinates By the Radar
skapiDefs.chopSpellDurations          =                  0x00800000;	// Display Spell Durations
skapiDefs.chopDisableHouseFX          =                  0x02000000;	// Disable House Restriction Effects
skapiDefs.chopDragStartsTrade         =                  0x04000000;	// Drag Item to Player Opens Trade
skapiDefs.chopShowAllegianceLogons    =                  0x08000000;	// Show Allegiance Logons
skapiDefs.chopUseChargeAttack         =                  0x10000000;	// Use Charge Attack
skapiDefs.chopAutoFellow              =                  0x20000000;	// Automatically Accept Fellowship Requests
skapiDefs.chopAllegianceChat          =                  0x40000000;	// Listen to Allegiance Chat
skapiDefs.chopCraftingDialog          =                  0x80000000;	// Use Crafting Chance of Success Dialog
skapiDefs.chopAcceptAllegiance        =                  0x00000004;	// (Old name for compatibility)
skapiDefs.chopAcceptFellowship        =                  0x00000008;	// (Old name for compatibility)

// chop2: Character option masks (extended)
skapiDefs.chop2AlwaysDaylight         =                  0x00000001;	// Always Daylight Outdoors
skapiDefs.chop2ShowDateOfBirth        =                  0x00000002;	// Allow Others to See Your Date of Birth
skapiDefs.chop2ShowChessRank          =                  0x00000004;	// Allow Others to See Your Chess Rank
skapiDefs.chop2ShowFishingSkill       =                  0x00000008;	// Allow Others to See Your Fishing Skill
skapiDefs.chop2ShowDeaths             =                  0x00000010;	// Allow Others to See Your Number of Deaths
skapiDefs.chop2ShowAge                =                  0x00000020;	// Allow Others to See Your Age
skapiDefs.chop2Timestamps             =                  0x00000040;	// Display Timestamps
skapiDefs.chop2SalvageMultiple        =                  0x00000080;	// Salvage Multiple Materials at Once

// cmc: Chat message colors
skapiDefs.cmcGreen                    =                           0;
skapiDefs.cmcWhite                    =                           2;	// Local broadcast chat.
skapiDefs.cmcYellow                   =                           3;
skapiDefs.cmcDarkYellow               =                           4;
skapiDefs.cmcMagenta                  =                           5;
skapiDefs.cmcRed                      = skapiDefs.cmcRed =                6;
skapiDefs.cmcLightBlue                =                           7;
skapiDefs.cmcRose                     =                           8;
skapiDefs.cmcPaleRose                 = skapiDefs.cmcPaleRose =           9;
skapiDefs.cmcDimWhite                 = skapiDefs.cmcDimWhite =          12;
skapiDefs.cmcCyan                     =                          13;
skapiDefs.cmcPaleBlue                 = skapiDefs.cmcPaleBlue =          14;	// SkunkWorks default color.
skapiDefs.cmcBlue                     =                          17;	// SpellcastinskapiDefs.
skapiDefs.cmcCoral                    =                          22;

// chrm: Chat recipient masks
skapiDefs.chrmLocal                   =                  0x00000001;	// Local chat broadcast
skapiDefs.chrmEmote                   =                  0x00000002;	// Local emote broadcast (@e)
skapiDefs.chrmFellowship              =                  0x00000800;	// Fellowship broadcast (@f)
skapiDefs.chrmVassals                 =                  0x00001000;	// Patron to vassal (@v)
skapiDefs.chrmPatron                  =                  0x00002000;	// Vassal to patron (@p)
skapiDefs.chrmMonarch                 =                  0x00004000;	// Follower to monarch (@m)
skapiDefs.chrmCovassals               =                  0x01000000;	// Covassal broadcast (@c)
skapiDefs.chrmAllegiance              =                  0x02000000;	// Allegiance broadcast by monarch or speaker (@a)

// cmid: Command IDs
// cmid definitions appear here in the same order they appear on AC's Keyboard Configuration screen.  Where the name of a cmid differs from the descriptive text on that screen, the text appears in the Description column below.
// cmid values may be concatenated using the + operator to specify a sequence of commands.
skapiDefs.cmidMovementJump            =            "{MovementJump}";
skapiDefs.cmidMovementForward         =         "{MovementForward}";
skapiDefs.cmidMovementBackup          =          "{MovementBackup}";
skapiDefs.cmidMovementTurnLeft        =        "{MovementTurnLeft}";
skapiDefs.cmidMovementTurnRight       =       "{MovementTurnRight}";
skapiDefs.cmidMovementWalkMode        =        "{MovementWalkMode}";
skapiDefs.cmidMovementStop            =            "{MovementStop}";
skapiDefs.cmidMovementStrafeRight     =     "{MovementStrafeRight}";
skapiDefs.cmidMovementStrafeLeft      =      "{MovementStrafeLeft}";
skapiDefs.cmidMovementRunLock         =         "{MovementRunLock}";
skapiDefs.cmidReady                   =                   "{Ready}";
skapiDefs.cmidCrouch                  =                  "{Crouch}";
skapiDefs.cmidSitting                 =                 "{Sitting}";
skapiDefs.cmidSleeping                =                "{Sleeping}";
skapiDefs.cmidSelectionSelf           =           "{SelectionSelf}";
skapiDefs.cmidSelectionPreviousCompassItem="{SelectionPreviousCompassItem}";
skapiDefs.cmidSelectionNextCompassItem="{SelectionNextCompassItem}";
skapiDefs.cmidSelectionUseNextUnopenedCorpse="{SelectionUseNextUnopenedCorpse}";
skapiDefs.cmidSelectionGive           =           "{SelectionGive}";
skapiDefs.cmidSelectionDrop           =           "{SelectionDrop}";
skapiDefs.cmidSelectionPickUp         =         "{SelectionPickUp}";
skapiDefs.cmidSelectionSplitStack     =     "{SelectionSplitStack}";
skapiDefs.cmidSelectionUseClosestUnopenedCorpse="{SelectionUseClosestUnopenedCorpse}";
skapiDefs.cmidSelectionClosestCompassItem="{SelectionClosestCompassItem}";
skapiDefs.cmidSelectionLastAttacker   =   "{SelectionLastAttacker}";
skapiDefs.cmidSelectionClosestMonster = "{SelectionClosestMonster}";
skapiDefs.cmidSelectionPreviousItem   =   "{SelectionPreviousItem}";
skapiDefs.cmidSelectionNextItem       =       "{SelectionNextItem}";
skapiDefs.cmidSelectionPreviousSelection="{SelectionPreviousSelection}";
skapiDefs.cmidSelectionClosestItem    =    "{SelectionClosestItem}";
skapiDefs.cmidSelectionPreviousMonster="{SelectionPreviousMonster}";
skapiDefs.cmidSelectionNextMonster    =    "{SelectionNextMonster}";
skapiDefs.cmidSelectionClosestPlayer  =  "{SelectionClosestPlayer}";
skapiDefs.cmidSelectionPreviousPlayer = "{SelectionPreviousPlayer}";
skapiDefs.cmidSelectionNextPlayer     =     "{SelectionNextPlayer}";
skapiDefs.cmidSelectionPreviousFellow = "{SelectionPreviousFellow}";
skapiDefs.cmidSelectionNextFellow     =     "{SelectionNextFellow}";
skapiDefs.cmidSelectionExamine        =        "{SelectionExamine}";
skapiDefs.cmidUSE                     =                     "{USE}";
skapiDefs.cmidToggleAbusePanel        =        "{ToggleAbusePanel}";
skapiDefs.cmidToggleCharacterInfoPanel="{ToggleCharacterInfoPanel}";
skapiDefs.cmidTogglePositiveEffectsPanel="{TogglePositiveEffectsPanel}";
skapiDefs.cmidToggleNegativeEffectsPanel="{ToggleNegativeEffectsPanel}";
skapiDefs.cmidToggleLinkStatusPanel   =   "{ToggleLinkStatusPanel}";
skapiDefs.cmidToggleUrgentAssistancePanel="{ToggleUrgentAssistancePanel}";
skapiDefs.cmidToggleVitaePanel        =        "{ToggleVitaePanel}";
skapiDefs.cmidToggleSocialPanel       =       "{ToggleSocialPanel}";
skapiDefs.cmidToggleSpellManagementPanel="{ToggleSpellManagementPanel}";
skapiDefs.cmidToggleSkillManagementPanel="{ToggleSkillManagementPanel}";
skapiDefs.cmidToggleMapPanel          =          "{ToggleMapPanel}";
skapiDefs.cmidToggleHousePanel        =        "{ToggleHousePanel}";
skapiDefs.cmidToggleGameplayOptionsPanel="{ToggleGameplayOptionsPanel}";
skapiDefs.cmidToggleCharacterOptionsPanel="{ToggleCharacterOptionsPanel}";
skapiDefs.cmidToggleConfigOptionsPanel="{ToggleConfigOptionsPanel}";
skapiDefs.cmidToggleRadarPanel        =        "{ToggleRadarPanel}";
skapiDefs.cmidToggleKeyboardPanel     =     "{ToggleKeyboardPanel}";
skapiDefs.cmidCaptureScreenshot       =       "{CaptureScreenshot}";
skapiDefs.cmidToggleHelp              =              "{ToggleHelp}";
skapiDefs.cmidTogglePluginManager     =     "{TogglePluginManager}";
skapiDefs.cmidToggleAllegiancePanel   =   "{ToggleAllegiancePanel}";
skapiDefs.cmidToggleFellowshipPanel   =   "{ToggleFellowshipPanel}";
skapiDefs.cmidToggleSpellbookPanel    =    "{ToggleSpellbookPanel}";
skapiDefs.cmidToggleSpellComponentsPanel="{ToggleSpellComponentsPanel}";
skapiDefs.cmidToggleAttributesPanel   =   "{ToggleAttributesPanel}";
skapiDefs.cmidToggleSkillsPanel       =       "{ToggleSkillsPanel}";
skapiDefs.cmidToggleWorldPanel        =        "{ToggleWorldPanel}";
skapiDefs.cmidToggleOptionsPanel      =      "{ToggleOptionsPanel}";
skapiDefs.cmidToggleInventoryPanel    =    "{ToggleInventoryPanel}";
skapiDefs.cmidLOGOUT                  =                  "{LOGOUT}";
skapiDefs.cmidSelectQuickSlot_6       =       "{SelectQuickSlot_6}";
skapiDefs.cmidSelectQuickSlot_7       =       "{SelectQuickSlot_7}";
skapiDefs.cmidSelectQuickSlot_8       =       "{SelectQuickSlot_8}";
skapiDefs.cmidSelectQuickSlot_9       =       "{SelectQuickSlot_9}";
skapiDefs.cmidSelectQuickSlot_1       =       "{SelectQuickSlot_1}";
skapiDefs.cmidSelectQuickSlot_2       =       "{SelectQuickSlot_2}";
skapiDefs.cmidSelectQuickSlot_3       =       "{SelectQuickSlot_3}";
skapiDefs.cmidSelectQuickSlot_4       =       "{SelectQuickSlot_4}";
skapiDefs.cmidSelectQuickSlot_5       =       "{SelectQuickSlot_5}";
skapiDefs.cmidCreateShortcut          =          "{CreateShortcut}";
skapiDefs.cmidUseQuickSlot_1          =          "{UseQuickSlot_1}";
skapiDefs.cmidUseQuickSlot_2          =          "{UseQuickSlot_2}";
skapiDefs.cmidUseQuickSlot_3          =          "{UseQuickSlot_3}";
skapiDefs.cmidUseQuickSlot_4          =          "{UseQuickSlot_4}";
skapiDefs.cmidUseQuickSlot_5          =          "{UseQuickSlot_5}";
skapiDefs.cmidUseQuickSlot_6          =          "{UseQuickSlot_6}";
skapiDefs.cmidUseQuickSlot_7          =          "{UseQuickSlot_7}";
skapiDefs.cmidUseQuickSlot_8          =          "{UseQuickSlot_8}";
skapiDefs.cmidUseQuickSlot_9          =          "{UseQuickSlot_9}";
skapiDefs.cmidToggleChatEntry         =         "{ToggleChatEntry}";
skapiDefs.cmidSTART_COMMAND           =           "{START_COMMAND}";
skapiDefs.cmidMonarchReply            =            "{MonarchReply}";
skapiDefs.cmidPatronReply             =             "{PatronReply}";
skapiDefs.cmidReply                   =                   "{Reply}";
skapiDefs.cmidEnterChatMode           =           "{EnterChatMode}";
skapiDefs.cmidCombatToggleCombat      =      "{CombatToggleCombat}";
skapiDefs.cmidCombatDecreaseAttackPower="{CombatDecreaseAttackPower}";
skapiDefs.cmidCombatIncreaseAttackPower="{CombatIncreaseAttackPower}";
skapiDefs.cmidCombatLowAttack         =         "{CombatLowAttack}";
skapiDefs.cmidCombatMediumAttack      =      "{CombatMediumAttack}";
skapiDefs.cmidCombatHighAttack        =        "{CombatHighAttack}";
skapiDefs.cmidCombatDecreaseMissileAccuracy="{CombatDecreaseMissileAccuracy}";
skapiDefs.cmidCombatIncreaseMissileAccuracy="{CombatIncreaseMissileAccuracy}";
skapiDefs.cmidCombatAimLow            =            "{CombatAimLow}";
skapiDefs.cmidCombatAimMedium         =         "{CombatAimMedium}";
skapiDefs.cmidCombatAimHigh           =           "{CombatAimHigh}";
skapiDefs.cmidUseSpellSlot_10         =         "{UseSpellSlot_10}";
skapiDefs.cmidUseSpellSlot_1          =          "{UseSpellSlot_1}";
skapiDefs.cmidUseSpellSlot_2          =          "{UseSpellSlot_2}";
skapiDefs.cmidUseSpellSlot_3          =          "{UseSpellSlot_3}";
skapiDefs.cmidUseSpellSlot_4          =          "{UseSpellSlot_4}";
skapiDefs.cmidUseSpellSlot_11         =         "{UseSpellSlot_11}";
skapiDefs.cmidUseSpellSlot_12         =         "{UseSpellSlot_12}";
skapiDefs.cmidCombatPrevSpellTab      =      "{CombatPrevSpellTab}";
skapiDefs.cmidCombatNextSpellTab      =      "{CombatNextSpellTab}";
skapiDefs.cmidCombatPrevSpell         =         "{CombatPrevSpell}";
skapiDefs.cmidCombatCastCurrentSpell  =  "{CombatCastCurrentSpell}";
skapiDefs.cmidCombatNextSpell         =         "{CombatNextSpell}";
skapiDefs.cmidCombatFirstSpellTab     =     "{CombatFirstSpellTab}";
skapiDefs.cmidCombatLastSpellTab      =      "{CombatLastSpellTab}";
skapiDefs.cmidCombatFirstSpell        =        "{CombatFirstSpell}";
skapiDefs.cmidCombatLastSpell         =         "{CombatLastSpell}";
skapiDefs.cmidUseSpellSlot_5          =          "{UseSpellSlot_5}";
skapiDefs.cmidUseSpellSlot_6          =          "{UseSpellSlot_6}";
skapiDefs.cmidUseSpellSlot_7          =          "{UseSpellSlot_7}";
skapiDefs.cmidUseSpellSlot_8          =          "{UseSpellSlot_8}";
skapiDefs.cmidUseSpellSlot_9          =          "{UseSpellSlot_9}";
skapiDefs.cmidAFKState                =                "{AFKState}";
skapiDefs.cmidAtEaseState             =             "{AtEaseState}";
skapiDefs.cmidATOYOT                  =                  "{ATOYOT}";
skapiDefs.cmidAkimbo                  =                  "{Akimbo}";
skapiDefs.cmidAkimboState             =             "{AkimboState}";
skapiDefs.cmidBeckon                  =                  "{Beckon}";
skapiDefs.cmidBeSeeingYou             =             "{BeSeeingYou}";
skapiDefs.cmidBlowKiss                =                "{BlowKiss}";
skapiDefs.cmidBowDeep                 =                 "{BowDeep}";
skapiDefs.cmidBowDeepState            =            "{BowDeepState}";
skapiDefs.cmidCheer                   =                   "{Cheer}";
skapiDefs.cmidClapHands               =               "{ClapHands}";
skapiDefs.cmidClapHandsState          =          "{ClapHandsState}";
skapiDefs.cmidCrossArmsState          =          "{CrossArmsState}";
skapiDefs.cmidCringe                  =                  "{Cringe}";
skapiDefs.cmidCry                     =                     "{Cry}";
skapiDefs.cmidCurtseyState            =            "{CurtseyState}";
skapiDefs.cmidDrudgeDance             =             "{DrudgeDance}";
skapiDefs.cmidDrudgeDanceState        =        "{DrudgeDanceState}";
skapiDefs.cmidHaveASeat               =               "{HaveASeat}";
skapiDefs.cmidHaveASeatState          =          "{HaveASeatState}";
skapiDefs.cmidHeartyLaugh             =             "{HeartyLaugh}";
skapiDefs.cmidHelper                  =                  "{Helper}";
skapiDefs.cmidKneel                   =                   "{Kneel}";
skapiDefs.cmidKneelState              =              "{KneelState}";
skapiDefs.cmidKnock                   =                   "{Knock}";
skapiDefs.cmidLaugh                   =                   "{Laugh}";
skapiDefs.cmidLeanState               =               "{LeanState}";
skapiDefs.cmidMeditateState           =           "{MeditateState}";
skapiDefs.cmidMimeDrink               =               "{MimeDrink}";
skapiDefs.cmidMimeEat                 =                 "{MimeEat}";
skapiDefs.cmidMock                    =                    "{Mock}";
skapiDefs.cmidNod                     =                     "{Nod}";
skapiDefs.cmidNudgeLeft               =               "{NudgeLeft}";
skapiDefs.cmidNudgeRight              =              "{NudgeRight}";
skapiDefs.cmidPlead                   =                   "{Plead}";
skapiDefs.cmidPleadState              =              "{PleadState}";
skapiDefs.cmidPoint                   =                   "{Point}";
skapiDefs.cmidPointState              =              "{PointState}";
skapiDefs.cmidPointDown               =               "{PointDown}";
skapiDefs.cmidPointDownState          =          "{PointDownState}";
skapiDefs.cmidPointLeft               =               "{PointLeft}";
skapiDefs.cmidPointLeftState          =          "{PointLeftState}";
skapiDefs.cmidPointRight              =              "{PointRight}";
skapiDefs.cmidPointRightState         =         "{PointRightState}";
skapiDefs.cmidPossumState             =             "{PossumState}";
skapiDefs.cmidPray                    =                    "{Pray}";
skapiDefs.cmidPrayState               =               "{PrayState}";
skapiDefs.cmidReadState               =               "{ReadState}";
skapiDefs.cmidSalute                  =                  "{Salute}";
skapiDefs.cmidSaluteState             =             "{SaluteState}";
skapiDefs.cmidScanHorizon             =             "{ScanHorizon}";
skapiDefs.cmidScratchHead             =             "{ScratchHead}";
skapiDefs.cmidScratchHeadState        =        "{ScratchHeadState}";
skapiDefs.cmidShakeHead               =               "{ShakeHead}";
skapiDefs.cmidShakeFist               =               "{ShakeFist}";
skapiDefs.cmidShakeFistState          =          "{ShakeFistState}";
skapiDefs.cmidShiver                  =                  "{Shiver}";
skapiDefs.cmidShiverState             =             "{ShiverState}";
skapiDefs.cmidShoo                    =                    "{Shoo}";
skapiDefs.cmidShrug                   =                   "{Shrug}";
skapiDefs.cmidSitState                =                "{SitState}";
skapiDefs.cmidSitBackState            =            "{SitBackState}";
skapiDefs.cmidSitCrossleggedState     =     "{SitCrossleggedState}";
skapiDefs.cmidSlouch                  =                  "{Slouch}";
skapiDefs.cmidSlouchState             =             "{SlouchState}";
skapiDefs.cmidSmackHead               =               "{SmackHead}";
skapiDefs.cmidSnowAngelState          =          "{SnowAngelState}";
skapiDefs.cmidSpit                    =                    "{Spit}";
skapiDefs.cmidSurrender               =               "{Surrender}";
skapiDefs.cmidSurrenderState          =          "{SurrenderState}";
skapiDefs.cmidTalktotheHandState      =      "{TalktotheHandState}";
skapiDefs.cmidTapFoot                 =                 "{TapFoot}";
skapiDefs.cmidTapFootState            =            "{TapFootState}";
skapiDefs.cmidTeapot                  =                  "{Teapot}";
skapiDefs.cmidThinkerState            =            "{ThinkerState}";
skapiDefs.cmidWarmHands               =               "{WarmHands}";
skapiDefs.cmidWave                    =                    "{Wave}";
skapiDefs.cmidWaveState               =               "{WaveState}";
skapiDefs.cmidWaveHigh                =                "{WaveHigh}";
skapiDefs.cmidWaveLow                 =                 "{WaveLow}";
skapiDefs.cmidWoah                    =                    "{Woah}";
skapiDefs.cmidWoahState               =               "{WoahState}";
skapiDefs.cmidWinded                  =                  "{Winded}";
skapiDefs.cmidWindedState             =             "{WindedState}";
skapiDefs.cmidYawnStretch             =             "{YawnStretch}";
skapiDefs.cmidYMCA                    =                    "{YMCA}";
skapiDefs.cmidCameraMoveToward        =        "{CameraMoveToward}";
skapiDefs.cmidCameraMoveAway          =          "{CameraMoveAway}";
skapiDefs.cmidCameraActivateAlternateMode="{CameraActivateAlternateMode}";
skapiDefs.cmidCameraInstantMouseLook  =  "{CameraInstantMouseLook}";
skapiDefs.cmidCameraRotateLeft        =        "{CameraRotateLeft}";
skapiDefs.cmidCameraRotateRight       =       "{CameraRotateRight}";
skapiDefs.cmidCameraRotateUp          =          "{CameraRotateUp}";
skapiDefs.cmidCameraRotateDown        =        "{CameraRotateDown}";
skapiDefs.cmidCameraViewDefault       =       "{CameraViewDefault}";
skapiDefs.cmidCameraViewFirstPerson   =   "{CameraViewFirstPerson}";
skapiDefs.cmidCameraViewLookDown      =      "{CameraViewLookDown}";
skapiDefs.cmidCameraViewMapMode       =       "{CameraViewMapMode}";
skapiDefs.cmidAltEnter                =                "{AltEnter}";
skapiDefs.cmidAltTab                  =                  "{AltTab}";
skapiDefs.cmidAltF4                   =                   "{AltF4}";
skapiDefs.cmidCtrlShiftEsc            =            "{CtrlShiftEsc}";
skapiDefs.cmidPointerX                =                "{PointerX}";
skapiDefs.cmidPointerY                =                "{PointerY}";
skapiDefs.cmidSelectLeft              =              "{SelectLeft}";
skapiDefs.cmidSelectRight             =             "{SelectRight}";
skapiDefs.cmidSelectMid               =               "{SelectMid}";
skapiDefs.cmidSelectDblLeft           =           "{SelectDblLeft}";
skapiDefs.cmidSelectDblRight          =          "{SelectDblRight}";
skapiDefs.cmidSelectDblMid            =            "{SelectDblMid}";
skapiDefs.cmidScrollUp                =                "{ScrollUp}";
skapiDefs.cmidScrollDown              =              "{ScrollDown}";
skapiDefs.cmidCursorCharLeft          =          "{CursorCharLeft}";
skapiDefs.cmidCursorCharRight         =         "{CursorCharRight}";
skapiDefs.cmidCursorPreviousLine      =      "{CursorPreviousLine}";
skapiDefs.cmidCursorNextLine          =          "{CursorNextLine}";
skapiDefs.cmidCursorPreviousPage      =      "{CursorPreviousPage}";
skapiDefs.cmidCursorNextPage          =          "{CursorNextPage}";
skapiDefs.cmidCursorWordLeft          =          "{CursorWordLeft}";
skapiDefs.cmidCursorWordRight         =         "{CursorWordRight}";
skapiDefs.cmidCursorStartOfLine       =       "{CursorStartOfLine}";
skapiDefs.cmidCursorStartOfDocument   =   "{CursorStartOfDocument}";
skapiDefs.cmidCursorEndOfLine         =         "{CursorEndOfLine}";
skapiDefs.cmidCursorEndOfDocument     =     "{CursorEndOfDocument}";
skapiDefs.cmidEscapeKey               =               "{EscapeKey}";
skapiDefs.cmidAcceptInput             =             "{AcceptInput}";
skapiDefs.cmidDeleteKey               =               "{DeleteKey}";
skapiDefs.cmidBackspaceKey            =            "{BackspaceKey}";
skapiDefs.cmidCopyText                =                "{CopyText}";
skapiDefs.cmidCutText                 =                 "{CutText}";
skapiDefs.cmidPasteText               =               "{PasteText}";
skapiDefs.cmidPlayerOption_ViewCombatTarget="{PlayerOption_ViewCombatTarget}";
skapiDefs.cmidPlayerOption_AdvancedCombatUI="{PlayerOption_AdvancedCombatUI}";
skapiDefs.cmidPlayerOption_AcceptLootPermits="{PlayerOption_AcceptLootPermits}";
skapiDefs.cmidPlayerOption_DisplayAllegianceLogonNotifications="{PlayerOption_DisplayAllegianceLogonNotifications}";
skapiDefs.cmidPlayerOption_UseChargeAttack="{PlayerOption_UseChargeAttack}";
skapiDefs.cmidPlayerOption_UseCraftSuccessDialog="{PlayerOption_UseCraftSuccessDialog}";
skapiDefs.cmidPlayerOption_HearAllegianceChat="{PlayerOption_HearAllegianceChat}";
skapiDefs.cmidPlayerOption_DisplayDateOfBirth="{PlayerOption_DisplayDateOfBirth}";
skapiDefs.cmidPlayerOption_DisplayAge = "{PlayerOption_DisplayAge}";
skapiDefs.cmidPlayerOption_DisplayChessRank="{PlayerOption_DisplayChessRank}";
skapiDefs.cmidPlayerOption_DisplayFishingSkill="{PlayerOption_DisplayFishingSkill}";
skapiDefs.cmidPlayerOption_DisplayNumberDeaths="{PlayerOption_DisplayNumberDeaths}";
skapiDefs.cmidPlayerOption_DisplayTimeStamps="{PlayerOption_DisplayTimeStamps}";
skapiDefs.cmidPlayerOption_SalvageMultiple="{PlayerOption_SalvageMultiple}";
skapiDefs.cmidPlayerOption_AutoRepeatAttack="{PlayerOption_AutoRepeatAttack}";
skapiDefs.cmidPlayerOption_IgnoreAllegianceRequests="{PlayerOption_IgnoreAllegianceRequests}";
skapiDefs.cmidPlayerOption_IgnoreFellowshipRequests="{PlayerOption_IgnoreFellowshipRequests}";
skapiDefs.cmidPlayerOption_IgnoreTradeRequests="{PlayerOption_IgnoreTradeRequests}";
skapiDefs.cmidPlayerOption_PersistentAtDay="{PlayerOption_PersistentAtDay}";
skapiDefs.cmidPlayerOption_AllowGive  =  "{PlayerOption_AllowGive}";
skapiDefs.cmidPlayerOption_ShowTooltips="{PlayerOption_ShowTooltips}";
skapiDefs.cmidPlayerOption_UseDeception="{PlayerOption_UseDeception}";
skapiDefs.cmidPlayerOption_ToggleRun  =  "{PlayerOption_ToggleRun}";
skapiDefs.cmidPlayerOption_StayInChatMode="{PlayerOption_StayInChatMode}";
skapiDefs.cmidPlayerOption_AutoTarget = "{PlayerOption_AutoTarget}";
skapiDefs.cmidPlayerOption_VividTargetingIndicator="{PlayerOption_VividTargetingIndicator}";
skapiDefs.cmidPlayerOption_FellowshipShareXP="{PlayerOption_FellowshipShareXP}";
skapiDefs.cmidPlayerOption_FellowshipShareLoot="{PlayerOption_FellowshipShareLoot}";
skapiDefs.cmidPlayerOption_FellowshipAutoAcceptRequests="{PlayerOption_FellowshipAutoAcceptRequests}";
skapiDefs.cmidPlayerOption_StretchUI  =  "{PlayerOption_StretchUI}";
skapiDefs.cmidPlayerOption_CoordinatesOnRadar="{PlayerOption_CoordinatesOnRadar}";
skapiDefs.cmidPlayerOption_SpellDuration="{PlayerOption_SpellDuration}";
skapiDefs.cmidPlayerOption_DisableHouseRestrictionEffects="{PlayerOption_DisableHouseRestrictionEffects}";
skapiDefs.cmidPlayerOption_DragItemOnPlayerOpensSecureTrade="{PlayerOption_DragItemOnPlayerOpensSecureTrade}";
skapiDefs.cmidCameraCloser            =            "{CameraCloser}";
skapiDefs.cmidCameraFarther           =           "{CameraFarther}";
skapiDefs.cmidCameraLeftRotate        =        "{CameraLeftRotate}";
skapiDefs.cmidCameraLower             =             "{CameraLower}";
skapiDefs.cmidCameraRaise             =             "{CameraRaise}";
skapiDefs.cmidCameraRightRotate       =       "{CameraRightRotate}";
skapiDefs.cmidFirstPersonView         =         "{FirstPersonView}";
skapiDefs.cmidFloorView               =               "{FloorView}";
skapiDefs.cmidMapView                 =                 "{MapView}";
skapiDefs.cmidResetView               =               "{ResetView}";
skapiDefs.cmidShiftView               =               "{ShiftView}";
skapiDefs.cmidAutoRun                 =                 "{AutoRun}";
skapiDefs.cmidHoldRun                 =                 "{HoldRun}";
skapiDefs.cmidHoldSidestep            =            "{HoldSidestep}";
skapiDefs.cmidJump                    =                    "{Jump}";
skapiDefs.cmidSideStepLeft            =            "{SideStepLeft}";
skapiDefs.cmidSideStepRight           =           "{SideStepRight}";
skapiDefs.cmidTurnLeft                =                "{TurnLeft}";
skapiDefs.cmidTurnRight               =               "{TurnRight}";
skapiDefs.cmidWalkBackwards           =           "{WalkBackwards}";
skapiDefs.cmidWalkForward             =             "{WalkForward}";
skapiDefs.cmidHighAttack              =              "{HighAttack}";
skapiDefs.cmidLowAttack               =               "{LowAttack}";
skapiDefs.cmidMediumAttack            =            "{MediumAttack}";
skapiDefs.cmidToggleCombat            =            "{ToggleCombat}";
skapiDefs.cmidDecreasePowerSetting    =    "{DecreasePowerSetting}";
skapiDefs.cmidIncreasePowerSetting    =    "{IncreasePowerSetting}";
skapiDefs.cmidClosestCompassItem      =      "{ClosestCompassItem}";
skapiDefs.cmidClosestItem             =             "{ClosestItem}";
skapiDefs.cmidClosestMonster          =          "{ClosestMonster}";
skapiDefs.cmidClosestPlayer           =           "{ClosestPlayer}";
skapiDefs.cmidLastAttacker            =            "{LastAttacker}";
skapiDefs.cmidNextCompassItem         =         "{NextCompassItem}";
skapiDefs.cmidNextFellow              =              "{NextFellow}";
skapiDefs.cmidNextItem                =                "{NextItem}";
skapiDefs.cmidNextMonster             =             "{NextMonster}";
skapiDefs.cmidNextPlayer              =              "{NextPlayer}";
skapiDefs.cmidPreviousCompassItem     =     "{PreviousCompassItem}";
skapiDefs.cmidPreviousFellow          =          "{PreviousFellow}";
skapiDefs.cmidPreviousItem            =            "{PreviousItem}";
skapiDefs.cmidPreviousMonster         =         "{PreviousMonster}";
skapiDefs.cmidPreviousPlayer          =          "{PreviousPlayer}";
skapiDefs.cmidPreviousSelection       =       "{PreviousSelection}";
skapiDefs.cmidDropSelected            =            "{DropSelected}";
skapiDefs.cmidExamineSelected         =         "{ExamineSelected}";
skapiDefs.cmidGiveSelected            =            "{GiveSelected}";
skapiDefs.cmidAutosortSelected        =        "{AutosortSelected}";
skapiDefs.cmidSelectSelf              =              "{SelectSelf}";
skapiDefs.cmidSplitSelected           =           "{SplitSelected}";
skapiDefs.cmidUseSelected             =             "{UseSelected}";
skapiDefs.cmidAllegiancePanel         =         "{AllegiancePanel}";
skapiDefs.cmidAttributesPanel         =         "{AttributesPanel}";
skapiDefs.cmidCharacterInformationPanel="{CharacterInformationPanel}";
skapiDefs.cmidCharacterOptionsPanel   =   "{CharacterOptionsPanel}";
skapiDefs.cmidFellowshipPanel         =         "{FellowshipPanel}";
skapiDefs.cmidHarmfulSpellsPanel      =      "{HarmfulSpellsPanel}";
skapiDefs.cmidHelpfulSpellsPanel      =      "{HelpfulSpellsPanel}";
skapiDefs.cmidHousePanel              =              "{HousePanel}";
skapiDefs.cmidInventoryPanel          =          "{InventoryPanel}";
skapiDefs.cmidLinkStatusPanel         =         "{LinkStatusPanel}";
skapiDefs.cmidMapPanel                =                "{MapPanel}";
skapiDefs.cmidOptionsPanel            =            "{OptionsPanel}";
skapiDefs.cmidSkillsPanel             =             "{SkillsPanel}";
skapiDefs.cmidSoundAndGraphicsPanel   =   "{SoundAndGraphicsPanel}";
skapiDefs.cmidSpellComponentsPanel    =    "{SpellComponentsPanel}";
skapiDefs.cmidSpellbookPanel          =          "{SpellbookPanel}";
skapiDefs.cmidTradePanel              =              "{TradePanel}";
skapiDefs.cmidVitaePanel              =              "{VitaePanel}";
skapiDefs.cmidAcceptCorpseLooting     =     "{AcceptCorpseLooting}";
skapiDefs.cmidAdvancedCombatInterface = "{AdvancedCombatInterface}";
skapiDefs.cmidAttemptToDeceivePlayers = "{AttemptToDeceivePlayers}";
skapiDefs.cmidAutoCreateShortcuts     =     "{AutoCreateShortcuts}";
skapiDefs.cmidAutoRepeatAttacks       =       "{AutoRepeatAttacks}";
skapiDefs.cmidAutoTarget              =              "{AutoTarget}";
skapiDefs.cmidAutoTrackCombatTargets  =  "{AutoTrackCombatTargets}";
skapiDefs.cmidDisableHouseEffect      =      "{DisableHouseEffect}";
skapiDefs.cmidDisableWeather          =          "{DisableWeather}";
skapiDefs.cmidDisplayTooltips         =         "{DisplayTooltips}";
skapiDefs.cmidIgnoreAllegianceRequests="{IgnoreAllegianceRequests}";
skapiDefs.cmidIgnoreFellowshipRequests="{IgnoreFellowshipRequests}";
skapiDefs.cmidIgnoreTradeRequests     =     "{IgnoreTradeRequests}";
skapiDefs.cmidInvertMouseLook         =         "{InvertMouseLook}";
skapiDefs.cmidLetPlayersGiveYouItems  =  "{LetPlayersGiveYouItems}";
skapiDefs.cmidMuteOnLosingFocus       =       "{MuteOnLosingFocus}";
skapiDefs.cmidRightClickToMouseLook   =   "{RightClickToMouseLook}";
skapiDefs.cmidRunAsDefaultMovement    =    "{RunAsDefaultMovement}";
skapiDefs.cmidShareFellowshipLoot     =     "{ShareFellowshipLoot}";
skapiDefs.cmidShareFellowshipXP       =       "{ShareFellowshipXP}";
skapiDefs.cmidShowRadarCoordinates    =    "{ShowRadarCoordinates}";
skapiDefs.cmidShowSpellDurations      =      "{ShowSpellDurations}";
skapiDefs.cmidStayInChatModeAfterSend = "{StayInChatModeAfterSend}";
skapiDefs.cmidStretchUI               =               "{StretchUI}";
skapiDefs.cmidVividTargetIndicator    =    "{VividTargetIndicator}";
skapiDefs.cmidCaptureScreenshotToFile = "{CaptureScreenshotToFile}";

// dmty: Damage type masks
// dmty values may be combined using the bitwise OR operator ("|" in JScript, "Or" in VBScript) to indicate more than one type of damage.
skapiDefs.dmtySlashing                =                      0x0001;
skapiDefs.dmtyPiercing                =                      0x0002;
skapiDefs.dmtyBludgeoning             =                      0x0004;
skapiDefs.dmtyCold                    =                      0x0008;
skapiDefs.dmtyFire                    =                      0x0010;
skapiDefs.dmtyAcid                    =                      0x0020;
skapiDefs.dmtyElectrical              =                      0x0040;
skapiDefs.dmtyNether                  =                      0x0400;


// eqm: Equipment type masks
// eqm values may be combined using the bitwise OR operator ("|" in JScript, "Or" in VBScript) to specify multiple equipment slots.
skapiDefs.eqmNil                      =                           0;	// No slot.
skapiDefs.eqmHead                     =                  0x00000001;
skapiDefs.eqmChestUnder               =                  0x00000002;
skapiDefs.eqmAbdomenUnder             =                  0x00000004;
skapiDefs.eqmUpperArmsUnder           =                  0x00000008;
skapiDefs.eqmLowerArmsUnder           =                  0x00000010;
skapiDefs.eqmHands                    =                  0x00000020;
skapiDefs.eqmUpperLegsUnder           =                  0x00000040;
skapiDefs.eqmLowerLegsUnder           =                  0x00000080;
skapiDefs.eqmFeet                     =                  0x00000100;
skapiDefs.eqmChestOuter               =                  0x00000200;
skapiDefs.eqmAbdomenOuter             =                  0x00000400;
skapiDefs.eqmUpperArmsOuter           =                  0x00000800;
skapiDefs.eqmLowerArmsOuter           =                  0x00001000;
skapiDefs.eqmUpperLegsOuter           =                  0x00002000;
skapiDefs.eqmLowerLegsOuter           =                  0x00004000;
skapiDefs.eqmNecklace                 =                  0x00008000;
skapiDefs.eqmRBracelet                =                  0x00010000;
skapiDefs.eqmLBracelet                =                  0x00020000;
skapiDefs.eqmBracelets                =                  0x00030000;	// Both bracelet slots.
skapiDefs.eqmRRing                    =                  0x00040000;
skapiDefs.eqmLRing                    =                  0x00080000;
skapiDefs.eqmRings                    =                  0x000C0000;	// Both ring slots.
skapiDefs.eqmWeapon                   =                  0x00100000;	// Melee weapon slot.
skapiDefs.eqmShield                   =                  0x00200000;
skapiDefs.eqmRangedWeapon             =                  0x00400000;	// Bow or crossbow slot.
skapiDefs.eqmAmmo                     =                  0x00800000;
skapiDefs.eqmFocusWeapon              =                  0x01000000;	// Wand, staff, or orb slot.
skapiDefs.eqmTwoHand                  =                  0x02000000;	// Two-handed weapons.
skapiDefs.eqmTrinketOne               =                  0x04000000,
skapiDefs.eqmCloak                    =                  0x08000000,
skapiDefs.eqmSigilOne                 =                  0x10000000,    // Blue aetheria
skapiDefs.eqmSigilTwo                 =                  0x20000000,    // Yellow aetheria
skapiDefs.eqmSigilThree               =                  0x40000000,    // Red aetheria
skapiDefs.eqmWeapons                  =                  0x03500000;	// All weapons of any kind.
skapiDefs.eqmAll                      =                  0x7FFFFFFF;	// All equipment slots.

// evid: Event IDs
skapiDefs.evidNil                     = skapiDefs.evidNil =               0;	// No event was received.
skapiDefs.evidOnVitals                =                           1;
skapiDefs.evidOnLocChangeSelf         =                           2;
skapiDefs.evidOnLocChangeOther        =                           3;
skapiDefs.evidOnStatTotalBurden       =                           4;
skapiDefs.evidOnStatTotalExp          =                           5;
skapiDefs.evidOnStatUnspentExp        =                           6;
skapiDefs.evidOnObjectCreatePlayer    =                           7;
skapiDefs.evidOnStatSkillExp          =                           8;
skapiDefs.evidOnAdjustStack           =                           9;
skapiDefs.evidOnChatLocal             = skapiDefs.evidOnChatLocal =      10;
skapiDefs.evidOnChatSpell             =                          11;
skapiDefs.evidOnDeathSelf             =                          12;
skapiDefs.evidOnMyPlayerKill          =                          13;
skapiDefs.evidOnDeathOther            =                          14;
skapiDefs.evidOnEnd3D                 =                          15;
skapiDefs.evidOnStart3D               =                          16;
skapiDefs.evidOnStartPortalSelf       =                          17;
skapiDefs.evidOnEndPortalSelf         =                          18;
skapiDefs.evidOnEndPortalOther        =                          19;
skapiDefs.evidOnCombatMode            =                          20;
skapiDefs.evidOnAnimationSelf         =                          21;
skapiDefs.evidOnTargetHealth          =                          22;
skapiDefs.evidOnAddToInventory        = skapiDefs.evidOnAddToInventory = 23;
skapiDefs.evidOnRemoveFromInventory   =                          24;
skapiDefs.evidOnAssessCreature        =                          25;
skapiDefs.evidOnAssessItem            =                          26;
skapiDefs.evidOnSpellFailSelf         =                          27;
skapiDefs.evidOnMeleeEvadeSelf        =                          28;
skapiDefs.evidOnMeleeEvadeOther       =                          29;
skapiDefs.evidOnMeleeDamageSelf       =                          30;
skapiDefs.evidOnMeleeDamageOther      =                          31;
skapiDefs.evidOnLogon                 =                          32;
skapiDefs.evidOnTellServer            =                          33;
skapiDefs.evidOnTell                  = skapiDefs.evidOnTell =           34;
skapiDefs.evidOnTellMelee             =                          35;
skapiDefs.evidOnTellMisc              =                          36;
skapiDefs.evidOnTellFellowship        = skapiDefs.evidOnTellFellowship = 37;
skapiDefs.evidOnTellPatron            =                          38;
skapiDefs.evidOnTellVassal            =                          39;
skapiDefs.evidOnTellFollower          =                          40;
skapiDefs.evidOnDeathMessage          =                          41;
skapiDefs.evidOnSpellCastSelf         =                          42;
skapiDefs.evidOnTradeStart            =                          43;
skapiDefs.evidOnTradeEnd              = skapiDefs.evidOnTradeEnd =       44;
skapiDefs.evidOnTradeAdd              = skapiDefs.evidOnTradeAdd =       45;
skapiDefs.evidOnTradeReset            = skapiDefs.evidOnTradeReset =     46;
skapiDefs.evidOnSetFlagSelf           =                          47;
skapiDefs.evidOnSetFlagOther          =                          48;
skapiDefs.evidOnApproachVendor        =                          49;
skapiDefs.evidOnChatBroadcast         =                          50;
skapiDefs.evidOnTradeAccept           =                          51;
skapiDefs.evidOnObjectCreate          =                          52;
skapiDefs.evidOnChatServer            = skapiDefs.evidOnChatServer =     53;
skapiDefs.evidOnSpellExpire           =                          54;
skapiDefs.evidOnSpellExpireSilent     =                          55;
skapiDefs.evidOnStatTotalPyreals      =                          56;
skapiDefs.evidOnStatLevel             =                          57;
skapiDefs.evidOnChatEmoteStandard     =                          58;
skapiDefs.evidOnChatEmoteCustom       = skapiDefs.evidOnChatEmoteCustom = 59;
skapiDefs.evidOnOpenContainer         =                          60;
skapiDefs.evidOnPortalStormWarning    =                          61;
skapiDefs.evidOnPortalStormed         =                          62;
skapiDefs.evidOnCommand               =                          63;
skapiDefs.evidOnControlEvent          = skapiDefs.evidOnControlEvent =   64;
skapiDefs.evidOnItemManaBar           =                          65;
skapiDefs.evidOnActionComplete        =                          66;
skapiDefs.evidOnObjectDestroy         =                          67;
skapiDefs.evidOnLocChangeCreature     =                          68;
skapiDefs.evidOnMoveItem              =                          69;
skapiDefs.evidOnSpellAdd              =                          70;
skapiDefs.evidOnFellowCreate          =                          71;
skapiDefs.evidOnFellowDisband         =                          72;
skapiDefs.evidOnFellowDismiss         =                          73;
skapiDefs.evidOnFellowQuit            =                          74;
skapiDefs.evidOnFellowRecruit         =                          75;
skapiDefs.evidOnMeleeLastAttacker     =                          76;
skapiDefs.evidOnLeaveVendor           =                          77;
skapiDefs.evidOnTimer                 = skapiDefs.evidOnTimer =          78;
skapiDefs.evidOnControlProperty       =                          79;
skapiDefs.evidOnPluginMsg             =                          80;
skapiDefs.evidOnTellAllegiance        = skapiDefs.evidOnTellAllegiance = 81;
skapiDefs.evidOnTellCovassal          =                          82;
skapiDefs.evidOnHotkey                =                          83;
skapiDefs.evidOnFellowInvite          =                          84;
skapiDefs.evidOnNavStop               =                          85;
skapiDefs.evidOnFellowUpdate          =                          86;
skapiDefs.evidOnDisconnect            =                          87;
skapiDefs.evidOnCraftConfirm          =                          88;
skapiDefs.evidOnChatBoxMessage        = skapiDefs.evidOnChatBoxMessage = 89;
skapiDefs.evidOnTipMessage            = skapiDefs.evidOnTipMessage =     90;
skapiDefs.evidOnOpenContainerPanel    =                          91;
skapiDefs.evidOnCloseContainer        =                          92;
skapiDefs.evidOnChatBoxClick          =                          93;
skapiDefs.evidMax                     =                          94;
skapiDefs.evidOnTick                  =                          95;
skapiDefs.evidOnObjectUpdate		  =							 96;

// ioc: Icon outline color masks
// ioc values may be combined using the bitwise OR operator ("|" in JScript, "Or" in VBScript).
skapiDefs.iocNil                      =                           0;
skapiDefs.iocEnchanted                =                  0x00000001;	// Has spells.
skapiDefs.iocUnknown2                 =                  0x00000002;	// (No known meaninskapiDefs.)
skapiDefs.iocHealing                  =                  0x00000004;	// Restores health.
skapiDefs.iocMana                     =                  0x00000008;	// Restores mana.
skapiDefs.iocHearty                   =                  0x00000010;	// Restores stamina.
skapiDefs.iocFire                     =                  0x00000020;
skapiDefs.iocLightning                =                  0x00000040;
skapiDefs.iocFrost                    =                  0x00000080;
skapiDefs.iocAcid                     =                  0x00000100;

// kmo: Key motions
skapiDefs.kmoDown                     =                        0x01;	// Press the key.
skapiDefs.kmoUp                       =                        0x02;	// Release the key.
skapiDefs.kmoClick                    =                        0x03;	// Press and release.

// material: Material codes
skapiDefs.materialCeramic             = skapiDefs.materialCeramic =          1;
skapiDefs.materialPorcelain           = skapiDefs.materialPorcelain =	       2;
skapiDefs.materialCloth               = skapiDefs.materialCloth =            3;
skapiDefs.materialLinen               = skapiDefs.materialLinen =            4;
skapiDefs.materialSatin               = skapiDefs.materialSatin =            5;
skapiDefs.materialSilk                = skapiDefs.materialSilk =             6;
skapiDefs.materialVelvet              = skapiDefs.materialVelvet =           7;
skapiDefs.materialWool                = skapiDefs.materialWool =             8;
skapiDefs.materialGem                 = skapiDefs.materialGem =              9;
skapiDefs.materialAgate               = skapiDefs.materialAgate =            10;
skapiDefs.materialAmber               = skapiDefs.materialAmber =            11;
skapiDefs.materialAmethyst            = skapiDefs.materialAmethyst =         12;
skapiDefs.materialAquamarine          = skapiDefs.materialAquamarine =       13;
skapiDefs.materialAzurite             = skapiDefs.materialAzurite =          14;
skapiDefs.materialBlackGarnet         = skapiDefs.materialBlackGarnet =      15;
skapiDefs.materialBlackOpal           = skapiDefs.materialBlackOpal =        16;
skapiDefs.materialBloodstone          = skapiDefs.materialBloodstone =       17;
skapiDefs.materialCarnelian           = skapiDefs.materialCarnelian =        18;
skapiDefs.materialCitrine             = skapiDefs.materialCitrine =          19;
skapiDefs.materialDiamond             = skapiDefs.materialDiamond =          20;
skapiDefs.materialEmerald             = skapiDefs.materialEmerald =          21;
skapiDefs.materialFireOpal            = skapiDefs.materialFireOpal =         22;
skapiDefs.materialGreenGarnet         = skapiDefs.materialGreenGarnet =      23;
skapiDefs.materialGreenJade           = skapiDefs.materialGreenJade =        24;
skapiDefs.materialHematite            = skapiDefs.materialHematite =         25;
skapiDefs.materialImperialTopaz       = skapiDefs.materialImperialTopaz =    26;
skapiDefs.materialJet                 = skapiDefs.materialJet =              27;
skapiDefs.materialLapisLazuli         = skapiDefs.materialLapisLazuli =      28;
skapiDefs.materialLavenderJade        = skapiDefs.materialLavenderJade =     29;
skapiDefs.materialMalachite           = skapiDefs.materialMalachite =        30;
skapiDefs.materialMoonstone           = skapiDefs.materialMoonstone =        31;
skapiDefs.materialOnyx                = skapiDefs.materialOnyx =             32;
skapiDefs.materialOpal                = skapiDefs.materialOpal =             33;
skapiDefs.materialPeridot             = skapiDefs.materialPeridot =          34;
skapiDefs.materialRedGarnet           = skapiDefs.materialRedGarnet =        35;
skapiDefs.materialRedJade             = skapiDefs.materialRedJade =          36;
skapiDefs.materialRoseQuartz          = skapiDefs.materialRoseQuartz =       37;
skapiDefs.materialRuby                = skapiDefs.materialRuby =             38;
skapiDefs.materialSapphire            = skapiDefs.materialSapphire =         39;
skapiDefs.materialSmokeyQuartz        = skapiDefs.materialSmokeyQuartz =     40; // Original SkapiDefs is spelled Smokey.
skapiDefs.materialSmokyQuartz         = skapiDefs.materialSmokyQuartz =      40; // In game material is spelled Smoky.
skapiDefs.materialSunstone            = skapiDefs.materialSunstone =         41;
skapiDefs.materialTigerEye            = skapiDefs.materialTigerEye =         42;
skapiDefs.materialTourmaline          = skapiDefs.materialTourmaline =       43;
skapiDefs.materialTurquoise           = skapiDefs.materialTurquoise =        44;
skapiDefs.materialWhiteJade           = skapiDefs.materialWhiteJade =        45;
skapiDefs.materialWhiteQuartz         = skapiDefs.materialWhiteQuartz =      46;
skapiDefs.materialWhiteSapphire       = skapiDefs.materialWhiteSapphire =    47;
skapiDefs.materialYellowGarnet        = skapiDefs.materialYellowGarnet =     48;
skapiDefs.materialYellowTopaz         = skapiDefs.materialYellowTopaz =      49;
skapiDefs.materialZircon              = skapiDefs.materialZircon =           50;
skapiDefs.materialIvory               = skapiDefs.materialIvory =            51;
skapiDefs.materialLeather             = skapiDefs.materialLeather =          52;
skapiDefs.materialDilloHide           = skapiDefs.materialDilloHide =        53;
skapiDefs.materialGromnieHide         = skapiDefs.materialGromnieHide =      54;
skapiDefs.materialReedsharkHide       = skapiDefs.materialReedsharkHide =    55;
skapiDefs.materialMetal               = skapiDefs.materialMetal =            56;
skapiDefs.materialBrass               = skapiDefs.materialBrass =            57;
skapiDefs.materialBronze              = skapiDefs.materialBronze =           58;
skapiDefs.materialCopper              = skapiDefs.materialCopper =           59;
skapiDefs.materialGold                = skapiDefs.materialGold =             60;
skapiDefs.materialIron                = skapiDefs.materialIron =             61;
skapiDefs.materialPyreal              = skapiDefs.materialPyreal =           62;
skapiDefs.materialSilver              = skapiDefs.materialSilver =           63;
skapiDefs.materialSteel               = skapiDefs.materialSteel =            64;
skapiDefs.materialStone               = skapiDefs.materialStone =            65;
skapiDefs.materialAlabaster           = skapiDefs.materialAlabaster =        66;
skapiDefs.materialGranite             = skapiDefs.materialGranite =          67;
skapiDefs.materialMarble              = skapiDefs.materialMarble =           68;
skapiDefs.materialObsidian            = skapiDefs.materialObsidian =         69;
skapiDefs.materialSandstone           = skapiDefs.materialSandstone =        70;
skapiDefs.materialSerpentine          = skapiDefs.materialSerpentine =       71;
skapiDefs.materialWood                = skapiDefs.materialWood =             72;
skapiDefs.materialEbony               = skapiDefs.materialEbony =            73;
skapiDefs.materialMahogany            = skapiDefs.materialMahogany =         74;
skapiDefs.materialOak                 = skapiDefs.materialOak =              75;
skapiDefs.materialPine                = skapiDefs.materialPine =             76;
skapiDefs.materialTeak                = skapiDefs.materialTeak =             77;

// mcm: Merchant category masks
skapiDefs.mcmNil                      =                  0x00000000;	// No category; not saleable.
skapiDefs.mcmWeaponsMelee             =                  0x00000001;
skapiDefs.mcmArmor                    =                  0x00000002;
skapiDefs.mcmClothing                 =                  0x00000004;
skapiDefs.mcmJewelry                  =                  0x00000008;
skapiDefs.mcmCreature                 =                  0x00000010;
skapiDefs.mcmFood                     =                  0x00000020;
skapiDefs.mcmPyreal                   =                  0x00000040;
skapiDefs.mcmMisc                     =                  0x00000080;
skapiDefs.mcmWeaponsMissile           =                  0x00000100;
skapiDefs.mcmContainers               =                  0x00000200;
skapiDefs.mcmMiscFletching            =                  0x00000400;
skapiDefs.mcmGems                     =                  0x00000800;
skapiDefs.mcmSpellComponents          =                  0x00001000;
skapiDefs.mcmBooksPaper               =                  0x00002000;
skapiDefs.mcmKeysTools                =                  0x00004000;
skapiDefs.mcmMagicItems               =                  0x00008000;
skapiDefs.mcmPortal                   =                  0x00010000;
skapiDefs.mcmLockable                 =                  0x00020000;	// Chests have this is addition to mcmContainers.
skapiDefs.mcmTradeNotes               =                  0x00040000;
skapiDefs.mcmManaStones               =                  0x00080000;
skapiDefs.mcmServices                 =                  0x00100000;
skapiDefs.mcmPlants                   =                  0x00200000;
skapiDefs.mcmCookingItems1            =                  0x00400000;
skapiDefs.mcmAlchemicalItems1         =                  0x00800000;
skapiDefs.mcmFletchingItems1          =                  0x01000000;
skapiDefs.mcmCookingItems2            =                  0x02000000;
skapiDefs.mcmAlchemicalItems2         =                  0x04000000;
skapiDefs.mcmFletchingItems2          =                  0x08000000;
skapiDefs.mcmLifestone                =                  0x10000000;
skapiDefs.mcmTinkeringTools           =                  0x20000000;	// The Ust is in this category.
skapiDefs.mcmSalvageBag               = skapiDefs.mcmSalvageBag = 0x40000000;
skapiDefs.mcmChessboard               =                  0x80000000;	// There may be other items in this category as well.

// Miscellaneous constants
skapiDefs.oidNil                      =                           0;	// Signifies no object.
skapiDefs.ipackMain                   =                           0;	// Index of the main pack.
skapiDefs.iitemNil                    =                          -1;	// Signifies the pack itself.
skapiDefs.clrNil                      =                  0x7FFFFFFF;	// VBScript has a problem with FFFFFFFF (turns it into 0000FFFF).
skapiDefs.spellidNil                  =                           0;	// Signifies no spell.

// ibutton: Mouse button IDs
skapiDefs.ibuttonLeft                 =                           0;	// Left button.
skapiDefs.ibuttonRight                =                           1;	// Right button.
skapiDefs.ibuttonMiddle               =                           2;	// Middle button.

// nopt: Navigation option masks
skapiDefs.noptWalk                    =                      0x0001;
skapiDefs.noptStopOnArrival           =                      0x0002;
skapiDefs.noptZigzag                  =                      0x0004;

// nsc: Navigation stop codes for OnNavStop
skapiDefs.nscArrived                  =                           0;	// You arrived at the target location.
skapiDefs.nscTimeout                  =                           1;	// You did not arrive before the timeout expired.
skapiDefs.nscCanceled                 =                           2;	// skapi.CancelNav was called.

// ocm: Object category masks
// ocm values may be combined using the bitwise OR operator ("|" in JScript, "Or" in VBScript) to specify multiple categories.
skapiDefs.ocmNil                      = skapiDefs.ocmNil =                     0x0000;	// Include no objects.
skapiDefs.ocmPlayer                   = skapiDefs.ocmPlayer =                     0x0001;
skapiDefs.ocmMonster                  = skapiDefs.ocmMonster =                     0x0002;
skapiDefs.ocmPlayerCorpse             = skapiDefs.ocmPlayerCorpse =                     0x0004;
skapiDefs.ocmMonsterCorpse            = skapiDefs.ocmMonsterCorpse =                      0x0008;
skapiDefs.ocmLifestone                = skapiDefs.ocmLifestone =                     0x0010;
skapiDefs.ocmPortal                   = skapiDefs.ocmPortal =                     0x0020;
skapiDefs.ocmMerchant                 = skapiDefs.ocmMerchant =                     0x0040;
skapiDefs.ocmEquipment                = skapiDefs.ocmEquipment =                     0x0080;
skapiDefs.ocmPK                       = skapiDefs.ocmPK =                     0x0100;
skapiDefs.ocmNonPK                    = skapiDefs.ocmNonPK =                     0x0200;
skapiDefs.ocmNPC                      = skapiDefs.ocmNPC =                     0x0400;
skapiDefs.ocmHook                     = skapiDefs.ocmHook =                     0x0800;
skapiDefs.ocmAll                      = skapiDefs.ocmAll =                         -1;	// Include all nearby objects of any kind.

// olc: Object location categories
// olc values may be combined using the bitwise OR operator ("|" in JScript, "Or" in VBScript) to specify multiple categories.
skapiDefs.olcNil                      =                      0x0000;	// Include no objects.
skapiDefs.olcInventory                =                      0x0001;	// Include objects in your inventory.
skapiDefs.olcContained                =                      0x0002;	// Include objects contained in another object (e.skapiDefs. a chest or corpse).
skapiDefs.olcEquipped                 = skapiDefs.olcEquipped =      0x0004;	// Include objects being worn or wielded (by you or someone else).
skapiDefs.olcOnGround                 =                      0x0008;	// Include objects out on their own in the game world.
skapiDefs.olcAll                      =                          -1;	// Include all object regardless of location.

// oty: Object type masks
skapiDefs.otyContainer                =                  0x00000001;
skapiDefs.otyInscribable              =                  0x00000002;
skapiDefs.otyNoPickup                 =                  0x00000004;
skapiDefs.otyPlayer                   =                  0x00000008;
skapiDefs.otySelectable               =                  0x00000010;
skapiDefs.otyPK                       =                  0x00000020;
skapiDefs.otyReadable                 =                  0x00000100;
skapiDefs.otyMerchant                 =                  0x00000200;
skapiDefs.otyDoor                     =                  0x00001000;
skapiDefs.otyCorpse                   =                  0x00002000;
skapiDefs.otyLifestone                =                  0x00004000;
skapiDefs.otyFood                     =                  0x00008000;
skapiDefs.otyHealingKit               =                  0x00010000;
skapiDefs.otyLockpick                 =                  0x00020000;
skapiDefs.otyPortal                   =                  0x00040000;
skapiDefs.otyFoci                     =                  0x00800000;
skapiDefs.otyPKL                      =                  0x02000000;
skapiDefs.otyIncludesSecondHeader     =                  0x04000000;
skapiDefs.otyBindStone                =                  0x08000000;
skapiDefs.otyVolatileRare             =                  0x10000000;
skapiDefs.otyWieldOnUse               =                  0x20000000;
skapiDefs.WieldLeft                   =                  0x40000000;

// opm: Output modes
// opm values may be combined using the bitwise OR operator ("|" in JScript, "Or" in VBScript) to send output to multiple destinations.
skapiDefs.opmDebugLog                 =                      0x0001;	// Sends the output to the debug terminal (using the Win32 OutputDebugString API).
skapiDefs.opmConsole                  =                      0x0002;	// Sends the output to the SkunkWorks console window.
skapiDefs.opmChatWnd                  =                      0x0010;	// Sends the output to the primary in-game chat window.
skapiDefs.opmChatWnd1                 =                      0x0020;	// Sends the output to floating chat window #1.
skapiDefs.opmChatWnd2                 =                      0x0040;	// Sends the output to floating chat window #2.
skapiDefs.opmChatWnd3                 =                      0x0080;	// Sends the output to floating chat window #3.
skapiDefs.opmChatWnd4                 =                      0x0100;	// Sends the output to floating chat window #4.
skapiDefs.opmChatWndAll               =                      0x01F0;	// Sends the output to all in-game chat windows.

// plig: Player in game codes
skapiDefs.pligNotRunning              =                           0;	// Game is not runninskapiDefs.
skapiDefs.pligAtLogin                 =                           1;	// Game is at login screen.
skapiDefs.pligInPortal                =                           2;	// Player is in portal space.
skapiDefs.pligInWorld                 =                           3;	// Player is in the game world.

// psw: Portal storm warning codes
skapiDefs.pswStormEnded               =                           0;	// The portal storm is over.
skapiDefs.pswMildStorm                =                           1;	// Mild portal storm.
skapiDefs.pswHeavyStorm               =                           2;	// Heavy portal storm.

// skid: Skill IDs
skapiDefs.skidNil                     = skapiDefs.skidNil =                          0;	// No skill.
skapiDefs.skidAxe                     = skapiDefs.skidAxe =                          1;
skapiDefs.skidBow                     = skapiDefs.skidBow =                          2;
skapiDefs.skidCrossbow                = skapiDefs.skidCrossbow =                          3;
skapiDefs.skidDagger                  = skapiDefs.skidDagger =                          4;
skapiDefs.skidMace                    = skapiDefs.skidMace =                          5;
skapiDefs.skidMeleeDefense            = skapiDefs.skidMeleeDefense =                          6;
skapiDefs.skidMissileDefense          = skapiDefs.skidMissileDefense =                          7;
skapiDefs.skidSpear                   = skapiDefs.skidSpear =                          9;
skapiDefs.skidStaff                   = skapiDefs.skidStaff =                         10;
skapiDefs.skidSword                   = skapiDefs.skidSword =                         11;
skapiDefs.skidThrownWeapons           = skapiDefs.skidThrownWeapons =                         12;
skapiDefs.skidUnarmedCombat           = skapiDefs.skidUnarmedCombat =                         13;
skapiDefs.skidArcaneLore              = skapiDefs.skidArcaneLore =                         14;
skapiDefs.skidMagicDefense            = skapiDefs.skidMagicDefense =                         15;
skapiDefs.skidManaConversion          = skapiDefs.skidManaConversion =                         16;
skapiDefs.skidItemTinkering           = skapiDefs.skidItemTinkering =                         18;
skapiDefs.skidAssessPerson            = skapiDefs.skidAssessPerson =                         19;
skapiDefs.skidDeception               = skapiDefs.skidDeception =                         20;
skapiDefs.skidHealing                 = skapiDefs.skidHealing =                         21;
skapiDefs.skidJump                    = skapiDefs.skidJump =                         22;
skapiDefs.skidLockpick                = skapiDefs.skidLockpick =                         23;
skapiDefs.skidRun                     = skapiDefs.skidRun =                         24;
skapiDefs.skidAssessCreature          = skapiDefs.skidAssessCreature =                         27;
skapiDefs.skidWeaponTinkering         = skapiDefs.skidWeaponTinkering =                         28;
skapiDefs.skidArmorTinkering          = skapiDefs.skidArmorTinkering =                         29;
skapiDefs.skidMagicItemTinkering      = skapiDefs.skidMagicItemTinkering =                         30;
skapiDefs.skidCreatureEnchantment     = skapiDefs.skidCreatureEnchantment =                         31;
skapiDefs.skidItemEnchantment         = skapiDefs.skidItemEnchantment =                         32;
skapiDefs.skidLifeMagic               = skapiDefs.skidLifeMagic =                         33;
skapiDefs.skidWarMagic                = skapiDefs.skidWarMagic =           34;
skapiDefs.skidLeadership              = skapiDefs.skidLeadership =                         35;
skapiDefs.skidLoyalty                 = skapiDefs.skidLoyalty =                         36;
skapiDefs.skidFletching               = skapiDefs.skidFletching =                         37;
skapiDefs.skidAlchemy                 = skapiDefs.skidAlchemy =                         38;
skapiDefs.skidCooking                 = skapiDefs.skidCooking =                         39;
skapiDefs.skidSalvaging               = skapiDefs.skidSalvaging =                         40;
skapiDefs.skidTwoHanded               = skapiDefs.skidTwoHanded =                         41;
skapiDefs.skidGearcraft               = skapiDefs.skidGearcraft =                         42;
skapiDefs.skidVoidMagic               = skapiDefs.skidVoidMagic =                         43;
skapiDefs.skidHeavyWeapons            = skapiDefs.skidHeavyWeapons =                         44;
skapiDefs.skidLightWeapons            = skapiDefs.skidLightWeapons =                         45;
skapiDefs.skidFinesseWeapons          = skapiDefs.skidFinesseWeapons =                         46;
skapiDefs.skidMissileWeapons          = skapiDefs.skidMissileWeapons =                         47;
skapiDefs.skidShield                  = skapiDefs.skidShield =                         48;
skapiDefs.skidDualWield               = skapiDefs.skidDualWield =                         49;
skapiDefs.skidRecklessness            = skapiDefs.skidRecklessness =                         50;
skapiDefs.skidSneakAttack             = skapiDefs.skidSneakAttack =                         51;
skapiDefs.skidDirtyFighting           = skapiDefs.skidDirtyFighting =                         52;
skapiDefs.skidSummoning               = skapiDefs.skidSummoning =                         54;
skapiDefs.skidMax                     = skapiDefs.skidMax =                         60;

// skts: Skill training status codes
skapiDefs.sktsUnusable                =                           0;
skapiDefs.sktsUntrained               =                           1;
skapiDefs.sktsTrained                 =                           2;
skapiDefs.sktsSpecialized             =                           3;

// Special key names
// {alt}
// {back}                                                        // Backspace
// {break}                                                       // Ctrl+Break
// {capslock}
// {control}
// {del}
// {down}                                                        // Down arrow
// {end}
// {esc}
// {f1}
// {f2}
// {f3}
// {f4}
// {f5}
// {f6}
// {f7}
// {f8}
// {f9}
// {f10}
// {f11}
// {f12}
// {home}
// {ins}
// {keypad 0}
// {keypad 1}
// {keypad 2}
// {keypad 3}
// {keypad 4}
// {keypad 5}
// {keypad 6}
// {keypad 7}
// {keypad 8}
// {keypad 9}
// {keypad *}
// {keypad +}
// {keypad -}
// {keypad /}
// {keypad .}
// {keypad enter}                                                // Keypad Enter
// {left}                                                        // Left arrow
// {numlock}
// {pause}
// {pgdn}
// {pgup}
// {prtsc}                                                       // Print Screen
// {return}                                                      // Enter (the regular one, above RShift)
// {right}                                                       // Right arrow
// {scroll}                                                      // Scroll Lock
// {shift}
// {space}                                                       // Spacebar
// {tab}
// {up}                                                          // Up arrow

// species: Species codes
skapiDefs.speciesInvalid              =                           0;
skapiDefs.speciesOlthoi               =                           1;
skapiDefs.speciesBanderling           =                           2;
skapiDefs.speciesDrudge               =                           3;
skapiDefs.speciesMosswart             =                           4;
skapiDefs.speciesLugian               =                           5;
skapiDefs.speciesTumerok              =                           6;
skapiDefs.speciesMite                 =                           7;
skapiDefs.speciesTusker               =                           8;
skapiDefs.speciesPhyntosWasp          =                           9;
skapiDefs.speciesRat                  =                          10;
skapiDefs.speciesAuroch               =                          11;
skapiDefs.speciesCow                  =                          12;
skapiDefs.speciesGolem                =                          13;
skapiDefs.speciesUndead               =                          14;
skapiDefs.speciesGromnie              =                          15;
skapiDefs.speciesReedshark            =                          16;
skapiDefs.speciesArmoredillo          =                          17;
skapiDefs.speciesFae                  =                          18;
skapiDefs.speciesVirindi              =                          19;
skapiDefs.speciesWisp                 =                          20;
skapiDefs.speciesKnathtead            =                          21;
skapiDefs.speciesShadow               =                          22;
skapiDefs.speciesMattekar             =                          23;
skapiDefs.speciesMumiyah              =                          24;
skapiDefs.speciesRabbit               =                          25;
skapiDefs.speciesSclavus              =                          26;
skapiDefs.speciesShallowsShark        =                          27;
skapiDefs.speciesMonouga              =                          28;
skapiDefs.speciesZefir                =                          29;
skapiDefs.speciesSkeleton             =                          30;
skapiDefs.speciesHuman                =                          31;
skapiDefs.speciesShreth               =                          32;
skapiDefs.speciesChittick             =                          33;
skapiDefs.speciesMoarsman             =                          34;
skapiDefs.speciesOlthoiLarvae         =                          35;
skapiDefs.speciesSlithis              =                          36;
skapiDefs.speciesDeru                 =                          37;
skapiDefs.speciesFireElemental        =                          38;
skapiDefs.speciesSnowman              =                          39;
skapiDefs.speciesUnknown              =                          40;
skapiDefs.speciesBunny                =                          41;
skapiDefs.speciesLightningElemental   =                          42;
skapiDefs.speciesRockslide            =                          43;
skapiDefs.speciesGrievver             =                          44;
skapiDefs.speciesNiffis               =                          45;
skapiDefs.speciesUrsuin               =                          46;
skapiDefs.speciesCrystal              =                          47;
skapiDefs.speciesHollowMinion         =                          48;
skapiDefs.speciesScarecrow            =                          49;
skapiDefs.speciesIdol                 =                          50;
skapiDefs.speciesEmpyrean             =                          51;
skapiDefs.speciesHopeslayer           =                          52;
skapiDefs.speciesDoll                 =                          53;
skapiDefs.speciesMarionette           =                          54;
skapiDefs.speciesCarenzi              =                          55;
skapiDefs.speciesSiraluun             =                          56;
skapiDefs.speciesAunTumerok           =                          57;
skapiDefs.speciesHeaTumerok           =                          58;
skapiDefs.speciesSimulacrum           =                          59;
skapiDefs.speciesAcidElemental        =                          60;
skapiDefs.speciesFrostElemental       =                          61;
skapiDefs.speciesElemental            =                          62;
skapiDefs.speciesStatue               =                          63;
skapiDefs.speciesWall                 =                          64;
skapiDefs.speciesAlteredHuman         =                          65;
skapiDefs.speciesDevice               =                          66;
skapiDefs.speciesHarbinger            =                          67;
skapiDefs.speciesDarkSarcophagus      =                          68;
skapiDefs.speciesChicken              =                          69;
skapiDefs.speciesGotrokLugian         =                          70;
skapiDefs.speciesMargul               =                          71;
skapiDefs.speciesBleachedRabbit       =                          72;
skapiDefs.speciesNastyRabbit          =                          73;
skapiDefs.speciesGrimacingRabbit      =                          74;
skapiDefs.speciesBurun                =                          75;
skapiDefs.speciesTarget               =                          76;
skapiDefs.speciesGhost                =                          77;
skapiDefs.speciesFiun                 =                          78;
skapiDefs.speciesEater                =                          79;
skapiDefs.speciesPenguin              =                          80;
skapiDefs.speciesRuschk               =                          81;
skapiDefs.speciesThrungus             =                          82;
skapiDefs.speciesViamontianKnight     =                          83;
skapiDefs.speciesRemoran              =                          84;
skapiDefs.speciesSwarm                =                          85;
skapiDefs.speciesMoar                 =                          86;
skapiDefs.speciesEnchantedArms        =                          87;
skapiDefs.speciesSleech               =                          88;
skapiDefs.speciesMukkir               =                          89;
skapiDefs.speciesMerwart              =                          90;
skapiDefs.speciesFood                 =                          91;
skapiDefs.speciesParadoxOlthoi        =                          92;
skapiDefs.speciesHarvest              =                          93;
skapiDefs.speciesEnergy               =                          94;
skapiDefs.speciesApparition           =                          95;
skapiDefs.speciesAerbax               =                          96;
skapiDefs.speciesTouched              =                          97;
skapiDefs.speciesBlightedMoarsman     =                          98;
skapiDefs.speciesGearKnight           =                          99;
skapiDefs.speciesGurog                =                         100;

// vital: Vital stat codes
skapiDefs.vitalNil                    =                           0;
skapiDefs.vitalHealthMax              =                           1;
skapiDefs.vitalHealthCur              =                           2;	// Cannot be used with skapi.SkinfoFromVital.
skapiDefs.vitalStaminaMax             =                           3;
skapiDefs.vitalStaminaCur             =                           4;	// Cannot be used with skapi.SkinfoFromVital.
skapiDefs.vitalManaMax                =                           5;
skapiDefs.vitalManaCur                =                           6;	// Cannot be used with skapi.SkinfoFromVital.
skapiDefs.vitalMax                    =                           7;

// wem: WaitEvent modes
skapiDefs.wemNormal                   =                           0;	// Returns when timeout elapsed or the queue is empty.
skapiDefs.wemSingle                   =                           1;	// Waits for a single event of any type.
skapiDefs.wemSpecific                 =                           2;	// Waits for a specific event type.
skapiDefs.wemFullTimeout              = skapiDefs.wemFullTimeout =        3;	// Always waits the full timeout interval.
