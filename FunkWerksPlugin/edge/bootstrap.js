﻿// dont cache this module
delete require.cache[__filename];


module.exports = function(interface, callback) {
    try {
        const ampn = require(interface.edge_path +  "app-module-path-node\\lib\\index.js");
        ampn.addPath(interface.edge_path);

        const deepExtend = require('deep-extend');
        const skapiDefs = require('skapiDefs');
        const Weenie = require('weenie');
        const noop = () => {};

        // todo: ditch global __FW__ in favor of a module that can be required
        global.__FW__ = deepExtend(global.__FW__ || {}, {
            base_script_path: interface.base_script_path,
            log: function log(t) {
                console.log(t);
                interface.log(t + '', noop);
            },
            eventIdLookup: interface.eventIdLookup,
            skapi_instances: {},
            weenieData: {}
        });

        if (typeof global.__FW__.has_uncaughtException_handler == 'undefined') {
            process.on('uncaughtException', function(err) {
                __FW__.log(err);
            });
            global.__FW__.has_uncaughtException_handler = true;
        }

        // assign skapiDefs to global namespace, to emulate skunkworks
        Object.assign(global, skapiDefs);
    } catch (e) { console.error(e); interface.log(e + ''); }

    callback();
}